﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnorarCollider : MonoBehaviour
{
    private Collider playerCol;
    private Collider miCollider;
    //public Collider piso;

    void Start()
    {
        miCollider = GetComponent<Collider>();
        playerCol = GameObject.FindGameObjectWithTag("Player").GetComponent<Collider>();
        Physics.IgnoreCollision(playerCol, miCollider);
        //Physics.IgnoreCollision(piso, miCollider);
    }

}