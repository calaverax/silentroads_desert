using System;
using UnityEngine;


namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]

   
    public class AICharacterControl : MonoBehaviour

   
    {
    
    /*La formula {get; private set} significa que solo la clase misma puede modificar un valor, pero
    cualquiera puede leer la info que almacena {agent esta compuesto de dos miembros uno publico y otro privado
    el set solo se puede hacer con el privado, lo que restringe el uso fuera de la clase}*/
        public UnityEngine.AI.NavMeshAgent agent { get; private set; } // almacena el navmesh (necesario para el pathfinding)
        public ThirdPersonCharacter character { get; private set; } // almacena el script que refiere al personaje que vamos a controlar.
        public Transform target; // El Target hacia el que se va a dirigir, esto viene del TargetPicker que llama la funcion

        // Use this for initialization
        private void Start()
        {
            
            // get the components on the object we need ( should not be null due to require component so no need to check )
            //asigna la primera variable, que es un componente de este objeto (AIThirdPersonController)... no se a que viene lo del children, porque no esta en los children sino q es un component.
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
			//asigna la segunda variable, que es un script que esta en este mismo gameobject (AIThirdPersonController)
            character = GetComponent<ThirdPersonCharacter>();

	        //setea dos booleanas del navMeshAgent. Primero queremo solo la posicion activa.
	        agent.updateRotation = false;
	        agent.updatePosition = true;
            
        }


        public void Update()
        {
            
            //Si el script TargetPicker manda una nueva posicion...
            if (target != null)
            {
                
                //Corre el metodo SetDestination (metodo propio del navAgent que traza un nuevo rumbo para que empiece a calcular).
                agent.ResetPath ();
                agent.SetDestination(target.position);
               

				
                /*Move es un metodo del script the third person character. Los parametros que toma son:
               El agent.desireVelocity es un read only que calcula el navmesh lo usa para definir la posicion, 
               ademas usa una bool para saber si esta agachado y la otra para saber si salta.*/
                character.Move(agent.desiredVelocity, false, false);

			
				//Esta es la parte para que no estuviera girando infinitamente
                if (!agent.pathPending) {//Crucial, fijate sin esto el personaje nunca puede arrancar.

                    if (agent.remainingDistance <= agent.stoppingDistance && !agent.pathPending) {//solo hacelo cuando ya estas muy cerquita del objetivo
						agent.ResetPath(); //que resetee el path para que frene ahi nomas.
                        target = null;  //limpia el target (para que no siga tratando de llegar)
					}				
				}	
			}
										
											
            //Si el personaje llego al target, detenelo (sin esto, el personaje sigue corriendo infinitamente).
            else {character.Move(Vector3.zero, false, false);}
        }



       //Esta funcion se llama desde el script del TargetPicker, que introduce el parametro Target en base al click del jugador.
        public void SetTarget(Transform target){
            this.target = target;

        }
    }
}


