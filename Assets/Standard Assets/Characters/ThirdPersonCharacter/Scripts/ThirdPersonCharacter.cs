using UnityEngine;


namespace UnityStandardAssets.Characters.ThirdPerson
{
	[RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(CapsuleCollider))]
	[RequireComponent(typeof(Animator))]
	public class ThirdPersonCharacter : MonoBehaviour
	{
		[SerializeField] float m_MovingTurnSpeed = 360;
		[SerializeField] float m_StationaryTurnSpeed = 180;
		[SerializeField] float m_JumpPower = 12f;
		[Range(1f, 4f)][SerializeField] float m_GravityMultiplier = 2f;
		[SerializeField] float m_RunCycleLegOffset = 0.2f; //esto viene del personaje que tenian con este script y esta diseñado a medida, habria que ajustarlo para el personaje
		[SerializeField] float m_MoveSpeedMultiplier = 1f;
		[SerializeField] float m_AnimSpeedMultiplier = 1f;
		[SerializeField] float m_GroundCheckDistance = 0.1f;

		Rigidbody m_Rigidbody;
		Animator m_Animator;
		float m_OrigGroundCheckDistance;
		const float k_Half = 0.5f;
		float m_TurnAmount;
		float m_ForwardAmount;
		Vector3 m_GroundNormal;
		bool m_IsGrounded;
		
		
		
		//comente estas variables porque no voy a usar esa funcion 
		//(es la que reduce el tamaño del collider cuando el personaje se agacha)
		//float m_CapsuleHeight; 
		//Vector3 m_CapsuleCenter;
		//CapsuleCollider m_Capsule;
		
		//public bool m_Crouching; //Esto ahora esta manejado desde otro script


		void Start()
		{
			m_Animator = GetComponent<Animator>();
			m_Rigidbody = GetComponent<Rigidbody>();
			m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
			m_OrigGroundCheckDistance = m_GroundCheckDistance;
			
			//Ya no estamos usando la funcion que convocaba estas vars.
			//m_Capsule = GetComponent<CapsuleCollider>();
			//m_CapsuleHeight = m_Capsule.height; 
			//m_CapsuleCenter = m_Capsule.center;

		
		}



//MOVE
		public void Move(Vector3 move, bool crouch, bool jump)
		{

			// convert the world relative moveInput vector into a local-relative
			// turn amount and forward amount required to head in the desired
			// direction.
			if (move.magnitude > 1f) move.Normalize();
			move = transform.InverseTransformDirection(move);
			CheckGroundStatus();
			move = Vector3.ProjectOnPlane(move, m_GroundNormal);
			m_TurnAmount = Mathf.Atan2(move.x, move.z);//siempre el problema con las rotaciones
			m_ForwardAmount = move.z;

			ApplyExtraTurnRotation();

			// control and velocity handling is different when grounded and airborne:
			if (m_IsGrounded)
			{
				HandleGroundedMovement(crouch, jump);
			}
			else
			{
				HandleAirborneMovement();
			}

			//ScaleCapsuleForCrouching(crouch);
			//PreventStandingInLowHeadroom(); //esta la quite junto con la funcion

			// send input and other state parameters to the animator
			UpdateAnimator(move);
		}


		//Cuando estas agachado esta funcion escala la capsula proporcionalmente
		//La deshabilite porque no la necesito por ahora
		/*
		void ScaleCapsuleForCrouching(bool crouch)
		{
			if (m_IsGrounded && crouch)
			{
				if (m_Crouching) return;
				m_Capsule.height = m_Capsule.height / 2f;
				m_Capsule.center = m_Capsule.center / 2f;
				m_Crouching = true;
			}
			else
			{
				Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
				float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
				if (Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength))
				{
					m_Crouching = true;
					return;
				}
				m_Capsule.height = m_CapsuleHeight;
				m_Capsule.center = m_CapsuleCenter;
				m_Crouching = false;
			}
		}
		*/
		
		//Esta funcion no la necesito, sirve para que quede agachado siempre si esta por ej
		//en una cueva.

/*
		void PreventStandingInLowHeadroom()
		{
			// prevent standing up in crouch-only zones
			if (!m_Crouching)
			{
				Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
				float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
				if (Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength))
				{
					m_Crouching = true;
				}
			}
		}
*/

/*/////////////////////////////////////////////////////////////////////////////////////////////////*/

		//ANIMATOR
		void UpdateAnimator(Vector3 move)
			// Todo el tiempo tiene que ir actualizando los parametros que afectan al animator, 
			//para que corra la animacion correspondiente. De eso se trata esta funcion
		{
			
			m_Animator.SetFloat("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
			m_Animator.SetFloat("Turn", m_TurnAmount, 0.1f, Time.deltaTime);
			//m_Animator.SetBool("Crouch", m_Crouching);
			m_Animator.SetBool("OnGround", m_IsGrounded);
			if (!m_IsGrounded) //Si no esta en el piso, es porque esta saltando
			{
				//setea la variable Jump del animator con la velocidad vertical del rigidbody
				m_Animator.SetFloat("Jump", m_Rigidbody.velocity.y);
			}



			//EL TEMA DE LAS PIERNAS EN EL SALTO
			/*Este sistema calcula que pierna esta detras para dejarla como colgando cuando pegue el salto
			(El codigo asume que estas usando las animaciones de ellos; en particular el offset de las piernas 
			y el hecho de que una pierna pasa a la otra en el clip normalizado en los tiempo 0.0 y 0.5)
			*/
			float runCycle =
				Mathf.Repeat(
					m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime + m_RunCycleLegOffset, 1);
			float jumpLeg = (runCycle < k_Half ? 1 : -1) * m_ForwardAmount;
			if (m_IsGrounded)
			{
				m_Animator.SetFloat("JumpLeg", jumpLeg);
			}



			//VELOCIDAD DE LA CAMINATA
			//Da un control de la velocidad de caminar y de correr para manejar en el inspector. Es solo un multiplier.
			//Se asegura de que lo use cuando esta en el piso, de otra manera el personaje caeria mas rapido y subiria mas
			//rapido en el salto.
			
			//Si el personaje no esta saltando o cayendo pero se esta moviendo...
			if (m_IsGrounded && move.magnitude > 0)
			{
				/*if (m_Animator.GetBool ("Crouch")){
				
					//m_MoveSpeedMultiplier = 1.2f;
				}
				*/

				//deriva la velocidad de las variable publica del inspector
				m_Animator.speed = m_AnimSpeedMultiplier;

				
			}
			else
			{
				//si esta en el aire, usa la que venia con las animaciones
				m_Animator.speed = 1;
			}
		}


/*/////////////////////////////////////////////////////////////////////////////////////////////////*/
								//FUNCIONES BASTANTE INUTILES (o sutiles)
/*/////////////////////////////////////////////////////////////////////////////////////////////////*/
		
		//GRAVEDAD (util solo para los saltos)
		//Esta es para alterar el efecto de la gravedad desde el inspector
		void HandleAirborneMovement()
		{
			// apply extra gravity from multiplier:
			Vector3 extraGravityForce = (Physics.gravity * m_GravityMultiplier) - Physics.gravity;
			m_Rigidbody.AddForce(extraGravityForce);

			m_GroundCheckDistance = m_Rigidbody.velocity.y < 0 ? m_OrigGroundCheckDistance : 0.01f;
		}







		//FUNCION QUE DOMINA AL PERSONAJE CUANDO ESTA EN EL SUELO
		void HandleGroundedMovement(bool crouch, bool jump)
		{
			// Si las condiciones son propicias para el salto (ie. esta en el suelo y no esta agachado)...
			if (jump && !crouch && m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Grounded"))
			{
				//Salta
				m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, m_JumpPower, m_Rigidbody.velocity.z);
				m_IsGrounded = false;
				m_Animator.applyRootMotion = false;
				m_GroundCheckDistance = 0.1f;
			}
			
		}

		//CONTROLA CASI TODA LA ROTACION 
		//(se adiciona a la rotacion del root de la animacion)
void ApplyExtraTurnRotation()

	/*	
		//Parte agregada
{
		 // Check if we've reached the destination
		if (!mNavMeshAgent.pathPending)
		{
			if (mNavMeshAgent.remainingDistance <= mNavMeshAgent.stoppingDistance)
			{
					if (!mNavMeshAgent.hasPath || mNavMeshAgent.velocity.sqrMagnitude === 0f)
					{return;}
			}
		}
		else
				//Fin de la parte agregada
				*/
				
				{
					//Turn speed es un lerp.
					float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
					//Como ves es un vector, y el eje de rotacion es y.
					transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
				}
//}



/*/////////////////////////////////////////////////////////////////////////////////////////////////*/
											//MOVIMIENTO (root motion)//
/*/////////////////////////////////////////////////////////////////////////////////////////////////*/
		public void OnAnimatorMove()
		{
			// we implement this function to override the default root motion.
			// this allows us to modify the positional speed before it's applied.
			if (m_IsGrounded && Time.deltaTime > 0)
			{
				Vector3 v = (m_Animator.deltaPosition * m_MoveSpeedMultiplier) / Time.deltaTime;

				// Esto lo usa para que la parte vertical del vector permanezca intacta
				v.y = m_Rigidbody.velocity.y;
				m_Rigidbody.velocity = v;
			}
		}

/*/////////////////////////////////////////////////////////////////////////////////////////////////*/
									//¿ESTA EL PERSONAJE EN EL PISO?//
/*/////////////////////////////////////////////////////////////////////////////////////////////////*/
		void CheckGroundStatus()
		{
			RaycastHit hitInfo;
#if UNITY_EDITOR
			// El clasico Debug.Drawline, para ver si tira bien el rayo
			//La linea va desde el tranform 0.1 hacia arriba (para que el rayo venga desde adentro del personaje > hasta la distancia que hayas establecido hacia abajo.
			//(igual guarda que el collider del personaje no golpee con el rayo) 
			Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
#endif
			
			// Asume que el transform del personaje esta en los pies 
			//[tendrian que haberlo puesto en base a la parte mas baja del rigid body como se hace siempre].
			if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
			{
				//Si golpea...
				m_GroundNormal = hitInfo.normal;//almacena la normal del piso
				m_IsGrounded = true; //el personaje esta en el suelo.
				m_Animator.applyRootMotion = true; // le dice al animator que tome control del movimiento (del desplazamiento digamos)
			}
			else
			//Si el rayo no golpea nada
			{
				m_IsGrounded = false; //Es porque no esta en el suelo
				m_GroundNormal = Vector3.up; //Estabiliza la normal del piso en base al vector hacia arriba del mundo.
				m_Animator.applyRootMotion = false; //Que el animator no controle el desplazamiento, sino que lo va a hacer la gravedad.
			}
		}
	}
}
