﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


    [InitializeOnLoad]
    public class DetectPlay
    {
        static DetectPlay()
        {
            EditorApplication.playmodeStateChanged += PlaymodeStateChanged;
        }

        private static void PlaymodeStateChanged()
        {
            foreach (GameObject go in GameObject.FindGameObjectsWithTag("ControlPrefab"))
                go.SetActive(!EditorApplication.isPlaying);
        }
    }
