﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DebugPanel : MonoBehaviour
{
    [Range(10,200)]
    public int TextLinesLimit = 200;

    bool _isPanelOpen = false;
    Animator _animator;
    ScrollRect _scrollRect;
    int _textLines = 0;

    [SerializeField]
    Toggle _activeToggle;
    [SerializeField]
    Text _textField;

    void Start()
    {
        _animator = GetComponent<Animator>();
        _scrollRect = transform.GetChild(0).GetComponent<ScrollRect>();
        //_textField = transform.GetChild(0).GetChild(0).GetComponent<Text>();
        CombatManager.Instance.debugPanel = this;

        CombatManager.Instance.debugPanel.SubmitText("Debug Panel Console Initialized... Submiting Test Text");
    }

    public void TogglePanel()
    {
        if (_animator == null) return;

        if (_isPanelOpen)
        {
            _animator.SetTrigger("Close");
            _isPanelOpen = false;
        }
        else
        {
            _animator.SetTrigger("Open");
            _isPanelOpen = true;
        }
    }

    public void GoFirstLine()
    {
        _scrollRect.verticalNormalizedPosition = 1.0f;
    }

    public void GoLastLine()
    {
        _scrollRect.verticalNormalizedPosition = 0.0f;
    }

    public void ClearText()
    {
        _textField.text = "";
        _textLines = 0;
    }

    private void deleteOldEntrys()
    {
        int newlines = 0;
        int subIndex = 0;

        for (int i = 0; i < _textField.text.Length; i++)
        {
            if (_textField.text[i] == '\n')
                newlines++;

            if (newlines >= 10)
            {
                subIndex = i;
                break;
            }
        }

        _textField.text = _textField.text.Substring(subIndex);
        _textLines -= 10;
    }


    public void SubmitText(string text, string color = "None")
    {
        if (!_activeToggle.isOn) return;

        _textLines++;
        if (_textLines >= TextLinesLimit)
            deleteOldEntrys();

        string tmpText = "";

        if (color != "None")
            tmpText = "<color=" + color + "> " + text + " </color>";
        else
            tmpText = text;

        _textField.text += tmpText + "\n";
    }

}
