﻿using UnityEngine;
using System.Collections;

public class CardsSmoothMovement : MonoBehaviour {

    /*
        1 - Salvar Posicion Original
        2 - Elejir un punto X,Y random cercano (OriginalX + X OriginalY +Y) 
            2.1 -- Guardarlo como Transform.
        3 - MoveTowards punto generado.
        4 - MoveTowards OriginalPos.
        Volver al punto 2
    */

    Vector3 _originalPosition;
    Vector3 _targetPosition;
    float _moveSpeed = 5f;
    bool haveRandomizedPosition = false;

    void Start()
    {
        Invoke("StartCardMovement", 2.5f);
    }

    public void StartCardMovement()
    {
        _originalPosition = this.gameObject.transform.localPosition;
        GetRandomPosition();
    }

    void GetRandomPosition()
    {
        _targetPosition = new Vector3(
                                    _originalPosition.x + Random.Range(-15, 15), //PPP ñp cambié porque cambió el tamaño del sprite.
                                    _originalPosition.y,
                                    _originalPosition.z + Random.Range(-15, 15)
                                    );

                                    //_originalPosition.x + Random.Range(-15, 15), 
                                    //_originalPosition.y + Random.Range(-15, 15), 
                                    //_originalPosition.z);
        haveRandomizedPosition = true;
    }

    void Update()
    {
        if (!haveRandomizedPosition) return; 

        this.gameObject.transform.localPosition = Vector3.MoveTowards(this.gameObject.transform.localPosition, _targetPosition, Time.deltaTime * _moveSpeed);

        float distanceSquared = (this.gameObject.transform.localPosition - _targetPosition).sqrMagnitude;

        if (distanceSquared < 2 * 2)
        {
            haveRandomizedPosition = false;
            Invoke("GetRandomPosition", Random.Range(1.0f, 2.5f));
        }
    }

    /*

    Transform _target;
    Vector3 _zigzag;

    float timer;
    public bool doZigZag = true;
    public bool isMoving = true;
    public float moveSpeed = 180;

    public void moveToTarget(Transform target)
    {
        _target = target;
        doZigZag = true;
        isMoving = true;
    }

    void ReachedTarget()
    {

    }

    void Update()
    {
        if (doZigZag)
        {
            timer += Time.deltaTime;

            if (timer < 2)
                _zigzag = UnityEngine.Random.Range(0, 30) * Vector3.up;//target.transform.up;
            if (timer >= 2 && timer < 4)
                _zigzag = (UnityEngine.Random.Range(0, 30) * -1) * Vector3.up;
            if (timer > 4)
                timer = 0;

            transform.position += _zigzag * Time.deltaTime;
        }

        if (isMoving)
            moveTowards();

    }

    private void moveTowards()
    {
        transform.position = Vector3.MoveTowards(transform.position, _target.localPosition, Time.deltaTime * moveSpeed);

        float distanceSquared = (transform.position - _target.localPosition).sqrMagnitude;

        if (distanceSquared < 150 * 150)
            doZigZag = false;
        if (distanceSquared < 15 * 15)
        {
            isMoving = false;
            ReachedTarget();
        }
    }
    */
}
