﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SetSelectedLevelOutside : MonoBehaviour {

    [SerializeField]
    GameObject GoBackInside;

    void Awake()
    {
        GameManager.Instance.selectedChapter = 1;
        GameManager.Instance.selectedLevel = 112;

        GameManager.Instance.setSelectedLevel();

        CombatManager.Instance.OnCombatEnded.AddListener(CombatEnded);

        if (GoBackInside != null)
            GoBackInside.GetComponent<Button>().onClick.AddListener(LoadRanchInside);

        Invoke("unPause", 0.5f);
    }

    void unPause()
    {
        CombatManager.Instance.ResumeCombat();
    }

    void LoadRanchInside()
    {
        // We set back LoadedLevelID to the Level1-1
        GameManager.Instance.selectedChapter = 1;
        GameManager.Instance.selectedLevel = 1;
        GameManager.Instance.setSelectedLevel();
        ScenesLoader.Instance.LoadScene("Chapter-1Level-1",true,false);
    }

    void CombatEnded()
    {
        if ((CombatManager.Instance.robert != null) && (CombatManager.Instance.robert.isAlive))
        {
            if (GoBackInside != null)
                GoBackInside.SetActive(true);
        }

    }


}
