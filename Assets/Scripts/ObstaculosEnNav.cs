﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*Este Script sirve para que los enemigos se anclen al nav mesh
 Para usarlos, poner a los enemigos arriba de la línea del nav, 
 posicionarlos en X y Z y darle reset al script para que los ajuste en Y.
 */

public class ObstaculosEnNav : MonoBehaviour
{

	[ExecuteInEditMode]

    private float offset = 0f; //Acá un offset para corregir el pivote en el medio. Se podría usar en vez el bound y sacar la mitad, pero para qué tanto cálculo al pedo?

	void Reset () {

        RaycastHit hit;
        RaycastHit hit2;

        
        if (Physics.Raycast(transform.position, -Vector3.up, out hit))
        {
            //print("Found an object - distance: " + hit.distance);
            //Debug.Log("Pegó en " + hit.collider.gameObject.name);
            float valor = this.transform.position.y - hit.distance + offset;
            this.transform.position = new Vector3(this.transform.position.x, valor, this.transform.position.z);

        }

        //Esta no está andando, no sé bien por qué
        else if (Physics.Raycast(transform.position, Vector3.up, out hit2))
            {
                //print("Found an object - distance: " + hit.distance);
                Debug.Log("Tiene arriba un " + hit.collider.gameObject.name);
                float valor = this.transform.position.y + hit.distance;
                this.transform.position = new Vector3(this.transform.position.x, valor, this.transform.position.z);

            }
        else 
        {Debug.Log("no tienen nada abajo ni arriba");}

       

	}

}
