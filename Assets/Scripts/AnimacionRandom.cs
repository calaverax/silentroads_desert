﻿using UnityEngine;
using System.Collections;

public class AnimacionRandom : MonoBehaviour {

	
	private Animator anim;
	
	void Start()
	{
	anim = GetComponent<Animator>();
	}
	
	void Update ()
	{
	bool randomBool = Random.value > 0.5f;
	anim.SetBool("trueno", randomBool);
	}
}	