﻿using UnityEngine;
using System;
using System.Collections;

public static class CardVisualFxTriggers
{
    public static string BLEEDING_TRIGGER = "BleedingFX";
    public static string BLEEDING_TICK = "BloodDamage";
}

public class CardVFXControllers : MonoBehaviour {

    private Action _callBack;
    private Animator _animator;

    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void triggerCardVFX(string triggerName, Action callBack)
    {
        if (_animator == null) _animator = GetComponent<Animator>();

        _animator.SetTrigger(triggerName);
        _callBack = callBack;
    }

    private void VFXFinalized()
    {
        if (_callBack != null)
            _callBack();
    }
    
}
