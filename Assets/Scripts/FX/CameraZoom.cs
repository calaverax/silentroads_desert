﻿using UnityEngine;
using System.Collections;

public class CameraZoom : MonoBehaviour
{
    bool _needToZoom = false;

    Vector3 _targetPosition;
    Vector3 _originPosition;

    float _originalZoom;
    float _targetZoom;

    Camera mainCamera;

    [Range(5,10)]
    public float _speed = 7.5f; 
    [Range(50,100)]
    public float _zoom = 50.0f;

    void Start()
    {
        mainCamera = Camera.main;
    }

    public void ZoomIn()
    {
        if (GameManager.Instance.levelManager.scenarioSize == LevelManager.ScenarioSize.Small) return;

        // Save Current Camera Values
        _originPosition = mainCamera.transform.position;
        _originalZoom = mainCamera.orthographicSize;
        

        // Position on Mouse Position 
        _targetPosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        _targetZoom = _zoom;
        _needToZoom = true;
    }

    public void ZoomOut()
    {
        _targetPosition = _originPosition;
        _targetZoom = _originalZoom;
        _needToZoom = true;
    }

    void Update()
    {
        if (!_needToZoom) return;

        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, _targetPosition, Time.deltaTime * _speed);
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, _targetZoom, Time.deltaTime * _speed);
        
        float distanceSquared = (mainCamera.transform.position - _targetPosition).sqrMagnitude;

        if (distanceSquared < 4) 
        {
            _needToZoom = false;
            Debug.Log("STOPPED ZOOMING");
        }
    }
}