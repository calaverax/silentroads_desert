﻿using UnityEngine;
using System.Collections;

public class CameraBleed : MonoBehaviour {

    public GameObject bleedFx;

    public void DoBleedFX()
    {
        if (bleedFx.activeInHierarchy)
            bleedFx.SetActive(false);

        bleedFx.SetActive(true);
    }

         
}
