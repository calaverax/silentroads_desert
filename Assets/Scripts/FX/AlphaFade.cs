﻿using UnityEngine;
using System.Collections;

public class AlphaFade : MonoBehaviour {

    SpriteRenderer renderer;
    Color spriteColor = Color.white;

    //[Range(0.1f, 1.0f)]
    private float fadeSpeed = 0.05f;

    private enum Fade
    {
        In,
        Out
    }
    private Fade fade = Fade.Out;
    private bool _isFading = false;

	void Start ()
    {
        renderer = this.gameObject.GetComponent<SpriteRenderer>();
        Invoke("startFade", Random.Range(1,3));
    }

    private void startFade()
    {
        _isFading = true;
    }

    private void fadeEnded(Fade mode)
    {
        _isFading = false;

        if (mode == Fade.In)
            fade = Fade.Out;
        else
            fade = Fade.In;

        Invoke("startFade", Random.Range(1, 3));
    }

	// Update is called once per frame
	void Update ()
    {
        if (!_isFading) return;

        if (fade == Fade.Out)
            spriteColor.a -= fadeSpeed * Time.deltaTime;
        else if (fade == Fade.In)
            spriteColor.a += fadeSpeed * Time.deltaTime;

        Mathf.Clamp(spriteColor.a, 0, 1);

        renderer.color = spriteColor;

        if (spriteColor.a >= 1)
            fadeEnded(fade); //fade = Fade.Out;
        else if (spriteColor.a <= 0)
            fadeEnded(fade);//fade = Fade.In;
    }
}
