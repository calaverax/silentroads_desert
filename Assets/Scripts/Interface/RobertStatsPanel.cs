﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RobertStatsPanel : MonoBehaviour {

    private Text strength, hitpoints, command, intelligence, agility, stealth, evasion, morale, accuracy, xp;
    private Text availablePoints;

    private string _lastIncreasedStat = "";
    private int _totalIncreasedPoints = 0;

    private Robert _robert;
    private bool _objectsObtained = false;

    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip increaseFx, decreaseFx;
    // Use this for initialization
    void Start()
    {
        _lastIncreasedStat = "";
        Initialize();
    }

    void obtainObjects()
    {
        //availablePoints = transform.Find("Center Panel").Find("Image").Find("PointsField").gameObject.GetComponent<Text>();
        availablePoints = transform.Find("Center Panel/Image/PointsField").gameObject.GetComponent<Text>();
        xp = transform.Find("Center Panel/XP Variable").gameObject.GetComponent<Text>();
        _robert = CombatManager.Instance.robert;

        // Get each stat GameObject.
        strength = transform.Find("Strength").gameObject.GetComponent<Text>();
        hitpoints = transform.Find("Hitpoints").gameObject.GetComponent<Text>();
        command = transform.Find("Command").gameObject.GetComponent<Text>();
        intelligence = transform.Find("Intelligence").gameObject.GetComponent<Text>();
        agility = transform.Find("Agility").gameObject.GetComponent<Text>();
        stealth = transform.Find("Stealth").gameObject.GetComponent<Text>();
        evasion = transform.Find("Evasion").gameObject.GetComponent<Text>();
        morale = transform.Find("Morale").gameObject.GetComponent<Text>();
        accuracy = transform.Find("Accuracy").gameObject.GetComponent<Text>();

        _objectsObtained = true;
    }

    void OnEnable()
    {
        _totalIncreasedPoints = 0;

        Initialize();
    }

    private void Initialize()
    {
        _audioSource = GetComponent<AudioSource>();

        if (_audioSource == null)
            this.gameObject.AddComponent<AudioSource>();

        if (!_objectsObtained)
            obtainObjects();

        if (_robert == null)
            _robert = CombatManager.Instance.robert;

        if (_robert == null)
            return;

        strength.text = _robert.characterStats.Strength.ToString();
        hitpoints.text = _robert.characterStats.TotalHitpoints.ToString();
        command.text = _robert.characterStats.Command.ToString();
        intelligence.text = _robert.characterStats.Intelligence.ToString();
        agility.text = _robert.characterStats.Agility.ToString();
        stealth.text = _robert.characterStats.Stealth.ToString();
        evasion.text = _robert.characterStats.Evasion.ToString();
        morale.text = _robert.characterStats.Morale.ToString();
        accuracy.text = _robert.characterStats.Accuracy.ToString();

        availablePoints.text = CombatManager.Instance.robert.characterStats.availablePoints.ToString();
        xp.text = CombatManager.Instance.robert.characterStats.Experience.ToString();

    }


    public void increaseStat(string stat)
    {
        _audioSource.PlayOneShot(increaseFx);

        if (_robert.characterStats.availablePoints <= 0)
            return;

        _lastIncreasedStat = stat;
        _totalIncreasedPoints++;

        if (stat == "Strength")
        {
            if (_robert.characterStats.Strength < 10)
            {
                _robert.characterStats.getStat(CharacterStats.StatStrength).IncreaseStat();
                _robert.characterStats.availablePoints--;
            }
        }
        else if (stat == "Hitpoints")
        {
            _robert.characterStats.getStat(CharacterStats.StatHitpoints).IncreaseStat(5);
            _robert.characterStats.getStat(CharacterStats.StatTotalHitpoints).IncreaseStat(5);
            _robert.UpdateHealthBar();
            _robert.characterStats.availablePoints--;
        }
        else if (stat == "Command")
        {
            if (_robert.characterStats.Command < 10)
            {
                _robert.characterStats.getStat(CharacterStats.StatCommand).IncreaseStat();
                _robert.characterStats.availablePoints--;
            }
        }
        else if (stat == "Intelligence")
        {
            if (_robert.characterStats.Intelligence < 10)
            {
                _robert.characterStats.getStat(CharacterStats.StatIntelligence).IncreaseStat();
                _robert.characterStats.availablePoints--;
            }
        }
        else if (stat == "Agility")
        {
            if (_robert.characterStats.Agility < 10)
            {
                _robert.characterStats.getStat(CharacterStats.StatAgility).IncreaseStat();
                _robert.characterStats.availablePoints--;
            }
        }
        else if (stat == "Stealth")
        {
            if (_robert.characterStats.Stealth < 10)
            {
                _robert.characterStats.getStat(CharacterStats.StatStealth).IncreaseStat();
                _robert.characterStats.availablePoints--;
            }
        }
        else if (stat == "Evasion")
        {
            if (_robert.characterStats.Evasion < 10)
            {
                _robert.characterStats.getStat(CharacterStats.StatEvasion).IncreaseStat();
                _robert.characterStats.availablePoints--;
            }
        }
        else if (stat == "Morale")
        {
            if (_robert.characterStats.Morale < 10)
            {
                _robert.characterStats.getStat(CharacterStats.StatMorale).IncreaseStat();
                _robert.characterStats.availablePoints--;
            }
        }
        else if (stat == "Accuracy")
        {
            if (_robert.characterStats.Accuracy < 10)
            {
                _robert.characterStats.getStat(CharacterStats.StatAccuracy).IncreaseStat();
                _robert.characterStats.availablePoints--;
            }
        }
        if(_robert.characterStats.availablePoints == 0)
        {
            CombatManager.Instance.GUIScript.XpWithoutPoint();
        }
        Initialize();
    }

    public void restoreSpentPoint()
    {
        _audioSource.PlayOneShot(decreaseFx);

        if (_totalIncreasedPoints <= 0)
            return;

        if (_lastIncreasedStat == "Hitpoints")
        {
            _robert.characterStats.getStat(CharacterStats.StatHitpoints).ReduceStat(5);
            _robert.characterStats.getStat(CharacterStats.StatTotalHitpoints).ReduceStat(5);
        }

        if (_lastIncreasedStat == "Strength")
            _robert.characterStats.getStat(CharacterStats.StatStrength).ReduceStat();
        else if (_lastIncreasedStat == "Command")
            _robert.characterStats.getStat(CharacterStats.StatCommand).ReduceStat();
        else if (_lastIncreasedStat == "Intelligence")
            _robert.characterStats.getStat(CharacterStats.StatIntelligence).ReduceStat();
        else if (_lastIncreasedStat == "Agility")
            _robert.characterStats.getStat(CharacterStats.StatAgility).ReduceStat();
        else if (_lastIncreasedStat == "Stealth")
            _robert.characterStats.getStat(CharacterStats.StatStealth).ReduceStat();
        else if (_lastIncreasedStat == "Evasion")
            _robert.characterStats.getStat(CharacterStats.StatEvasion).ReduceStat();
        else if (_lastIncreasedStat == "Morale")
            _robert.characterStats.getStat(CharacterStats.StatMorale).ReduceStat();
        else if (_lastIncreasedStat == "Accuracy")
            _robert.characterStats.getStat(CharacterStats.StatAccuracy).ReduceStat();

        _totalIncreasedPoints--;
        _robert.characterStats.availablePoints++;

        Initialize();
    }

    void OnDisable()
    {
        /*METODO VIEJO
        PlayerPrefs.SetInt(CharacterStats.StatAccuracy, _robert.characterStats.getStat(CharacterStats.StatAccuracy).OriginalValue);
        PlayerPrefs.SetInt(CharacterStats.StatStrength, _robert.characterStats.getStat(CharacterStats.StatStrength).OriginalValue);
        PlayerPrefs.SetInt(CharacterStats.StatAgility, _robert.characterStats.getStat(CharacterStats.StatAgility).OriginalValue);
        PlayerPrefs.SetInt(CharacterStats.StatCommand, _robert.characterStats.getStat(CharacterStats.StatCommand).OriginalValue);
        PlayerPrefs.SetInt(CharacterStats.StatEvasion, _robert.characterStats.getStat(CharacterStats.StatEvasion).OriginalValue);
        PlayerPrefs.SetInt(CharacterStats.StatIntelligence, _robert.characterStats.getStat(CharacterStats.StatIntelligence).OriginalValue);
        PlayerPrefs.SetInt(CharacterStats.StatStealth, _robert.characterStats.getStat(CharacterStats.StatStealth).OriginalValue);
        PlayerPrefs.SetInt(CharacterStats.StatMorale, _robert.characterStats.getStat(CharacterStats.StatMorale).OriginalValue);
        PlayerPrefs.SetInt(CharacterStats.AvailableStatsPoints, _robert.characterStats.availablePoints);

        PlayerPrefs.SetInt(CharacterStats.StatHitpoints, _robert.characterStats.getStat(CharacterStats.StatHitpoints).OriginalValue);
        PlayerPrefs.SetInt(CharacterStats.StatTotalHitpoints, _robert.characterStats.getStat(CharacterStats.StatTotalHitpoints).OriginalValue);


        PlayerPrefs.Save();
        */
        PlayerDataManager.Instance.SetValue(CharacterStats.StatAccuracy, _robert.characterStats.getStat(CharacterStats.StatAccuracy).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatStrength, _robert.characterStats.getStat(CharacterStats.StatStrength).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatAgility, _robert.characterStats.getStat(CharacterStats.StatAgility).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatCommand, _robert.characterStats.getStat(CharacterStats.StatCommand).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatEvasion, _robert.characterStats.getStat(CharacterStats.StatEvasion).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatIntelligence, _robert.characterStats.getStat(CharacterStats.StatIntelligence).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatStealth, _robert.characterStats.getStat(CharacterStats.StatStealth).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatMorale, _robert.characterStats.getStat(CharacterStats.StatMorale).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.AvailableStatsPoints, _robert.characterStats.availablePoints);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatHitpoints, _robert.characterStats.getStat(CharacterStats.StatHitpoints).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatTotalHitpoints, _robert.characterStats.getStat(CharacterStats.StatTotalHitpoints).OriginalValue);
        PlayerDataManager.Instance.Save();
    }

    public void DebugReturnAllPoints()
    {
        /*Metodo viejo
        PlayerPrefs.SetInt(CharacterStats.StatAccuracy, _robert.characterStats.getStat(CharacterStats.StatAccuracy).overrideDebugSetValue(3));
        PlayerPrefs.SetInt(CharacterStats.StatStrength, _robert.characterStats.getStat(CharacterStats.StatStrength).overrideDebugSetValue(3));
        PlayerPrefs.SetInt(CharacterStats.StatAgility, _robert.characterStats.getStat(CharacterStats.StatAgility).overrideDebugSetValue(3));
        PlayerPrefs.SetInt(CharacterStats.StatCommand, _robert.characterStats.getStat(CharacterStats.StatCommand).overrideDebugSetValue(3));
        PlayerPrefs.SetInt(CharacterStats.StatEvasion, _robert.characterStats.getStat(CharacterStats.StatEvasion).overrideDebugSetValue(3));
        PlayerPrefs.SetInt(CharacterStats.StatIntelligence, _robert.characterStats.getStat(CharacterStats.StatIntelligence).overrideDebugSetValue(3));
        PlayerPrefs.SetInt(CharacterStats.StatStealth, _robert.characterStats.getStat(CharacterStats.StatStealth).overrideDebugSetValue(3));
        PlayerPrefs.SetInt(CharacterStats.StatHitpoints, _robert.characterStats.getStat(CharacterStats.StatHitpoints).overrideDebugSetValue(50));
        PlayerPrefs.SetInt(CharacterStats.StatMorale, _robert.characterStats.getStat(CharacterStats.StatMorale).OriginalValue);
        PlayerPrefs.SetInt(CharacterStats.AvailableStatsPoints, 10);
        
        _robert.characterStats.availablePoints = 10;

        PlayerPrefs.Save();

        Initialize();
        */

        PlayerDataManager.Instance.SetValue(CharacterStats.StatAccuracy, _robert.characterStats.getStat(CharacterStats.StatAccuracy).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatStrength, _robert.characterStats.getStat(CharacterStats.StatStrength).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatAgility, _robert.characterStats.getStat(CharacterStats.StatAgility).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatCommand, _robert.characterStats.getStat(CharacterStats.StatCommand).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatEvasion, _robert.characterStats.getStat(CharacterStats.StatEvasion).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatIntelligence, _robert.characterStats.getStat(CharacterStats.StatIntelligence).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatStealth, _robert.characterStats.getStat(CharacterStats.StatStealth).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatMorale, _robert.characterStats.getStat(CharacterStats.StatMorale).OriginalValue);
        PlayerDataManager.Instance.SetValue(CharacterStats.AvailableStatsPoints, 10);

        _robert.characterStats.availablePoints = 10;

        PlayerDataManager.Instance.Save();

    }

}
