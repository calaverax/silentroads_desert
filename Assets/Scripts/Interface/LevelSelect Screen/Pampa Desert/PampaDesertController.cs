﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PampaDesertController : MonoBehaviour {

    [SerializeField] GameObject FadingScreen;
    [SerializeField] AudioSource BGMSource;
    [SerializeField] GameObject LevelButtons;
    [SerializeField] GameObject FlagObject;
    [SerializeField] GameObject warningPopup;

    Button[] _buttons;
    bool _haveTargetPosition = false;
    Transform _targetPosition;
    float _moveSpeed = 50.5f;
    // Use this for initialization
    void Awake()
    {
        _buttons = LevelButtons.GetComponentsInChildren<Button>();

        FadingScreen.SetActive(true);

        EvaluateProgression();
        showLevelStats();
    }
    
    void EvaluateProgression()
    {
        // Check Robert Ranch Exterior is Won
        if (GameManager.Instance.isLevelWon("Chapter-1Level-12"))
        {
            _buttons[1].interactable = true;
            LevelSelectButton lsButton = _buttons[1].gameObject.GetComponent<LevelSelectButton>();

            if (lsButton != null)
                lsButton.UnlockLevels();
        }

        // Analize the rest of the levels
        for (int i = 0; i < _buttons.Length - 1; i++)
        {
            if (GameManager.Instance.isLevelWon("Chapter-1Level-" + (i + 1)))
            {
                _buttons[i + 1].interactable = true;

                LevelSelectButton lsButton = _buttons[1].gameObject.GetComponent<LevelSelectButton>();

                if (lsButton != null)
                    lsButton.UnlockLevels();
            }
            else
            {
                break;
            }
        }

        //InstantActivateNewArrow(PlayerPrefs.GetInt("LastSelectedLevel"));
    }

    public void DebugLoadLevel(string levelName)
    {
        ScenesLoader.Instance.LoadScene(levelName, false, false);
    }

    public void LoadNewLevel(int level)
    {
        // Check For Robert Water.
        // - Warning Popup, 
        processLevelTravel(level);
        if (warningPopup.activeInHierarchy)
            return;

        // - Animate Flag
        _targetPosition = _buttons[level - 1].gameObject.transform;
        _haveTargetPosition = true;

        
        GameManager.Instance.selectedLevel = level;
    }

    void Update()
    {
        if (!_haveTargetPosition) return;

        FlagObject.transform.localPosition = Vector3.MoveTowards(FlagObject.transform.localPosition ,_targetPosition.localPosition, Time.deltaTime * _moveSpeed);
        //this.gameObject.transform.localPosition = Vector3.MoveTowards(this.gameObject.transform.localPosition, _targetPosition, Time.deltaTime * _moveSpeed);

        float distanceSquared = (FlagObject.transform.localPosition - _targetPosition.transform.localPosition).sqrMagnitude;
        //float distanceSquared = (this.gameObject.transform.localPosition - _targetPosition).sqrMagnitude;

        if (distanceSquared < 2 * 2)
        {
            _haveTargetPosition = false;
            flagReachedNewLevel();
            //Invoke("GetRandomPosition", Random.Range(1.0f, 2.5f));
        }
    }

    void flagReachedNewLevel()
    {
        // - Load New Level + FadeOutScreen
        FadeOutScreen();
        Invoke("StartGame",2.5f);
    }

    void StartGame()
    {
        MenuBGMcontroller.Instance.stopAudio();
        GameManager.Instance.setSelectedLevel();
        ScenesLoader.Instance.LoadScene(GameManager.Instance.selectedLevelId, false, false);
    }

    private void FadeOutScreen()
    {
        Animator _animator = FadingScreen.GetComponent<Animator>();

        if (_animator != null)
            _animator.SetTrigger("FadeOut");

        Invoke("StopBGM", 1.5f);
    }

    private void StopBGM()
    {
        BGMSource.Stop();
    }

    void processLevelTravel(int level)
    {
        int lastLevel = PlayerPrefs.GetInt("LasPlayedLevel", 1);

        int robertWater = PlayerPrefs.GetInt(CharacterStats.Statwater, 5);
        string toConsume = "";

        if (robertWater >= 0)
        {
            if (lastLevel < level)
            {
                if ((level - lastLevel) >= 4)
                {
                    warningPopup.SetActive(true);
                    return;
                }
                toConsume = "Health_" + (level - lastLevel);
            }
        }
        else
        {
            toConsume = "Water_" + 5 * (level - lastLevel);
        }

        PlayerPrefs.SetString("ConsumeRobertStat", toConsume);

        PlayerPrefs.SetInt("LasPlayedLevel", level);
        PlayerPrefs.Save();
    }

    bool _statsCalculated = false;

    public void showLevelStats()
    {
        if (!_statsCalculated)
        {
            // Load All Levels and Populate LevelStats
            SerializableLevel level = new SerializableLevel();

            // For Level 1: (Interior: STATS / Exterior: STATS)
            level = Serializer.LoadSerializableObjectFromFile<SerializableLevel>(Application.persistentDataPath + "Chapter1Level1.LevelData");
            // _buttons[0] - Load Chapter1Level1 / Chapter1RanchExterior
            Text ranchText = _buttons[0].GetComponent<LevelSelectionElement>().statsField;
            string ranchStats = "";

            int totalEnemies = 0;
            string enemiesTypes = "";
            for (int i = 0; i < level.enemiesGroups.Count; i++)
            {
                totalEnemies += level.enemiesGroups[i].generatedAmount;
                if (!(enemiesTypes.Contains(level.enemiesGroups[i].enemies[i].Type)))
                    enemiesTypes += level.enemiesGroups[i].enemies[i].Type + ", ";
            }

            ranchStats = "Total Enemies: " + totalEnemies + "\n" + enemiesTypes;
            // ranchStats += Ranch Exterior 
            ranchText.text = ranchStats;

            for (int i = 1; i < _buttons.Length; i++)
            {
                _buttons[i].GetComponent<LevelSelectionElement>().statsField.text = "Not Generated";
            }


        }
    }

}
