﻿using UnityEngine;
using System.Collections;

public class DebugShowHideLevelStats : MonoBehaviour {

    public GameObject buttonsElements;

    UnityEngine.UI.Button[] _buttons;

    void Awake()
    {
        _buttons = buttonsElements.GetComponentsInChildren<UnityEngine.UI.Button>();
    }

    public void DebugShowHideStats()
    {
        

        for (int i = 0; i < _buttons.Length; i++)
        {
            _buttons[i].transform.GetChild(1).gameObject.SetActive(!_buttons[i].transform.GetChild(1).gameObject.activeInHierarchy);
        }
    }

}
