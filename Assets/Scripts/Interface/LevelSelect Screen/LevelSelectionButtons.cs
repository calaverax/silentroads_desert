﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelSelectionButtons : MonoBehaviour {

    [SerializeField]GameObject _indicatorPrefab;
    Button[] _levelsButtons;
    int _lastActivatedLevel = 0;
    
    void Start()
    {
        _levelsButtons = GetComponentsInChildren<Button>();

        //EvaluateActiveButtons();

        EvaluateProgression();
    }

    public void DisableButtons()
    {
        for (int i = 0; i < _levelsButtons.Length; i++)
            _levelsButtons[i].interactable = false;
    }

    private void EvaluateProgression()
    {

        // Check Robert Ranch Exterior is Won
        if (GameManager.Instance.isLevelWon("Chapter-1Level-12"))
        {
            _levelsButtons[1].interactable = true;
            LevelSelectButton lsButton = _levelsButtons[1].gameObject.GetComponent<LevelSelectButton>();

            if (lsButton != null)
                lsButton.UnlockLevels();
        }

        // Analize the rest of the levels
        for (int i = 0; i < _levelsButtons.Length - 1; i++)
        {
            if (GameManager.Instance.isLevelWon("Chapter-1Level-" + (i + 1)))
            {
                _levelsButtons[i + 1].interactable = true;

                LevelSelectButton lsButton = _levelsButtons[1].gameObject.GetComponent<LevelSelectButton>();

                if (lsButton != null)
                    lsButton.UnlockLevels();
            }
            else
            {
                break;
            }
        }

        /*
        int blhe = PlayerPrefs.GetInt("LastSelectedLevel");
        LevelSelectButton btn = _levelsButtons[PlayerPrefs.GetInt("LastSelectedLevel")].gameObject.GetComponent<LevelSelectButton>();
        if (btn != null)
            btn.MarkAsActualLevel(_indicatorPrefab);
            */
        InstantActivateNewArrow(PlayerPrefs.GetInt("LastSelectedLevel"));
    }



    private void EvaluateActiveButtons()
    {
        // Check if we have won Ranch OutSide.
        // TEMP FIX. we only have 3 levels.
        if (GameManager.Instance.isLevelWon("Chapter-1Level-12"))
        {
            _levelsButtons[1].interactable = true;
        }
    }


    public void DeactivateLastArrow()
    {
        _levelsButtons[_lastActivatedLevel].gameObject.GetComponent<LevelSelectButton>().HideMark();
    }

    public void DelayedActivateNewArrow(int idx)
    {
        _lastActivatedLevel = idx;
        Invoke("ValidateNewMark", 0.5f);
    }

    public void InstantActivateNewArrow(int idx)
    {
        _lastActivatedLevel = idx;
        ValidateNewMark();
    }

    private void ValidateNewMark()
    {
        LevelSelectButton btn = _levelsButtons[_lastActivatedLevel].gameObject.GetComponent<LevelSelectButton>();

        if (btn != null)
        {
            btn.MarkAsActualLevel(_indicatorPrefab);
        }
    }
}
