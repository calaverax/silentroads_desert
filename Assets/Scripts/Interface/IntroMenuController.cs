﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IntroMenuController : BaseMenu
{
    public ScrollRect scrollingRect;

    public MenuBGMcontroller _menuBGMController;

    public AudioClip introBGM;
    public AudioClip robertSays;
    
    private bool _scrolling = false;
    private bool _robertTalk = false;

    private AudioSource introSourceAudio;

    public GameObject[] MenuExtras;

    public override void ActivateMenuExtras()
    {

        if (MenuExtras.Length > 0)
        {
            for (int i = 0; i < MenuExtras.Length; i++)
                if (!MenuExtras[i].activeInHierarchy)
                    MenuExtras[i].SetActive(true);
        }
    }

    public override void DeActivateMenuExtras()
    {

        if (MenuExtras.Length > 0)
        {
            for (int i = 0; i < MenuExtras.Length; i++)
                if (MenuExtras[i].activeInHierarchy)
                    MenuExtras[i].SetActive(false);
        }
    }

    private Vector2 _originalPosition = Vector2.zero;

    void Awake()
    {
        introSourceAudio = GetComponent<AudioSource>();
    }

    private void resetValues()
    {
        scrollingRect.normalizedPosition = _originalPosition;

        scrollingRect.gameObject.SetActive(true);
        _scrolling = true;
        _robertTalk = false;

    }

    protected override void OpenCallback()
    {
        if (_originalPosition == Vector2.zero)
            _originalPosition = scrollingRect.normalizedPosition;
        
        base.OpenCallback();
        _menuBGMController.playBGMNow(introBGM);

        //Invoke("startScrolling", 2f);
        _scrolling = true;
        //_scrolling = true;
    }

    private void startScrolling()
    {
        _scrolling = true;
    }
    protected override void CloseCallback()
    {
        resetValues();

        base.CloseCallback();

        _menuBGMController.source.volume = 1;
        _menuBGMController.selectRandomClip();
    }

    void playRobertSaying()
    {
        _menuBGMController.source.volume = 0.5f;

        introSourceAudio.PlayOneShot(robertSays, 1);

        Invoke("restoreAudioVolume", 21);
    }

    void restoreAudioVolume()
    {
        _menuBGMController.source.volume = 1;
    }
    
    void Update()
    {
        if (scrollingRect.normalizedPosition.y <= 0.02f)
        {
            scrollingRect.gameObject.SetActive(false);
            _scrolling = false;

            /*
            if (!_robertTalk)
            {
               // playRobertSaying();
                _robertTalk = true;
            }
            */
        } 

        if (_scrolling)
            scrollingRect.normalizedPosition = new Vector2(scrollingRect.normalizedPosition.x, scrollingRect.normalizedPosition.y - (0.01f * Time.deltaTime));
    }
    
}
