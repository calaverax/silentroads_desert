﻿using UnityEngine;
using System.Collections;

public class LevelSelectButton : MonoBehaviour
{
    [SerializeField]GameObject[] unlockLevels;
    GameObject _mark;
    public void UnlockLevels()
    {
        if ((unlockLevels == null) || (unlockLevels.Length == 0)) return;

        for (int i = 0; i < unlockLevels.Length; i++)
            unlockLevels[i].SetActive(true);
    }

    public void MarkAsActualLevel(GameObject Prefab)
    {
        _mark = Instantiate(Prefab) as GameObject;
        _mark.transform.SetParent(this.transform);
        _mark.transform.localScale = new Vector3(1, 1, 1);
        _mark.transform.localPosition = new Vector3(0, 0, 0);
    }

    public void HideMark()
    {
        _mark.GetComponent<Animator>().SetTrigger("FadeOut");
    }


    //------------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------
    /*
    // OLD Code
    public enum LevelButtonState
    {
        LOCKED = 0,
        AVAILABLE = 1,
        COMPLETED = 2
    }

    public LevelButtonState buttonState;
    public List<LevelSelectButton> conectedTo = new List<LevelSelectButton>();
    public Sprite completedSprite;
    public LineRenderer lineRenderer;
    // Use this for initialization
    public void configure()
    {
        if (buttonState == LevelButtonState.AVAILABLE)
            this.gameObject.SetActive(true);
        else if (buttonState == LevelButtonState.LOCKED)
            this.gameObject.SetActive(false);

        if (buttonState == LevelButtonState.COMPLETED)
        {
            this.gameObject.SetActive(true);
            this.gameObject.GetComponentInChildren<UnityEngine.UI.Button>().image.sprite = completedSprite;
        }
	}
	
	// Update is called once per frame
	void Awake ()
    {
        lineRenderer = this.gameObject.GetComponent<LineRenderer>();
	}
    */
}
