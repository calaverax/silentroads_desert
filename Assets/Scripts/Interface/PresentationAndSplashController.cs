﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PresentationAndSplashController : MonoBehaviour {

    MovieTexture _movieTexture;

    public GameObject presentation;
    public GameObject splashScreen;
    public Text splashInfoTextfield;
    private bool finishedLoading = false;
    private AsyncOperation loadMainScene;
    private bool presentationEnded = false;

    void Awake()
    {
        //if (_movieTexture.isReadyToPlay)
            //_movieTexture.Play();
        GameManager.Instance.loadSavedGame();
/*
#if !UNITY_EDITOR
        StartCoroutine("loadMainSceneAsync");
#endif
*/
        splashInfoTextfield.text = "Game is Loading";
    }

//#if UNITY_EDITOR
    private void DebugChangeLoadingText() { splashInfoTextfield.text = "Touch To Continue"; finishedLoading = true; }
//#endif

    IEnumerator loadMainSceneAsync()
    {
        loadMainScene = Application.LoadLevelAsync("mainMenu");
        loadMainScene.allowSceneActivation = false;
        yield return loadMainScene;
    }

    void Update()
    {
        if ((!finishedLoading) && (presentationEnded))
        {
            if (loadMainScene == null) return;

            if (loadMainScene.isDone)
            {
                finishedLoading = true;
                splashInfoTextfield.text = "Touch To Continue";
            }
        }
    }

    public void presentationFadeEnded()
    {
        presentation.SetActive(false);
        splashScreen.SetActive(true);
        presentationEnded = true;
//#if UNITY_EDITOR
        // Wait 3 Seconds to change Loading Text
        Invoke("DebugChangeLoadingText", 3.0f);
//#endif

    }

    public void showMenues()
    {
        
        if (finishedLoading)
        {
  //          #if UNITY_EDITOR
                Application.LoadLevel("mainMenu");
    //        #else
      //          loadMainScene.allowSceneActivation = true;
      //      #endif
        }
        
    }
}
