﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyDetailsWindow : MonoBehaviour
{

    [SerializeField] Text _agentName, _distance, _chanceToHit, _hostility, _health, _equipment, _effects;
    [SerializeField] Image characterSprite;

    BaseEnemy _enemy;
    Transform detailsWindow;

    public void ActivateWindow(BaseEnemy enemy)
    {
        // if (_agentName || _distance || _chanceToHit || _hostility || _health || _equipment || _effects == null) return;
        _enemy = enemy;

        detailsWindow = this.gameObject.transform.GetChild(1);

        //Vector3 screenPos = Camera.main.WorldToScreenPoint(_enemy.gameObject.transform.position);
        Vector3 screenPos = new Vector3(Screen.width * 0.5f, Screen.height * 0.5f);

        detailsWindow.position = screenPos;
        detailsWindow.localScale = new Vector3(0.1f, 0.1f, 1);

        _agentName.text = enemy.enemyType.ToString().ToLower() + " " + enemy.characterName;

        float distanceToRobert = CombatResolver.calculateDistance(enemy.gameObject, CombatManager.Instance.robert.gameObject);
        _distance.text = "Distance: " + distanceToRobert.ToString("f2");

        _chanceToHit.text = "Chance To Hit (TORSO): N/A";

        //--------------------------------------
        // Color Acording To Status
        string color = "black";
        if (enemy.currentBehavior == BaseEnemy.Behavior.FRIENDLY)
            color = "green";
        else if (enemy.currentBehavior == BaseEnemy.Behavior.HOSTILE)
            color = "red";
        else if (enemy.currentBehavior == BaseEnemy.Behavior.NEUTRAL)
            color = "yellow";

        _hostility.text += "<color=" + color + "> "+ enemy.currentBehavior.ToString().ToLower() + "</color>";


        //--------------------------------------
        // Color Acording to %
        if (enemy.characterStats.Hitpoints >= enemy.characterStats.TotalHitpoints * 0.8f)
            color = "green";
        else if ((enemy.characterStats.Hitpoints >= enemy.characterStats.TotalHitpoints * 0.4f) && (enemy.characterStats.Hitpoints < enemy.characterStats.TotalHitpoints * 0.8f))
                color = "yellow";
        else if (enemy.characterStats.Hitpoints < enemy.characterStats.TotalHitpoints * 0.4f)
            color = "red";

        _health.text = "Health: <color=" + color + "> "+ enemy.characterStats.Hitpoints + "</color>";

        //--------------------------------------
        _equipment.text += "N/A";
        //--------------------------------------
        _effects.text += "N/A";
        //--------------------------------------

        /*
        //-------------------------------------------------------------------------------------------------------
        // Search for Character Sprite:
        bool hasMultipleSprite = false;
        GameObject multipleSprite = null;
        GameObject singleSprite = null;
        GameObject copiedSprite = null;
        for (int i = 0; i < enemy.gameObject.transform.childCount; i++)
        {
            if (enemy.gameObject.transform.GetChild(i).name == "Agent Sprite (1)")
            {
                hasMultipleSprite = true;
                multipleSprite = enemy.gameObject.transform.GetChild(i).gameObject;
            }

            if (enemy.gameObject.transform.GetChild(i).name == "Agent Sprite")
            {
                singleSprite = enemy.gameObject.transform.GetChild(i).gameObject;
            }
        }
        if (hasMultipleSprite && multipleSprite != null)
        {
            copiedSprite = Instantiate(multipleSprite) as GameObject;
        }
        else
        {
            if (singleSprite != null)
                copiedSprite = Instantiate(singleSprite) as GameObject;
        }

        if (copiedSprite != null)
        {
            copiedSprite.transform.parent = enemySprite;
            copiedSprite.transform.localPosition = new Vector3(0, 0, 0);
        }
        */

        characterSprite.overrideSprite = enemy.gameObject.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite;

        /*
        GameObject singleSprite = null;
        for (int i = 0; i < enemy.gameObject.transform.childCount; i++)
        {
            if (enemy.gameObject.transform.GetChild(i).name == "Agent Sprite")
            {
                singleSprite = enemy.gameObject.transform.GetChild(i).gameObject;
            }
        }

        if (singleSprite != null)
        {
            characterSprite.overrideSprite = singleSprite.GetComponent<SpriteRenderer>().sprite;
        }
        */
        this.gameObject.SetActive(true);
        needToMove = true;
    }

    //-------------------------------------------------------------------------------------------------------
    public void SubmitTalk()
    {
        GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().combatDialog.SetActive(true);
        DialogCombatManager.Instance.talkSelected(_enemy);
        this.gameObject.SetActive(false);
    }
    //-------------------------------------------------------------------------------------------------------
    public void SubmitAttack(string part)
    {
        CombatManager.Instance.Logger.SubmitText("Robert Targets " + part.ToLower());
        BaseEnemy.BodyObjetive targetedPart = BaseEnemy.BodyObjetive.TORSO;

        if (part == "HEAD")
            targetedPart = BaseEnemy.BodyObjetive.HEAD;
        else if (part == "ARMS")
            targetedPart = BaseEnemy.BodyObjetive.ARMS;
        else if (part == "TORSO")
            targetedPart = BaseEnemy.BodyObjetive.TORSO;
        else if (part == "LEGS")
            targetedPart = BaseEnemy.BodyObjetive.LEGS;

        CombatManager.Instance.attackSelected(_enemy, targetedPart);
        this.gameObject.SetActive(false);
    }

    bool needToMove = false;

    void Update()
    {
        if (!needToMove) return;

        detailsWindow.localPosition = Vector3.MoveTowards(detailsWindow.localPosition, Vector3.zero, Time.deltaTime * 15000);

        detailsWindow.localScale = Vector3.MoveTowards(detailsWindow.localScale, new Vector3(1, 1, 1), Time.deltaTime * 15f);
       

        //if ((detailsWindow.localScale.x < 1) && (detailsWindow.localScale.y < 1))
          //  detailsWindow.localScale += new Vector3(0.02f, 0.02f,0);

        float distanceSquared = (detailsWindow.localPosition - Vector3.zero).sqrMagnitude;

        if ((distanceSquared < 2 * 2) && (detailsWindow.localScale.x >= 1f))
        {
            needToMove = false;
        }
    }
}
