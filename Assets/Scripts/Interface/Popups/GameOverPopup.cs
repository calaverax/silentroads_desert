﻿using UnityEngine;
using System.Collections;

public class GameOverPopup : MonoBehaviour {

    public AudioClip bgmOne, bgmTwo;
    public AudioSource source;

    void OnEnable()
    {
        if (Random.Range(0.0f, 1.0f) >= 0.5f)
            source.clip = bgmOne;
        else
            source.clip = bgmTwo;

        source.Play();
    }
	
}
