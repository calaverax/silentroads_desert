﻿using UnityEngine;
using System;
using System.Collections;

public class ResetGameDataPopup : MonoBehaviour {

    Action _confirmCallback, _cancelCallback;

    public void showPopup(Action confirm, Action cancel)
    {
        this.gameObject.SetActive(true);

        _confirmCallback = confirm;
        _cancelCallback = cancel;

    }

    public void Confirm()
    {
        this.gameObject.SetActive(false);
        if (_confirmCallback != null)
            _confirmCallback();
    }

    public void Cancel()
    {
        this.gameObject.SetActive(false);
        if (_cancelCallback != null)
            _cancelCallback();
    }
    
}
