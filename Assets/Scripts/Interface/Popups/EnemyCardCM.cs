﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EnemyCardCM : MonoBehaviour
{
    
    [SerializeField]
    GameObject _bossHeader;
    [SerializeField]
    Image _characterSprite, _hostilityFill;
    [SerializeField]
    Text _distanceField;
    public Text _nameField;
    public Text _hostilityField;
    public Text _moralField;
    public string arma;

    public Slider _hostility; 
    public Slider _healtBar;
    public BaseEnemy _enemy;
    public TarjetasEnemigos scriptDelMenu;
    [SerializeField]
    public GameObject botonOscurecer;
    public float distanceToRobert;
    public float distancia;
    public GameObject panel;
    public Coroutine corutina;

    Ray ray;
    

    //private Material[] materials;

    public void Start()
    {
        //Comentado porque crea algunos problemas
        /*
        _nameField.text = "CodeName: " + _enemy.characterName;
        _moralField.text = "Moral: " + _enemy.getMorale().ToString();
        _hostilityField.text = "State: " + _enemy.currentBehavior.ToString().ToLower();
        arma = "Arma: " + _enemy.selectedWeapon.GetModelName(); */
        //distancia = _enemy.distanceToTarget;
    }

    public void Update()
    {
        _distanceField.text = "Dist: " + distancia + "mts.";
        
        
        
        //_healtBar.value = 10;
        
        


        if (Input.GetMouseButtonDown(0) && gameObject.activeSelf)
        {//Cuando el jugador apirete el mouse,

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);//Tirá un raycast
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.collider.tag == "Tarjeta" || hit.collider.tag == "GUI") { } //Si pega en la tarjeta o en la gui no hagas nada.
                //else { gameObject.SetActive(false); }//Pero si pegó en otro lado, desactivá la tarjeta
            }

        }



    }


    public void ActivateWindow(BaseEnemy enemy)
    {
        _enemy = enemy;

        _bossHeader.SetActive(enemy.isPackLeader);

        _characterSprite.overrideSprite = enemy.gameObject.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite;

        _distanceField.text = "Dist: " + distancia + "mts.";

        if (enemy.characterStats == null) { Debug.Log("character stats null"); enemy.generateStarterStats(); }

        _hostility.value = enemy.characterStats.Hostility;
        if (enemy.currentBehavior == BaseEnemy.Behavior.FRIENDLY)
            _hostilityFill.color = Color.green;
        else if (enemy.currentBehavior == BaseEnemy.Behavior.HOSTILE)
            _hostilityFill.color = Color.red;
        else if (enemy.currentBehavior == BaseEnemy.Behavior.NEUTRAL)
            _hostilityFill.color = Color.yellow;
        if (!_healtBar) { Debug.Log("healtbar es null;;;;;;;;;;;;;;;;;;;;;;;;"); }
       
        _healtBar.maxValue = enemy.characterStats.TotalHitpoints;
        _healtBar.minValue = 0;
        _healtBar.value = enemy.characterStats.Hitpoints;

        this.gameObject.SetActive(true);
        //Trasparentar();
    }

    //-------------------------------------------------------------------------------------------------------
    public void SubmitTalk()
    {
        GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().combatDialog.SetActive(true);
        DialogCombatManager.Instance.talkSelected(_enemy);
        scriptDelMenu.Desactivar(scriptDelMenu.tarjetaActiva);
        scriptDelMenu.freeze = true;
        //this.gameObject.SetActive(false);
    }
    //-------------------------------------------------------------------------------------------------------
    public void SubmitAttack(string part)
    {
        CombatManager.Instance.Logger.SubmitText("Robert Targets " + part.ToLower());
        BaseEnemy.BodyObjetive targetedPart = BaseEnemy.BodyObjetive.TORSO;

        if (part == "HEAD")
            targetedPart = BaseEnemy.BodyObjetive.HEAD;
        else if (part == "ARMS")
            targetedPart = BaseEnemy.BodyObjetive.ARMS;
        else if (part == "TORSO")
            targetedPart = BaseEnemy.BodyObjetive.TORSO;
        else if (part == "LEGS")
            targetedPart = BaseEnemy.BodyObjetive.LEGS;

        CombatManager.Instance.attackSelected(_enemy, targetedPart);
    }



    /*
    //no la estoy usando esta PPP
    void Trasparentar()
    {
        
        foreach (Material material in materials) {
            //material.color.a = 0.2f;
            //material.SetColor ("_Color", new Color (1,1,1,0.3f));

            Color color = material.color;
            color.a = 0.3f;
            material.color = color;
        }



    }*/


    public void FrenarScroll(bool booleana){
        scriptDelMenu.scroll = !booleana;
    }


    public void BotonOn(GameObject imagen)
    {
        scriptDelMenu.ActivarTarjeta(imagen, true);
    }
    public void BotonOff(GameObject imagen)
    {
        scriptDelMenu.ActivarTarjeta(imagen, false);
    }



    //Esta corutina cambia el tamaño de la tarjeta seleccionada por sobre las otras.
    public IEnumerator Desplegar(GameObject tarjeta, Vector3 source, Vector3 target, float overTime)
    {

        Transform brillos = tarjeta.transform.GetChild(0);
        Vector3 brillosScale = brillos.transform.localScale;


        float startTime = Time.time;
        while (Time.time < startTime + overTime)
        {
            tarjeta.transform.localScale = Vector3.Lerp(source, target, (Time.time - startTime) / overTime);
            brillos.transform.localScale = brillosScale;
            yield return null;
        }
        tarjeta.transform.localScale = target;
        brillos.transform.localScale = brillosScale;

    }


}





