﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class StartGamePopup : MonoBehaviour {

    public string[] levelNames;

    public Text titleField;

    Action _confirmCallback, _cancelCallback;


    public void showPopup(Action confirm, Action cancel)
    {
        this.gameObject.SetActive(true);

        if ((levelNames.Length > 0) && (GameManager.Instance.selectedLevel <= levelNames.Length))
            titleField.text = levelNames[GameManager.Instance.selectedLevel - 1];

        _confirmCallback = confirm;
        _cancelCallback = cancel;

    }
    public void DelayedConfirm(float time)
    {
        Invoke("Confirm", time);
    }

    public void Confirm()
    {
        MenuBGMcontroller.Instance.stopAudio();

        this.gameObject.SetActive(false);
        if (_confirmCallback != null)
            _confirmCallback();
    }

    public void Cancel()
    {
        this.gameObject.SetActive(false);
        if (_cancelCallback != null)
            _cancelCallback();
    }
}
