﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCardCloseUp : MonoBehaviour
{
    [SerializeField] GameObject _bossHeader;
    [SerializeField] Image _characterSprite, _hostilityFill;
    [SerializeField] Text _distanceField;
    [SerializeField] Slider _hostility, _healtBar;

    BaseEnemy _enemy;

    public void ActivateWindow(BaseEnemy enemy)
    {
        _enemy = enemy;

        _bossHeader.SetActive(enemy.isPackLeader);
        //_characterSprite.overrideSprite = enemy.gameObject.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite;
        _characterSprite.overrideSprite = enemy.gameObject.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite;
        Debug.Log("******************************* "+ enemy.gameObject.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite.name);

        float distanceToRobert = CombatResolver.calculateDistance(enemy.gameObject, CombatManager.Instance.robert.gameObject);
        _distanceField.text = "Dist: " + distanceToRobert;

        _hostility.value = enemy.characterStats.Hostility;
        if (enemy.currentBehavior == BaseEnemy.Behavior.FRIENDLY)
            _hostilityFill.color = Color.green;
        else if (enemy.currentBehavior == BaseEnemy.Behavior.HOSTILE)
            _hostilityFill.color = Color.red;
        else if (enemy.currentBehavior == BaseEnemy.Behavior.NEUTRAL)
            _hostilityFill.color = Color.yellow;

        _healtBar.maxValue = enemy.characterStats.TotalHitpoints;
        _healtBar.minValue = 0;
        _healtBar.value = enemy.characterStats.Hitpoints;

        this.gameObject.SetActive(true);
    }

    //-------------------------------------------------------------------------------------------------------
    public void SubmitTalk()
    {
        GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().combatDialog.SetActive(true);
        DialogCombatManager.Instance.talkSelected(_enemy);
        this.gameObject.SetActive(false);
    }
    //-------------------------------------------------------------------------------------------------------
    public void SubmitAttack(string part)
    {
        CombatManager.Instance.Logger.SubmitText("Robert Targets " + part.ToLower());
        BaseEnemy.BodyObjetive targetedPart = BaseEnemy.BodyObjetive.TORSO;

        if (part == "HEAD")
            targetedPart = BaseEnemy.BodyObjetive.HEAD;
        else if (part == "ARMS")
            targetedPart = BaseEnemy.BodyObjetive.ARMS;
        else if (part == "TORSO")
            targetedPart = BaseEnemy.BodyObjetive.TORSO;
        else if (part == "LEGS")
            targetedPart = BaseEnemy.BodyObjetive.LEGS;

        CombatManager.Instance.attackSelected(_enemy, targetedPart);
        this.gameObject.SetActive(false);
    }

    Ray ray;
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && gameObject.activeSelf)
        {//Cuando el jugador apirete el mouse,

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);//Tirá un raycast
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.collider.tag == "Tarjeta" || hit.collider.tag == "GUI") { } //Si pega en la tarjeta o en la gui no hagas nada.
                else { gameObject.SetActive(false); }//Pero si pegó en otro lado, desactivá la tarjeta
            }

        }
    }

}
         
         

	

