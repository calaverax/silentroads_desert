﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuManager : MonoBehaviour {

    public BaseMenu startingMenu;
    public GameObject dustStorm;
    public GameObject[] backgrounds;
    public GameObject currentBackground;
    public MenuBGMcontroller menuBGMController;
         
    //public Image backgoundField;

    private BaseMenu _currentMenu;
    private BaseMenu _menuToShow;

    public void switchBackground(int index)
    {
        if (currentBackground != null)
            currentBackground.SetActive(false);

        backgrounds[index].transform.localScale = new Vector3(1,1,1);
        backgrounds[index].SetActive(true);

        currentBackground = backgrounds[index];

        if (_currentMenu.GetType() == typeof(MainMenuController))
        {
            ((MainMenuController)_currentMenu).DeActivateMenuExtras();
            ((MainMenuController)_currentMenu).backgroundTypeIdx = index;
            ((MainMenuController)_currentMenu).ActivateMenuExtras();
        }
    }

    public void ShowMenu(BaseMenu menuToShow)
    {
        if (menuToShow == null)
        {
            Debug.LogError("Invalid Menu Given. Check Call to ShowMenu");
            return;
        }
        _menuToShow = menuToShow;

        _currentMenu.CloseMenu();
        /*
        if ((!(menuToShow.GetType() == typeof(OptionsMenuController)))
            ||
            (!(_currentMenu.GetType() == typeof(OptionsMenuController)))
            )
            MenuBGMcontroller.Instance.FadeAudioOut();
            */
        //if (!(menuToShow.GetType() == typeof(OptionsMenuController)))
        MenuBGMcontroller.Instance.FadeAudioOut();

    }

    private void setCurrentMenu(BaseMenu menu)
    {
        if (menu == null)
        {
            Debug.LogError("Trying to set an invalid menu as CurentMenu");
            return;
        }

        _currentMenu = menu;

        _currentMenu.openCallback.AddListener(OnMenuOpen);
        _currentMenu.closeCallback.AddListener(OnMenuClosed);
        _currentMenu.ShowMenu();
        _currentMenu.ActivateMenuExtras();
    }

    private void OnMenuOpen()
    {
        Debug.Log("Opening Menu " + _currentMenu);
        MenuBGMcontroller.Instance.FadeAudioIn();
        //StartCoroutine(FadeAudio(fadeTime, Fade.In));
    }

    private void OnMenuClosed()
    {
        _currentMenu.DeActivateMenuExtras();
        _currentMenu.closeCallback.RemoveListener(OnMenuClosed);
        _currentMenu.closeCallback.RemoveListener(OnMenuOpen);

        setCurrentMenu(_menuToShow);
    }


	// Use this for initialization
	void Start ()
    {
        GamePreferences.Instance.loadGamePreferences();

        if (startingMenu == null)
        {
            Debug.LogError("Starting Menu Invalid");
            return;
        }

        _currentMenu = startingMenu;

        _currentMenu.openCallback.AddListener(OnMenuOpen);
        _currentMenu.closeCallback.AddListener(OnMenuClosed);

        _currentMenu.ShowMenu();
    }

  
    ///////////////////////////////////////////////////////////////////////////////PPPP
    public void IrAlMapa()
    {
        ScenesLoader.Instance.LoadScene("Level_Selection", true, true, true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
