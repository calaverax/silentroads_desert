﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuController : BaseMenu {

    public static int CHAPTER_ONE_LEVELS_AMOUNT = 23;

    public Button continueButton;
    public MenuManager _menuManager;
    public ResetGameDataPopup confirmPopup;
    public GameObject RobertDiedPopup;
    public Button _continue;

    private GameManager _manager;
    public int backgroundTypeIdx = 0;
    private AsyncOperation loadScene;

    public GameObject[] backgroundType1Extras, backgroundType2Extras, backgroundType3Extras;
    public BackgroundSizer _bgSizer;

    private GameObject[] checkMenuExtras()
    {
        if ((backgroundTypeIdx == 0) && (backgroundType1Extras.Length > 0))
            return backgroundType1Extras;
        if ((backgroundTypeIdx == 1) && (backgroundType2Extras.Length > 0))
            return backgroundType2Extras;
        if ((backgroundTypeIdx == 2) && (backgroundType3Extras.Length > 0))
            return backgroundType3Extras;

        return null;
    }
    public override void ActivateMenuExtras()
    {
        //_menuManager.FadeAudioIn();
        MenuBGMcontroller.Instance.FadeAudioIn();

        GameObject[] objects = checkMenuExtras();

        if (objects != null)
        {
            for (int i = 0; i < objects.Length; i++)
                if (!objects[i].activeInHierarchy)
                    objects[i].SetActive(true);
        }
        _bgSizer.startEffect();
        /*
        if (dependantGO.Length > 0)
            for (int i = 0; i < dependantGO.Length; i++)
                if (!dependantGO[i].activeInHierarchy)
                    dependantGO[i].SetActive(true);
        */
    }

    public override void DeActivateMenuExtras()
    {
        GameObject[] objects = checkMenuExtras();

        if (objects != null)
        {
            for (int i = 0; i < objects.Length; i++)
                if (objects[i].activeInHierarchy)
                    objects[i].SetActive(false);
        }
        /*
        if (dependantGO.Length > 0)
            for (int i = 0; i < dependantGO.Length; i++)
                if (!dependantGO[i].activeInHierarchy)
                    dependantGO[i].SetActive(true);
        */
    }

    protected override void CloseCallback()
    {
        base.CloseCallback();
        _bgSizer.stopEffect();
        _bgSizer.resetSize();
    }

    protected override void OpenCallback()
    {
        base.OpenCallback();
        _bgSizer.startEffect();
    }

    // Use this for initialization
    void Start ()
    {
        _manager = GameManager.Instance;
        Debug.Log("AA");
        if (LevelHistory.Instance.ExistLevels() == false)
        {
            Debug.Log("BB");
            _continue.interactable = false;
        }
        // Check if There is an already saved Game 
        // Enable / Disable Continue
        //continueButton.interactable = _manager.savedGame.tutorialCompleted;
        ActivateMenuExtras();
        _bgSizer.startEffect();
    }

    public void newGame()
    {
        if (_manager.savedGame.tutorialCompleted)
        {
            confirmPopup.showPopup(resetAndLoadNewGame, null);
        }
        else
        {
            loadNewGame();
        }
    }

    private void resetAndLoadNewGame()
    {
        MenuBGMcontroller.Instance.FadeAudioOut();
        //CloseCallback();
        Invoke("loadNewGame", 1.0f);
        //loadNewGame();
    }

    private void loadNewGame()
    {
        // First Clean al saved Games.
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
        GameManager.Instance.resetSavedGame();
        Debug.Log("La cuenta da : " + GameManager.Instance.savedGame.levelsWon.Count);
        LootPersistenceManager.Instance.EmptyLoot();
        LevelHistory.Instance.EmptyHistory();
        EnemyHistory.Instance.EmptyHistory();
        PlayerDataManager.Instance.EmptyData();

        GameManager.Instance.selectedChapter = 1;
        GameManager.Instance.selectedLevel = 1;

        processLevelsData();

#if UNITY_EDITOR
        Application.LoadLevel("chapterSelection");
#else
        StartCoroutine(loadGameplayAsync("chapterSelection"));
#endif

    }

	public void Load3dTestLevel()
	{
		// First Clean al saved Games.
		PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();
		_manager.resetSavedGame();
		
		
		GameManager.Instance.selectedChapter = 1;
		GameManager.Instance.selectedLevel = 1;
		
		processLevelsData();
		
		#if UNITY_EDITOR
		Application.LoadLevel("TheLongRoad3dTest");
		#else
		StartCoroutine(loadGameplayAsync("TheLongRoad3dTest"));
		#endif
	}

    void processLevelsData()
    {
        // Process Water Wells.
        int[] levelsWithWell = { 0, 0, 0};

        do
        {
            for (int i = 0; i < levelsWithWell.Length; i++)
                levelsWithWell[i] = Random.Range(2, CHAPTER_ONE_LEVELS_AMOUNT); ;
        }
        while (levelsWithWell[0] == levelsWithWell[1] || levelsWithWell[0] == levelsWithWell[2] || levelsWithWell[1] == levelsWithWell[2]);

        PlayerPrefs.SetInt("Well1", levelsWithWell[0]);
        PlayerPrefs.SetInt("Well2", levelsWithWell[1]);
        PlayerPrefs.SetInt("Well3", levelsWithWell[2]);

        PlayerPrefs.Save();
    }

    public void continueGame()
    {
        if (PlayerPrefs.GetInt("RobertDied", 0) == 0)
        {
            MenuBGMcontroller.Instance.FadeAudioOut();
            Invoke("delayedContinue", 1.0f);
        }
        else
        {
            RobertDiedPopup.SetActive(true);
        }
        //CloseCallback();
    }

    private void delayedContinue()
    {
#if UNITY_EDITOR
        Debug.Log(LevelHistory.Instance.LastLevel());
        ScenesLoader.Instance.LoadScene(LevelHistory.Instance.LastLevel());
        //Application.LoadLevel(LevelHistory.Instance._levels.level_list[LevelHistory.Instance._levels.level_list.Count - 1]);
#else
        ScenesLoader.Instance.LoadScene(LevelHistory.Instance.LastLevel());
#endif
    }


    IEnumerator loadGameplayAsync(string sceneName)
    {
        loadScene = Application.LoadLevelAsync(sceneName);
        loadScene.allowSceneActivation = true;
        yield return loadScene;
    }

}
