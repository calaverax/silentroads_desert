﻿using UnityEngine;
using System.Collections;

public class LevelSelectAnimation : MonoBehaviour
{
    [SerializeField]private GameObject _dustStorm;
    [SerializeField]private GameObject _animations;


    void OnEnable()
    {
        Time.timeScale = 0.6f;
    }

    private void EnableDustStorm()
    {
        if (_dustStorm != null) _dustStorm.SetActive(true);
        if (_animations != null) _animations.SetActive(true);
    }
    private void DisableDustStorm()
    {
        if (_dustStorm != null) _dustStorm.SetActive(false);
        if (_animations != null) _animations.SetActive(false);
    }

}
