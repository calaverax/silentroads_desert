﻿using UnityEngine;
using System.Collections;

public class ClockNiddle : MonoBehaviour {

    private bool combatTimeRunning = false;

    void Start()
    {
        CombatManager.Instance.OnCombatResumed.AddListener(combatResumed);
        CombatManager.Instance.OnCombatPaused.AddListener(combatPaused);
    }

    private void combatResumed()
    {
        combatTimeRunning = true;
    }

    private void combatPaused()
    {
        combatTimeRunning = false;
    }
    

	void Update ()
    {
        if (combatTimeRunning)
            this.gameObject.transform.RotateAround(new Vector3(0, 0, 1), - 0.5f * Time.deltaTime);
	}
}
