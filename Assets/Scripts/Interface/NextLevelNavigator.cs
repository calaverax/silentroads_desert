﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NextLevelNavigator : MonoBehaviour
{
    GameObject _nextLevelObject;

    void Start()
    {
        _nextLevelObject = transform.GetChild(0).gameObject;

        if (_nextLevelObject != null)
            _nextLevelObject.GetComponent<Button>().onClick.AddListener(loadNextLevel);

        CombatManager.Instance.OnCombatEnded.AddListener(CombatEnded);
    }

    void CombatEnded()
    {
        if (_nextLevelObject != null)
            _nextLevelObject.SetActive(true);
    }

    void loadNextLevel()
    {
        if (Application.loadedLevelName == "Chapter-1Level-1")
            ScenesLoader.Instance.LoadScene("Robert's Ranch Exterior");
        else
            ScenesLoader.Instance.LoadScene("TheLongRoad");
            //ScenesLoader.Instance.LoadScene("Chapter 1 Level Selection");
    }
}
