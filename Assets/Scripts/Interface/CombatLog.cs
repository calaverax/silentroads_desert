﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CombatLog : MonoBehaviour {

    // Use this for initialization
    [SerializeField]
    private ScrollRect _scrollRect;
    [SerializeField]
    private Text _logger;

    private int linesCount = 0;


    void OnEnable()
    {
        if (_scrollRect == null)
            _scrollRect = transform.GetComponent<ScrollRect>();
    }

	void Start ()
    {
        if (_scrollRect == null)
            _scrollRect = transform.GetComponent<ScrollRect>();
	}

    public void SubmitText(string text)
    {
        if (_logger == null)
        {
            Transform trans = transform.Find("Mask/Text");
            if (trans != null)
            {
                _logger = trans.gameObject.GetComponent<Text>();
            }
        }
        _logger.text += text + "\n";
        _scrollRect.verticalNormalizedPosition = 0.0f;

        linesCount++;

        if (linesCount >= 50)
            deleteOldEntrys();
    }

    private void deleteOldEntrys()
    {
        int newlines = 0;
        int subIndex = 0;

        for (int i = 0; i < _logger.text.Length; i++)
        {
            if (_logger.text[i] == '\n')
                newlines++;

            if (newlines >= 10)
            {
                subIndex = i;
                break;
            }
        }

        _logger.text = _logger.text.Substring(subIndex);
        linesCount -= 10;
    }
}
