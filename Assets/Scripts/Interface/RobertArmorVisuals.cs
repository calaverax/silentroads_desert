﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Wasabi.InventorySystem;
using Wasabi.InventorySystem.Inventory;

public class RobertArmorVisuals : MonoBehaviour {

    [SerializeField]
    GameObject Helmet, Torso, Arms, Legs;

    void Start()
    {
        ISInventoryManager.instance.OnItemEquiped.AddListener(ItemEquiped);
        ISInventoryManager.instance.OnItemUnequiped.AddListener(ItemUnequiped);
    }


    private void ItemEquiped(ISObject item)
    {
        if (item is ISArmor)
        {
            ISArmor armor = item as ISArmor;

            if (armor.EquipSlot == EquipmentSlot.HEAD)
            {
                //Helmet.SetActive(true);
                Text text = Helmet.transform.GetChild(0).GetComponent<Text>();
                if (text != null)
                    text.text = armor.Protection.ToString();
            }
            else if (armor.EquipSlot == EquipmentSlot.TORSO)
            {
                //Torso.SetActive(true);
                Text text = Torso.transform.GetChild(0).GetComponent<Text>();
                if (text != null)
                    text.text = armor.Protection.ToString();
            }
            else if (armor.EquipSlot == EquipmentSlot.ARMS)
            {
                //Arms.SetActive(true);
                Text text = Arms.transform.GetChild(0).GetComponent<Text>();
                if (text != null)
                    text.text = armor.Protection.ToString();
            }
            else if (armor.EquipSlot == EquipmentSlot.LEGS)
            {
                //Legs.SetActive(true);
                Text text = Legs.transform.GetChild(0).GetComponent<Text>();
                if (text != null)
                    text.text = armor.Protection.ToString();
            }
        }
    }

    private void ItemUnequiped(ISObject item)
    {
        if (item is ISArmor)
        {
            ISArmor armor = item as ISArmor;

            if (armor.EquipSlot == EquipmentSlot.HEAD)
                Helmet.SetActive(false);
            else if (armor.EquipSlot == EquipmentSlot.TORSO)
                Torso.SetActive(false);
            else if (armor.EquipSlot == EquipmentSlot.ARMS)
                Arms.SetActive(false);
            else if (armor.EquipSlot == EquipmentSlot.LEGS)
                Legs.SetActive(false);
        }
    }
}
