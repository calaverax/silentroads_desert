﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AbnormalStatusPanel : MonoBehaviour {

    // Controls and holders
    Sprite[] _statesSprites;
    Image _animatedStateImage;
    GameObject _container;

    [SerializeField] GameObject _animatedStatePrefab;
    [SerializeField] GameObject _statusElement;

    List<GameObject> _pooledStatus = new List<GameObject>(5);



	void Start ()
    {
        _statesSprites = Resources.LoadAll<Sprite>("Atlas/Status Icons/Enemy Status");
        _container = transform.GetChild(0).GetChild(0).gameObject;
        _container.SetActive(false);

	}
    
    public void TriggerNewState(string stateName)
    {
        GameObject go = Instantiate(_animatedStatePrefab);
        go.transform.SetParent(this.transform);
        go.transform.localScale = new Vector3(1, 1, 1);
        go.GetComponent<SimpleDestoyElement>().configure(stateName, this,getSpriteByState(stateName));
    }

    public void RemoveState(string stateName)
    {
        // Encontrar si tengo el elemento.
        int idx = -1;

        for (int i = 0; i < _pooledStatus.Count; i++)
        {
            if (_pooledStatus[i].name == stateName)
            {
                idx = i;
                break;
            }
        }

        // Deletearlo y sacarlo de la lista
        if (idx != -1)
        {
            DestroyImmediate(_pooledStatus[idx]);
            _pooledStatus.RemoveAt(idx);
        }

        // Recalcular Container
        recalculateContainer();
    }

    public void addState(string stateName)
    {
        if (_pooledStatus.Find(x => x.name == stateName) != null) return;

        _container.SetActive(true);
        GameObject go = Instantiate(_statusElement);
        go.GetComponent<Image>().overrideSprite = getSpriteByState(stateName);
        go.name = stateName;

        go.transform.SetParent(_container.transform);
        go.transform.localScale = new Vector3(1, 1, 1);
        go.transform.localPosition = new Vector3(0, 0, 0);

        _pooledStatus.Add(go);

        recalculateContainer();

    }

    void recalculateContainer()
    {
        _container.GetComponent<RectTransform>().sizeDelta = new Vector2(1081, 391 * _pooledStatus.Count);
        _container.transform.localPosition = new Vector3(_container.transform.localPosition.x, 0,1);
    }

    Sprite getSpriteByState(string stateName)
    {
		if (_statesSprites == null)
			Resources.LoadAll<Sprite>("Atlas/Status Icons/Enemy Status");
		if ((_statesSprites == null) || (_statesSprites.Length == 0))
			return null;

        for (int i = 0; i < _statesSprites.Length; i++)
            if (_statesSprites[i].name == stateName)
                return _statesSprites[i];

        return _statesSprites[0];
    }
}
