﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Touchable : Graphic
{
    public override bool Raycast(Vector2 sp, Camera eventCamera)
    {
        return true;
    }

    protected override void OnFillVBO(List<UIVertex> vbo)
    {
        //base.OnFillVBO(vbo);
    }

    public void Touched()
    {
        Debug.Log("Touched Stuff");
    }
}
