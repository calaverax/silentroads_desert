﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthBarController : MonoBehaviour {

    Slider frontHealthBar, backHealthBar;
    Animator _damageAnimator;
    Text _damageLabel;

    bool needsUpdate;
    bool initialized = false;

    // Use this for initialization
    void Start()
    {
        if (!initialized)
            getElements();

    }
    void getElements()
    {
        GameObject slidersParent = transform.Find("Sliders").gameObject;
        
        frontHealthBar = slidersParent.transform.Find("FrontSlider").GetComponent<Slider>();
        backHealthBar = slidersParent.transform.Find("BackSlider").GetComponent<Slider>();

        initialized = true;
    }
    public void Configure(int healthAmount)
    {
        if ((frontHealthBar == null) || (backHealthBar == null)) getElements();

        frontHealthBar.maxValue = frontHealthBar.value = backHealthBar.maxValue = backHealthBar.value = healthAmount;
    }

    public void ApplyDamage(int amount, bool isCritical = false)
    {
        int lifeLost = (int)frontHealthBar.value - amount;
        animateDamage(lifeLost, isCritical);

        frontHealthBar.value = amount;

        if (!needsUpdate)
            Invoke("toggleUpdate", 0.5f);
    }

    void animateDamage(int amount, bool isCritical = false)
    {
        if (_damageAnimator == null)
            _damageAnimator = transform.Find("Floating Text").GetComponent<Animator>();

        if (_damageLabel == null)
            _damageLabel = transform.Find("Floating Text").Find("Text").GetComponent<Text>();

        if ((_damageAnimator != null) && (_damageLabel != null))
        {
            if (isCritical)
            {
                _damageLabel.text = "<b><color=yellow>-" + amount.ToString() + "</color><b>";
            }
            else
            {
                _damageLabel.text = "<color=red>-" + amount.ToString() + "</color>";
            }
            _damageAnimator.SetTrigger("ShowDamage");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!needsUpdate) return;

        if (backHealthBar.value > frontHealthBar.value)
            backHealthBar.value -= 0.25f;
        else
            needsUpdate = false;
    }

    void toggleUpdate()
    {
        needsUpdate = true;
    }
}
