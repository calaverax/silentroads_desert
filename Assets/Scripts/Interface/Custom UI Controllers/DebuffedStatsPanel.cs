﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DebuffedStatsPanel : MonoBehaviour
{
    [SerializeField] private GameObject _debuffInfoPrefab;
    [SerializeField] private GameObject _debuffsPanel;
    [SerializeField] private RectTransform _detailsContainer;
    [SerializeField] private BaseEnemy _parentCharacter;

    public void ToggleDetailsPanel()
    {
        if (_debuffsPanel.active)
            _debuffsPanel.SetActive(false);
        else
            activateDetails();
    }

    private void populateDebuffsInfo()
    {
        GameObject newDebuffInfo;
        int penaltiesAmount = 0;

        // Primero limpiamos todos los hijos que el panel pueda contener
        foreach (Transform child in _detailsContainer)
            Destroy(child.gameObject);

        // Iteramos por todos los CharacterStat
        foreach (KeyValuePair<string, CharacterStat> entry in _parentCharacter.characterStats.stats)
        {
            // Verificamos si tiene alguna penalidad
            if (entry.Value.penalties.Count > 0)
            {
                // Recorremos todas las penalidades disponibles.
                for (int i = 0; i < entry.Value.penalties.Count; i++)
                {
                    // Incrementamos el contador total de detalles
                    penaltiesAmount++;

                    // Intanciamos el boton
                    newDebuffInfo = Instantiate(_debuffInfoPrefab);
                    newDebuffInfo.transform.SetParent(_detailsContainer);
                    newDebuffInfo.transform.localScale = new Vector3(1, 1, 1);

                    // y lo configuramos
                    newDebuffInfo.GetComponent<DebuffInfo>().Initialize(entry.Key, entry.Value.penalties[i].ratio, entry.Value.penalties[i].duration);
                }
            }
        }

        // Acomodamos el tamaño del container de acuerdo a la cantidad de botones disponibles.
        //_debuffsPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(_detailsContainer.sizeDelta.x, 250 * penaltiesAmount);
        _detailsContainer.sizeDelta = new Vector2(_detailsContainer.sizeDelta.x, 250 * penaltiesAmount);

    }

    private void activateDetails()
    {
        populateDebuffsInfo();
        _debuffsPanel.SetActive(true);

        Invoke("deactivateDetails", 5.0f);
    }

    private void deactivateDetails()
    {
        _debuffsPanel.SetActive(false);
    }
}
