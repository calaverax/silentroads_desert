﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class SliderColorPair
{
    public Color color;
    public float minValue, maxValue;
}

[ExecuteInEditMode]
[RequireComponent(typeof(Slider))]
public class CustomSliderColors : MonoBehaviour {

    /*
    -100 a 100
    desde -50 a -100 es Friendly
    desde -49 a +20 es Neutral
    de 20 a 100 es Hostil
    Agentes arrancan con +80 Hostile
    Agente Boss +100
    */

    [SerializeField]
    SliderColorPair[] colorPairs;

    Slider _slider;
    [SerializeField]
    Image _sliderImage;
    // Use this for initialization
    void OnEnable ()
    {
        _slider = GetComponent<Slider>();
        _slider.onValueChanged.AddListener(sliderValueChanged);
	}

    void sliderValueChanged(float value)
    {
        for (int i = 0; i < colorPairs.Length; i++)
        {
            if (value > colorPairs[i].minValue && value < colorPairs[i].maxValue)
            {
                _sliderImage.color = colorPairs[i].color;
                break;
            }
        }
    }
}
