﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DebuffInfo : MonoBehaviour
{
    [SerializeField]
    Text _debuffTitle, _debuffDescription;


    public void Initialize(string affectedStat, float affectedRatio, int affectedDuration)
    {
        _debuffTitle.text = affectedStat;
        _debuffDescription.text = "Affected by " + affectedRatio * 100 + "%" + "\n" + "Turns remaining: " + affectedDuration;
    }

}
