﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TargetFieldsManager : MonoBehaviour {

    private Image _head, _torso, _arms, _legs;

    [SerializeField]
    private BaseCharacter parent;
	// Use this for initialization
	void Start ()
    {
        _head = transform.Find("Head").gameObject.GetComponent<Image>();
        _torso = transform.Find("Torso").gameObject.GetComponent<Image>();
        _arms = transform.Find("Arms").gameObject.GetComponent<Image>();
        _legs = transform.Find("Legs").gameObject.GetComponent<Image>();
    }

    void OnEnable()
    {
        if (parent == null)
            return;

        if (parent.bodyPartHealth[BaseCharacter.BodyObjetive.HEAD].CurrentValue <= 0)
            _head.gameObject.GetComponent<Button>().interactable = false;

        if (parent.bodyPartHealth[BaseCharacter.BodyObjetive.TORSO].CurrentValue <= 0)
            _torso.gameObject.GetComponent<Button>().interactable = false;

        if (parent.bodyPartHealth[BaseCharacter.BodyObjetive.ARMS].CurrentValue <= 0)
            _arms.gameObject.GetComponent<Button>().interactable = false;

        if (parent.bodyPartHealth[BaseCharacter.BodyObjetive.LEGS].CurrentValue <= 0)
            _legs.gameObject.GetComponent<Button>().interactable = false;
    }
}
