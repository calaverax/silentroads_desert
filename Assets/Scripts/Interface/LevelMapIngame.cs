﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelMapIngame : MonoBehaviour {

    CanvasGroup _canvas;
    bool _startFade = false;
    bool _isOnPreview = false;

    public void PreviewMap()
    {
        this.gameObject.SetActive(true);
        _canvas = gameObject.GetComponent<CanvasGroup>();
        if (_canvas != null)
            _canvas.alpha = 1;
        _isOnPreview = true;
    }
    public void ClosePreview()
    {
        if (!_isOnPreview) return;

        if (_canvas == null) _canvas = gameObject.GetComponent<CanvasGroup>();

        if (_canvas != null)
            _canvas.alpha = 0;

        GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().ZoneMapToggle.isOn = false;

        _isOnPreview = false;
        this.gameObject.SetActive(false);
    }

    public void EnableMap()
    {
        this.gameObject.SetActive(true);
            
        _canvas = gameObject.GetComponent<CanvasGroup>();

        if (_canvas != null)
            _startFade = true;
    }

    void MoveFlag()
    {
        Debug.Log("Moving Flag");
    }

    void FixedUpdate()
    {
        if (_startFade)
        {
            _canvas.alpha += 0.01f;
            if (_canvas.alpha >= 1)
            {
                _startFade = false;
                MoveFlag();
            }
        }

        
    }

}
