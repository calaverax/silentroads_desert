﻿using UnityEngine;
using System.Collections;

public class CreditsMenuController : BaseMenu
{
    public MenuBGMcontroller _menuBGMController;
    public AudioClip menuBGM;

    protected override void OpenCallback()
    {
        base.OpenCallback();
        _menuBGMController.playBGMNow(menuBGM);

    }

    protected override void CloseCallback()
    {
        base.CloseCallback();

        _menuBGMController.selectRandomClip();
        Debug.Log("Closing Credits Menu");
    }


}
