﻿using UnityEngine;
using System.Collections;

public class MapLevelInfoController : MonoBehaviour {

    private AudioSource _source;

    // Use this for initialization
    void Awake()
    {
        _source = GetComponent<AudioSource>();
    }

    void OnEnable()
    {
        Invoke("playIntro", 2.0f);
    }

    private void playIntro()
    {
        if (_source != null)
            _source.Play();
    }
	
}
