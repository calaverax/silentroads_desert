﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CategoryGroup : MonoBehaviour {

    Button addPointsBtn, remPointsBtn;
    Toggle[] points;
    public int SpentPoints
    {
        get
        {
            int amnt = 0;
            if (points != null)
                for (int i = 0; i < points.Length; i++)
                    if (points[i].isOn)
                        amnt++;
            return amnt;
        }
    }
    Image backGround;

    [SerializeField]
    Color _originalColor, _bonusColor;

    FocusSystemController _controller;

    // Control Variables
    bool _bonusWasActive = false;
    int _toggleIndex = 0;
    //int _totalPossiblePoints = 0;
    FocusSystemController.FocusCategory _focusCategory;

    void Start ()
    {
        if (gameObject.name == "Attack")
            _focusCategory = FocusSystemController.FocusCategory.Attack;
        else if (gameObject.name == "Dialogue")
            _focusCategory = FocusSystemController.FocusCategory.Dialogue;
        else if (gameObject.name == "Heal")
            _focusCategory = FocusSystemController.FocusCategory.Heal;
        else if (gameObject.name == "Escape")
            _focusCategory = FocusSystemController.FocusCategory.Escape;
        else
            Debug.LogError("Focus Panel Category Not Setted, Check Names.");

        _controller = transform.parent.parent.GetComponent<FocusSystemController>();

        backGround = GetComponent<Image>();

        addPointsBtn = transform.Find("Add Points").GetComponent<Button>();
        remPointsBtn = transform.Find("Remove Points").GetComponent<Button>();

        addPointsBtn.onClick.AddListener(assignPoints);
        remPointsBtn.onClick.AddListener(removePoints);

        points = transform.Find("Buttons").GetComponentsInChildren<Toggle>();

        // Set Everything to not interactable by the user;
        if (points != null)
            for (int i = 0; i < points.Length; i++)
                points[i].isOn = false;

        addPointsBtn.interactable = false;
        remPointsBtn.interactable = false;

        // TODO: Temp - Controll this from the FocusSystemManager when Checking custimizable System.
        EnableComponents();
    }

    public void EnableComponents()
    {
        addPointsBtn.interactable = true;
        remPointsBtn.interactable = true;

        // Get AvaiablePoints (Total from int + agi)
        //_totalPossiblePoints = CombatManager.Instance.robert.characterStats.Intelligence + CombatManager.Instance.robert.characterStats.Agility;
    }

    public void DisableComponents()
    {
        addPointsBtn.interactable = false;
        remPointsBtn.interactable = false;

        _toggleIndex = 0;
        bonusEnabled(false);

        for (int i = 0; i < points.Length; i++)
            points[i].isOn = false;
    }

    public int assignPoints(int amount)
    {
        int toAssign = 0;
        int rest = 0;

        if (amount > points.Length)
        {
            toAssign = points.Length;
            rest = amount - points.Length;
        }
        else
        {
            toAssign = amount;
        }

        for (int i = 0; i < toAssign; i++)
        {
            _controller.availablePoints--;
            _controller.spentPoints++;
            points[i].isOn = true;
        }

        _toggleIndex = toAssign;
        checkForBonus();
        // Return wherever have points rest or not.
        return rest;
    }

    public void assignPoints()
    {
        if (_controller.availablePoints == 0)
            return;
        /*
        if (_toggleIndex >= _controller.availablePoints)
            return;
        */
        if (_toggleIndex < points.Length)
        {
            _toggleIndex++;
            _controller.availablePoints--;
            _controller.spentPoints++;
        }

        points[_toggleIndex-1].isOn = true;


        checkForBonus();
    }

    public void removePoints()
    {
        if (_toggleIndex > 0)
            points[_toggleIndex-1].isOn = false;

        if (_toggleIndex > 0)
        {
            _toggleIndex--;
            _controller.availablePoints++;
            _controller.spentPoints--;
        }

        bonusEnabled(false);
    }

    private void checkForBonus()
    {
        // Bonus debe estar activo si tenemos todos los puntos activados, o si todos los puntos posibles de asignar estan en este control.
        if ((_toggleIndex >= points.Length) || ((_toggleIndex >= _controller.spentPoints) && (_controller.availablePoints == 0)))
            bonusEnabled(true);
    }

    public void bonusEnabled(bool value)
    {
        if (value)
        {
            if (!_bonusWasActive)
            {
                _controller.OnFocusBonusTriggered.Invoke(_focusCategory, true);
                backGround.color = _bonusColor;
            }
        }
        else
        {
            if (_bonusWasActive)
            {
                _controller.OnFocusBonusTriggered.Invoke(_focusCategory, false);
                backGround.color = _originalColor;
            }
        }
        _bonusWasActive = value;
    }
}
