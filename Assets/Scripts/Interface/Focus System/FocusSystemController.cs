﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;

public class FocusSystemController : MonoBehaviour {
    static FocusSystemController _instance;
    public static FocusSystemController instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = GameObject.FindGameObjectWithTag("FocusSystem");
                if (go != null)
                    _instance = go.GetComponent<FocusSystemController>();
            }
            return _instance;
        }
    }

    // Control Variables:
    public int spentPoints = 0;
    private int _originalTotalPoints = 0;
    public int originalTotalPoints { get { return _originalTotalPoints; } }
    private int _availablePoints = 0;
    public int availablePoints { get { return _availablePoints; } set { _availablePoints = value; _availablePointsLabel.text = availablePoints.ToString(); } }

    // Events
    public class FocusBonusTrigger : UnityEvent<FocusSystemController.FocusCategory, bool> { }
    public FocusBonusTrigger OnFocusBonusTriggered;

    // Elements
    CategoryGroup _attack, _dialogue, _healing, _escape;
    Slider _presetsSlider;
    Text _availablePointsLabel;
    Toggle _customizedPoints;

    public enum FocusCategory
    {
        Attack,
        Dialogue,
        Heal,
        Escape
    }

    void Awake()
    {
        GameObject go = GameObject.FindGameObjectWithTag("FocusSystem");
        if (go != null)
            _instance = go.GetComponent<FocusSystemController>();
    }
	// Use this for initialization
	void Start ()
    {
        OnFocusBonusTriggered = new FocusBonusTrigger();
        OnFocusBonusTriggered.AddListener(BonusTriggered);

        // Get Elements.
        _attack = transform.Find("Panels/Attack").GetComponent<CategoryGroup>();
        _dialogue = transform.Find("Panels/Dialogue").GetComponent<CategoryGroup>();
        _healing = transform.Find("Panels/Heal").GetComponent<CategoryGroup>();
        _escape = transform.Find("Panels/Escape").GetComponent<CategoryGroup>();

        _availablePointsLabel = transform.Find("Panels/Header/Available Points").GetComponent<Text>();
        _customizedPoints = transform.Find("Panels/Header/Toggle").GetComponent<Toggle>();
        _customizedPoints.onValueChanged.AddListener(ToggleCustomize);

        _presetsSlider = transform.Find("Focus Slider").GetComponent<Slider>();
        _presetsSlider.onValueChanged.AddListener(PresetChanged);

        _originalTotalPoints = availablePoints = CombatManager.Instance.robert.characterStats.Intelligence + CombatManager.Instance.robert.characterStats.Agility;

        Invoke("DelayedInit", 0.5f);

    }

    private void DelayedInit()
    {
        //EvaluatePoints();

        _customizedPoints.isOn = false;
        ToggleCustomize(false);
    }

    public void EvaluatePoints()
    {
        // Get Total Stats Points:
        _originalTotalPoints = CombatManager.Instance.robert.characterStats.Intelligence + CombatManager.Instance.robert.characterStats.Agility;

        if (originalTotalPoints > spentPoints)
        {
            availablePoints = originalTotalPoints - spentPoints;
        }

        _availablePointsLabel.text = availablePoints.ToString();
    }

    private void resetPoints()
    {
        _originalTotalPoints = availablePoints = CombatManager.Instance.robert.characterStats.Intelligence + CombatManager.Instance.robert.characterStats.Agility;
        spentPoints = 0;
        _availablePointsLabel.text = availablePoints.ToString();
    }

    private void ToggleCustomize(bool value)
    {
        if (value)
        {
            _attack.EnableComponents();
            _dialogue.EnableComponents();
            _healing.EnableComponents();
            _escape.EnableComponents();
        }
        else
        {
            _attack.DisableComponents();
            _dialogue.DisableComponents();
            _healing.DisableComponents();
            _escape.DisableComponents();
            PresetChanged(4);
        }
    }

    private void PresetChanged(float value)
    {
        resetPoints();
        _customizedPoints.isOn = false;

        _attack.DisableComponents();
        _dialogue.DisableComponents();
        _healing.DisableComponents();
        _escape.DisableComponents();

        int rest = 0;
        if (value == 4)
        {
            // Preset Attack
            _attack.assignPoints(_availablePoints);
        }
        else if (value == 3)
        {
            // Preset Dialogue
            rest = _dialogue.assignPoints(_availablePoints);
            if (rest > 0)
                _attack.assignPoints(_availablePoints);
        }
        else if (value == 2)
        {
            // Preset Healing
            rest = _healing.assignPoints(_availablePoints);
            if (rest > 0)
                _dialogue.assignPoints(_availablePoints);
        }
        else if (value == 1)
        {
            // Preset Escape
            rest = _escape.assignPoints(_availablePoints);
            if (rest > 0)
                rest = _healing.assignPoints(availablePoints);
            if (rest > 0)
                _dialogue.assignPoints(availablePoints);
        }
        _availablePointsLabel.text = availablePoints.ToString();
    }

    void BonusTriggered(FocusCategory category, bool value)
    {
        //Debug.Log("ASD : " + category);
        if (value)
        {
            if (!CombatManager.Instance.focusActiveBonus.Contains(category))
                CombatManager.Instance.focusActiveBonus.Add(category);
        }
        else
        {

            if (!value)
                if (CombatManager.Instance.focusActiveBonus.Contains(category))
                    CombatManager.Instance.focusActiveBonus.Remove(category);
        }

        string state = value ? "Active" : "Inactive";
        CombatManager.Instance.Logger.SubmitText(category.ToString() + " Bonus, is now " + state);
    }


    //----------------------------------------------------------------------------------------------
    public int attackBonusAmnt
    {
        get
        {
            if (_attack == null)
                Start();

            return _attack.SpentPoints / 5;
        }
    }
}
