﻿using UnityEngine;
using System.Collections;

public class LevelSelectionButton : MonoBehaviour
{
    [SerializeField]GameObject[] unlockLevels;

    public void UnlockLevels()
    {
        if ((unlockLevels == null) || (unlockLevels.Length == 0)) return;

        for (int i = 0; i < unlockLevels.Length; i++)
            unlockLevels[i].SetActive(true);
    }

}
