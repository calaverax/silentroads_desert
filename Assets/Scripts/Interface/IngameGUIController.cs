﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Wasabi.InventorySystem;
using Wasabi.InventorySystem.Inventory;
using UnityEngine.SceneManagement;

public class IngameGUIController : MonoBehaviour {

    public Material material; //PPP material de Robert con la armadura.
    public GameObject camara;//PPP La cámara
    private GameObject robert;//PPP Robert
    private Animator robertAnim;//PPP El animator de Robert
    private CombatManager _combatManager;
    private GameManager _gameManager;
    public Sprite[] casioRepresentation;
    public GameObject pausedButton;
    private RectTransform _originalLevelSize;
    public GameObject xpAdd;

    [SerializeField]
    private Slider focusSlider, shootModeSlider;
    [SerializeField]
    private Text autoBurstLabel;

    [SerializeField]
    private Image statsIcon;

    [SerializeField]
    private Image _selectedWeapon;
    [SerializeField]
    private Text _weaponAmmoLabel;
    [SerializeField]
    private GameObject _unavailableLabel;
    //private Image rangedWeaponField, meleeWeaponField, throwingWeaponField;

    [SerializeField]
    private Toggle showLevelInfoToggle;
    public Toggle levelInfoToggle { get { return showLevelInfoToggle; } }

    [SerializeField]
    private Toggle showZoneMap;
    public Toggle ZoneMapToggle { get { return showZoneMap; } }

    [SerializeField]
    private Button fleeButton;

    [SerializeField]
    private GameObject gameOverMenu;

    public Slider waterReservoir;
    public Slider robertHealthBar;
    public Text healthLabel;
    public Slider robertArmorBar;
    public GameObject NoEscape;
    public AudioClip PauseAudioClip, UnPauseAudioClip;
    //public GameObject BackToMenuesBtn;
    //public Image PauseButtonImage;
    public GameObject combatDialog;
    public GameObject fleeDisable;
    public GameObject combatMode;

    private AudioSource _audioSource;

    private bool statIconHighligthed = false;
    private bool fleeIconHighligthed = false;
    private bool hideIconHighligthed = false;

    private BaseCharacter.DamageType _currentSelectedType = BaseCharacter.DamageType.RANGED;

    // TODO: Temporal Checking for Flee Action
    public int turnsForFlee = 0;
    public int turnsPassed = 0;


    private GameObject _weaponReloadAlert;

    public List<ISSlotMain> newSlotsOrder = new List<ISSlotMain> { };

    private List<EquipmentSlot> slotsOrder = new List<EquipmentSlot> { EquipmentSlot.MAIN_HAND, EquipmentSlot.OFF_HAND, EquipmentSlot.THROWABLE };

    //public EnemyDetailsWindow enemyDetailsWindow;
    //public EnemyCardCloseUp cardCloseUp; //PPP
    public LevelNavigationPopup levelNavigationPopup;


    public void CombatTimeChange(Slider slider)
    {
        if (slider.value == 1)
        {
            CombatManager.Instance.Logger.SubmitText("Combat Speed: Normal");
            CombatManager.Instance.COMBAT_FAST = false;
        }
        else if (slider.value == 2)
        {
            CombatManager.Instance.Logger.SubmitText("Combat Speed: Fast");
            CombatManager.Instance.COMBAT_FAST = true;
        }
    }

    private void CombatEnded()
    {
        
        
        Debug.Log("FLEE");
        if (!fleeButton.interactable)
        {
            Debug.Log("FLEE 2");
            fleeDisable.SetActive(false);
            fleeButton.interactable = true;
        }

        InvokeRepeating("fleeButtonAlert", 0.0f, .33f);
    }

    public void XpNewPoint()
    {
        xpAdd.SetActive(true);
    }

    public void XpWithoutPoint()
    {
        xpAdd.SetActive(false);
    }

    private void ActivarEscape()
    {
        
        Debug.Log("FLEE");
        if (!fleeButton.interactable)
        {
            Debug.Log("FLEE 2");
            fleeDisable.SetActive(false);
            fleeButton.interactable = true;
            

            //EnemyHistory.Instance.MarkFollowers();
        }

        InvokeRepeating("fleeButtonAlert", 0.0f, .33f);
    }

    void Start()
    {

        camara = GameObject.FindGameObjectWithTag("MainCamera");//PPP
        robert = GameObject.FindGameObjectWithTag("Player"); //PPP
        robertAnim = robert.GetComponent<Animator>();//PPP
        robert.transform.GetChild(1).GetChild(3).gameObject.GetComponent<Renderer>().material = material;
        

        
 

        _combatManager = CombatManager.Instance;
        _combatManager.OnCombatEnded.AddListener(CombatEnded);
        _combatManager.RobertCanEscape.AddListener(ActivarEscape);

        ISInventoryManager.instance.OnItemEquiped.AddListener(UpdateRobertGear);

        if (_combatManager.robert != null)
        {
            _combatManager.robert.registerGuiController(this);
            robertHealthBar.maxValue = _combatManager.robert.characterStats.getStat(CharacterStats.StatTotalHitpoints).OriginalValue;
            robertHealthBar.value = _combatManager.robert.characterStats.Hitpoints;
            healthLabel.text = robertHealthBar.value + " / " + robertHealthBar.maxValue;
            // TODO: Implement Shield Bar.
        }

        //precisionTargeting.onValueChanged.AddListener(togglePrecisionTargeting);

        _combatManager.OnRobertTurnEnded.AddListener(robertTurnEnded);
        // TODO: Temporal Checking for Flee Action
        turnsForFlee = 2; //Random.Range(2, 5);

        _gameManager = GameManager.Instance;

        GameObject level = GameObject.FindWithTag("Level");
        _gameManager.spawnedLevel = level;
        _originalLevelSize = level.GetComponent<RectTransform>();
        //_originalLevelSize = _gameManager.spawnedLevel.GetComponent<RectTransform>();

        if (focusSlider != null)
            focusSlider.onValueChanged.AddListener(SetFocus);

        if (shootModeSlider != null)
            shootModeSlider.onValueChanged.AddListener(setShootMode);

        if (_combatManager.robert.getShootMode() == BaseCharacter.ShootMode.SINGLE)
            shootModeSlider.gameObject.SetActive(false);

        _weaponReloadAlert = transform.Find("BottomBar/Weapon/Reload Weapon Warning").gameObject;

        InvokeRepeating("delayedUpdate", 0.0f, 1.0f);

        if (_combatManager.NumEnemigos() == 0) {
            _combatManager.PauseCombat();
        }

    }

    private void robertTurnEnded()
    {
        turnsPassed++;
        if (turnsPassed >= turnsForFlee)
            if (fleeButton != null)
                ActivarEscape(); //PPP
                //fleeButton.interactable = true;
        //fleeDisable = false;
    }

    public void gotoMainMenu()
    {
        ScenesLoader.Instance.LoadScene("mainMenu");
    }

    public void fleeAction() {
        if (!CombatManager.Instance.combatEnded) {

            CombatManager.Instance.Escape();
            //CombatManager.Instance.nextLevelNavigations.CombatEnded();
            

        }
    }

    public void fleeActionVieja()
    {
        // TODO: Implementar
        // Titilar en Verde para cuando el combate Termina (Seria el nuevo Leave)
        // Implementar Accion de Flee del combate para cuando el combate sigue en curso.
        // Destruir Singletons correspondientes cuando se sale de la escena de Gameplay.
        
        if ((CombatManager.Instance.combatEnded) && (CombatManager.Instance.robert.isAlive))
        {
            CombatManager.Instance.NpcFXController.PlayEventSound("LeaveMap");

            GameManager.Instance.ingameBgmController.stopCombatWonAudio();
            /* TOMAS : DESDE ACA COMENTE PARA HACER LO DEL ESCAPE DEL COMBATE HASTA EL ASTERISCO SIGUIENTE
            if (SceneManager.GetActiveScene().name  == "Chapter-1Level-1")
                ScenesLoader.Instance.LoadScene("Robert's Ranch Exterior");
            else
                ScenesLoader.Instance.LoadScene("Chapter 1 Level Selection");
             */

            if (SceneManager.GetActiveScene().name  == "01_Robert_Ranch_Inside")
            {
                if (NoEscape != null)
                {
                    NoEscape.SetActive(true);
                }
            }
            else
            {
                ScenesLoader.Instance.LoadScene(LevelHistory.Instance._levels.level_list[LevelHistory.Instance._levels.level_list.Count -2] , true, true, true);
            }


            /*
            if (SceneManager.GetActiveScene().name  == "Robert's Ranch Exterior")
                ScenesLoader.Instance.LoadScene("levelSelection");
            else
                ScenesLoader.Instance.LoadScene("Robert's Ranch Exterior");
            */
        }
        else
        {
            if (SceneManager.GetActiveScene().name  == "01_Robert_Ranch_Inside")
            {
                if (NoEscape != null)
                {
                    NoEscape.SetActive(true);
                }
            }
            else
            {
                Debug.Log("FLEE");
                ScenesLoader.Instance.LoadScene(LevelHistory.Instance._levels.level_list[LevelHistory.Instance._levels.level_list.Count - 2], true, true, true);
            }
        }
    }

    private bool statsBtnAlertRunning = false;
    private bool hideBrnAlertRunning = false;
    private void delayedUpdate()
    {
        if (CombatManager.Instance.robert != null)
        {
            if (CombatManager.Instance.robert.characterStats.availablePoints > 0)
            {
                if (!statsBtnAlertRunning)
                {
                    InvokeRepeating("StatsBtnAlert", 0.0f, .33f);
                    statsBtnAlertRunning = true;
                }
            }
            else
            {
                if (statsBtnAlertRunning)
                {
                    CancelInvoke("StatsBtnAlert");
                    statsBtnAlertRunning = false;
                }
            }

            if (CombatManager.Instance.robert.isHidden)
            {
                if (!hideBrnAlertRunning)
                {
                    InvokeRepeating("HideButtonAlert", 0.0f, 0.33f);
                    hideBrnAlertRunning = true;
                }
            }
            else
            {
                if (hideBrnAlertRunning)
                {
                    CancelInvoke("HideButtonAlert");
                    hideBrnAlertRunning = false;
                }
            }
        }
        else
        {
            if (statsBtnAlertRunning)
            {
                CancelInvoke("StatsBtnAlert");
                statsBtnAlertRunning = false;
            }
        }
    }

    private void togglePrecisionTargeting(bool isOn)
    {
        _combatManager.precisionTargeting = isOn;
    }
    public void statsPanelOpened()
    {
        CancelInvoke("StatsBtnAlert");
    }

    private void fleeButtonAlert()
    {
        if (!fleeIconHighligthed)
        {
            fleeIconHighligthed = true;
            fleeButton.image.color = Color.green;///new Color(10,255,10,255);
        }
        else
        {
            fleeIconHighligthed = false;
            fleeButton.image.color = new Color(191, 177, 156, 255);
        }
    }

    private void StatsBtnAlert()
    {
        if (!statIconHighligthed)
        {
            statIconHighligthed = true;
            statsIcon.color = Color.green;///new Color(10,255,10,255);
        }
        else
        {
            statIconHighligthed = false;
            statsIcon.color = new Color(191, 177, 156, 255);
        }
    }
    /*
    private void HideButtonAlert()
    {
        if (!hideIconHighligthed)
        {
            hideIconHighligthed = true;
            hideIcon.color = Color.green;///new Color(10,255,10,255);
        }
        else
        {
            hideIconHighligthed = false;
            hideIcon.color = new Color(191, 177, 156, 255);
        }
    }
    */
    private bool _waitingSoundEvent = false;

    private void soundFinalizedPlaying()
    {
        EventManager.StopListening("SoundFinalizedPlaying", soundFinalizedPlaying);
        _waitingSoundEvent = false;
        PauseCombat();
    }

    public void PauseCombat( )
    {
  

            if (_waitingSoundEvent) return;

            if (CombatManager.Instance.soundFxPlaying)
            {
                _waitingSoundEvent = true;
                EventManager.StartListening("SoundFinalizedPlaying", soundFinalizedPlaying);

                if (CombatManager.Instance.pausedForSound)
                    CombatManager.Instance.Logger.SubmitText("<color=#DDD063FF>Pausing Combat (\n) (⌐■_■)–︻╦╤─ –  (╥﹏╥) </color>");
                else
                    CombatManager.Instance.Logger.SubmitText("<color=#DDD063FF>Resuming Combat (\n) ┌∩┐(◣_◢)┌∩┐</color>");
                return;
            }

            if (_audioSource == null)
                _audioSource = this.gameObject.GetComponent<AudioSource>();

            if (_combatManager.pausedCombat) //PPP le agregué esta para que el botón sólo actúe cuando hay un combate.
            {
                if ((_audioSource != null) && (UnPauseAudioClip != null))
                    _audioSource.PlayOneShot(UnPauseAudioClip);

                _combatManager.ResumeCombat();
                //TogglePauseButton();
                //_combatManager.Logger.SubmitText("<color=#DDD063FF>Combat Resumed (\n) (⌐■_■)–︻╦╤─ –  (╥﹏╥)</color>");
            }
            else
            {
                if ((_audioSource != null) && (PauseAudioClip != null))
                    _audioSource.PlayOneShot(PauseAudioClip);

                _combatManager.PauseCombat();
                //_combatManager.Logger.SubmitText("<color=#DDD063FF>Combat Paused (\n) ┌∩┐(◣_◢)┌∩┐</color>");
            }
        }


    public void ZoomInLevel()
    {
        _gameManager.spawnedLevel.GetComponent<LevelProperties>().LevelZoomIn();
        //((RectTransform)_gameManager.spawnedLevel.transform.Find("Level Transform").transform).sizeDelta = new Vector2(2048, 1536);
        //_gameManager.spawnedLevel.GetComponent<RectTransform>().sizeDelta = new Vector2(_originalLevelSize.sizeDelta.x, _originalLevelSize.sizeDelta.y);
    }

    public void ZoomOutLevel()
    {
        _gameManager.spawnedLevel.GetComponent<LevelProperties>().LevelZoomOut();
        //((RectTransform)_gameManager.spawnedLevel.transform.Find("Level Transform").transform).sizeDelta = new Vector2(2048, 1536);
        //_gameManager.spawnedLevel.GetComponent<RectTransform>().sizeDelta = new Vector2(_originalLevelSize.sizeDelta.x * 0.5f, _originalLevelSize.sizeDelta.y * 0.5f);
    }

    public void showLevelInfo()
    {
        
        GameManager.Instance.levelManager.levelInfo.SetActive(showLevelInfoToggle.isOn);

        if (showLevelInfoToggle.isOn)
            GameManager.Instance.ingameBgmController.changeSnapshot("MapInfo");
        else
            GameManager.Instance.ingameBgmController.changeSnapshot("Normal");
    }
    private void SetFocus(float value)
    {
        CombatManager.Instance.robert.setFocus((int)value);
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    int selectedWeaponSlot = 0;
    int newSelectedWeaponSlot = 0;

    private void UpdateRobertGear(ISObject item)
    {
        if (item is ISWeapon)
        {
            ProcessSelectedWeaponImage();
            /*
            ISWeapon weapon = item as ISWeapon;

            if (weapon.EquipSlot == CombatManager.Instance.robert.selectedEquipmentSlot)
            {
                _selectedWeapon.sprite = weapon.GraphicRepresentation;
            }
            */
        }
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public void GetNextWeaponSlot()
    {

        //newSelectedWeaponSlot = (newSelectedWeaponSlot + 1) % newSlotsOrder.Count;///PPP
        selectedWeaponSlot = (selectedWeaponSlot + 1) % slotsOrder.Count;


       
        CombatManager.Instance.robert.setSelectedWeaponSlot(slotsOrder[selectedWeaponSlot]); 
        CombatManager.Instance.robert.getSelectedWeapon();

        ProcessSelectedWeaponImage();

        ConsoleAnnounceSelectedWeaponSlot();
        //Debug.Log("Selected Weapon EquipSlot: " + slotsOrder[selectedWeaponSlot].ToString());
        //nextWayPoint = (nextWayPoint + 1) % enemy.wayPoints.Length;
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    private void ConsoleAnnounceSelectedWeaponSlot()
    {
        string text = "";
        if (slotsOrder[selectedWeaponSlot] == EquipmentSlot.MAIN_HAND)
            text = "Main Hand";
        else if (slotsOrder[selectedWeaponSlot] == EquipmentSlot.OFF_HAND)
            text = "Off Hand";
        else if (slotsOrder[selectedWeaponSlot] == EquipmentSlot.THROWABLE)
            text = "Throwable";

        CombatManager.Instance.Logger.SubmitText("<color=#DDD063FF>Selected " + text + " Weapon Slot</color>");
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public void GetPrevWeaponSlot()
    {
        if (selectedWeaponSlot == 0)
            selectedWeaponSlot = 3;

        selectedWeaponSlot = (selectedWeaponSlot - 1) % slotsOrder.Count;

        CombatManager.Instance.robert.setSelectedWeaponSlot(slotsOrder[selectedWeaponSlot]);
        CombatManager.Instance.robert.getSelectedWeapon();

        ProcessSelectedWeaponImage();

        ConsoleAnnounceSelectedWeaponSlot();
        //Debug.Log("Selected Weapon EquipSlot: " + slotsOrder[selectedWeaponSlot].ToString());
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public void ProcessSelectedWeaponImage()
    {
        ISWeapon weapon = CombatManager.Instance.robert.selectedWeapon;
        if (weapon != null)
        {
            _unavailableLabel.SetActive(false);
            _selectedWeapon.gameObject.SetActive(true);
            _selectedWeapon.sprite = weapon.Casio;
            /*
            if (casioRepresentation[weapon.ObjectID] != null)
            {
                _selectedWeapon.sprite = casioRepresentation[weapon.ObjectID];
            }
            else
            {
                _selectedWeapon.sprite = weapon.GraphicRepresentation;
            }*/

            processWeaponBullets(weapon);
        }
        else
        {
            _selectedWeapon.gameObject.SetActive(false);
            _weaponAmmoLabel.gameObject.SetActive(false);
            shootModeSlider.gameObject.SetActive(false);
            _unavailableLabel.SetActive(true);
        }
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public void reloadWeaponClicked()
    {
        ISObject bullets = ISInventoryManager.instance.inventory.findBulletsByCaliber(CombatManager.Instance.robert.selectedWeapon.Caliber);
        if (bullets != null)
            CombatManager.Instance.robert.reloadAction(bullets);
        else
            CombatManager.Instance.Logger.SubmitText("Not Enough Bullets for This Weapon");

    }
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public void processWeaponBullets(ISWeapon weapon)
    {
        if (weapon.ShootsInRound <= 0)
        {
            if (!_weaponReloadAlert.activeInHierarchy)
                _weaponReloadAlert.SetActive(true);
        }
        else
        {
            if (_weaponReloadAlert.activeInHierarchy)
                _weaponReloadAlert.SetActive(false);
        }

        if (weapon.MaxFireRate == 1)
            setShootModeSlider(BaseCharacter.ShootMode.SINGLE);
        else if (weapon.MaxFireRate == 3)
            setShootModeSlider(BaseCharacter.ShootMode.AUTO);
        else if (weapon.MaxFireRate == 6)
            setShootModeSlider(BaseCharacter.ShootMode.BURST);

        if (!_weaponAmmoLabel.gameObject.activeInHierarchy)
            _weaponAmmoLabel.gameObject.SetActive(true);

        _weaponAmmoLabel.text = "Ammo x " + weapon.ShootsInRound;
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    private void setShootMode(float value)
    {
        CombatManager.Instance.robert.setShootMode((int)value);
    }

    public void setShootModeSlider(BaseCharacter.ShootMode shootMode)
    {
        if (shootMode == BaseCharacter.ShootMode.SINGLE)
        {
            shootModeSlider.gameObject.SetActive(false);
        }
        else if (shootMode == BaseCharacter.ShootMode.BURST)
        {
            shootModeSlider.minValue = 1;
            shootModeSlider.maxValue = 3;
            autoBurstLabel.text = "Auto";
            shootModeSlider.gameObject.SetActive(true);
        }
        else if (shootMode == BaseCharacter.ShootMode.AUTO)
        {
            shootModeSlider.minValue = 1;
            shootModeSlider.maxValue = 2;
            autoBurstLabel.text = "Burst";
            shootModeSlider.gameObject.SetActive(true);
        }
    }

    public void setEquippedWeapon(BaseCharacter.DamageType damageType, Sprite sprite)
    {

        ProcessSelectedWeaponImage();
        /*
        if (damageType == BaseCharacter.DamageType.MELEE)
        {
            meleeWeaponField.sprite = sprite;
            meleeWeaponField.color = Color.white;
        }
        else if (damageType == BaseCharacter.DamageType.RANGED)
        {
            rangedWeaponField.sprite = sprite;
            rangedWeaponField.color = Color.white;
        }
        else if (damageType == BaseCharacter.DamageType.THROWING)
        {
            throwingWeaponField.sprite = sprite;
            throwingWeaponField.color = Color.white;
        }
        */
    }

    public void switchToNextWeapon()
    {
        if (_currentSelectedType == BaseCharacter.DamageType.RANGED)
            _currentSelectedType = BaseCharacter.DamageType.MELEE;
        else if (_currentSelectedType == BaseCharacter.DamageType.RANGED)
            _currentSelectedType = BaseCharacter.DamageType.MELEE;
        else if (_currentSelectedType == BaseCharacter.DamageType.RANGED)
            _currentSelectedType = BaseCharacter.DamageType.MELEE;
    }

    public void switchToPrevWeapon()
    {
        if (_currentSelectedType == BaseCharacter.DamageType.RANGED)
            _currentSelectedType = BaseCharacter.DamageType.MELEE;
        else if (_currentSelectedType == BaseCharacter.DamageType.RANGED)
            _currentSelectedType = BaseCharacter.DamageType.MELEE;
        else if (_currentSelectedType == BaseCharacter.DamageType.RANGED)
            _currentSelectedType = BaseCharacter.DamageType.MELEE;
    }


    public void ToggleInventory() {
        camara.GetComponent<ISInventoryManager>().ToggleInventory();//PPP
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------


    public void RestartGame()
    {
        GameManager.Instance.ingameBgmController.stopCombatWonAudio();

        DestroyImmediate(CombatManager.Instance.gameObject);
        DestroyImmediate(GameManager.Instance.gameObject);

        Application.LoadLevel(SceneManager.GetActiveScene().name );
    }

    /*
    public void setPauseBtnTint(bool restore = false)
    {
        if (restore)
            PauseButtonImage.color = Color.white;
        else
            PauseButtonImage.color = new Color(0.62f, 0.62f, 0.62f, 1.0f);
    }
    */
    public void BackToMenues()
    {
        GameManager.Instance.ingameBgmController.stopCombatWonAudio();

        DestroyImmediate(CombatManager.Instance.gameObject);
        DestroyImmediate(GameManager.Instance.gameObject);

        ScenesLoader.Instance.LoadScene("mainMenu");
    }

    public void ShowGameOver()
    {
        robertAnim.SetBool("IsDead", true);//PPP
        StartCoroutine(EsperarAnimacion("Caida")); //PPP Esta corrutina sirve para esperar a que se vea que Robert muere.

    }


    public void GUIFadeToBlackEnded()
    {
        Debug.Log("Gui Faded To Black");
    }

    public void GUIFadeToAlphaEnded()
    {
        Debug.Log("Gui Faded To Alpha");
    }


    //PPP Una corutina para que banque un segundo antes de mostrar el cartel de gameover.
    private IEnumerator EsperarAnimacion (string nombreAnimacion)
    {
        yield return new WaitForSeconds(2);
        gameOverMenu.SetActive(true); 
    }

}
