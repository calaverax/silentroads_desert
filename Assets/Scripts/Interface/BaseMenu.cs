﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class BaseMenu : MonoBehaviour
{
    //---------------------------------------------------------
    // Extended Menu Animation Systems
    public bool overrideAnimator = true;
    public bool animatedOpen;
    public AnimationClip openAnimation;
    public bool animatedIdle;
    public AnimationClip idleAnimation;
    public bool animatedClose;
    public AnimationClip closeAnimation;
    protected AnimatorOverrideController _overideController;
    //---------------------------------------------------------
    protected Animator _animator;
    protected bool _menuOpen = false;
    protected UnityEvent _closeCallback = new UnityEvent();
    protected UnityEvent _openCallback = new UnityEvent();

    public bool menuOpen { get { return _menuOpen; } }
    public UnityEvent openCallback { get { return _openCallback; } }
    public UnityEvent closeCallback { get { return _closeCallback; } }


    public virtual void ActivateMenuExtras()
    {

    }
    public virtual void DeActivateMenuExtras()
    {

    }

    protected virtual void CloseCallback()
    {
        _closeCallback.Invoke();
        _menuOpen = false;
        this.gameObject.SetActive(false);
    }

    protected virtual void OpenCallback()
    {
        _openCallback.Invoke();
    }

    public void ShowMenu()
    {
        this.gameObject.SetActive(true);

        _menuOpen = true;

        if (_animator == null)
            _animator = GetComponent<Animator>();

        _animator.SetTrigger("OpenMenu");
    }

    public void CloseMenu()
    {
        if (_animator == null)
            _animator = GetComponent<Animator>();

        _animator.SetTrigger("CloseMenu");
    }

	// Use this for initialization
	void Awake ()
    {
        RectTransform rect = GetComponent<RectTransform>();
        rect.offsetMin = rect.offsetMax = new Vector2(0, 0);

        _animator = gameObject.GetComponent<Animator>();

        // Code Setting for use a standar Animator.
        if (overrideAnimator)
        {
            _overideController = new AnimatorOverrideController();
            _overideController.runtimeAnimatorController = _animator.runtimeAnimatorController;

            if (animatedOpen)
                _overideController["MenuOpenBaseClip"] = openAnimation;
            if (animatedIdle)
                _overideController["MenuIdleBaseClip"] = idleAnimation;
            if (animatedClose)
                _overideController["MenuCloseBaseClip"] = closeAnimation;

            _animator.runtimeAnimatorController = _overideController;    
        }
	}
}
