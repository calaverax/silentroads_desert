﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LoadingScreenController : MonoBehaviour {

    #region LoadingTipsList

    private List<string> LoadingTips = new List<string>
    {
        "Loading Tip",
        "Loading Tip",
        "Loading Tip",
        "Loading Tip",
        "Loading Tip",
        "Loading Tip"
    };

    #endregion
    [SerializeField] private Image backgroundField;
    [SerializeField] private Text loadingTipField;

    private AsyncOperation loadScene;
    private string SceneName = "";

    void Start ()
    {
        configureLoadingScreen();
        SceneName = ScenesLoader.Instance.sceneName;

#if UNITY_EDITOR
        loadSceneSync();
#else
        StartCoroutine(loadSceneAsync());
#endif

    }

	/*
	void Update ()
    {
        if (loadScene == null) return;
        if ((loadScene.progress >= 1) || (loadScene.isDone))
        {
            loadScene.allowSceneActivation = true;
            StopCoroutine(loadSceneAsync());
            DestroyImmediate(this.gameObject);
        }
	}
    */

    void configureLoadingScreen()
    {
        // Load Background From Resourcers.
        int RandomBG = Random.Range(1, 5);
        string spriteName = "LoadingBG" + RandomBG;

        Sprite sprite = Resources.Load<Sprite>("Backgrounds/"+ spriteName + "");

        if (sprite != null)
            if (backgroundField != null)
                backgroundField.sprite = sprite;

        // Configure Loading Tip.
        if (loadingTipField != null)
        {
            string tip = LoadingTips[Random.Range(0, LoadingTips.Count)];
            if (tip != "Loading Tip")
                loadingTipField.text = tip;
        }
    }

    IEnumerator loadSceneAsync()
    {
        loadScene = Application.LoadLevelAsync(SceneName);
        loadScene.allowSceneActivation = true;
        yield return loadScene;
    }

    void loadSceneSync()
    {
        Application.LoadLevel(SceneName);
    }

}
