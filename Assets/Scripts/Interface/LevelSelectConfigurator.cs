﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelSelectConfigurator : MonoBehaviour {

    public GameObject warningPopup;

    public Image backGroundField;
    public AudioClip bgmClip;
    public Animator canvasAnimator;

    public Sprite[] chapterBackgrounds;
    public GameObject[] chapterButtons;

    //public ButtonsConectionManager buttonsConectionManager;
    public StartGamePopup startGamePopup;
    private AsyncOperation loadScene;


    // Use this for initialization
    void Start()
    {
        /*
        if (bgmClip != null)
        {
            MenuBGMcontroller.Instance.playBGMNow(bgmClip);
            MenuBGMcontroller.Instance.FadeAudioIn();
        }
        */
        backGroundField.sprite = chapterBackgrounds[0];
        if (PlayerPrefs.GetInt("LastSelectedLevel", -1) == -1)
        {
            PlayerPrefs.SetInt("LastSelectedLevel", 0);
            PlayerPrefs.Save();
        }

        //buttonsConectionManager.setButtonsContainer(chapterButtons[0]);
    }

    void processLevelTravel(int level)
    {
        int lastLevel = PlayerPrefs.GetInt("LasPlayedLevel", 1);

        int robertWater = PlayerPrefs.GetInt(CharacterStats.Statwater, 5);
        string toConsume = "";

        if (robertWater <= 0)
        {
            if (lastLevel < level)
            {
                if ((level - lastLevel) >= 4)
                {
                    warningPopup.SetActive(true);
                    return;
                }
                toConsume = "Health_" + (level - lastLevel);
            }
        }
        else
        {
            toConsume = "Water_" + 5 * (level - lastLevel);
        }

        PlayerPrefs.SetString("ConsumeRobertStat", toConsume);

        PlayerPrefs.SetInt("LasPlayedLevel", level);
        PlayerPrefs.Save();
    }

    public void selectLevel(int level)
    {
        processLevelTravel(level);

        if (warningPopup.activeInHierarchy)
            return;

        GameManager.Instance.selectedLevel = level;

        //startGamePopup.showPopup(startGame, null);

        Invoke("startGame", 3f);
        Invoke("triggerAlphaTransition", 0.5f);
        //startGame();
    }

    private void triggerAlphaTransition()
    {
        canvasAnimator.SetTrigger("TransitionOut");
    }

    public void startGame()
    {
        Time.timeScale = 1.0f;

        MenuBGMcontroller.Instance.stopAudio();

        GameManager.Instance.setSelectedLevel();

        ScenesLoader.Instance.LoadScene(GameManager.Instance.selectedLevelId, false, false);

        /*
#if UNITY_EDITOR
        ScenesLoader.Instance.LoadScene(GameManager.Instance.selectedLevelId, false, false);
        //Application.LoadLevel("Robert's Ranch");
#else
        StartCoroutine(loadGameplay());
#endif
*/
    }

    /*
    IEnumerator loadGameplay()
    {
        loadScene = Application.LoadLevelAsync(GameManager.Instance.selectedLevelId);
        loadScene.allowSceneActivation = true;
        yield return loadScene;
    }
    */

    public void goBack()
    {
#if UNITY_EDITOR
        ScenesLoader.Instance.LoadScene("chapterSelection", false, false);
        //Application.LoadLevel("chapterSelection");
#else
        StartCoroutine(loadChapterSelect());
#endif
    }

    IEnumerator loadChapterSelect()
    {
        loadScene = Application.LoadLevelAsync("chapterSelection");
        loadScene.allowSceneActivation = true;
        yield return loadScene;
    }


}
