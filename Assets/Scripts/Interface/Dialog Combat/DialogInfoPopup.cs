﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialogInfoPopup : MonoBehaviour {

    public Text Category, RequiredInt, AffectedEnemies;

    public void ShowPopup(DialogDefinition dialog)
    {
        //description.text = dialog.robertSaying;
        Category.text = "Category: " + dialog.category.ToString();
        RequiredInt.text = "Required INT: " + dialog.intLevel;

        AffectedEnemies.text = "Affected Enemies:";
        foreach (BaseEnemy.EnemyType type in dialog.affectedEnemies)
            AffectedEnemies.text += " " + type.ToString();

        this.gameObject.SetActive(true);
    }

}
