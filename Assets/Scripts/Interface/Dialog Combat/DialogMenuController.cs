﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class DialogMenuController : MonoBehaviour {

    public GameObject categorySelector;
    public GameObject dialogsParent;
    public RectTransform dialogsContainer;
    public DialogInfoPopup dialogInfoPopup;
    public ScrollRect scrollRect;
    public GameObject dialogButtonPrefab;
    public DialogDefinition.Category menuCategory;

    public DialogsDatabase dialogsDatabase;

    private List<GameObject> instanciatedButtons = new List<GameObject>();

    private List<GameObject> influenceButtons = new List<GameObject>();
    private List<GameObject> intimidateButtons = new List<GameObject>();

    Transform _body;
    bool _needToMove = false;
    void OnEnable()
    {
        if (menuCategory == DialogDefinition.Category.Influence)
            processContent(dialogsDatabase.influenceDialog);
        else if (menuCategory == DialogDefinition.Category.Intimidate)
            processContent(dialogsDatabase.intimitadionDialog);

        // Get Header And Body:
        _body = this.gameObject.transform.GetChild(1);

        // Position Them at Talk Button & scale them
        //Vector3 screenPos = Camera.main.WorldToScreenPoint(DialogCombatManager.Instance.talkingTarget.gameObject.transform.position);
        Vector3 screenPos = new Vector3(Screen.width * 0.5f, Screen.height * 0.5f);

        _body.position = screenPos;
        _body.localScale = new Vector3(0.1f, 0.1f, 1);

        _needToMove = true;
    }

    public void ConstructMenu(string category)
    {
        if (category == "Influence")
            menuCategory = DialogDefinition.Category.Influence;
        else if (category == "Intimidate")
            menuCategory = DialogDefinition.Category.Intimidate;

        if (menuCategory == DialogDefinition.Category.Influence)
          //  populateButtons(dialogsDatabase.influenceDialog);
        processContent(dialogsDatabase.influenceDialog);
        else if (menuCategory == DialogDefinition.Category.Intimidate)
        //    populateButtons(dialogsDatabase.intimitadionDialog);
        processContent(dialogsDatabase.intimitadionDialog);
    }
    /*
    private void populateButtons(List<DialogDefinition> dialogsList)
    {
        List<GameObject> buttons;

        if (menuCategory == DialogDefinition.Category.Influence)
            buttons = influenceButtons;
        else
            buttons = intimidateButtons;

        if (buttons.Count > 0)
        {
            for (int i = 0; i < buttons.Count; i++)
            {
                //if (CombatManager.Instance.robert.characterStats.Intelligence >= dialogsList[i].intLevel)
                    buttons[i].gameObject.SetActive(true);
            }

            if ((menuCategory == DialogDefinition.Category.Influence) && (intimidateButtons.Count > 0))
            {
                for (int i = 0; i < intimidateButtons.Count; i++)
                    intimidateButtons[i].gameObject.SetActive(false);
            }
            else if ((menuCategory == DialogDefinition.Category.Intimidate) && (influenceButtons.Count > 0))
            {
                for (int i = 0; i < influenceButtons.Count; i++)
                    influenceButtons[i].gameObject.SetActive(false);
            }

            dialogsContainer.sizeDelta = new Vector2(dialogsContainer.sizeDelta.x, dialogsList.Count * 90);
            scrollRect.verticalNormalizedPosition = 0f;
            Canvas.ForceUpdateCanvases();
            return;
        }

        GameObject button;

        for (int i = 0; i < dialogsList.Count; i++)
        {
            button = Instantiate(dialogButtonPrefab);
            button.transform.localScale = new Vector3(1, 1, 1);
            button.transform.SetParent(dialogsContainer.gameObject.transform);

            Button btn = button.GetComponent<Button>();
            DialogCombatButton script = button.GetComponent<DialogCombatButton>();

            script.Configure(dialogsList[i], dialogInfoPopup, this.gameObject);
            script.moreInfoBtn.onClick.AddListener(script.MoreInfoClicked);

            btn.onClick.AddListener(script.OnClick);

            if (menuCategory == DialogDefinition.Category.Influence)
                influenceButtons.Add(button);
            else
                intimidateButtons.Add(button);
        }
        //scrollContainer.sizeDelta = new Vector2(scrollContainer.sizeDelta.x, lootSlots.Count * 82.5f);

        if (menuCategory == DialogDefinition.Category.Influence)
        {
            if (intimidateButtons.Count > 0)
                for (int i = 0; i < intimidateButtons.Count; i++)
                    intimidateButtons[i].gameObject.SetActive(false);
        }
        else
        {
            if (influenceButtons.Count > 0)
                for (int i = 0; i < influenceButtons.Count; i++)
                    influenceButtons[i].gameObject.SetActive(false);
        }

        dialogsContainer.sizeDelta = new Vector2(dialogsContainer.sizeDelta.x, dialogsList.Count * 90);
        scrollRect.verticalNormalizedPosition = 0;
        Canvas.ForceUpdateCanvases();
        //dialogsParent.SetActive(true);
    }
    */
    private void processContent(List<DialogDefinition> dialogsList)
    {
        if (instanciatedButtons.Count > 0)
            for (int i = 0; i < instanciatedButtons.Count; i++)
                DestroyImmediate(instanciatedButtons[i]);

        instanciatedButtons.Clear();

        GameObject button;

        int count = 0;
        for (int i = 0; i < dialogsList.Count; i++)
        {
            if (dialogsList[i].intLevel <= CombatManager.Instance.robert.characterStats.Intelligence)
            {
                count++;
                button = Instantiate(dialogButtonPrefab);
                button.transform.localScale = new Vector3(1, 1, 1);
                button.transform.SetParent(dialogsContainer.gameObject.transform);

                Button btn = button.GetComponent<Button>();
                DialogCombatButton script = button.GetComponent<DialogCombatButton>();

                script.Configure(dialogsList[i], dialogInfoPopup, this.gameObject);
                script.moreInfoBtn.onClick.AddListener(script.MoreInfoClicked);

                btn.onClick.AddListener(script.OnClick);

                if (CombatManager.Instance.combatUsedDialogs.Contains(dialogsList[i]))
                    btn.interactable = false;

                instanciatedButtons.Add(button);
            }
        }

        dialogsContainer.sizeDelta = new Vector2(dialogsContainer.sizeDelta.x, count * 90);
        scrollRect.verticalNormalizedPosition = 0f;
        Canvas.ForceUpdateCanvases();
        dialogsParent.SetActive(true);
    }

    public void setImageAsGray(Image image)
    {
        image.color = Color.gray;
    }

    public void setImageAsWhite(Image image)
    {
        image.color = Color.white;
    }


    
    void Update()
    {
        if (!_needToMove) return;

        // Header and body Position:
     //   _header.localPosition = Vector3.MoveTowards(_header.localPosition, Vector3.zero, Time.deltaTime * 2500);
        _body.localPosition = Vector3.MoveTowards(_body.localPosition, Vector3.zero, Time.deltaTime * 2500);

        // Header And Body Scale:
       // _header.localScale = Vector3.MoveTowards(_header.localScale, new Vector3(1, 1, 1), Time.deltaTime * 3f);
        _body.localScale = Vector3.MoveTowards(_body.localScale, new Vector3(1, 1, 1), Time.deltaTime * 3.5f);


        //if ((detailsWindow.localScale.x < 1) && (detailsWindow.localScale.y < 1))
        //  detailsWindow.localScale += new Vector3(0.02f, 0.02f,0);

        float distanceSquared = (_body.localPosition - Vector3.zero).sqrMagnitude;

        if ((distanceSquared < 2 * 2) && (_body.localScale.x >= 1f))
        {
            _needToMove = false;
        }
    }


}
