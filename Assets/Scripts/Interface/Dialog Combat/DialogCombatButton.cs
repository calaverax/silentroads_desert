﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialogCombatButton : MonoBehaviour
{
    public Text dialogCaption;
    public Button moreInfoBtn;
    public Image dialogueIcon, levelTexture;

    DialogInfoPopup _dialogInfoPopup;
    DialogDefinition _dialogDefinition;
    GameObject _menuParent;
  

    public void Configure(DialogDefinition dialogDefinition, DialogInfoPopup dialogPopup, GameObject menuParent)
    {
        dialogCaption.text = dialogDefinition.shortText;
        _dialogInfoPopup = dialogPopup;
        _dialogDefinition = dialogDefinition;
        _menuParent = menuParent;

        if (CombatManager.Instance.robert.characterStats.Intelligence < _dialogDefinition.intLevel)
            gameObject.GetComponent<Button>().interactable = false;

        this.transform.localScale = new Vector3(1, 1, 1);

        if (dialogueIcon != null)
            dialogueIcon.overrideSprite = Resources.Load<Sprite>("Atlas/dialogos/Dialog_conversation_icons/Dialogue_" + dialogDefinition.shortText.Split(' ')[0]);
        if (levelTexture != null)
        {
            int level = -1;
            if (dialogDefinition.shortText.IndexOf('1') != -1)
                level = 1;
            else if (dialogDefinition.shortText.IndexOf('2') != -1)
                level = 2;
            if (dialogDefinition.shortText.IndexOf('3') != -1)
                level = 3;

            if (level != -1)
                levelTexture.overrideSprite = Resources.Load<Sprite>("Atlas/dialogos/Dialog_conversation_icons/Dialogue_level_" + level);
        }
    }

    void OnEnable()
    {
        this.transform.localScale = new Vector3(1, 1, 1);
        if (_dialogDefinition != null)
        {
            if (CombatManager.Instance.robert.characterStats.Intelligence >= _dialogDefinition.intLevel)
                gameObject.GetComponent<Button>().interactable = true;
            else
                gameObject.GetComponent<Button>().interactable = false;
        }
    }

    public void OnClick()
    {
        
        CombatManager.Instance.robert.talkAction(_dialogDefinition);
        //DialogCombatManager.Instance.triggerDialog(_dialogDefinition);
        _menuParent.SetActive(false);
        GameObject.FindGameObjectWithTag("CombatMode").GetComponent<TarjetasEnemigos>().freeze = false;
        //if (_dialogInfoPopup != null)
        //    _dialogInfoPopup.ShowPopup(_dialogDefinition);


    }

    public void MoreInfoClicked()
    {
        if (_dialogInfoPopup != null)
            _dialogInfoPopup.ShowPopup(_dialogDefinition);
    }
}
