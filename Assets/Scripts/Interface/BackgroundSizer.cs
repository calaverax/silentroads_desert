﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BackgroundSizer : MonoBehaviour {

    private bool resizing = false;

    void Update()
    {
        if (resizing)
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(5, 5, 1), Time.deltaTime * 0.001f);
    }

    public void stopEffect()
    {
        resizing = false;
    }

    public void startEffect()
    {
        resizing = true;
    }
    public void resetSize()
    {
        transform.localScale = new Vector3(1,1,1);
    }
    /*
	void ResizeSpriteToScreen()
	{
		Image image = GetComponent<Image>();

		if (image == null) return;
		
		transform.localScale = new Vector3(1,1,1);
		
		float width = image.sprite.bounds.size.x;
		float height = image.sprite.bounds.size.y;
		
		float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
		float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

		Vector3 newTransform = new Vector3((worldScreenWidth / width) * 2, (worldScreenHeight / height) *2, 1);

		transform.localScale = newTransform;
	}
    */
}


