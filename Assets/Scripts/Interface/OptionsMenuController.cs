﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionsMenuController : BaseMenu {

    // 
    public Text qualityField;
    public Text soundField;
    public Text bgmField;
    public Text musicStatusLabel;

        
    public MenuBGMcontroller menuBGMController;

    // Use this for initialization
    void Start ()
    {
        //soundField.text = "Sounds: " + (GamePreferences.Instance.savedPreferences.soundFX ? "On" : "Off"); //PPP
        soundField.text = (GamePreferences.Instance.savedPreferences.soundFX ? "On" : "Off");
        //bgmField.text = "Music: " + (GamePreferences.Instance.savedPreferences.backGroundMusic ? "On" : "Off"); //PPP
        bgmField.text = (GamePreferences.Instance.savedPreferences.backGroundMusic ? "On" : "Off");
        //qualityField.text = "Quality: " + QualitySettings.names[GamePreferences.Instance.savedPreferences.qualityLevel]; //PPP
        qualityField.text = QualitySettings.names[GamePreferences.Instance.savedPreferences.qualityLevel];

        checkLabels();
    }

    public void setQuality()
    {
        if (GamePreferences.Instance.savedPreferences.qualityLevel < QualitySettings.names.Length -1)
            GamePreferences.Instance.savedPreferences.qualityLevel++;
        else
            GamePreferences.Instance.savedPreferences.qualityLevel = 0;

        QualitySettings.SetQualityLevel(GamePreferences.Instance.savedPreferences.qualityLevel, true);
        qualityField.text = QualitySettings.names[GamePreferences.Instance.savedPreferences.qualityLevel];
        //qualityField.text = "Quality: " + QualitySettings.names[GamePreferences.Instance.savedPreferences.qualityLevel];
    }

    void Awake()
    {
        checkLabels();
    }

    void OnEnable()
    {
        checkLabels();
    }

    private void checkLabels()
    { 
        if (musicStatusLabel != null)
        {
            if (PlayerPrefs.GetInt("BGM-Muted") == 0)
            {
                //musicStatusLabel.text = "Music: On"; //PPP
                musicStatusLabel.text = "On";
            }
            else
            {
                //musicStatusLabel.text = "Music: Off"; //PPP
                musicStatusLabel.text = "Off";
            }
            /*
            if (menuBGMController.source.mute)
                musicStatusLabel.text = "Music: Off";
            else
                musicStatusLabel.text = "Music: On";
                */
        }
    }
    protected override void OpenCallback()
    {
        base.OpenCallback();

        //menuBGMController

        
    }


    public void TurnMusic()
    {
        menuBGMController.source.mute = !menuBGMController.source.mute;

        if (menuBGMController.source.mute)
            //musicStatusLabel.text = "Music: Off"; //PPP
            musicStatusLabel.text = "Off";
        else
            //musicStatusLabel.text = "Music: On"; //PPP
            musicStatusLabel.text = "On";

        if (menuBGMController.source.mute)
            PlayerPrefs.SetInt("BGM-Muted", 1);
        else
            PlayerPrefs.SetInt("BGM-Muted", 0);

        PlayerPrefs.Save();
    }

}
