﻿using UnityEngine;
using System.Collections;

public class ChapterSelectController : MonoBehaviour {

    AsyncOperation loadScene;
    public AudioClip bgm;
    public GameObject clouds;

    [SerializeField]
    private Animator _animator;

    void Start()
    {
        if (_animator == null)
            _animator = GetComponent<Animator>();

        if (bgm != null)
        {
            MenuBGMcontroller.Instance.playBGMNow(bgm);
            MenuBGMcontroller.Instance.FadeAudioIn();
        }
    }

    public void selectChapter(int chapter)
    {
        GameManager.Instance.selectedChapter = chapter;

        MenuBGMcontroller.Instance.FadeAudioOut();

        if (chapter == 1)
            _animator.SetTrigger("SelectDesert");

        Invoke("delayedLoad", 0.75f);
    }

    public void hideClouds()
    {
        clouds.SetActive(false);
    }

    private void delayedLoad()
    {
        //#if UNITY_EDITOR
        //Application.LoadLevel("Chapter 1 Level Selection");
        //Application.LoadLevel("Chapter-1Level-1");
        //ScenesLoader.Instance.LoadScene("Chapter-1Level-1", false, true, true);
        //ScenesLoader.Instance.LoadScene("01_Robert_Ranch_Inside", false, true, true);
        
            ScenesLoader.Instance.LoadScene("01b_Robert_Ranch_Outside", false, true, true);
        //#else
        //      StartCoroutine(loadLevelSelection());
        //#endif

    }

    /*
    IEnumerator loadLevelSelection()
    {
        //loadScene = Application.LoadLevelAsync("Chapter 1 Level Selection");
        loadScene = Application.LoadLevelAsync("Chapter-1Level-1");
        loadScene.allowSceneActivation = true;
        yield return loadScene;
    }
    */
    public void GoBack()
    {
        MenuBGMcontroller.Instance.stopAudio();
#if UNITY_EDITOR
        Application.LoadLevel("mainMenu");
#else
        StartCoroutine(loadMainMenu());
#endif
    }

    IEnumerator loadMainMenu()
    {
        loadScene = Application.LoadLevelAsync("mainMenu");
        loadScene.allowSceneActivation = true;
        yield return loadScene;
    }
}
