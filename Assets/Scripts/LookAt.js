﻿#pragma strict


@script ExecuteInEditMode()
// This complete script can be attached to a camera to make it 
// continuously point at another object.
// The target variable shows up as a property in the inspector. 
// Drag another object onto it to make the camera look at it.

     var lookingAt : Transform;
     transform.rotation = Quaternion.LookRotation(transform.position - lookingAt.position);
    //transform.LookAt(lookingAt.position);