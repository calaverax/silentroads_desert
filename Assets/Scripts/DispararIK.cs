﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Wasabi.InventorySystem;
using Wasabi.InventorySystem.Inventory;


[RequireComponent(typeof(Animator))]

public class DispararIK : MonoBehaviour
{

    //VARIABLES
    Action _callBack; //Como ves esto es una acción, acá va a meter una función
    public Action callBack { get { return _callBack; } set { _callBack = value; } } //Acá va a almacenar una función por así decirlo


    protected Animator animator;
    public Animator robertAnimator { get { return animator; } }

    //public Animator animator { get; set; }
    public Transform objetivo = null; //El transform del enemigo al que queremos atacar.
    public GameObject obj; //Este es el objeto objetivo, para poder mover el transform (capaz convenga borrar el anterior).
			
			//por si hay que ajustar un poco la rotacion de la mano que se sostiene el arma.
			public float mano_RotX =0f;
			public float mano_RotY =0f;
			public float mano_RotZ =0f;
			
			public GameObject targetPicker;//para cortar el movimiento del personaje mientras corren las animaciones.

            public Transform cabeza;

			public GameObject[] armas;//El array que tiene las armas.
            public Dictionary <string, GameObject> Armas = new Dictionary <string, GameObject>();
            public Dictionary<string, GameObject> Cascos = new Dictionary<string, GameObject>();
			
            //public int numArma = 0;//para seleccionar el arma dentro del array.
            public string nombArma;
            public string nombCasco;
            public GameObject particulas;//El null que tiene todas las particulas como child.
			public GameObject[] partArmas;//El array que tiene las particulas de las armas de fuego.
			public float speed = 2f;//La velocidad con que sale disparado el casquillo (que tiene un rigidbody).
			public Rigidbody casquillo;//El casquillo en si.
            public rastroBala rastro;//El script de la estela que deja el disparo.
            public string enemyObjetive;
            private CombatManager combatManager;

			
			//variables de la funcion arrojar
			public float firingAngle = 10.0f;//en que angulo queremos que tire el objeto
			public float gravity = 20f; //para simular la gravedad sin un rigidbody.
			public GameObject explosion;
			public Transform mano;
            public Material trailMat;
			
			//booleanas para chequar
			public bool bajandoElBrazo = false;
			private bool bang = false;
			public bool ikDisparo = false;
			private bool mirando;
			private bool apuntando =true;
			
			private float lerp = 0; //para hacer lerps.
			private Quaternion miQuaternion; //para almacenar temporalmente quaterniones calculados.
			private UnityEngine.AI.NavMeshAgent nav; //el nav mesh del pesonaje.
			private Transform myTransform; //conviene almacenarla aca que citarla con la funcion pre hecha que es mas lenta.
			private Transform Projectile; //El proyectil que vas a tirar.
            private ISInventoryManager inventoryManager; 
            




    [SerializeField]        
    private GameObject perdigon;

            //Links a otros scripts
            private UnityStandardAssets.Characters.ThirdPerson.AICharacterControl scriptMovimiento;
            private Robert robertScript; //link al script Robert.cs
            //public BaseEnemy baseEnemyScript; //El link al BaseEnemy.cs 
			
			void Start () 

			{
				
                
                animator = GetComponent<Animator>();
				nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();
				targetPicker = GameObject.FindGameObjectWithTag ("targetPicker");
				scriptMovimiento = GetComponent <UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>();
				//armas = GameObject.FindGameObjectsWithTag("Arma");
				//foreach (GameObject arma in armas) {arma.SetActive(false);}
				partArmas = GameObject.FindGameObjectsWithTag("ArmaParticulas");
				foreach (GameObject partArma in partArmas) {partArma.SetActive(false);}
				//armas[numArma].SetActive(true);	
				myTransform = transform;
                rastro = GameObject.Find("rastro_Robert").GetComponent<rastroBala>();
                trailMat = rastro.GetComponent<Renderer>().material;
                robertScript = GameObject.FindGameObjectWithTag("Player").transform.GetChild(0).GetComponent<Robert>();
                //baseEnemyScript = GameObject.FindGameObjectWithTag("Enemy").GetComponent<BaseEnemy>();
                obj = objetivo.gameObject;
                gameObject.layer = 13;
                perdigon = Resources.Load("Perdigon") as GameObject;
                casquillo = perdigon.GetComponent<Rigidbody>();
                combatManager= CombatManager.Instance;
                inventoryManager = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<ISInventoryManager>();
                if (inventoryManager == null) { Debug.Log("INVENTORY MANAGER NULLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL"); }
                inventoryManager.CargarModelos();
                cabeza = transform.GetChild(1).transform.GetChild(5).transform.GetChild(0).transform.GetChild(2).GetChild(0).GetChild(0).GetChild(1).GetChild(0);
				
			}

	
			
			
			
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			
			
			//El Update
			void Update (){

                if (nav.remainingDistance > 0f) {combatManager.OnRobertMoving.Invoke(); combatManager.robertIsMoving = true; }
                else { 
                    if (combatManager.robertIsMoving) { 
                        combatManager.robertIsMoving = false; 
                        combatManager.OnRobertMoved.Invoke(); 
                    } 
                }
                //enemyObjetive = robertScript._enemyTarget;

				//Cambio de arma
				if(Input.GetKeyDown (KeyCode.LeftArrow)){ cambiarArma(-1);}
				if(Input.GetKeyDown (KeyCode.RightArrow)){cambiarArma(+1);}
				
				

				//BANG
				if (bang)
				{

                    Casquillo(); 
					bajandoElBrazo = true;
					bang = false;
			        Vector3 puntaFusil = Armas[nombArma].GetComponent<Renderer>().bounds.max;
			        particulas.transform.position = puntaFusil;
			        foreach (GameObject partArma in partArmas) {partArma.SetActive(true);}
                    rastro.pos1.position = puntaFusil;
                    rastro.activar = true;
				}				
			}
				
			public void OnAnimatorIK(){
		
				//if(objetivo != null) {
				
						if (ikDisparo){
								
								targetPicker.SetActive(false);
								scriptMovimiento.target = null;
								//float dot = Vector3.Dot(transform.forward, (objetivo.position - transform.position + new Vector3 (0.8f,0,0)).normalized);									
								
								//mano
								animator.SetIKPositionWeight(AvatarIKGoal.RightHand,lerp);
								animator.SetIKRotationWeight(AvatarIKGoal.RightHand,lerp);
								animator.SetIKPosition(AvatarIKGoal.RightHand,objetivo.position);
								animator.SetIKRotation (AvatarIKGoal.RightHand, Quaternion.LookRotation(objetivo.position -transform.position)* Quaternion.Euler(mano_RotX,mano_RotY,mano_RotZ));
								
								//Cabeza
								animator.SetLookAtWeight(lerp *2);
								animator.SetLookAtPosition(objetivo.position);
								
								//Cuerpo
								Quaternion hacia = Quaternion.LookRotation( objetivo.transform.position - transform.position + new Vector3 (0.8f,0,0));
								Quaternion miQuaternion = Quaternion.Slerp( transform.rotation, hacia, Time.deltaTime*2);
								transform.rotation= Quaternion.Euler(new Vector3(0f, miQuaternion.eulerAngles.y, 0f));
			
								
								//apuntando
								if (apuntando && lerp <1){
									lerp += 0.02f; 
									if (lerp> 0.9f){
										apuntando = false; 
										bang = true;
                                        robertScript.bang = true;//Importante
                                        robertScript.ChequearObstaculos(); //Para ver si el tiro pega en algo.
                                        
										nav.velocity = new Vector3 (0,0,0);
										nav.ResetPath();
										partArmas[1].SetActive(false);
                                        
										StartCoroutine("unRatoDespues");
                                        
									}
								}
                                //Debug.Log("Llegó!-------------------------------------------------------");
							
								//bajando el brazo.
								if (bajandoElBrazo)
								{							
									
									lerp -= 0.02f;
									if (lerp <= 0) 
									{ikDisparo = false; 
									lerp =0; apuntando = true;   

									//targetPicker.SetActive(true);
									bajandoElBrazo = false;
                                    rastro.activar = false;
									//foreach (GameObject partArma in partArmas) {partArma.SetActive(false);} //esta linea va en la corutina "Un rato después"
                                    //Debug.Log("Robert terminó de bajar el brazo>-------------------------------------------------");
									}
								}
							}
						}
				//}
			
			///*
	IEnumerator unRatoDespues()
	{
		yield return new WaitForSeconds(1);
		//foreach (GameObject partArma in partArmas) {partArma.SetActive(false);} 
        //rastro.activar = false;
        //if (_callBack != null) //Si callback tiene algo adentro, activalo.
           // _callBack(); //Luego fijate que la llama como si fuera una función; en realidad está llamando la función que metió, por ej. si la llama Robert esta es (afterSoot())
	}//*/
		
		public void cambiarArma (int num){
			/* PPP tempralmente deshabilitada
             * 
            armas [numArma].SetActive (false);
			numArma += num;
			if (numArma <0){numArma = armas.Length-1;} 
			if (numArma> armas.Length-1){numArma = 0;}
			armas[numArma].SetActive (true);
             */


		}

        public void NextWeapon()
    {
        cambiarArma(+1);
    }

    public void PrevWeapon()
    {
        cambiarArma(-1);
    }
		
		void Casquillo () {
            GameObject perdigonClone = Instantiate (perdigon, Armas[nombArma].transform.position, Armas[nombArma].transform.rotation);
            Rigidbody casquilloClone = perdigonClone.GetComponent<Rigidbody>();
			casquilloClone.velocity = Armas[nombArma].transform.right * speed;
			//Destroy (casquilloClone.gameObject,5f);
		}


    //Esta función chequea que el arma activa sea la correcta:
        public void ActivarModeloArma(string key) {
            

            if (!Armas[key].activeSelf) {


                foreach (KeyValuePair<string, GameObject> arma in Armas)
                {
                    arma.Value.SetActive(false);
                }              
                Armas[key].SetActive(true);
                nombArma = Armas[key].name;
                
            }

        
        }
        public void DesactivarModeloArma() {
            foreach (KeyValuePair<string, GameObject> arma in Armas)
            {
                arma.Value.SetActive(false);
            }              
        }

    ////////////////////////////////////////////////////////////////
        //Esta función chequea que el casco activo sea el correcto:
        public void ActivarModeloCasco(string key)
        {

            Debug.Log("Entró en ActivarModeloCasco(), el string es " + key);
            Debug.Log("Cascos.Count = " + Cascos.Count);
            if (!Cascos[key].activeSelf)
            {
                Debug.Log("!Cascos[key].activeSelf");

                foreach (KeyValuePair<string, GameObject> casco in Cascos)
                {
                    casco.Value.SetActive(false);
                }
                Cascos[key].SetActive(true);
                nombCasco = Cascos[key].name; ///OJO
                Debug.Log("nombre del casco " + key);
            }
            Debug.Log("FIN DE LA FUNCIÓN DEL DISPARAR IK" + key);

        }
        public void DesactivarModeloCasco()
        {
            foreach (KeyValuePair<string, GameObject> casco in Cascos)
            {
                casco.Value.SetActive(false);
            }
        }
/////////////////////////////////////////////////////////////////

					//Disparo
				public void Ataque()
					{
						//if (tipoDeArma == "cuerpoACuerpo")
						if (Armas[nombArma].name.Contains("0"))
						{CuerpoACuerpo();}
						
						//if (tipoDeArma == "armaDeFuego")
						else if (Armas[nombArma].name.Contains("1"))
						{ikDisparo = !ikDisparo;}
						
						//if (tipoDeArma == "proyectil")
						else if (Armas[nombArma].name.Contains("2"))
						{ StartCoroutine (Arrojar());}
					}





	IEnumerator Arrojar(){
		
		if (animator.GetBool ("Crouch")){animator.SetBool("Crouch", false); yield return new WaitForSeconds(0.25f);}
			
            //Quitar al jugador el control del personaje.
			targetPicker.SetActive(false);
			scriptMovimiento.target = null;
			nav.ResetPath();

			//que gire el cuerpo hacia el objetivo
		float elapse_time = 0;
		while (elapse_time < 1.3f)
		{
			Quaternion hacia = Quaternion.LookRotation( objetivo.transform.position- transform.position);
			Quaternion miQuaternion = Quaternion.Slerp( transform.rotation, hacia, Time.deltaTime*2);
			transform.rotation= Quaternion.Euler(new Vector3(0f, miQuaternion.eulerAngles.y, 0f));
			elapse_time +=Time.deltaTime;
		}
		yield return null;
			

			//aca le puse esto para correr la animacion y cortarla cuando haya terminado.
			animator.SetBool("Granada", true);

			// Aca por si queres poner un delay antes de que tire el objeto(que hay que ajustarlo en base a la duracion de la animacion)
			yield return new WaitForSeconds(1.2f);
			animator.SetBool("Granada", false);

            //Trail que deja el objeto lanzado:
            TrailRenderer tr = Armas[nombArma].AddComponent<TrailRenderer>();
            tr.material = trailMat;
            tr.time = 250f;
            tr.widthMultiplier = 0.1f;

            //Un gradiente hecho por código para el trail
            Gradient gradient = new Gradient();
            gradient.SetKeys(
                new GradientColorKey[] { new GradientColorKey(Color.white, 0.0f), new GradientColorKey(Color.red, 1.0f) },
                new GradientAlphaKey[] { new GradientAlphaKey(0.09f, 0.0f), new GradientAlphaKey(0.0f, 1.0f) }
                );
            tr.colorGradient = gradient;

			
            //Projectile va a ser la granada, la molotov o lo que sea, con esto hacemos que vuelva a la mano de Robert tras cada explosión
			Projectile = Armas[nombArma].transform;
			Projectile.parent = null;
			Projectile.position = myTransform.position + new Vector3(0.7f, 3.2f, -2.4f);// Para poner el proyectil en la mano del personaje + el offset para posicionarlo en la mano.

            
            
        //Con esta parte tiramos el proyectil
            
            // Hay que calcular la distancia entre el personaje y el objetivo
            Debug.DrawLine(Projectile.position, objetivo.position, Color.red, 3f);
            float target_Distance = Vector3.Distance(Projectile.position, objetivo.position);//Está bien calculada

            //Primero me fijo a qué velocidad quiero que vaya el objeto (Vz):
            float Vz = 50f;

            //Con esto calculo el tiempo que va a tardar en recorrer esa distancia (t= d/v).
            float tiempo = target_Distance / Vz;

            //Para obtener la Vy, multiplico el tiempo * la gravedad y lo dividio por 2, porque (v= g^t/2)
            float Vy = ((tiempo / 2) * 9.8f) / 2;

            // Este es para rotar el proyectil de forma que mire en direccion al target (no te olvides que no tiene rigidbody)
            Projectile.rotation = Quaternion.LookRotation(objetivo.position - Projectile.position);


            //Finalmente el while loop para hacer que recorra la curvatura.

            elapse_time = 0;
            //while (Vector3.Distance(Projectile.position, objetivo.position) > 5f)
            while (elapse_time < (tiempo))
            {

                //Cada línea tiene un complemento del vector3 (x,y,z)
                Projectile.Translate
                    (0, // eje x
                   0.2f * Mathf.Sin(((elapse_time + tiempo * 2 *0.25f) * 2 * Mathf.PI) / (tiempo * 2)),//eje y
                    //explicación: la fórmula es "altura * sin (2*pi*tiempoTranscurrido) / período".
                    //el problema es que nosotros queremos usar sólo 1/4 de la sinusoide y queremos que tenga 1/8 de offset 
                    //(lo que se traduce en una suma constante al tiempo transcurrido).
                    Vz * Time.deltaTime); //eje z
                
                //Este es el que incrementa en base al tiempo
                elapse_time += Time.deltaTime;

                yield return null;
            }
            Destroy(Armas[nombArma].GetComponent<TrailRenderer>());//Una vez que llegó destruí el trail.

		//Esto instancia la explosión en el lugar:
        GameObject explosionClone = (GameObject) Instantiate(explosion, Projectile.transform.position, Projectile.transform.rotation);
		Destroy (explosionClone.gameObject,5f);
			Projectile.parent = mano;
			Projectile.transform.position = mano.position + new Vector3(-0.02f,-0.3f,0);
            robertScript.bang = true;
			targetPicker.SetActive(true);
            
		
		}  
		
		
		

		void CuerpoACuerpo(){
			Debug.Log("este codigo no esta hecho");
            robertScript.bang = true;
		}
		


	}
	