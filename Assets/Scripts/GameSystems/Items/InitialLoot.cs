﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialLoot : MonoBehaviour {

    void Start()
    {
        LootPersistenceManager.Instance.LoadLoot();
        LootPersistenceManager.Instance._lvlLootPersistence.LootLevelItems();
    }
}
