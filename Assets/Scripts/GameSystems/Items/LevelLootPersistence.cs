﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Wasabi.InventorySystem;
using Wasabi.InventorySystem.Inventory;
using UnityEngine.SceneManagement;

[System.Serializable]
public class LevelLootPersistence
{
    public List<LevelLoot> levelsAndTheirLoot = new List<LevelLoot>();
    public Object[] whatever;
    private bool CheckExistLevel(string levelName)
    {
        bool result = levelsAndTheirLoot.Exists(x => x.GetLevelID() == levelName);
        return result;
    }

    private LevelLoot FindLevelLoot(string levelName)
    {
        LevelLoot nivel = levelsAndTheirLoot.Find(x => x.GetLevelID() == levelName);
        return nivel;
    }

    public void AddItem(string itType, int itID)
    {
        bool _exist = CheckExistLevel(SceneManager.GetActiveScene().name );
        if (!_exist)
        {
            Debug.Log("No existe Loot del nivel : " + SceneManager.GetActiveScene().name );
            LevelLoot newLvl = new LevelLoot(SceneManager.GetActiveScene().name );
            newLvl.itemsPersisted.Add("weapons", new List<int> { });
            newLvl.itemsPersisted.Add("bullets", new List<int> { });
            newLvl.itemsPersisted.Add("consumables", new List<int> { });
            levelsAndTheirLoot.Add(newLvl);
        }
        //levelsAndTheirLoot.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).itemsPersisted.FirstOrDefault(t => t.Key == itType).Value.Add(itID);
        string name = SceneManager.GetActiveScene().name ;
        levelsAndTheirLoot.Find(x => x.GetLevelID() == name).itemsPersisted.FirstOrDefault(t => t.Key == itType).Value.Add(itID);
        LootPersistenceManager.Instance.SaveLoot();

    }

    public void RemoveItem(ISObject obj)
    {

        bool _exist = CheckExistLevel(SceneManager.GetActiveScene().name );
        if (_exist)
        {
            Debug.Log("Borrar item " + obj.ObjectID);
            string tipo;
            if (obj is ISBullet)
            {
                tipo = "bullets";
                //Debug.Log("ES bully");
            }else { 
            if (obj is ISConsumable)
            {
                tipo = "consumables";
                //Debug.Log("ES consu");
            }
                else
                {
                    tipo = "weapons";
                    //Debug.Log("ES weapons");
                }
            }
            levelsAndTheirLoot.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).itemsPersisted.FirstOrDefault(t => t.Key == tipo).Value.Remove(obj.ObjectID);
            LootPersistenceManager.Instance.SaveLoot();
        }

    }

    public void RemoveItemByName(string tipo, int id)
    {

        bool _exist = CheckExistLevel(SceneManager.GetActiveScene().name );
        if (_exist)
        {

            levelsAndTheirLoot.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).itemsPersisted.FirstOrDefault(t => t.Key == tipo).Value.Remove(id);
            LootPersistenceManager.Instance.SaveLoot();
        }
    }

    public void LootLevelItems()
    {
        bool _exist = CheckExistLevel(SceneManager.GetActiveScene().name );
        if (_exist)
        {
            int numero = levelsAndTheirLoot.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).itemsPersisted.FirstOrDefault(t => t.Key == "weapons").Value.Count();
            for (int i = 0; i < levelsAndTheirLoot.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).itemsPersisted.FirstOrDefault(t => t.Key == "weapons").Value.Count(); i++)
            {
                //Debug.Log("ARMA : " + (i + 1));
                //Debug.Log("ARMA : " + ISInventoryManager.instance.weaponsDatabase.GetElementByID(levelsAndTheirLoot.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).itemsPersisted.FirstOrDefault(t => t.Key == "weapons").Value[i]));
                ISObject obj = ISInventoryManager.instance.weaponsDatabase.GetElementByID(levelsAndTheirLoot.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).itemsPersisted.FirstOrDefault(t => t.Key == "weapons").Value[i]);
                ISInventoryManager.instance.lootWindow.addItemL(obj);
            }
            for (int i = 0; i < levelsAndTheirLoot.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).itemsPersisted.FirstOrDefault(t => t.Key == "consumables").Value.Count(); i++)
            {
                //Debug.Log("CONSUMIBLE : " + (i + 1));
                //Debug.Log("CONSUMIBLE : " + ISInventoryManager.instance.consumablesDatabase.GetElementByID(levelsAndTheirLoot.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).itemsPersisted.FirstOrDefault(t => t.Key == "consumables").Value[i]));
                ISObject obj = ISInventoryManager.instance.consumablesDatabase.GetElementByID(levelsAndTheirLoot.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).itemsPersisted.FirstOrDefault(t => t.Key == "consumables").Value[i]);
                ISInventoryManager.instance.lootWindow.addItemL(obj);
            }
            for (int i = 0; i < levelsAndTheirLoot.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).itemsPersisted.FirstOrDefault(t => t.Key == "bullets").Value.Count(); i++)
            {
                //Debug.Log("BALA : " + (i + 1));
                //Debug.Log("BALA : " + ISInventoryManager.instance.bulletsDatabase.GetElementByID(levelsAndTheirLoot.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).itemsPersisted.FirstOrDefault(t => t.Key == "bullets").Value[i]));
                ISObject obj = ISInventoryManager.instance.bulletsDatabase.GetElementByID(levelsAndTheirLoot.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).itemsPersisted.FirstOrDefault(t => t.Key == "bullets").Value[i]);
                ISInventoryManager.instance.lootWindow.addItemL(obj);
            }
        }
        else
        {
            Debug.Log("No hay loot para este nivel : " + SceneManager.GetActiveScene().name );
        }
    }

    public void ScavengeLoot(string levelID)
    {
        /*
        LevelLoot level = FindLevelLoot(levelID);
        foreach (KeyValuePair<string, List<int>> item in level.itemsPersisted) {
            if (Random.Range(0, 1) < 0.5f) { 
            //Pasar item al marauder
                Debug.Log("falta decidir que hacer con el item " + item.Key +" que fue seleccionado para ser robado por los scavengers^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
            }
        }*/


    }


    public void Display()
    {
        foreach (var a in levelsAndTheirLoot)
        {
            Debug.Log("El nivel ID es : " + a.levelId);
            foreach (var b in a.itemsPersisted)
            {
                Debug.Log("Tipo de item : " + b.Key);
                foreach (var c in b.Value)
                {
                    Debug.Log("Lista de items : " + c.ToString());
                }
            }
        }
    }
    [System.Serializable]
    public struct LevelLoot
    {
        public string levelId;
        public Dictionary<string, List<int>> itemsPersisted;

        public LevelLoot(string lvlID)
        {
            levelId = lvlID;
            itemsPersisted = new Dictionary<string, List<int>>();
        }

        public string GetLevelID()
        {
            return this.levelId;
        }
    }

}
