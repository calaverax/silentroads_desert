﻿using UnityEngine;
using System.Collections;

public class BaseWeapon : BaseItem {

    public enum DamageType
    {
        Blunt,
        Slash,
        Piercing
    }

    public enum WeaponSlot
    {
        OneHanded,
        MainHanded,
        OffHanded,
        TwoHanded
    }

    public DamageType damageType;
    public WeaponSlot weaponSlot;

    public float migth;
    public float maxEffectiveDistance;

    public int minDamage, maxDamage;
    public float criticalHitChance;
    public float criticalHitMultiplier;
   

}
