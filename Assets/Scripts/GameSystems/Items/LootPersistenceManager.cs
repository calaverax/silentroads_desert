﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootPersistenceManager : Singleton<LootPersistenceManager>
{

    public LevelLootPersistence _lvlLootPersistence;

    public void SaveLoot()
    {
        Serializer.SaveSerializableObjectToFile <LevelLootPersistence>(_lvlLootPersistence, Application.persistentDataPath + "/"  + "loot.persistence");
    }

    public void LoadLoot()
    {
        _lvlLootPersistence = Serializer.LoadSerializableObjectFromFile<LevelLootPersistence>(Application.persistentDataPath + "/" + "loot.persistence");
        if (_lvlLootPersistence == null)
        {
            _lvlLootPersistence = new LevelLootPersistence();
        }else
        {
            _lvlLootPersistence.Display();
            _lvlLootPersistence.LootLevelItems();
        }
        
    }
    public void ScavengeLoot( string levelID ) {
        _lvlLootPersistence.ScavengeLoot(levelID);
    
    }

    public void EmptyLoot()
    {
        _lvlLootPersistence = new LevelLootPersistence();
        SaveLoot();
    }

}
