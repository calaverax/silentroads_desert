﻿using UnityEngine;
using System.Collections;

public class RangedWeapon : BaseWeapon
{

    public bool doesAreaDamage;
    public int splashDamageMin, splashDamageMax;

    public float accuracy;

    public float bulletsPerRound;
    public int fireRate;

    public float reloadSpeed;

}
