﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GFInventory {

    public int Gold = 0;
    public List<BaseItem> Backpack = new List<BaseItem>();

    public Dictionary<EquipSlots, BaseItem> EquipedItems = new Dictionary<EquipSlots, BaseItem>();
    public Dictionary<EquipedWeapon, BaseItem> EquipedWeapons = new Dictionary<EquipedWeapon, BaseItem>();

    public enum EquipedWeapon
    {
        LEFT_HAND = 0,
        RIGHT_HAND = 1,
        BOTH_HANDED = 2,
        SPECIAL = 3
    }

    public enum EquipSlots
    {
        HEAD = 0,
        TORSO = 1,
        LEGS = 2,
        ARMS = 3,
        SPECIAL = 4
    }

    // Use this for initialization
    void Start () {
	
	}

}
