﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class BaseItem : MonoBehaviour {
    
    // ---------------------------------------------------------
    // Properties
    public string itemID;
    public string itemName;
    public string itemDescription;
    public bool canDrop = true;
    public bool canPickUp = true;
    public bool notifyItemDroped = true;
    public bool notifyItemPickedUp = true;
    // ---------------------------------------------------------


    // ---------------------------------------------------------
    // Events
    public class OnPickedUpEvent : UnityEvent<BaseItem> { }
    public class OnDropedEvent : UnityEvent<BaseItem> { }

    public OnPickedUpEvent OnItemPickUp;
    public OnDropedEvent OnItemDrop;
    // ---------------------------------------------------------

    void Awake ()
    {
        OnItemDrop = new OnDropedEvent();
        OnItemDrop = new OnDropedEvent();
	}

    public virtual void DropItem()
    {
        if (!canDrop)
            return;

        // --------- Item Drop Logic Here

        if (notifyItemDroped)
            OnItemDrop.Invoke(this);
    }

    public virtual void PickUpItem()
    {
        if (!canPickUp)
            return;

        // --------- Item PickUp Logic Here

        if (notifyItemPickedUp)
            OnItemPickUp.Invoke(this);
    }
	
}
