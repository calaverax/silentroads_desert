﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
//using Devdog.InventorySystem;
using Wasabi.InventorySystem;
using Wasabi.InventorySystem.Inventory;

public class BaseCharacter : MonoBehaviour {

   // public Sprite crazySprt = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/Resources/Atlas/Status Icons/Enemy Status.psd", typeof(Sprite));
    //public Texture2D texture;
    //public Sprite[] sprites;
    public const int AttackCost = 3;
    public const int ReloadCost = 2;
    public const int TalkCost = 2;
    public const int ConsumeCost = 1;
    public const int WalkCost = 1;
    private Image subtarjeta; //PPP
    //  HIDE - FLEE ?

    protected int _actionPointsLeft;


    //PPP Estos son objetos con toda la info que se necesita tener para saber cómo se produce el ataque.
    //Quién ataca, quién es la víctima, cuál es el tipo de bala, etc.
    public struct AttackInfo
    {
        public BaseCharacter attacker, victim; //Atacante y víctima
        public BodyObjetive bodyObjective; //A qué parte del cuerpo le tiró.
        public DamageType damageType; //Tipo de daño (Ranged, melee, throwing).
        public ShootMode shootMode; //El modo del disparo (single (1), burst (3), auto(6))
        public bool scopeWeapon, sneakAttack;
        public int attackAccuracy; //La precisión que va a tener
        public BulletType bulletType; //Almacena el tipo de bala (normal, con radiación, con veneno)
        public ISWeapon weapon; //Almacena directamente cuál es el arma que está usando

        public AttackInfo(BaseCharacter attacker, BaseCharacter victim, BodyObjetive bodyObjective, DamageType damageType, ShootMode shootMode, bool scopeWeapon, bool sneakAttack, int attackAccuracy, BulletType bulletType = BulletType.Normal, ISWeapon weapon = null)
        {
            this.attacker = attacker;
            this.victim = victim;
            this.bodyObjective = bodyObjective;
            this.damageType = damageType;
            this.shootMode = shootMode;
            this.scopeWeapon = scopeWeapon;
            this.sneakAttack = sneakAttack;
            this.attackAccuracy = attackAccuracy;
            this.bulletType = bulletType;
            this.weapon = weapon;
        }


    }
    //muy parecido al anterior, pasa la info del resultado del ataque.
    public struct AttackResult
    {
        public BaseCharacter attacker, victim;
        public BodyObjetive bodyObjective;
        public int damage;
        public bool isCriticalHit;
        public float hitChance;
        public BulletType bulletType;

        public AttackResult(BaseCharacter attacker, BaseCharacter victim, BodyObjetive bodyObjective, int damage, bool isCriticalHit, int hitChance, BulletType bulletType = BulletType.Normal)
        {
            this.attacker = attacker;
            this.victim = victim;
            this.bodyObjective = bodyObjective;
            this.damage = damage;
            this.isCriticalHit = isCriticalHit;
            this.hitChance = hitChance;
            this.bulletType = bulletType;
        }
    }



    //public GFInventory inventory;// = new Inventory();
    public string characterName;
    public CharacterStats characterStats;
    public float reactionTime = 5.0f;
    public float cooldown = 5.0f;
    public GameObject interactions;
    //public float might;

    protected bool _isAlive = true;

    public bool isAlive { get { return _isAlive; } }

    public enum BulletType
    {
        Normal,
        Poison,
        Radiated
    }

    public enum DamageType
    {
        RANGED,
        MELEE,
        THROWING
    }

    public enum ShootMode
    {
        SINGLE = 1,
        BURST = 3,
        AUTO = 6
    }

    public enum BodyObjetive
    {
        HEAD,
        ARMS,
        TORSO,
        LEGS
    }

    public Dictionary<BodyObjetive, CharacterStat> bodyPartHealth = new Dictionary<BodyObjetive, CharacterStat>();

    public enum AbnormalStates
    {
        Undefined,
        Uncounciouss,
        Panic,
        DamageSelf,
        Traitor,
        Suicide,
        SuddenDeath,
        Poisoned,
        Radiated,
        ParalizedLegs,
        ParalizedArms,
        Surrender,
        Crazy,
        Confusion,
        Bleeding,
        CatastrophicDamageSelf,
        Blinded
    }

    // TODO: Temp Checking on Disables Character
    protected List<AbnormalStates> abnormalStates = new List<AbnormalStates>();
    protected ShootMode _shootMode = ShootMode.SINGLE;
    protected DamageType _damageType = DamageType.RANGED;

    protected bool _isDisabled = false;
    protected bool _cantWalk = false;
    protected bool _isBlinded = false;

    protected bool _isActing = false;
    public bool isActing { get { return _isActing; } }

    public class AbnormalStateEvent : UnityEvent<AbnormalStates> { }
    protected AbnormalStateEvent abnormalStateTriggered;
    protected AbnormalStateEvent abnormalStateEnded;

    // Combat and equipment stuff
    [SerializeField]
    protected ISWeapon _selectedWeapon;
    public ISWeapon selectedWeapon
    {
        get
        {
            if (_selectedWeapon == null)
                getSelectedWeapon();
            return _selectedWeapon;
        }
    }

    //protected LevelNagivationManager _levelNavMgr;

    protected int _attacksPerTurn = 1;
    protected int _attacksDone = 0;
    protected bool _actionPointsEvaluated = false;

    protected void processTurnActionPoints()
    {
        if (!_actionPointsEvaluated)
        {
            _actionPointsLeft = characterStats.Agility;
            _actionPointsEvaluated = true;
        }
    }

    protected void realizedAction(int action)
    {
        _actionPointsLeft -= action;
    }
    protected bool canMakeAction(int action)
    {
        if (_actionPointsLeft - action >= 0)
            return true;

        return false;
    }

    public virtual void FixedTurnStarted()
    {
        processTurnActionPoints();
    }

    public virtual void FixedTurnEnded(bool announce = true)
    {
        Invoke("finalizeTurn", 0.5f);
    }

    void finalizeTurn()
    {
        _actionPointsEvaluated = false;
		CombatManager.Instance.fixedTurnInProgress = false;
        CombatManager.Instance.OnFixedTurnEnded.Invoke();
    }


    void Awake()
    {
        
        //subtarjeta.active = false; //Cuando arranques apagá la subtarjeta.
        abnormalStateTriggered = new AbnormalStateEvent();
        abnormalStateEnded = new AbnormalStateEvent();

    }

    void Start()
    {

        Debug.Log("START!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"); //Demostración de que la función Start ni siquiera está corriendo.
        
        GameObject spawns = GameObject.FindWithTag("LevelSpawnPoints");

        //if (spawns != null)
          //  _levelNavMgr = spawns.GetComponent<LevelNagivationManager>();

        if (interactions == null)
            interactions = transform.Find("Interactions").gameObject;
    
        
        
    }


    public virtual void getSelectedWeapon()
    {
        EquipmentSlot slot = EquipmentSlot.MAIN_HAND;

        if (_damageType == DamageType.RANGED)
            slot = EquipmentSlot.MAIN_HAND;
        else if (_damageType == DamageType.MELEE)
            slot = EquipmentSlot.MELEE;
        else if (_damageType == DamageType.THROWING)
            slot = EquipmentSlot.THROWABLE;

        _selectedWeapon = ISInventoryManager.instance.getEquipedItem(slot) as ISWeapon;
    }

    public virtual void init()
    {
    }

    public virtual void DoAction()
    {
        if (!_isAlive)
            return;

        // TODO: Hardcoded "Callback" -> Allways Call Action Finished after Doing Character Action.
        Invoke("ActionFinished", 0.5f);
        CombatManager.Instance.Logger.SubmitText("" + characterName + " Turn Started");
    }

    public virtual void ActionFinished()
    {
        if (!_isAlive)
            return;

        CombatManager.Instance.ReinitActor(this);
        CombatManager.Instance.Logger.SubmitText("" + characterName + " Turn Ended");
    }

    public virtual void OnClicked()
    {
        if (interactions.activeInHierarchy)
        {
            CancelInvoke("hideInteractions");
            interactions.SetActive(false);
        }
        else
        {
            interactions.SetActive(true);
            Invoke("hideInteractions", 3.0f);
        }
    }

    protected void hideInteractions()
    {
        CancelInvoke("hideInteractions");
        interactions.SetActive(false);
    }

    public virtual void recieveDamage(AttackResult attackInfo)
    {
        //Camera.main.GetComponent<CameraShake>().DoShake(0.6f, 2.5f);
        

        int armor = getArmor(attackInfo.bodyObjective);

        if (armor >= attackInfo.damage)
        {
            if ((characterName == "Robert") || (characterName == "Sparky"))
                CombatManager.Instance.Logger.SubmitText("<b>" + characterName + "</b> get hit in the " + attackInfo.bodyObjective.ToString().ToLower() + " but it's armor mitigates the damage");
            else
                CombatManager.Instance.Logger.SubmitText(characterName + " get hit in the " + attackInfo.bodyObjective.ToString().ToLower() + " but it's armor mitigates the damage");
            return;
        }
        else
        {
            if ((characterName == "Robert") || (characterName == "Sparky"))
                CombatManager.Instance.Logger.SubmitText("<b>" + characterName + "</b> gets hit in the " + attackInfo.bodyObjective.ToString().ToLower() + " for <color=red>" + attackInfo.damage + " damage </color> ");
            else
                CombatManager.Instance.Logger.SubmitText(characterName + " gets hit in the " + attackInfo.bodyObjective.ToString().ToLower() + " for <color=red>" + attackInfo.damage + " damage </color> ");

            if (attackInfo.isCriticalHit)
            {
                CombatManager.Instance.Logger.SubmitText("<color=red>CRITICAL HIT </color> ");
                //Camera.main.GetComponent<CameraShake>().DoShake(0.6f,2.5f);
            }

            // Lower the Body Part Health Pool And check for Normal Pool.
            if (bodyPartHealth.ContainsKey(attackInfo.bodyObjective))
            {
                int damage = 0;
                float dmgModifier = 1.0f;

                if (attackInfo.victim is Robert)
                {
                    if (attackInfo.bodyObjective == BodyObjetive.ARMS)
                        dmgModifier = 0.8f;
                    else if (attackInfo.bodyObjective == BodyObjetive.LEGS)
                        dmgModifier = 0.8f;
                    else if (attackInfo.bodyObjective == BodyObjetive.TORSO)
                        dmgModifier = 1.0f;
                    else if (attackInfo.bodyObjective == BodyObjetive.HEAD)
                        dmgModifier = 1.3f;
                    /*
                    if (attackInfo.bodyObjective == BodyObjetive.ARMS)
                        damage = Mathf.FloorToInt((float)attackInfo.damage * 0.8f);
                    else if (attackInfo.bodyObjective == BodyObjetive.LEGS)
                        damage = Mathf.FloorToInt((float)attackInfo.damage * 0.8f);
                    else if (attackInfo.bodyObjective == BodyObjetive.TORSO)
                        damage = Mathf.FloorToInt((float)attackInfo.damage * 1.0f);
                    else if (attackInfo.bodyObjective == BodyObjetive.HEAD)
                        damage = Mathf.FloorToInt((float)attackInfo.damage * 1.3f);
                        */
                }
                else
                {
                    if (attackInfo.bodyObjective == BodyObjetive.ARMS)
                        dmgModifier = 1.5f;
                    else if (attackInfo.bodyObjective == BodyObjetive.LEGS)
                        dmgModifier = 1.5f;
                    else if (attackInfo.bodyObjective == BodyObjetive.TORSO)
                        dmgModifier = 1.0f;
                    else if (attackInfo.bodyObjective == BodyObjetive.HEAD)
                        dmgModifier = 3;
                    /*
                    if (attackInfo.bodyObjective == BodyObjetive.ARMS)
                        damage = Mathf.FloorToInt((float)attackInfo.damage * 1.5f);
                    else if (attackInfo.bodyObjective == BodyObjetive.LEGS)
                        damage = Mathf.FloorToInt((float)attackInfo.damage * 1.5f);
                    else if (attackInfo.bodyObjective == BodyObjetive.TORSO)
                        damage = Mathf.FloorToInt((float)attackInfo.damage * 1.0f);
                    else if (attackInfo.bodyObjective == BodyObjetive.HEAD)
                        damage = attackInfo.damage * 3;
                        */
                }

                CombatManager.Instance.debugPanel.SubmitText("Raw Damage To Apply: (" + damage + ") - Modified By: (" +dmgModifier + " By targeting " + attackInfo.bodyObjective.ToString()+ ")", "Red");
                damage = Mathf.FloorToInt((float)attackInfo.damage * dmgModifier);

                CombatManager.Instance.debugPanel.SubmitText(characterName + " Recieved Attack From: " + attackInfo.attacker + " - (" + damage + ") Damage", "Red");
                characterStats.Hitpoints -= damage;
                bodyPartHealth[attackInfo.bodyObjective].modifyValue(-damage);
            }
            else
            {
                Debug.LogError("Error, trying to access an invalid body part.");
            }


            if (_isAlive)
            {
                if (characterStats.Hitpoints <= 0)
                    _isAlive = false;
            }

            if (!_isAlive)
            {
                OnCharacterDeath();
                return;
            }

            if (bodyPartHealth[attackInfo.bodyObjective].CurrentValue <= 0)
            {

                if (attackInfo.bodyObjective == BodyObjetive.ARMS)
                    _isDisabled = true;
                else if (attackInfo.bodyObjective == BodyObjetive.HEAD)
                    _isBlinded = true;

                /*
                if (attackInfo.bodyObjective == BodyObjetive.HEAD)
                    _isAlive = false;
                else if (attackInfo.bodyObjective == BodyObjetive.ARMS)
                    _isDisabled = true;
                else if (attackInfo.bodyObjective == BodyObjetive.TORSO)
                    _isAlive = false;
                else if (attackInfo.bodyObjective == BodyObjetive.LEGS)
                    _cantWalk = true;
                */
            }
        }
    }
   
    public virtual void OnCharacterDeath()
    {
        CombatManager.Instance.UnRegister(this);
        CombatManager.Instance.Logger.SubmitText(this.characterName + " dies");

        if ((gameObject != null) && (this.gameObject != null))
            GameObject.Destroy(this.gameObject);
    }

    protected virtual void recieveDamage(int damage)
    {
        characterStats.Hitpoints -= damage;

        if (characterStats.Hitpoints <= 0)
            OnCharacterDeath();
    }

    #region Abnormal States
    //-----------------------------------------------------------------
    public bool checkIfAbnormalStatusIsActive(AbnormalStates state)
    {
        if (abnormalStates.Contains(state))
            return true;

        return false;
    }
    public void removeAbnormalStatus(AbnormalStates state)
    {
        if (abnormalStates.Contains(state))
        {
            abnormalStates.Remove(state);
            abnormalStateEnded.Invoke(state);
        }
    }
    public void triggerAbnormalState(AbnormalStates state, bool shootsEvent = true)
    {
        //PPP LAS SUBTARJETAS
        subtarjeta = GameObject.FindGameObjectWithTag("Subtarjeta").GetComponent<Image>();//PPP Debiera estar en Start pero por alguna razón no anda en este script.
        subtarjeta.sprite = Resources.Load<Sprite>("Sprites/SubTarjeta/" + state.ToString());
        Debug.Log(state.ToString());
        //subtarjeta.material.color = new Color(1, 1, 1, 1);
        subtarjeta.enabled = true;
        
        
        if (state == AbnormalStates.Uncounciouss)
            CombatManager.Instance.NpcFXController.PlayStatesSound("Uncounciouss");
        else if (state == AbnormalStates.Panic)
            CombatManager.Instance.NpcFXController.PlayStatesSound("Panic");
        else if (state == AbnormalStates.DamageSelf)
            CombatManager.Instance.NpcFXController.PlayStatesSound("DamageSelf");
        else if (state == AbnormalStates.Traitor)
            CombatManager.Instance.NpcFXController.PlayStatesSound("Traitor");
        else if (state == AbnormalStates.Suicide)
            CombatManager.Instance.NpcFXController.PlayStatesSound("Suicide");
        else if (state == AbnormalStates.SuddenDeath)
            CombatManager.Instance.NpcFXController.PlayStatesSound("SuddenDeath");
        else if (state == AbnormalStates.Poisoned)
            CombatManager.Instance.NpcFXController.PlayStatesSound("Poisoned");
        else if (state == AbnormalStates.Radiated)
            CombatManager.Instance.NpcFXController.PlayStatesSound("Radiated");
        else if (state == AbnormalStates.Crazy)
            CombatManager.Instance.NpcFXController.PlayStatesSound("Crazy");
        else if (state == AbnormalStates.Confusion)
            CombatManager.Instance.NpcFXController.PlayStatesSound("Confusion");
        else if (state == AbnormalStates.Bleeding)
            CombatManager.Instance.NpcFXController.PlayStatesSound("Bleeding");
        else if (state == AbnormalStates.CatastrophicDamageSelf)
            CombatManager.Instance.NpcFXController.PlayStatesSound("CatastrophicDamageSelf");
        else if (state == AbnormalStates.Blinded)
            CombatManager.Instance.NpcFXController.PlayStatesSound("Blinded");
        else if (state == AbnormalStates.ParalizedArms)
            CombatManager.Instance.NpcFXController.PlayStatesSound("ParalizedArms");
        else if (state == AbnormalStates.ParalizedLegs)
            CombatManager.Instance.NpcFXController.PlayStatesSound("ParalizedLegs");
        else if (state == AbnormalStates.Surrender)
            CombatManager.Instance.NpcFXController.PlayStatesSound("Surrender");


        // TODO: Call Animations for each Abnormal Status Trigger, And Effect each Turn.
        abnormalStates.Add(state);

        if (shootsEvent)
        {
            if (abnormalStateTriggered == null)
                abnormalStateTriggered = new AbnormalStateEvent();

            abnormalStateTriggered.Invoke(state);
        }
    }
    //-----------------------------------------------------------------
    //-----------------------------------------------------------------
    protected void checkUnconcioussTrigger()
    {
        // If below 20% Health
        if (characterStats.Hitpoints <= ((characterStats.TotalHitpoints * 20) / 100))
            if (Random.Range(0.0f, 1.0f) <= 0.5f) // 50% chance of getting unconciouss
                triggerAbnormalState(AbnormalStates.Uncounciouss);//abnormalStates.Add(AbnormalStates.Uncounciouss);
    }
    //-----------------------------------------------------------------
    protected void checkUncounciousAftermath()
    {
        if (checkIfAbnormalStatusIsActive(AbnormalStates.Uncounciouss))
        {
            if (Random.Range(0.0f, 1.0f) <= 0.5f)
            {
                removeAbnormalStatus(AbnormalStates.Uncounciouss);
                CombatManager.Instance.Logger.SubmitText(this.characterName + " woke up");
            }
        }
    }
    //-----------------------------------------------------------------
    protected void checkPanicTrigger()
    {
        if (getMorale() < 5)
            if (Random.Range(0, 101) <= (10 * getMorale()))
                triggerAbnormalState(AbnormalStates.Panic, true);

        // If we still are not in panick, check trigger two
        if (!(checkIfAbnormalStatusIsActive(AbnormalStates.Panic)))
        {
            //Might difference with player 10% chance per 20% difference
            float robertMight = CombatManager.Instance.robert.characterStats.Might;

            float temp = (characterStats.Might * 20) / 100;
            if (robertMight > characterStats.Might)
            {

            }
        }
    }
    //-----------------------------------------------------------------
    protected bool checkDamageSelf()
    {
        if (getAgility() < 5)
        {
            if (Random.Range(0, 101) <= (10 * getAgility()))
            {
                int damage = Random.Range(5, 16);
                recieveDamage(damage);

                CombatManager.Instance.debugPanel.SubmitText("<color=Red>" + characterName + " Damage itself (" + damage + " Damage)</color>");
                return true;
            }
        }
        return false;
    }
    //-----------------------------------------------------------------
    protected void checkTraitorTrigger()
    {
        //< 50 % HP, > 5 intelligence
        if ((characterStats.Hitpoints <= characterStats.TotalHitpoints * 0.5f) && (getIntelligence() > 5))
            triggerAbnormalState(AbnormalStates.Traitor);
    }
    //-----------------------------------------------------------------
    protected void checkSuicideTrigger()
    {
        //> 100 % difference in might with player
        if (CombatManager.Instance.robert.characterStats.Might > characterStats.Might * 2)
            if (Random.Range(0.0f, 1.0f) <= 0.2f)
                triggerAbnormalState(AbnormalStates.Suicide);
    }
    //-----------------------------------------------------------------
    protected void checkSuddenDeathTrigger()
    {
        //Morale < 2, > 80 % might, strength < 4
        if ((getMorale() < 2) && (CombatManager.Instance.robert.characterStats.Might > characterStats.Might * 1.8) && (getStrength() < 4))
            triggerAbnormalState(AbnormalStates.SuddenDeath);
    }
    //-----------------------------------------------------------------
    protected void checkBleedTrigger()
    {
        if (Random.Range(0.0f, 1.0f) <= 0.8f)
            triggerAbnormalState(AbnormalStates.Bleeding);

    }
    //-----------------------------------------------------------------
    protected void doBleedEffect()
    {
        int damage = Random.Range(1, 4);
        recieveDamage(damage);

        CombatManager.Instance.debugPanel.SubmitText("<color=Red>" + characterName + " is hurt by Bleeding ("+damage+" Damage)</color>");
        CombatManager.Instance.Logger.SubmitText("<color=orange>" + this.characterName + " is hurt by bleeding</color>");
    }
    //-----------------------------------------------------------------
    protected void checkPoisonedTrigger()
    {
        if (Random.Range(0.0f, 1.0f) <= 0.75f)
            triggerAbnormalState(AbnormalStates.Poisoned);
    }
    //-----------------------------------------------------------------
    protected void doPoisonEffect()
    {
        int damage = Random.Range(2, 6);
        recieveDamage(damage);

        CombatManager.Instance.debugPanel.SubmitText("<color=Red>" + characterName + " is hurt by Poison (" + damage + " Damage)</color>");
        CombatManager.Instance.Logger.SubmitText("<color=orange>" + this.characterName + " is hurt by poisoning</color>");
    }
    //-----------------------------------------------------------------
    protected void checkRadiatedTrigger()
    {
        if (Random.Range(0.0f, 1.0f) <= 0.75f)
            triggerAbnormalState(AbnormalStates.Radiated);
    }
    //-----------------------------------------------------------------
    protected void doRadiatedEffect()
    {
        int damage = Random.Range(10, 21);
        recieveDamage(damage);

        CombatManager.Instance.debugPanel.SubmitText("<color=Red>" + characterName + " is hurt by Radiation (" + damage + " Damage)</color>");
        CombatManager.Instance.Logger.SubmitText("<color=orange>" + this.characterName + " is hurt by radiation</color>");
    }
    //-----------------------------------------------------------------
    protected void checkParalizedLegsTrigger(bool isProtected = false)
    {
        float Chance = 0.8f;

        if (isProtected)
            Chance = 0.4f;

        if (Random.Range(0.0f, 1.0f) <= Chance)
            triggerAbnormalState(AbnormalStates.ParalizedLegs);
    }
    //-----------------------------------------------------------------
    protected void checkParalizedArmsTrigger(bool isProtected = false)
    {
        float Chance = 0.8f;

        if (isProtected)
            Chance = 0.4f;

        if (Random.Range(0.0f, 1.0f) <= Chance)
            triggerAbnormalState(AbnormalStates.ParalizedArms);
    }
    //-----------------------------------------------------------------
    protected void checkSurrenderTrigger()
    {
        //< 50 % HP, > 5 intelligence
        if ((characterStats.Hitpoints <= characterStats.TotalHitpoints * 0.5f) && (getIntelligence() > 5))
            if (Random.Range(0.0f, 1.0f) <= 0.15f)
                triggerAbnormalState(AbnormalStates.Surrender, true);
    }
    //-----------------------------------------------------------------
    protected void checkCrazyTrigger()
    {
        //< 5 % HP, < 3 intelligence
        if ((characterStats.Hitpoints <= characterStats.TotalHitpoints * 0.5f) && (getIntelligence() < 3))
            if (Random.Range(0.0f, 1.0f) <= 0.15f)
                triggerAbnormalState(AbnormalStates.Crazy);
    }
    //-----------------------------------------------------------------
    protected bool checkCatastrophicDamageSelfTrigger()
    {
        //< 3 Strength, < 3 Intelligence  
        if ((getStrength() < 3) && (getIntelligence() < 3))
        {
            if (Random.Range(0.0f, 1.0f) <= 0.1f)
            {
                //abnormalStateTriggered.Invoke(AbnormalStates.CatastrophicDamageSelf);
                triggerAbnormalState(AbnormalStates.CatastrophicDamageSelf);

                int damage = Random.Range(20, 31);
                recieveDamage(damage);

                CombatManager.Instance.debugPanel.SubmitText("<color=Red>" + characterName + " Catastrophically Damage itself (" + damage + " Damage)</color>");
                return true;
            }
        }
        return false;
    }
    //-----------------------------------------------------------------
    protected bool checkBlindedTrigger()
    {
        //Damage to head
        if (Random.Range(0.0f, 1.0f) <= 0.25f)
        {
            triggerAbnormalState(AbnormalStates.Blinded);
            return true;
        }

        return false;
    }
    //-----------------------------------------------------------------

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------
    #endregion

    #region STATS_GETTERS

    //Estos parecieran valores por default que no está usando, tienen overrides en los respectivos scripts
    // -------------------------------------------------------
    public virtual int getArmor(BodyObjetive bodyPart)
    {
        /*
        if (bodyPart == BodyObjetive.HEAD)
        {
            // Check Armor attached to Slot,
            // Get Armor property
        }
        */

        return 0;
    }
    // -------------------------------------------------------
    public virtual float getRange()
    {
        return 100;
    }
    // -------------------------------------------------------
    public virtual int getIntelligence()
    {
        return characterStats.Intelligence;
    }
    // -------------------------------------------------------
    public virtual int getStrength()
    {
        return characterStats.Strength;
    }
    // -------------------------------------------------------
    public virtual int getAgility()
    {
        return characterStats.Agility;
    }
    // -------------------------------------------------------
    public virtual int getAccuracy(string opt)
    {


        if (opt.ToLower() == "self")
        {
            if (checkIfAbnormalStatusIsActive(AbnormalStates.Blinded))
                return 1;
            else
                return characterStats.Accuracy;
        }
        else if (opt.ToLower() == "weapon")
        {
            return 3;
        }
        else if (opt.ToLower() == "shooting")
        {
            return 5;
        }
        else
            Debug.LogError("Get Accuracy Option field Invalid: { " + opt + " }");

        return 0;
    }
    // -------------------------------------------------------
    public virtual float getEvasion()
    {
        return 1;
    }
    // -------------------------------------------------------
    public virtual float getFocus()
    {
        return 1.2f;
    }
    // -------------------------------------------------------
    public virtual int getMorale()
    {
        return 5;
    }
    // -------------------------------------------------------
    public virtual int getDamage()
    {
        
        return 10;
    }
    // -------------------------------------------------------
    public virtual ShootMode getShootMode()
    {
        return ShootMode.SINGLE;
    }
    // -------------------------------------------------------

    //PPP Esta función no está hecha, pero la pongo vacía como están las otras para tener un estado null.
    public virtual BulletType getBulletType()
    {
        return BulletType.Normal;
    
    }
    #endregion

    #region StatsAffection

    

    #endregion
}
