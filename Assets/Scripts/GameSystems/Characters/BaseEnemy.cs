﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wasabi.InventorySystem;
using Wasabi.InventorySystem.Inventory;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.AI;


//using Devdog.InventorySystem;

public class BaseEnemy : BaseCharacter
{
    /////////////////////////////////////////////////////PPP Líneas puesta por Pablo:
    //public GameObject tarjeta;//almacenando la tarjeta explosiva
    //public GameObject muerto;//almacenando el mesh para la animación de muerte
    public GameObject HUD;
    public Sprite primerPlano; //Animación del disparo
    public Sprite imagen; //Para pasar de forma persistente
    //public int index; //Índice persistencia
    public int cantidadMovimiento = 60;
    public NavMeshAgent navAgent;
    public float distanceToTarget;
    public GameObject quad;
    public Material quadImage;
    private DispararIK dispararIK;
    public SerializableEnemy serializableEnemy;

    
    //variables para almacenar los huesos de Robert y hacer que le peguen en el lugar adecuado.
    private Transform robertEsqueleto; //Este tiene la base para el esqueleto de Robert, de acá sacamos todos los huesos que están abajo.
    private Transform torso;
    private Transform cabeza;
    private Transform brazo;
    private Transform pierna;
    private Transform lugarDelQueDispara; //De acá van a salir los disparos que haga.
    [SerializeField]
    private Transform objetivo; //Esta almacena a dónde dispara
    private GameObject obj; //Almacena el gameObject "Objetivo" de la escena que se usa para poner la posición de los disparos.


    private string obstaculo; //Acá almacenás con qué chocó la bala.
    private GameObject instancia;//var para almacenar la instancia del prefab
    private Transform robertTransform; //Almacena el transform de Robert
    private Transform GUI;
    private float robertScale;
    private float valor;//Esta sirve para definir la altura del personaje en base a Robert
    private int cantMovValorIncial;
    private GameObject disparoAnim; //El gameobject con la animación de disparo;

    //El COMBAT MODE
    private TarjetasEnemigos tarjetas;
    [SerializeField]
    private GameObject tarjeta;
    public EnemyCardCM scriptTarjeta;
    private GameObject combatMode;
    public bool soyTarjetaActiva = false;
    public Vector3 offset;

    //Spawn group para la persistencia


 

    //////////////////////////////////////PPPP


    //Definiciones del enemigo
    public Genre genre = Genre.UNDEFINED; //Si es hombre o mujer (masculino,femenino)
    public Behavior currentBehavior; //Su estado de ánimo (neutral, hostil)
    public EnemyType enemyType; //El grupo al que pertenece (survivor)
    public float speed = 100; //Esta sirve para que el enemigo se desplace (pero actualmente no parece estar en uso)
    public bool isPackLeader = false; //Si es el líder del grupo o no
    public bool useGeneratedStats = true;

    //La zona a la que apunta
    public BodyObjetive targetedPart; //A qué parte del cuerpo apunta (cabeza, torso,etc)
    public GameObject targets; //Objeto de la tarjeta que se usa para que el jugador elija la parte del cuerpo

    public UnityEngine.UI.Image speakingIcon; //El botón de la GUI para hablar

    //PRIVADAS
    private IngameBGMController _audio; //Guarda el script InGameBGMCobntroller
    private Transform startPosition, finalPosition; //Transforms para almacenar la posición inicial y final del desplazamiento.
    private bool _configured = false; //Esta sirve para saber si el personaje ya fue configurado (género, nombre, tipo etc.) o si hay que hacerlo.


    //---------------------------------------------------------- 
    // AI Characters Gear.
    public List<int> AvailableConsumablesIds;
    public int ConsumablesMinAmnt, ConsumablesMaxAmnt;

    public List<int> AvailableWeaponsIds;

    public List<int> AvailablesBulletsIds;
    public int BulletsMinAmnt, BulletsMaxAmnt;

    private List<ISWeapon> _availableWeapons = new List<ISWeapon>();
    private List<ISConsumable> _consumables = new List<ISConsumable>();
    private List<ISBullet> _bullets = new List<ISBullet>();

    [SerializeField]
    AbnormalStatusPanel _abnormalStatusPanel;
    protected ShuffleBag onHitFX = new ShuffleBag(20);
    protected ShuffleBag onDeathFX = new ShuffleBag(10);



    //Enemy Container 
    //EnemyContainer.cs es un script que controlaba un montón de datos que ahora pasan a estar en la tarjeta. 
    //Creo que actualmente es completamente obsoleto, pero habría que ver, porque puede que el desplazamiento del personaje lo use.
    //Además lo usa por ejemplo el GetName()
    private EnemyContainer _container; //esta es la variable con que almacena el container para usar en el código.
    public EnemyContainer container //no sé bien cuál es la historia con esto, es casi el mismo código que está en el init()
    {
        get
        {
            if (_container == null)
            {
                _container = gameObject.GetComponent<EnemyContainer>();

                if (_container == null)
                    _container = gameObject.AddComponent<EnemyContainer>();

                _container.config(this);
            }
            return _container;
        }
    }


    ///////////////////////////ENUMS
    public enum Behavior
    {
        NEUTRAL,
        HOSTILE,
        FRIENDLY
    }

    public enum EnemyType
    {
        RANDOM,
        BANDIT,
        AGENT,
        SURVIVOR,
        WATERMAN,
        MUTANT,
        MARAUDER,
        KRAKEN,
        SCAVENGER,
        VENDOR,
        CULTIST,
        MILITIA,
        POLICE,
        JUNKIE,
        ROBOT,
        SOLDIER
    }

    public enum Genre
    {
        UNDEFINED,
        MALE,
        FEMALE
    }



    /*
//private float _hostility;
//public float hostility {  get { return _hostility; } }


public List<ISConsumable> powerUps = new List<ISConsumable>();
public ISWeapon offHandWeapon;
public ISArmor headSlot, torsoSlot, legsSlot, armsSlot;





public List<EquippableInventoryItem> availableWeapons = new List<EquippableInventoryItem>();
public EquippableInventoryItem headSlot, bodySlot, legsSlot;
public List<ConsumableInventoryItem> powerUps = new List<ConsumableInventoryItem>();
public List<ConsumableInventoryItem> healthPotions = new List<ConsumableInventoryItem>();

public EquippableInventoryItem mainWeapon;
public EquippableInventoryItem offWeapon;
*///código comentado



    void Start()
    {
        init();
        if (HUD != null) //El selector de los enemigos.
        {
            HUD.SetActive(false);
        }
        robertTransform = GameObject.FindGameObjectWithTag("Player").transform;
        dispararIK = robertTransform.GetComponent<DispararIK>();
        GUI = GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<Transform>();
        disparoAnim = GUI.GetChild(0).gameObject; //La animación de disparo de los enemigos en primer plano.
        navAgent = this.GetComponent<NavMeshAgent>(); //El nav mesh
        tarjetas = GUI.GetChild(1).GetComponent<TarjetasEnemigos>();
        tarjeta = tarjetas.NuevaTarjeta(this);
        combatMode = tarjetas.gameObject;
        scriptTarjeta = tarjeta.GetComponent<EnemyCardCM>();
        tarjetas.inicializado = true;
        _abnormalStatusPanel = tarjeta.transform.GetChild(0).GetChild(7).GetChild(5).GetComponent<AbnormalStatusPanel>();
        quad = this.transform.GetChild(2).gameObject;
        quadImage = quad.GetComponent<MeshRenderer>().material;
        quadImage.mainTexture = this.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite.texture;

        robertEsqueleto = GameObject.Find("rob_aujereado_2:Robert1_Reference").transform; //Encontrá el esqueleto de Robert.

        lugarDelQueDispara = this.transform.GetChild(0);
        obj = GameObject.Find("Objetivo");

        gameObject.layer = 14;

        //Almacenamos los huesos para que le peguen en el lugar justo
        pierna = robertEsqueleto.GetChild(0).GetChild(0).GetChild(0);
        torso = robertEsqueleto.GetChild(0).GetChild(2).GetChild(0);
        cabeza = robertEsqueleto.GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(1).GetChild(0);
        brazo = robertEsqueleto.GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0);


        CombatManager.Instance.OnRobertMoving.AddListener(delegate { CalculateDistance(true); });
        CombatManager.Instance.OnRobertMoved.AddListener(delegate { CalculateDistance(false); });
        CalculateDistance(false);

        //Que primer plano corresponda al sprite del personaje.
        //Assets\Modelos\0-PERSONAJES\Animaciones_Enemigos
        string path = "Sprites/AnimacionDisparo/animacion_disparo_personaje_" + quadImage.mainTexture.name;
        primerPlano = Resources.Load<Sprite>(path);
        if (primerPlano == null) { Debug.Log("No encontré el primerPlano este => " + path + "******************************"); }

    }



    //----------------------------------------------------------
    //ICONO PARA HABLAR
    //Esto se usa para cambiar el color del ícono de diálogo de la tarjeta (me parece que no está andando)
    public void ShowSpeakingIcon(bool active) 
    {
        if (active){ speakingIcon.color = Color.green; }
        else { speakingIcon.color = Color.white; }
    }
    //----------------------------------------------------------
    //GET NAME
    //Obtener el nombre del enemigo en cuestión en un string
    public string getName() 
    {
        if (_container != null)
        {
            return _container.selectedName;
        }

        return enemyType.ToString();
    }
    //----------------------------------------------------------
    //SHOW DETAILS WINDOW
    //Es para abrir la tarjeta del enemigo
    public void ShowDetailsWindow()
    {
        //CombatManager.Instance.Logger.SubmitText("Starting Ennemy Details");

        tarjetas.Activar(tarjeta);
        tarjetas.Centrar();

        //GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().cardCloseUp.ActivateWindow(this); //PPP cambiado por el nuevo sistema de tarjetas
    }
    //----------------------------------------------------------
    

    //INIT
    public override void init()
    {
        base.init();

        if (targets == null) //Si targets es null ponele el gameObject Targets (hud con partes del cuerpo para elegir a donde tirar)
            targets = transform.Find("Targets").gameObject;

        if (_container == null) //Si la variable conatiner está vacía, ponele el container del enemigo.
            _container = gameObject.GetComponent<EnemyContainer>();

        if (_container == null)
            _container = gameObject.AddComponent<EnemyContainer>(); //Si el getcomponent no funcionó, usá AddComponent en vez.

        //Crea listeners para saber si los Abnormal States [pánico, ciego, loco, etc.] hay que prenderlos o apagarlos
        abnormalStateTriggered.AddListener(TriggeredState);
        abnormalStateEnded.AddListener(TriggeredStateEnded);

        valor = (GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().localScale.x) * 0.33f / 2.3f; //PPP

        //////////////////////////////////////////////////////////////////////////////////////////////
#region Esto estaba en Start()
 
    }
#endregion
    //----------------------------------------------------------

    //UPDATE
    //Actualmente sólo la usa la transición.

    void Update()
    {
        if (startTransition)
            MoveTowardsTarget();

        //El HUD que marca al personaje activo en la escena.
        if (soyTarjetaActiva) {
            
            Vector3 screenPos = Camera.main.WorldToViewportPoint(this.gameObject.transform.GetChild(0).position);
            tarjetas.HUD.transform.position = new Vector3 (screenPos.x * Screen.width,screenPos.y * Screen.height, 0) + offset;

        
        } 
    }


    //----------------------------------------------------------

    //* ABNORMAL STATES 
    //son lo que aparecen en las subtarjetas como pánico, locura, tendencia suicida, piernas paralizadas, etc.
    //Modifican la conducta general del personaje

    void TriggeredState(AbnormalStates state) //Esta los activa
    {
        _abnormalStatusPanel.TriggerNewState(state.ToString());
    }

    void TriggeredStateEnded(AbnormalStates state) //Esta los apaga
    {
        _abnormalStatusPanel.RemoveState(state.ToString());
    }
    //----------------------------------------------------------



    //CONFIGURACIÓN DEL PERSONAJE
     //Sin duda uno de las funciones más importantes



    public void configureCharacter(bool randomized, Genre gen = Genre.MALE)
    {
        if (_configured) return; //Si ya está configurado, volvé

        
        
        //GÉRNERO

        if (randomized) //"randomized" es un parámetro de la función
        {
            //Dale un género al azar
            if (genre == Genre.UNDEFINED)
            {
                if (Random.Range(0.0f, 1.0f) >= 0.51f)
                    genre = Genre.FEMALE;
                else
                    genre = Genre.MALE;
            }
        }
        else //Si no está randomizado, usá el género que te viene dado en el parámetro
        {
            if (genre == Genre.UNDEFINED)
                genre = gen;
        }


        _characterDatabase = GameManager.Instance.levelManager.charactersDatabase; //Almacená el characterDatabase del nivel

        //TIPO DE PERSONAJE
        if (_thisCharacter.CharacterType == null) //Si el personaje no tiene tipo
            AnalizeCharacterType(); //Corré esta función

        generateCharacterGear(); //Creale un INVENTARIO
        generateStarterStats(); //Creale STATS
        getSounds(); //Traé sus SONIDOS

        _container.config(this); //Configura el Container
        checkCharacterAvatar(); //Asignale el avatar

        if (isPackLeader) //Si es el líder del grupo
            container.ActivateHeader(); //Ponele la coronita arriba del container

        if (enemyType == EnemyType.MARAUDER) //Si es un marauder
            CombatManager.Instance.OnMarauderAttacked.AddListener(MarauderAttacked); //Ponele el listener epecial de ese grupo

       
        /* PPP saqué esta en favor de la nueva versión
        //Esto es del sistema viejo en que la subtarjeta aparecía sobre el personaje:
        Transform go = transform.Find("Hideable Canvas/Abnormal States"); 
        if (go != null) {_abnormalStatusPanel = go.GetComponent<AbnormalStatusPanel>();}
        */


        gameObject.transform.localScale = new Vector3(valor, valor, valor); //PPP Función que determina la escala de los enemigos en todos los niveles. 

        _configured = true;//Al llegar acá el personaje ya fue configurado.

    }


    //----------------------------------------------------------

    //AVATAR
    //Para darle un sprite al personaje
    private void checkCharacterAvatar()
    {
        // GetCharaacterSprite
        SpriteRenderer spRenderer = transform.GetChild(1).GetComponent<SpriteRenderer>();

        if (spRenderer != null)
        {
            // Get Random Avatar From AvatarList
            string chAvatar = "";
            if (genre == Genre.FEMALE)
                chAvatar = _thisCharacter.FemaleAvatar[Random.Range(0, _thisCharacter.FemaleAvatar.Length)];
            else
                chAvatar = _thisCharacter.MaleAvatar[Random.Range(0, _thisCharacter.MaleAvatar.Length)];

            // Load Image From Resources & Set It as Sprite for CharaacterSprite
            spRenderer.sprite = Resources.Load<Sprite>("Sprites/NewCharacters/" + chAvatar);
            EnemyHistory.Instance.AddEnemy(this, "Sprites/NewCharacters/" + chAvatar, 0, 0, gameObject.name);

 
        }

    }

    //EL ATAQUE DE LOS MAUREDERS
    //Los merodeadores tienen un ataque epecial entre todos los tipos de personajes, y acá está la función que lo dispara
    protected void MarauderAttacked()
    {
        if (enemyType == EnemyType.MARAUDER)
        {
            if (currentBehavior != Behavior.HOSTILE)
            {
                CombatManager.Instance.NpcFXController.EnemyBehaviourChange();

                characterStats.Hostility = 50;
                checkHostility();
                CombatManager.Instance.OnMarauderAttacked.RemoveListener(MarauderAttacked);
            }
        }
    }

    //SONIDOS
    //Carga los sonidos propios en base al personaje que haya tocado
    private void getSounds()
    {
        string character = "";
        character += genre.ToString();

        // TODO: HardCode till we have Marauder Sounds
        if (enemyType == EnemyType.MARAUDER)
        {
            character += BaseEnemy.EnemyType.AGENT.ToString();
        }
        else
        {
            character += enemyType.ToString();
        }

        character += "Generic";

        string pathToFiles = "AudioClips/Characters/" + character + "/";

        AudioClip[] allSounds = Resources.LoadAll<AudioClip>(pathToFiles);

        for (int i = 0; i < allSounds.Length; i++)
        {
            string[] clipNameFields = allSounds[i].name.Split('_');

            if (clipNameFields[1].ToLower() == "ishit")
            {
                if (!onHitFX.Contains(allSounds[i]))
                    onHitFX.Add(allSounds[i]);
            }
            else if (clipNameFields[1].ToLower() == "dies")
            {
                if (!onDeathFX.Contains(allSounds[i]))
                    onDeathFX.Add(allSounds[i]);
            }
        }
    }

    //ARMA
    //Se fija cuál es el arma que estás 
    public override void getSelectedWeapon()
    {
        if (_selectedWeapon == null) //Si no tenés arma seleccionada
            if (_availableWeapons.Count > 0) //Y tenés armas en el array para elegir
                _selectedWeapon = _availableWeapons[0]; //Elegi la primera (PPP que función decepcionante!)
    }






    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //DESPLAZAMIENTO DEL PERSONAJE
    //Función importante pero que parece no estar andando. Concretamente permite al personaje moverse. 
    //Nahuel la había diseñado para el 3D, así que habría que ver por qué no anda.

    //Creando variables
    EnemySoundsManager soundManager; //variable que almacena el script EnemysoundManger.cs (PPP para qué no las puso arriba?) 
    bool startTransition = false; //Bool arrancó
    bool stopedFootsteps = false; //Bool se detuvo



    public void startWithTransition(Transform fromPos, Transform toPos)
     //Función con que arranca el movimiento del personaje
   
    {

        //Estas dos creo que son obsoletas, que hacían que la info alrededor del personaje desapareciera cuando se movía.
        //_container.SetupForTransition();
        //_container.TriggerCardVisible();

        //Se fija cuál sound manager del levelManager está libre y lo asigna a la variable soundManager que creó más arriba
        if (!GameManager.Instance.levelManager.enemiesSoundManager1.isBusy)
            soundManager = GameManager.Instance.levelManager.enemiesSoundManager1;
        else if (!GameManager.Instance.levelManager.enemiesSoundManager2.isBusy)
            soundManager = GameManager.Instance.levelManager.enemiesSoundManager2;

        //Si ya tenés un soundManager ponele el sonido de pasos.
        if (soundManager != null)
            soundManager.playLoopableFX(soundManager.footSteps);

        startPosition = fromPos; //Transform de dónde arranca la transición
        finalPosition = toPos; //Transform hacia donde va.
        startTransition = true; //Arrancá la transición = sí
        cantMovValorIncial = cantidadMovimiento;
    }

    private void MoveTowardsTarget()
        //Función que domina el desplazamiento
    {
        //the speed, in units per second, we want to move towards the target
        //move towards the center of the world (or where ever you like)

        //El vector3 de la posición a la que quiero ir
        Vector3 targetPosition = new Vector3(finalPosition.position.x, finalPosition.position.y, finalPosition.position.z);
        //El vector3 de la posición en la que estoy
        Vector3 currentPosition = this.transform.position;



        //first, check to see if we're close enough to the target
        if (Vector3.Distance(currentPosition, targetPosition) > 10f && cantidadMovimiento >0 )//PPP no hay que usar distance
        {

            navAgent.SetDestination(targetPosition);

            /*

               Vector3 directionOfTravel = targetPosition - currentPosition;
               //now normalize the direction, since we only want the direction information
               directionOfTravel.Normalize();
               //scale the movement on each axis by the directionOfTravel vector components

               this.transform.Translate(
                   (directionOfTravel.x * speed * Time.deltaTime),
                   (directionOfTravel.y * speed * Time.deltaTime),
                   (directionOfTravel.z * speed * Time.deltaTime),
                   Space.World);
             *           */
            cantidadMovimiento--;
        }
        else
        {
            cantidadMovimiento = cantMovValorIncial;
            startTransition = false;
            navAgent.Stop();
            navAgent.ResetPath();

            //Invoke("FadeCardIn", 0.5f);

            if (!stopedFootsteps)
            {
                if (soundManager != null)
                    soundManager.stopLoopingFX(soundManager.footSteps);
                stopedFootsteps = true;
            }
        }
    }


    public void SetupCardTransition() //Creo que es obsoleta.
    {
        // Hide Everything
        _container.SetupForTransition();
    }

    void FadeCardIn()
    {
        // Show Stuff
        //_container.TriggerCardVisible();
        StartCardMovement();
    }

    public void StartCardMovement()
    {
        CardsSmoothMovement movement = this.gameObject.GetComponent<CardsSmoothMovement>();
        if (movement != null)
            movement.StartCardMovement();
    }


 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 



    //CREANDO EL INVENTARIO DEL PERSONAJE

    private bool GetStartingElements()
    {
        if (_thisCharacter.CharacterType == null)
            AnalizeCharacterType();

        if ((_thisCharacter.Items == null) || (_thisCharacter.Items.Length <= 0)) return false;

        for (int i = 0; i < _thisCharacter.Items.Length; i++)
        {
            if (_thisCharacter.Items[i].Contains("weapon"))
            {
                //AvailableWeaponsIds
                string weaponID = _thisCharacter.Items[i].Split(new string[] { "weapon" }, System.StringSplitOptions.None)[1];
                AvailableWeaponsIds.Add(int.Parse(weaponID));
            }
            else if (_thisCharacter.Items[i].Contains("ammo"))
            {
                string ammoID = _thisCharacter.Items[i].Split(new string[] { "ammo" }, System.StringSplitOptions.None)[1];
                AvailablesBulletsIds.Add(int.Parse(ammoID));
            }
            else if (_thisCharacter.Items[i].Contains("armor"))
            {
                // TODO: Implement Armors Lists And usage By NPC
            }
            else if (_thisCharacter.Items[i].Contains("item"))
            {
                //AvailableConsumablesIds
                string itemID = _thisCharacter.Items[i].Split(new string[] { "item" }, System.StringSplitOptions.None)[1];
                AvailableConsumablesIds.Add(int.Parse(itemID));
            }
        }
        return true;
    }

    //----------------------------------------------------------

    private void generateCharacterGear()
    {
        // Clear List from Trash Values
        _availableWeapons.Clear();
        _consumables.Clear();
        _bullets.Clear();

        //if (GameManager.Instance.levelManager.charactersDatabase.)
        //if (GetStartingElements()) return;
        GetStartingElements();

        if (AvailableWeaponsIds.Count <= 0)
        {
            // No Weapons Specified - GenerateBasicWeapon

            ISWeapon BaseWeapon = ISInventoryManager.instance.weaponsDatabase.GetElementByID(3) as ISWeapon;
            BaseWeapon.ShootsInRound = BaseWeapon.MaxShootsPerRound;
            _availableWeapons.Add(BaseWeapon);

            ISBullet BaseBullets = ISInventoryManager.instance.bulletsDatabase.GetElementByID(1) as ISBullet;
            BaseBullets.Stack = Random.Range(4, 6);
            _bullets.Add(BaseBullets);

            return;
        }

        // Define Temporal Weapons Container.
        ISWeapon weapon;

        // Iterate all weapons id, instanciate them, and add them to the list.
        for (int i = 0; i < AvailableWeaponsIds.Count; i++)
        {
            weapon = ISInventoryManager.instance.weaponsDatabase.GetElementByID(AvailableWeaponsIds[i]) as ISWeapon;
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            _availableWeapons.Add(weapon);
        }

        ISConsumable consumable;
        for (int i = 0; i < AvailableConsumablesIds.Count; i++)
        {
            consumable = ISInventoryManager.instance.consumablesDatabase.GetElementByID(AvailableConsumablesIds[i]) as ISConsumable;
            Debug.Log("AvailableConsumablesIds.Count = " + AvailableConsumablesIds.Count);
            Debug.Log("Acá estoy!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)");
            if (consumable == null) { Debug.Log("consumable es null"); }

            consumable.Stack = Random.Range(ConsumablesMinAmnt, ConsumablesMaxAmnt);
            _consumables.Add(consumable);
        }

        ISBullet bullets;

        for (int i = 0; i < AvailablesBulletsIds.Count; i++)
        {
            bullets = ISInventoryManager.instance.bulletsDatabase.GetElementByID(AvailablesBulletsIds[i]) as ISBullet;
            bullets.Stack = Random.Range(BulletsMinAmnt, BulletsMaxAmnt);
            _bullets.Add(bullets);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    //TIPO DE PERSONAJE
    
    //Creando variables
    ISCharactersDatabase _characterDatabase; //Almacena el characte database
    ISCharacter _thisCharacter = new ISCharacter(); //PAra guardar el ISCharacter del personaje

    void AnalizeCharacterType()
    {
        if (enemyType == EnemyType.BANDIT)
            _thisCharacter = _characterDatabase.GetByType("Bandit");
        else if (enemyType == EnemyType.AGENT)
            _thisCharacter = _characterDatabase.GetByType("Agent");
        else if (enemyType == EnemyType.SURVIVOR)
            _thisCharacter = _characterDatabase.GetByType("Survivor");
        else if (enemyType == EnemyType.WATERMAN)
            _thisCharacter = _characterDatabase.GetByType("Waterman");
        else if (enemyType == EnemyType.MUTANT)
            _thisCharacter = _characterDatabase.GetByType("Mutant");
        else if (enemyType == EnemyType.MARAUDER)
            _thisCharacter = _characterDatabase.GetByType("Marauder");
        else if (enemyType == EnemyType.KRAKEN)
            _thisCharacter = _characterDatabase.GetByType("Kraken");
        else if (enemyType == EnemyType.SCAVENGER)
            _thisCharacter = _characterDatabase.GetByType("Scavenger");
        else if (enemyType == EnemyType.VENDOR)
            _thisCharacter = _characterDatabase.GetByType("Vendor");
        else if (enemyType == EnemyType.CULTIST)
            _thisCharacter = _characterDatabase.GetByType("Cultist");
        else if (enemyType == EnemyType.MILITIA)
            _thisCharacter = _characterDatabase.GetByType("Militia");
        else if (enemyType == EnemyType.POLICE)
            _thisCharacter = _characterDatabase.GetByType("Police");
        else if (enemyType == EnemyType.JUNKIE)
            _thisCharacter = _characterDatabase.GetByType("Junkie");
        else if (enemyType == EnemyType.ROBOT)
            _thisCharacter = _characterDatabase.GetByType("Robot");
        else if (enemyType == EnemyType.SOLDIER)
            _thisCharacter = _characterDatabase.GetByType("Soldier");

    }



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////7
    




    //EL GENERADOR DE LAS STATS

    public virtual void generateStarterStats()
    {

        Debug.Log("generandoStarterStatsÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑ");



        float statsFactor = 1.0f;

        if (isPackLeader)
            statsFactor = _thisCharacter.BossFactor;


        if (useGeneratedStats)
        {

            characterStats.AddStat(CharacterStats.StatAccuracy, Mathf.FloorToInt(Random.Range(_thisCharacter.MinAccuracy, _thisCharacter.MaxAccuracy) * statsFactor));
            characterStats.AddStat(CharacterStats.StatStrength, Mathf.FloorToInt(Random.Range(_thisCharacter.MinStrength, _thisCharacter.MaxStrength) * statsFactor));
            characterStats.AddStat(CharacterStats.StatAgility, Mathf.FloorToInt(Random.Range(_thisCharacter.MinAgility, _thisCharacter.MaxAgility) * statsFactor));
            characterStats.AddStat(CharacterStats.StatMorale, Mathf.FloorToInt(Random.Range(_thisCharacter.MinMorale, _thisCharacter.MaxMorale) * statsFactor));
            characterStats.AddStat(CharacterStats.StatCommand, Mathf.FloorToInt(Random.Range(_thisCharacter.MinCommand, _thisCharacter.MaxCommand) * statsFactor));
            characterStats.AddStat(CharacterStats.StatEvasion, Mathf.FloorToInt(Random.Range(_thisCharacter.MinEvasion, _thisCharacter.MaxEvasion) * statsFactor));
            characterStats.AddStat(CharacterStats.StatIntelligence, Mathf.FloorToInt(Random.Range(_thisCharacter.MinIntelligence, _thisCharacter.MaxIntelligence) * statsFactor));
            characterStats.AddStat(CharacterStats.StatStealth, Mathf.FloorToInt(Random.Range(_thisCharacter.MinStealth, _thisCharacter.MaxStealth) * statsFactor));
            if (characterStats.Agility < 3)
                characterStats.Agility = 3;
        }

        // Health Points Generation
        bodyPartHealth[BodyObjetive.TORSO] = new CharacterStat("TORSO", Mathf.FloorToInt(Random.Range(_thisCharacter.MinHitpoints, _thisCharacter.MaxHitpoints) * statsFactor));
        bodyPartHealth[BodyObjetive.HEAD] = new CharacterStat("HEAD", (bodyPartHealth[BodyObjetive.TORSO].OriginalValue * 20) / 100);
        bodyPartHealth[BodyObjetive.ARMS] = new CharacterStat("ARMS", (bodyPartHealth[BodyObjetive.TORSO].OriginalValue * 30) / 100);
        bodyPartHealth[BodyObjetive.LEGS] = new CharacterStat("LEGS", (bodyPartHealth[BodyObjetive.TORSO].OriginalValue * 40) / 100);

        characterStats.AddStat(CharacterStats.StatTotalHitpoints, bodyPartHealth[BodyObjetive.TORSO].OriginalValue);

        //------------------------------------------------------------------------------------------------------------------------------
        // Hitpoints Generation
        characterStats.AddStat(CharacterStats.StatHitpoints, characterStats.TotalHitpoints);
        characterStats.AddStat(CharacterStats.StatHostility, Random.Range(_thisCharacter.MinHostility, _thisCharacter.MaxHostility));

        initBehaviour();

        reactionTime = Random.Range(3, 5);

        if (characterStats.Agility > 5)
            reactionTime -= 0.5f * (characterStats.Agility - 5);

        //--------------------------------------------------------------------------------------------------------------------------------
        // Calculate Basic Might, 
        // ++ Equipmennt Might each time an item is equiped
        int startingMight = 0;

        startingMight += characterStats.Accuracy;
        startingMight += characterStats.Strength;
        startingMight += characterStats.Agility;
        startingMight += characterStats.Evasion;
        startingMight += characterStats.Intelligence;
        startingMight += characterStats.TotalHitpoints * 2;


        if (_selectedWeapon == null)
            getSelectedWeapon();

        startingMight += (int)_selectedWeapon.Might;

        characterStats.AddStat(CharacterStats.StatMight, startingMight);

        //--------------------------------------------------------------------------------------------------------------------------------

        CombatManager.Instance.debugPanel.SubmitText("---------------------------------------------");
        CombatManager.Instance.debugPanel.SubmitText(" - Generating Enemy - ");
        CombatManager.Instance.debugPanel.SubmitText("Generated Enemy: " + characterName + " (" + enemyType.ToString() + ")", "Yellow");
        CombatManager.Instance.debugPanel.SubmitText("Generated Enemy Stat(Accuracy): " + characterStats.Accuracy, "Yellow");
        CombatManager.Instance.debugPanel.SubmitText("Generated Enemy Stat(TotalHitpoints): " + characterStats.TotalHitpoints, "Yellow");
        CombatManager.Instance.debugPanel.SubmitText("Generated Enemy Stat(Strength): " + characterStats.Strength, "Yellow");
        CombatManager.Instance.debugPanel.SubmitText("Generated Enemy Stat(Agility): " + characterStats.Agility, "Yellow");
        CombatManager.Instance.debugPanel.SubmitText("Generated Enemy Stat(Morale): " + characterStats.Morale, "Yellow");
        CombatManager.Instance.debugPanel.SubmitText("Generated Enemy Stat(Command): " + characterStats.Command, "Yellow");
        CombatManager.Instance.debugPanel.SubmitText("Generated Enemy Stat(Evasion): " + characterStats.Evasion, "Yellow");
        CombatManager.Instance.debugPanel.SubmitText("Generated Enemy Stat(Intelligence): " + characterStats.Intelligence, "Yellow");
        CombatManager.Instance.debugPanel.SubmitText("Generated Enemy Stat(Stealth): " + characterStats.Stealth, "Yellow");
        CombatManager.Instance.debugPanel.SubmitText("Generated Enemy Stat(Hostility): " + characterStats.Hostility, "Yellow");
        CombatManager.Instance.debugPanel.SubmitText("Generated Enemy Stat(reactionTime): " + reactionTime, "Yellow");
        CombatManager.Instance.debugPanel.SubmitText("Generated Enemy Stat(Might): " + characterStats.Might, "Yellow");
        CombatManager.Instance.debugPanel.SubmitText("Enemy Behavior: " + currentBehavior, "Yellow");
        CombatManager.Instance.debugPanel.SubmitText(" - End Generating Enemy - ");
        CombatManager.Instance.debugPanel.SubmitText("---------------------------------------------");

        if (characterStats == null) { Debug.Log("Las putas stats siguen siendo nulasÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑ"); }
        else { Debug.Log("las estats no parecen ser nulas ÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑÑ + " + this.characterName);}
    }

   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    




    //COMPORTAMIENTO

    //Compoartamiento inicial
    private void initBehaviour()
    {
        if (characterStats.Hostility < -50)
            currentBehavior = Behavior.FRIENDLY;
        else if (characterStats.Hostility > -50 && characterStats.Hostility < 20)
            currentBehavior = Behavior.NEUTRAL;
        else if (characterStats.Hostility > 20)
            currentBehavior = Behavior.HOSTILE;
    }

    //--------------------------------------------------------------------------------------------------------------------------------
    
    //Determinar la hostilidad
    public void checkHostility()
    {


        if (characterStats.Hostility < -50)
        {
            if (currentBehavior != Behavior.FRIENDLY)
                CombatManager.Instance.Logger.SubmitText(characterName + " is now <color=green>Friendly</color>");
            currentBehavior = Behavior.FRIENDLY;
        }
        else if (characterStats.Hostility > -50 && characterStats.Hostility < 20)
        {
            if (currentBehavior != Behavior.NEUTRAL)
                CombatManager.Instance.Logger.SubmitText(characterName + " is now <color=yellow>Neutral</color>");
            currentBehavior = Behavior.NEUTRAL;
        }
        else if (characterStats.Hostility > 20)
        {
            if (currentBehavior != Behavior.HOSTILE)
            {
                CombatManager.Instance.NpcFXController.EnemyBehaviourChange();
                CombatManager.Instance.Logger.SubmitText(characterName + " is now <color=red>Hostile</color>");
            }
            currentBehavior = Behavior.HOSTILE;
        }

        scriptTarjeta._hostility.value = characterStats.Hostility;

    }

    /*
        -100 a 100
        desde -50 a -100 es Friendly
        desde -49 a +20 es Neutral
        de 20 a 100 es Hostil
        Agentes arrancan con +80 Hostile
        Agente Boss +100
    */ //código sin usar de la función anterior




    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   





        //EL SISTEMA DE TURNOS




    //ARRANCA EL TURNO
    public override void FixedTurnStarted()
    {
        //Activar el HUD
        if (HUD != null)
        {
            HUD.SetActive(true);
        }

        //StartCoroutine(Esperar(10f));
        //ZZZZZ
        tarjetas.Desactivar(tarjetas.tarjetaActiva);
        tarjetas.Activar(tarjeta);
        tarjetas.Centrar();

        base.FixedTurnStarted();

        //Si Robert o el personaje estan muertos el turno terminó
        if ((CombatManager.Instance.robert == null) ||
            (!CombatManager.Instance.robert.isAlive) ||
            (!isAlive))
        {

            _container.tintCharacterActing(false); //PPP Habría que analizar esto qué es.
            _isActing = false;
            FixedTurnEnded(false);

        }

        //De otra forma arrancá el turno
        CombatManager.Instance.Logger.SubmitText(characterName + " turn started");
        _isActing = true; //El personaje está actuando
        _container.tintCharacterActing(true); //Actualmente obsoleto

        // Agregar chequeo de AbnormalStates. //PPP queda para hacer

        // Evaluar AI
        processAI(); //Ojo process AI es una copia de evaluate actions, no sé cuál es la posta y cuál no.

    }
    //-------------------------------------------------------------------
    
    //TERMINA EL TURNO
    public override void FixedTurnEnded(bool announce = true)
    {
        if (announce) CombatManager.Instance.Logger.SubmitText(characterName + " turn finalized");

        _isActing = false;
        _container.tintCharacterActing(false);
        base.FixedTurnEnded();
        if (HUD != null)
        {
            HUD.SetActive(false);
        }
    }
    //-------------------------------------------------------------------
   






    //INTELIGENCIA ARTIFICIAL
    void processAI()
    {
        // 1) Chequear comportamiento.
        if (currentBehavior == Behavior.NEUTRAL)
        {
            CombatManager.Instance.Logger.SubmitText(characterName + " is not hostile and wont attack unless he is provoked");
            FixedTurnEnded();
        }

        // 2) Trigger de Panico.
        if (checkIfAbnormalStatusIsActive(AbnormalStates.Panic))
        {
            CombatManager.Instance.Logger.SubmitText(this.characterName + " is in panic and skips his turn");
            FixedTurnEnded();
        }

        // 3) Trigger Escapar.
        // TODO: Implementar.

        // 4) Chequear Hitpoints y Consumibles.
        // TODO: Implementar

        // 5) CHECK DISTANCE

        //distanceToTarget = CombatResolver.calculateDistance(this.gameObject, robertTransform.gameObject); //PPP sacada

        //Debug.Log("La distancia es : " + distanceToTarget + "******************************************************************" + base.characterName);
        //Debug.Log(this.gameObject.transform.localPosition);
        //Debug.Log(distanceToTarget + " = Distancia entre el enemigo y Robert");
        //Vector2.Distance(this.gameObject.transform.position, CombatManager.Instance.robert.gameObject.transform.position) / 10;

        // 6) RANK WEAPONS USING DISTANCE and DAMAGE (High damage first) (discard those with no bullets)
        bool canShoot = false;

        List<ISWeapon> usableWeapons = new List<ISWeapon>();

        foreach (ISWeapon weapon in _availableWeapons)
            if (weapon.MaxRange >= distanceToTarget)
                usableWeapons.Add(weapon);

        if (usableWeapons.Count > 0)
        {
            canShoot = true;
            if (selectedWeapon == null)
                _selectedWeapon = usableWeapons[0];
        }

        // Select The Weapon with the Highest Damage
        if ((_selectedWeapon != null) && (canShoot))
        {
            foreach (ISWeapon weapon in usableWeapons)
                if (weapon.Damage >= selectedWeapon.Damage)
                    _selectedWeapon = weapon;
        }

        // 7) IF NO WEAPONS FOR DISTANCE, MOVE NEAR ROBERT or SPARKY(random 50 %)
        if (!canShoot)
        {
            if (checkIfAbnormalStatusIsActive(AbnormalStates.ParalizedLegs))
            {
                CombatManager.Instance.Logger.SubmitText(characterName + " is not in range for shooting but his paralized legs dont allow him to get closer");
            }
            else
            {
                CombatManager.Instance.Logger.SubmitText(characterName + " Cant shoot, trying to move closer.");
                //PPP acá hay que llamar el movimiento del enemigo
                Transform robertPosition = GameObject.Find("rob_aujereado_2:Robert1_Ctrl_Reference").transform;
                startWithTransition (this.transform, robertPosition); //PPP a ver si anda.
                //Debug.Log( characterName + " está usando un " +selectedWeapon.Name + "------------------------------------------------------------------ " + usableWeapons.Count);
            }
            FixedTurnEnded();//PPP Esto no está cortando, el script sigue corriendo.
            return;//PPP puse el return porque no estaba cortando
        }

        // 9) IF THROWN ITEMS, 50 % CHANCE of THROWING

        // 10) SHOOT
        if (checkIfAbnormalStatusIsActive(AbnormalStates.ParalizedArms))
        {
            CombatManager.Instance.Logger.SubmitText(this.characterName + " is Disabled and cant attack");
            FixedTurnEnded();
        }
        else
        {
            /*
            // Check if character does damage to himself
            if (!checkCatastrophicDamageSelfTrigger())
            {
                if (checkDamageSelf())
                {
                    CombatManager.Instance.Logger.SubmitText(this.characterName + " Hurts himself");
                    FixedTurnEnded();
                }
            }
            else
            {
                CombatManager.Instance.Logger.SubmitText(this.characterName + " Hurts himself really bad");
                FixedTurnEnded();
            }
            */
            // Check if its a traitor.
            if (checkIfAbnormalStatusIsActive(AbnormalStates.Traitor))
                targetCharacter = CombatManager.Instance.getRandomEnemy();
            else
                targetCharacter = CombatManager.Instance.robert;

            if (targetCharacter == null)
            {
                CombatManager.Instance.Logger.SubmitText(this.characterName + " Can't acquire a suitable target");
                FixedTurnEnded();
            }

            float random = Random.Range(0.0f, 1.0f);
            // Mas de   20 -> 50% torso - 20%Piernas 20%Brazos 10%cabeza
            // Pre Projectile Stuff
            if (distanceToTarget > 20)
            {
                if ((random > 0) && (random < 0.2))
                    target = BodyObjetive.LEGS;
                else if ((random > 0.2) && (random < 0.4))
                    target = BodyObjetive.ARMS;
                else if ((random > 0.4) && (random < 0.5))
                    target = BodyObjetive.HEAD;
                else
                { target = BodyObjetive.TORSO; };
            }
            // Menos de 20 -> 30% Head 30%Torso 30%Brazos 10%piernas
            else if ((distanceToTarget < 20) && (distanceToTarget > 10))
            {
                if ((random > 0) && (random < 0.3))
                    target = BodyObjetive.HEAD;
                else if ((random > 0.3) && (random < 0.6))
                    target = BodyObjetive.TORSO;
                else if ((random > 0.6) && (random < 0.9))
                    target = BodyObjetive.ARMS;
                else
                    target = BodyObjetive.LEGS;
            }
            // Menos de 10 -> 50%Head / 50%Torso
            else if (distanceToTarget < 10)
            {
                if ((random > 0) && (random < 0.5))
                    target = BodyObjetive.HEAD;
                else
                { target = BodyObjetive.TORSO; }

            }
            
            if (isPackLeader)
            {
                //if (_damageType == DamageType.MELEE)
                //   EventManager.TriggerEvent("OnAttackingMelee");
                //else
                EventManager.TriggerEvent("OnAttackingRanged");
            }

            /// Sound Manager Stuff - Copiado del If Anterior.
            if (soundManager == null)
            {
                if (!GameManager.Instance.levelManager.enemiesSoundManager1.isBusy)
                    soundManager = GameManager.Instance.levelManager.enemiesSoundManager1;
                else if (!GameManager.Instance.levelManager.enemiesSoundManager2.isBusy)
                    soundManager = GameManager.Instance.levelManager.enemiesSoundManager2;
            }

            //soundManager.playFX(selectedWeapon.ShootSound); //PPP Temporalmente apagada, aunque más tarde puede ser útil.

            postShooting();

        }
    }
    //-------------------------------------------------------------------

    public override void DoAction()
    {
        Debug.Log(this.characterName + " is acting");

        if ((CombatManager.Instance.robert == null) ||
            (!CombatManager.Instance.robert.isAlive) ||
            (!isAlive) ||
            (CombatManager.Instance.robert.isHidden))
        {
            FinalizeTurn();
        }

        _isActing = true;

        CombatManager.Instance.debugPanel.SubmitText("---------------------------------");
        CombatManager.Instance.debugPanel.SubmitText("" + characterName + " Turn Started");

        checkAbnormalStatesEffects(continueAction);
        characterStats.checkPenalties();
        ValidateAffectedStats();

        _container.tintCharacterActing(true);
    }

    public void ValidateAffectedStats()
    {
        /*
        if (characterStats.hasAnyPenalty())
        {
            if (_container.debuffedStats != null)
                _container.debuffedStats.SetActive(true);
        }
        else
        {
            if (_container.debuffedStats != null)
                _container.debuffedStats.SetActive(false);
        }
        */
        checkHostility();
    }

    private void continueAction()
    {
        if (checkIfAbnormalStatusIsActive(AbnormalStates.Uncounciouss))
        {
            CombatManager.Instance.Logger.SubmitText(this.characterName + " is uncounciouss");
            ActionFinished();
        }
        else
        {
            evaluateActions();
            Invoke("triggerMiddleCombatFX", Random.Range(0.5f, 1.5f));
        }
    }

    void triggerMiddleCombatFX()
    {
        if (isPackLeader)
            if (Random.Range(0.0f, 1.0f) >= 0.5f)
                EventManager.TriggerEvent("OnMiddleCombat");
    }

    private void checkAbnormalStatesEffects(System.Action callBack)
    {
        bool waitForAnimatedFX = false;

        checkUnconcioussTrigger();
        checkPanicTrigger();
        checkTraitorTrigger();
        // Check Death Triggers
        //------------------------------------------------------------
        checkSuicideTrigger();
        if (checkIfAbnormalStatusIsActive(AbnormalStates.Suicide))
        {
            CombatManager.Instance.Logger.SubmitText(this.characterName + " commits suicide!!!");
            CombatManager.Instance.debugPanel.SubmitText("" + characterName + " Commits Suicide");
            OnCharacterDeath();
            return;
        }
        //------------------------------------------------------------
        checkSuddenDeathTrigger();
        if (checkIfAbnormalStatusIsActive(AbnormalStates.SuddenDeath))
        {
            CombatManager.Instance.Logger.SubmitText(this.characterName + " dies suddenly!!!");
            CombatManager.Instance.debugPanel.SubmitText("" + characterName + " Sudden Death");
            OnCharacterDeath();
            return;
        }
        //------------------------------------------------------------
        checkSurrenderTrigger();
        if (checkIfAbnormalStatusIsActive(AbnormalStates.Surrender))
        {
            CombatManager.Instance.Logger.SubmitText(this.characterName + " Surrender and run away!!!");
            CombatManager.Instance.debugPanel.SubmitText("" + characterName + " Surrenders");
            OnCharacterDeath();
            return;
        }
        //------------------------------------------------------------
        checkCrazyTrigger();
        if (checkIfAbnormalStatusIsActive(AbnormalStates.Crazy))
        {
            CombatManager.Instance.debugPanel.SubmitText("" + characterName + " Crazy State (Not Implemented AI)");
            // TODO: Define what are we going to do with this state.
        }
        //------------------------------------------------------------
        if (checkIfAbnormalStatusIsActive(AbnormalStates.Bleeding))
        {
            doBleedEffect();
            waitForAnimatedFX = false;
            CombatManager.Instance.debugPanel.SubmitText("" + characterName + " Is Bleeding");
            //container.triggerCardVFX(CardVisualFxTriggers.BLEEDING_TICK, callBack);
        }
        if (checkIfAbnormalStatusIsActive(AbnormalStates.Poisoned))
            doPoisonEffect();
        if (checkIfAbnormalStatusIsActive(AbnormalStates.Radiated))
            doRadiatedEffect();

        /*
        Blinded
        Uncounciouss,
        */
        if (!waitForAnimatedFX)
            callBack();
    }





    BodyObjetive target = BodyObjetive.TORSO;
    BaseCharacter targetCharacter;

    public virtual void evaluateActions()
    {
        if (currentBehavior == Behavior.NEUTRAL)
        {
            CombatManager.Instance.Logger.SubmitText(characterName + " is not hostile and wont attack unless he is provoked");
            FinalizeTurn();
            return;
        }
        /*
        Panic,
        DamageSelf,
        Traitor,
        Suicide,
        SuddenDeath,
        ParalizedLegs,
        ParalizedArms,
        Surrender,
        Crazy,
        CatastrophicDamageSelf,
        */

        // 1) CHECK POWERUPS, if any drug for enhancing, consume
        /*
        if (powerUps.Count > 0)
        {
            // Add the effect of the drug 
            // Remove Drug from inventory
            powerUps[0].Consume();
            //powerUps[0].Use();
        }
        */

        // 2) CHECK Panic Trigger
        if (checkIfAbnormalStatusIsActive(AbnormalStates.Panic))
        {
            CombatManager.Instance.Logger.SubmitText(this.characterName + " is in panic and skips his turn");
            ActionFinished();
            return;
        }

        // Check Flee
        //EventManager.TriggerEvent("OnFlee");

        // 4) CHECK HITPOINTS, if low then consume drugs to restore
        if (characterStats.Hitpoints < characterStats.TotalHitpoints)
        {
            if (isPackLeader)
                if (characterStats.Hitpoints <= characterStats.TotalHitpoints * 0.3f)
                    EventManager.TriggerEvent("OnLowHP");

            if (_consumables.Count > 0)
            {
                // Use Health Potion
                // Remove Health Potion from Inventory
                _consumables[0].Consume();
                //healthPotions[0].Use();
            }
        }

        // 5) CHECK DISTANCE
        // Temporary Disabled untill new Distance Check in 3D Space.
        //distanceToTarget = CombatResolver.calculateDistance(this.gameObject, CombatManager.Instance.robert.gameObject);// sacada Vector2.Distance(this.gameObject.transform.position, CombatManager.Instance.robert.gameObject.transform.position) / 10;
        Debug.Log(characterName);
        //float distanceToTarget = 0;

        // 6) RANK WEAPONS USING DISTANCE and DAMAGE (High damage first) (discard those with no bullets)
        bool canShoot = false;

        List<ISWeapon> usableWeapons = new List<ISWeapon>();

        foreach (ISWeapon weapon in _availableWeapons)
            if (weapon.MaxRange >= distanceToTarget)
                usableWeapons.Add(weapon);

        if (usableWeapons.Count > 0)
        {
            canShoot = true;
            if (selectedWeapon == null)
                _selectedWeapon = usableWeapons[0];
        }

        // Select The Weapon with the Highest Damage
        if ((_selectedWeapon != null) && (canShoot))
        {
            foreach (ISWeapon weapon in usableWeapons)
                if (weapon.Damage >= selectedWeapon.Damage)
                    _selectedWeapon = weapon;
        }

        // 7) IF NO WEAPONS FOR DISTANCE, MOVE NEAR ROBERT or SPARKY(random 50 %)
        if (!canShoot)
        {
            if (checkIfAbnormalStatusIsActive(AbnormalStates.ParalizedLegs))
            {
                CombatManager.Instance.Logger.SubmitText(characterName + " is not in range for shooting but his paralized legs dont allow him to get closer");
            }
            {
                CombatManager.Instance.Logger.SubmitText(characterName + " Cant shoot, trying to move closer.");

            }
        }

        // 9) IF THROWN ITEMS, 50 % CHANCE of THROWING

        // 10) SHOOT
        if (checkIfAbnormalStatusIsActive(AbnormalStates.ParalizedArms))
        {
            CombatManager.Instance.Logger.SubmitText(this.characterName + " is Disabled and cant attack");
            ActionFinished();
        }
        else
        {
            

            // Check if character does damage to himself
            if (!checkCatastrophicDamageSelfTrigger())
            {
                if (checkDamageSelf())
                {
                    CombatManager.Instance.Logger.SubmitText(this.characterName + " Hurts himself");
                    ActionFinished();
                }
            }
            else
            {
                CombatManager.Instance.Logger.SubmitText(this.characterName + " Hurts himself really bad");
                ActionFinished();
            }

            // Check if its a traitor.
            if (checkIfAbnormalStatusIsActive(AbnormalStates.Traitor))
                targetCharacter = CombatManager.Instance.getRandomEnemy();
            else
                targetCharacter = CombatManager.Instance.robert;

            if (targetCharacter == null)
                return;
            //targetCharacter = CombatManager.Instance.robert.sparky;

            // Check for Behavior To Attack Agents and others.
            // Add "Get Random Enemy" to Combat Manager.
            //if (currentBehavior == Behavior.FRIENDLY) <--- If My Behavior is Friendly with robert. Attack a random Robert enemy.
            //CombatResolver.ResolveAttack(ActionFinished, this, CombatManager.Instance.robert, (BodyObjetive)Random.Range(0, 3));

            
            float random = Random.Range(0.0f, 1.0f);
            // Mas de   20 -> 50% torso - 20%Piernas 20%Brazos 10%cabeza
            // Pre Projectile Stuff
            if (distanceToTarget > 20)
            {
                if ((random > 0) && (random < 0.2))
                    target = BodyObjetive.LEGS;
                else if ((random > 0.2) && (random < 0.4))
                    target = BodyObjetive.ARMS;
                else if ((random > 0.4) && (random < 0.5))
                    target = BodyObjetive.HEAD;
                else
                    target = BodyObjetive.TORSO;
            }
            // Menos de 20 -> 30% Head 30%Torso 30%Brazos 10%piernas
            else if ((distanceToTarget < 20) && (distanceToTarget > 10))
            {
                if ((random > 0) && (random < 0.3))
                    target = BodyObjetive.HEAD;
                else if ((random > 0.3) && (random < 0.6))
                    target = BodyObjetive.TORSO;
                else if ((random > 0.6) && (random < 0.9))
                    target = BodyObjetive.ARMS;
                else
                    target = BodyObjetive.LEGS;
            }
            // Menos de 10 -> 50%Head / 50%Torso
            else if (distanceToTarget < 10)
            {
                if ((random > 0) && (random < 0.5))
                    target = BodyObjetive.HEAD;
                else
                    target = BodyObjetive.TORSO;
            }
            //--------------------------------------

            /*
            if (distanceToTarget > 20)
            {
                if ((random > 0) && (random < 0.2))
                    CombatResolver.ResolveAttack(new AttackInfo(this, targetCharacter, BodyObjetive.LEGS, _damageType, getShootMode(), false, false, getAccuracy("Shooting")));
                else if ((random > 0.2) && (random < 0.4))
                    CombatResolver.ResolveAttack(new AttackInfo(this, targetCharacter, BodyObjetive.ARMS, _damageType, getShootMode(), false, false, getAccuracy("Shooting")));
                else if ((random > 0.4) && (random < 0.5))
                    CombatResolver.ResolveAttack(new AttackInfo(this, targetCharacter, BodyObjetive.HEAD, _damageType, getShootMode(), false, false, getAccuracy("Shooting")));
                else
                    CombatResolver.ResolveAttack(new AttackInfo(this, targetCharacter, BodyObjetive.TORSO, _damageType, getShootMode(), false, false, getAccuracy("Shooting")));
            }
            // Menos de 20 -> 30% Head 30%Torso 30%Brazos 10%piernas
            else if ((distanceToTarget < 20) && (distanceToTarget > 10))
            {
                if ((random > 0) && (random < 0.3))
                    CombatResolver.ResolveAttack(new AttackInfo(this, targetCharacter, BodyObjetive.HEAD, _damageType, getShootMode(), false, false, getAccuracy("Shooting")));
                else if ((random > 0.3) && (random < 0.6))
                    CombatResolver.ResolveAttack(new AttackInfo(this, targetCharacter, BodyObjetive.TORSO, _damageType, getShootMode(), false, false, getAccuracy("Shooting")));
                else if ((random > 0.6) && (random < 0.9))
                    CombatResolver.ResolveAttack(new AttackInfo(this, targetCharacter, BodyObjetive.ARMS, _damageType, getShootMode(), false, false, getAccuracy("Shooting")));
                else
                    CombatResolver.ResolveAttack(new AttackInfo(this, targetCharacter, BodyObjetive.LEGS, _damageType, getShootMode(), false, false, getAccuracy("Shooting")));
            }
            // Menos de 10 -> 50%Head / 50%Torso
            else if (distanceToTarget < 10)
            {
                if ((random > 0) && (random < 0.5))
                    CombatResolver.ResolveAttack(new AttackInfo(this, targetCharacter, BodyObjetive.HEAD, _damageType, getShootMode(), false, false, getAccuracy("Shooting")));
                else
                    CombatResolver.ResolveAttack(new AttackInfo(this, targetCharacter, BodyObjetive.TORSO, _damageType, getShootMode(), false, false, getAccuracy("Shooting")));
            }
            */
            if (isPackLeader)
            {
                if (_damageType == DamageType.MELEE)
                    EventManager.TriggerEvent("OnAttackingMelee"); ///PPP dónde está este evento?
                else
                    EventManager.TriggerEvent("OnAttackingRanged");
            }

			/*
			 * Temporalmente desactivados proyectiles.
            ProjectileManager projectileManager = this.gameObject.GetComponent<ProjectileManager>();

            if (projectileManager != null)
            {
                if (projectileManager.projectilePrefab != null)
                {
                    CombatManager.Instance.ReinitActor(this);
                    projectileManager.shootTo(CombatManager.Instance.robert, target, postShooting, ActionFinished);

                    if (soundManager == null)
                    {
                        if (!GameManager.Instance.levelManager.enemiesSoundManager1.isBusy)
                            soundManager = GameManager.Instance.levelManager.enemiesSoundManager1;
                        else if (!GameManager.Instance.levelManager.enemiesSoundManager2.isBusy)
                            soundManager = GameManager.Instance.levelManager.enemiesSoundManager2;
                    }

                    soundManager.playFX(selectedWeapon.ShootSound); //PPP Temporalmente apagado aunque puede ser útil-
                }
            }
            else
            */
            {
				/// Sound Manager Stuff - Copiado del If Anterior.
				if (soundManager == null) 
				{
					if (!GameManager.Instance.levelManager.enemiesSoundManager1.isBusy)
                    {
                        soundManager = GameManager.Instance.levelManager.enemiesSoundManager1;

                    }
					else if (!GameManager.Instance.levelManager.enemiesSoundManager2.isBusy)
                    {soundManager = GameManager.Instance.levelManager.enemiesSoundManager2;

                    }
						
				}
				
				//soundManager.playFX(selectedWeapon.ShootSound); //PPP temporalmente apagado aunque puede ser útil.

				postShooting();
            }
        }
    }



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //EFECTOS DEL ATAQUE
    //En esta función están la gran parte de los resultados visuales del ataque

    rastroBala rastro; ///PPP Acá está el tiro.
    void postShooting()
    {
        
        if (targetCharacter == null)
        {
            FixedTurnEnded();
        }
        else
        {
            if (canMakeAction(AttackCost))
            {

                realizedAction(AttackCost);

                if (rastro == null) //Buscá si el rastro está almacenado
                     rastro = GameObject.Find("rastro").GetComponent<rastroBala>(); //Si no está, buscá el game object en la escena y tomá el script.
                if (rastro == null) //Si no lo encontraste...
                {
                    Debug.LogWarning("No encontró el rastro, no puede tirar");
                    
                    ///CombatResolver.ResolveAttack(new AttackInfo(this, targetCharacter, target, _damageType, getShootMode(), false, false, getAccuracy("Shooting")), callFixedTurnEnded); //No tiene sentido poner esto acá, y genera confusión. El rastro tiene que estar siempre
                   
                }
                else //Si encontraste el rastro...
                {
                    if (CombatManager.Instance.COMBAT_FAST)//Si el combate está en modo rápido simplemente guardá la info y no corras ninguna animación.
                    {
                        CombatResolver.ResolveAttack(new AttackInfo(this, targetCharacter, target, _damageType, getShootMode(), false, false, getAccuracy("Shooting"), getBulletType(), _selectedWeapon), callFixedTurnEnded);
                    }
                    else
                    {

                        
                        disparoAnim.transform.GetChild(3).GetComponent<Image>().overrideSprite = primerPlano; //ZZZ

                        //StartCoroutine(Esperar(1f));
                        tarjetas.SwitchChilds(false);
                        //combatMode.SetActive(false);
                        disparoAnim.SetActive(true);//Activa la animación del disparo.
                        

                        //Esto es para que cuando le pegue a la pierna el tiro vaya a la pierna.
                        objetivo = robertEsqueleto;


                        if (target == BodyObjetive.HEAD) { objetivo = cabeza; }
                        if (target == BodyObjetive.TORSO) { objetivo = torso; }
                        if (target == BodyObjetive.LEGS) { objetivo = pierna; }
                        if (target == BodyObjetive.ARMS) { objetivo = brazo; }

                    
                        StartCoroutine(Espera(1.07f));//Esta corutina es para que disparen cuando sea el momento adecuado en base a la animación que aparece arriba.

                    }
                }
            }
            else
            {
                FixedTurnEnded();
            }
        }
    }

    IEnumerator Esperar(float segundos) {
        yield return new WaitForSeconds(segundos);
    
    }

    IEnumerator Espera(float segundos) //PPP Función para hacer que banque un toque a la animación antes de activar el rastro
    {
        yield return new WaitForSeconds(segundos);


        Ray ray = new Ray(objetivo.position, lugarDelQueDispara.position - objetivo.position);
        //RaycastHit hit;
        RaycastHit[] hits;
        Debug.DrawRay(objetivo.position, lugarDelQueDispara.position - objetivo.position, Color.blue, 5f); //Este es para testeo nuestro
        
        int layerMask = (1 << 13) | (1 << 14) | (1 << 10);

        //El raycast lo tiramos desde Robert hacia el enemigos (o sea lo tiramos al revés) para que los hitpoints queden en la cara visible del collider.
        hits = Physics.RaycastAll(ray, Mathf.Infinity, layerMask);

        int fuerza = 100;

        for (var i = hits.Length -2; i >= 0 ; i--){ //"hits.length -2" xq queremos ignorar el collider del enemigo que hizo el disparo
            
            string tipo = hits[i].collider.tag;

            if (tipo == "Caja") { GameObject particula = Instantiate(rastro.impactos[2], hits[i].point, Quaternion.identity) as GameObject; fuerza -= 10; }
            if (tipo == "Roca") { GameObject particula = Instantiate(rastro.impactos[1], hits[i].point, Quaternion.identity) as GameObject; fuerza -= 20; }
            if (tipo == "Enemy") { GameObject particula = Instantiate(rastro.impactos[1], hits[i].point, Quaternion.identity) as GameObject; fuerza -= 10; }
            
        }

        //Debug.DrawRay(lugarDelQueDispara.position, hits[hits.Length -2].point - lugarDelQueDispara.position, Color.red, 5f); //Este es para testeo nuestro

        Debug.Log("el disparo impacto con " +(hits.Length -1) + " obstáculos");

        obj.transform.position = objetivo.position;

        //Este es el que activa la animación del fogonazo del tiro
        rastro.ConfigureFromTo(lugarDelQueDispara, obj.transform);
        rastro.activar = true; //PPP acá debería sonar el disparo.
        //Debug.DrawRay(lugarDelQueDispara.position, objetivo.position - lugarDelQueDispara.position, Color.red, 5f); //Este es para testeo nuestro
        //Acá hace el ataque concreto (!)
        CombatResolver.ResolveAttack(new AttackInfo(this, targetCharacter, target, _damageType, getShootMode(), false, false, getAccuracy("Shooting"), getBulletType(), _selectedWeapon), afterParticlesShoot, obstaculo);

    }




    /*
    IEnumerator Espera(float segundos) //PPP Función para hacer que banque un toque a la animación antes de activar el rastro
    { yield return new WaitForSeconds(segundos);


    Ray ray = new Ray(lugarDelQueDispara.position, objetivo.position - lugarDelQueDispara.position);
    //RaycastHit hit;
      RaycastHit[] hits;
    Debug.DrawRay(lugarDelQueDispara.position, objetivo.position - lugarDelQueDispara.position, Color.blue, 5f); //Este es para testeo nuestro a ver si 
    int layerMask = (1 << 13) | (1 << 14) | (1 << 10);


    //if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
    hits = Physics.RaycastAll(ray, Mathf.Infinity, layerMask);

        
         //hit.collider.bounds
         


        if (hits[0].transform.tag == "Player")
        {
            obstaculo = "Player";
            rastro.impactoNum = 0;
            //Debug.Log("El disparo dio contra el jugador");
        }

        else if (hits[0].transform.tag == "Enemy")
        {
            //Esto sería para friendly fire
            obstaculo = "Enemy";
            rastro.impactoNum = 0;

        }
        else if (hits[0].transform.tag == "Roca")
        {
            obstaculo = "Roca";
            rastro.impactoNum = 1;
            //Debug.Log("El disparo del enemigo dio contra una roca");
        }
        else if (hits[0].transform.tag == "Caja")
        {
            obstaculo = "Caja";
            rastro.impactoNum = 2;
            //Debug.Log("El disparo del enemigo dio contra una caja");

        }
        else
        {
            obstaculo = null;
            rastro.impactoNum = 0;
            //Debug.Log("error el disparo dio contra algo inespareado:" + hit.transform.name + " xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"); 
        }
    //}
    //else { Debug.Log ("El disparo del enemigo no está pegando en nada"); }
        Debug.Log("el disparo impacto con x objetos" + hits.Length);
    if (obstaculo != null && obstaculo != "Player")
    { objetivo.position = hits[0].point; obj.transform.position = hits[0].point; }
    else { obj.transform.position = objetivo.position; } //Si el disparo da en el jugador que no se frene en el hitpoint sino en la articulación.

     //Este es el que activa la animación del fogonazo del tiro
    rastro.ConfigureFromTo(lugarDelQueDispara, obj.transform);
    rastro.activar = true; //PPP acá debería sonar el disparo.
    Debug.DrawRay(lugarDelQueDispara.position, objetivo.position - lugarDelQueDispara.position, Color.red, 5f); //Este es para testeo nuestro
    //Acá hace el ataque concreto (!)
    CombatResolver.ResolveAttack(new AttackInfo(this, targetCharacter, target, _damageType, getShootMode(), false, false, getAccuracy("Shooting")), afterParticlesShoot, obstaculo); 
    
    }
    */

    //LLAMAR AL FIN DEL ATAQUE
    private void callFixedTurnEnded()
    {
        FixedTurnEnded();
        rastro.activar = false;
    }
    private void afterParticlesShoot()
    {
        Invoke("callFixedTurnEnded", 2.5f);
    }

    private void FinalizeTurn()
    {
        if (canMakeAction(AttackCost))
        {
            DoAction();
        }
        else
        {

            FixedTurnEnded();
        }

        Debug.Log(this.characterName + " finished");
  
    }
    /*
  checkUncounciousAftermath();

  // Temporalmente dehabilitado, para usar nuevo sistema de turnos.
  //CombatManager.Instance.FinalizeAttack();

  CombatManager.Instance.debugPanel.SubmitText("" + characterName + " Turn Ended");
  CombatManager.Instance.debugPanel.SubmitText("---------------------------------");

        
  if (!_isAlive)
      return;

  CombatManager.Instance.ReinitActor(this);
  */ //Texto comentado de la función anterior

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////7



    //RECIBIR DAÑO

    public override int getDamage()
    {

        int rawDamage = selectedWeapon.Damage;
        return UnityEngine.Random.Range((rawDamage - ((rawDamage * 30) / 100)), (rawDamage + ((rawDamage * 30) / 100)));
        //Esta función estaba a medio hacer con mucho texto comentado que está colapsada acá abajo del corchete
    }
    /*
int rawDamage = 0;
if (damageType == DamageType.MELEE)
{
    foreach (var prop in mainWeapon.properties)
        if (prop.name == "Melee Damage")
            rawDamage = prop.intValue;
}
else if (damageType == DamageType.RANGED)
{
    foreach (var prop in mainWeapon.properties)
        if (prop.name == "Ranged Damage")
            rawDamage = prop.intValue;
}
else if (damageType == DamageType.THROWING)
{
    foreach (var prop in mainWeapon.properties)
        if (prop.name == "Throwing Damage")
            rawDamage = prop.intValue;
}

if (rawDamage > 0)
{
    rawDamage = UnityEngine.Random.Range((rawDamage - ((rawDamage * 30) / 100)), (rawDamage + ((rawDamage * 30) / 100)));
}

return rawDamage;
*/ //partes comentadas al final del script anterior


    //RECIBIR DAÑO**********************************************
    public override void recieveDamage(AttackResult attackInfo)
    {
        base.recieveDamage(attackInfo);

        EnemyHistory.Instance.UpdateEnemy(serializableEnemy);

        if (enemyType == EnemyType.MARAUDER)
            CombatManager.Instance.OnMarauderAttacked.Invoke();

        if (_isAlive)
        {
            // Camera FX Temporalmente Deshabilitados:
            /*
            if (attackInfo.attacker is Robert)
            {
                Camera.main.GetComponent<CameraBleed>().DoBleedFX();
            }
            DoShake();
            */
            //container.triggerCardVFX(CardVisualFxTriggers.BLEEDING_TICK, null);

            if (attackInfo.isCriticalHit)
            {
                checkBleedTrigger();
                CombatManager.Instance.NpcFXController.PlayEventSound("HeavyDamage");
            }

            if (attackInfo.bulletType != BulletType.Normal)
            {
                if (attackInfo.bulletType == BulletType.Poison)
                    checkPoisonedTrigger();
                else if (attackInfo.bulletType == BulletType.Radiated)
                    checkRadiatedTrigger();
            }

            if (attackInfo.bodyObjective == BodyObjetive.HEAD)
                checkBlindedTrigger(); // <---  Remove and uncomment next.
            //if (checkBlindedTrigger())
            //container.triggerCardVFX(CardVisualFxTriggers.BLEEDING_TRIGGER, null);

            else if (attackInfo.bodyObjective == BodyObjetive.ARMS)
                checkParalizedArmsTrigger(isPackLeader); // Temporal Protectionn to Pack Leader
            else if (attackInfo.bodyObjective == BodyObjetive.LEGS)
                checkParalizedLegsTrigger(isPackLeader); // Temporal Protectionn to Pack Leader

            scriptTarjeta._healtBar.value = characterStats.Hitpoints;
            //_container.updateHealth(characterStats.Hitpoints, attackInfo.isCriticalHit);

            if (isPackLeader)
                EventManager.TriggerEvent("OnHit");
            else
            {
                //NPCSFXControl es un script, la función PlayOnHit, corre un grito en base a si es varón o mujer
                CombatManager.Instance.NpcFXController.PlayOnHitFx(genre); //PPP comentada por mí para que el sonido suene cuando debe sonar
                
                
                
                
                /*
                if (soundManager == null)
                {
                    if (!GameManager.Instance.levelManager.enemiesSoundManager1.isBusy)
                        soundManager = GameManager.Instance.levelManager.enemiesSoundManager1;
                    else if (!GameManager.Instance.levelManager.enemiesSoundManager2.isBusy)
                        soundManager = GameManager.Instance.levelManager.enemiesSoundManager2;
                }
                soundManager.playFX(onHitFX.Next());
                */
            }
        }
    }



    protected override void recieveDamage(int damage)
    {
        base.recieveDamage(damage);

        if (enemyType == EnemyType.MARAUDER)
            CombatManager.Instance.OnMarauderAttacked.Invoke();

        if (_isAlive)
        {
            scriptTarjeta._healtBar.value = characterStats.Hitpoints;
            //_container.updateHealth(characterStats.Hitpoints);

            if (soundManager == null)
            {
                if (!GameManager.Instance.levelManager.enemiesSoundManager1.isBusy)
                    soundManager = GameManager.Instance.levelManager.enemiesSoundManager1;
                else if (!GameManager.Instance.levelManager.enemiesSoundManager2.isBusy)
                    soundManager = GameManager.Instance.levelManager.enemiesSoundManager2;
            }
            soundManager.playFX(onHitFX.Next());
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //ACCIONES



    public void AttackAction()
    {
        //hideInteractions();

        if (CombatManager.Instance.precisionTargeting)
        {
            targets.SetActive(true);
            Invoke("hideTargets", 3.0f);
        }
        else
        {
            BeingAttacked("TORSO");
        }
    }

    //--------------------------------------------------------
    public void BeingAttacked(string objetive)
    {

        if (objetive == "HEAD")
            targetedPart = BodyObjetive.HEAD;
        else if (objetive == "ARMS")
            targetedPart = BodyObjetive.ARMS;
        else if (objetive == "TORSO")
            targetedPart = BodyObjetive.TORSO;
        else if (objetive == "LEGS")
            targetedPart = BodyObjetive.LEGS;

        CombatManager.Instance.attackSelected(this, targetedPart);
        CombatManager.Instance.NpcFXController.PlayEventSound("TargetHumanFigure");
    }

    //--------------------------------------------------------
    public void TalkAction()
    {
        DialogCombatManager.Instance.talkSelected(this);
        GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().combatDialog.SetActive(true);

    }

    //--------------------------------------------------------

    public override void ActionFinished()
    {
        //Invoke("FinalizeTurn", CombatManager.Instance.TIME_DELAY_FOR_TURNS);
        FinalizeTurn();
    }


    protected void hideTargets()
    {   
        CancelInvoke("hideTargets");
        targets.SetActive(false);
    }



    //--------------------------------------------------------
    
    public void AttackByDefault()
    {
        //if (!CombatManager.Instance.robert.isOnAction)
        CombatManager.Instance.Logger.SubmitText("Robert Targets Torso");
        targetedPart = BodyObjetive.TORSO;
        CombatManager.Instance.attackSelected(this, targetedPart);
    }



    //----------------------------------------------------------------------------------




    //MUERTE DEL PERSONAJE
    public override void OnCharacterDeath()
    {

        /////////////////////////////PPPP 


        Vector3 tarjetaPosicion = tarjeta.transform.position; //Guardamos la posición de la tarjeta antes de deshabilitarla.
        //Destroy(tarjeta);
        tarjeta.SetActive(false); //deshabilitamos al tarjeta.

        //Instanciación de la tarjeta explosiva.
        Instantiate(Resources.Load("tarjeta"), tarjetaPosicion, Quaternion.identity);
   
        /*
        //Instanciación del muerto
        instancia = Instantiate(Resources.Load("muerto 1"), this.transform.position, Quaternion.identity) as GameObject;
        instancia.transform.localScale = robertTransform.localScale; //Le asigna el tamaño al muerto en base a Robert.
        instancia.transform.localPosition += new Vector3(0, -3.5f, 0); //Offset
         */
        
        /////////////////////////////PPPP 

        EnemyHistory.Instance.KillEnemy(serializableEnemy);
        
         
        container.characterWillDie();

        if (isActing)
        {
            _isActing = false;
            FinalizeTurn();
            CombatManager.Instance.FinalizeAttack();
        }

        CombatManager.Instance.OnCharacterDies.Invoke(this);

        if (isPackLeader)
        {
            EventManager.TriggerEvent("OnDeath");
            CombatManager.Instance.packLeaderIsAlive = false;
        }
        else
        {
            if (CombatManager.Instance.packLeaderIsAlive)
                EventManager.TriggerEvent("OnAnotherEnemyDies");

            if (soundManager == null)
            {
                if (!GameManager.Instance.levelManager.enemiesSoundManager1.isBusy)
                    soundManager = GameManager.Instance.levelManager.enemiesSoundManager1;
                else if (!GameManager.Instance.levelManager.enemiesSoundManager2.isBusy)
                    soundManager = GameManager.Instance.levelManager.enemiesSoundManager2;
            }
            soundManager.playFX(onHitFX.Next());
        }



        /////////////////////////////////////LOOT///////////////////////////////


        Debug.Log("Adding Dead Character Items to Loot List");
        LevelManager manager = GameManager.Instance.levelManager;


        for (int i = 0; i < _availableWeapons.Count; i++)
            ISInventoryManager.instance.lootWindow.addItem(_availableWeapons[i]);
        for (int i = 0; i < _consumables.Count; i++)
            ISInventoryManager.instance.lootWindow.addItem(_consumables[i]);
        for (int i = 0; i < _bullets.Count; i++)
            ISInventoryManager.instance.lootWindow.addItem(_bullets[i]);

        /////////////////////////////////////////////////////////////////////////

        /// XP
        if (_thisCharacter == null)
        {
            AnalizeCharacterType();
        }
        int experiencia = Random.Range(_thisCharacter.MinAwardedExp, _thisCharacter.MaxAwardedExp);
        CombatManager.Instance.UnRegister(this);
        CombatManager.Instance.Logger.SubmitText(this.characterName + " dies");
        CombatManager.Instance.robert.AddXP(experiencia);

        StartCoroutine ("EsperarAnimacionRobert");
            /*
        if ((gameObject != null) && (this.gameObject != null))
            GameObject.Destroy(this.gameObject);*/
        
        ///Fin XP
        //base.OnCharacterDeath(); COMENTADO PARA CORREGIR MENSAJE DE XP DESPUES QUE APAREZCA QUE EL ENEMIGO MURIO.
    }


    private IEnumerator EsperarAnimacionRobert(){

        while (dispararIK.bajandoElBrazo)
        {
        yield return null;
        }
        if ((gameObject != null) && (this.gameObject != null))
            GameObject.Destroy(this.gameObject);
    }

            


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //FUNCIONES QUE PARECEN OBSOLETAS


    //----------------------------------------------------------------------------------
    //SHAKE
    //Estas funciones son actualmente obsoleta.
    //Al parecer hacía temblar a la tarjeta ante un ataque.

    private float duration;
    private float magnitude;

    public void DoShake(float duration = 0.6f, float magnitude = 2.5f)
    {
        this.duration = duration;
        this.magnitude = magnitude;

        StartCoroutine(Shake());
    }


    
    IEnumerator Shake()
    {
        float elapsed = 0.0f;

        Vector3 originalPosition = transform.position;

        while (elapsed < duration)
        {

            elapsed += Time.deltaTime;

            float percentComplete = elapsed / duration;
            float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

            // map value to [-1, 1]
            float x = Random.value * 2.0f - 1.0f;
            float y = Random.value * 2.0f - 1.0f;
            x *= magnitude * damper;
            y *= magnitude * damper;

            transform.position = new Vector3(originalPosition.x + x, originalPosition.y + y, originalPosition.z);

            yield return null;
        }

        transform.position = originalPosition;
    }


    //----------------------------------------------------------------------------------


    public void CalculateDistance(bool actualizaSoloTarjetaActiva){
        //Debug.Log("robertSeEstaMoviendo");

        //Actualiza la distancia en tiempo real para la tarjeta activa
        if (actualizaSoloTarjetaActiva)
        {
            if (soyTarjetaActiva)
            {
                distanceToTarget = CombatResolver.calculateDistance(this.gameObject, robertTransform.gameObject);
                scriptTarjeta.distancia = distanceToTarget;
            }
        }

        //Actualiza la distancia una sola vez para todas las tarjetas una vez que Robert se terminó de mover
        else 
        {
            distanceToTarget = CombatResolver.calculateDistance(this.gameObject, robertTransform.gameObject);
            scriptTarjeta.distancia = distanceToTarget;
        }
    
    
    }
}
