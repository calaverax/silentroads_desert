﻿using UnityEngine;
using System.Collections;

public class RobertAnimationsController : MonoBehaviour {

    // Use this for initialization
    Robert _robert;

	void Awake ()
    {
        _robert = this.transform.parent.GetComponent<Robert>();
	}

    private void ShootFrameEvent()
    {
        _robert.ShootEvent();
    }

    private void ShowFireFx()
    {
        _robert.showFireFX();
    }
	
}
