﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Wasabi.InventorySystem;
using Wasabi.InventorySystem.Inventory;

public class Robert : BaseCharacter
{
    // Control de Modelo 3D
    [SerializeField]DispararIK _dispararIK;


    // General
    public Sparky sparky;
    public RobertSoundsManager robertSoundManager;
    private IngameGUIController _ingameGuiController;
    [SerializeField]
    private Animator _robertAnimator, _shootingAnimator;

    //PPP
    public bool bang = false;
    private GameObject robert;//PPP Robert.
    private Animator robertAnim;//PPP El animator de Robert.
    public Transform targetCuerpo; //PPP El lugar del cuerpo al que va a ir el tiro.
    public Transform hombro; //es el hueso del hombro, sirve para sabaer hacia donde va a tirar.
    private string obstaculo; //Este te dice si el disparo pegó con algo y con qué pegó.
    private rastroBala rastro;

    // Combat
    private bool _canDoAction = false;
    public bool canDoAction { get { return _canDoAction; } }

    private BaseEnemy _enemyToAttack; //Fijate que esto no es el enemigos sino el scrip BaseEnemy.
    private BodyObjetive _enemyTarget; //Esto define a dónde va el tiro, pero es un enum con cuatro posibilidades {HEAD, TORSO, ARMS, LEGS}
    private bool _firstGameplayAction = true;
    private float _focus = 1.2f;

    DialogDefinition _selectedDialog;
    private int shootsAmount = 0;
    private bool _isHidden = false;
    public bool isHidden { get { return _isHidden; } }

    [SerializeField]
    private ActionPoolManager _actionPoolManager;
    private PoolableAction _lastAction = null;

    private EquipmentSlot _selectedEquipmentSlot = EquipmentSlot.MAIN_HAND;
    public EquipmentSlot selectedEquipmentSlot { get { return _selectedEquipmentSlot; } }

    public GameObject targetpicker;

    //-----------------------------------------------------------------------------------------------------------
    void Awake()
    {
        //this.gameObject.transform.localPosition = new Vector3(0, 0, 0);
        generateBasicStats();
        processHitPoints();
        processMight();
        
    }
    //-----------------------------------------------------------------------------------------------------------
    void OnDisable()
    {
        /* METODO VIEJO
        PlayerPrefs.SetInt(CharacterStats.StatHitpoints, characterStats.Hitpoints);
        PlayerPrefs.SetInt(CharacterStats.Statwater, characterStats.water);
        PlayerPrefs.SetInt(CharacterStats.StatMight, characterStats.Might);

        //PlayerPrefs.SetInt(CharacterStats.StatTotalHitpoints, characterStats.TotalHitpoints);
        PlayerPrefs.Save();
        */

        PlayerDataManager.Instance.SetValue(CharacterStats.StatHitpoints, characterStats.Hitpoints);
        PlayerDataManager.Instance.SetValue(CharacterStats.Statwater, characterStats.water);
        PlayerDataManager.Instance.SetValue(CharacterStats.StatMight, characterStats.Might);
        PlayerDataManager.Instance.Save();

    }
    //-----------------------------------------------------------------------------------------------------------
    void Start()
    {

        robert = GameObject.FindGameObjectWithTag("Player"); //PPP
        robertAnim = robert.GetComponent<Animator>();//PPP
        CombatManager.Instance.OnCombatResumed.AddListener(CombatResumed);
        //----------------------
        ISInventoryManager.instance.OnItemEquiped.AddListener(ItemEquiped);
        ISInventoryManager.instance.OnItemUnequiped.AddListener(ItemUnequiped);
        ISInventoryManager.instance.OnItemUsed.AddListener(ItemConsumed);
        hombro = robert.transform.GetChild(1).transform.GetChild(5).transform.GetChild(0).transform.GetChild(2).transform.GetChild(0).transform.GetChild(0).transform.GetChild(2).transform.GetChild(0).transform.GetChild(0).transform.GetChild(0);


        rastro = GameObject.Find("rastro_Robert").GetComponent<rastroBala>();
        /*
        if ((GameManager.Instance.selectedChapter == 1) &&
            ((GameManager.Instance.selectedLevel == 1) || (GameManager.Instance.selectedLevel == 112)))
            _isHidden = false;
        else
            _isHidden = true;
        */
        // Temporary disable Hiding.
        _isHidden = false;

        if (_isHidden)
        {
            foreach (SpriteRenderer sp in gameObject.GetComponentsInChildren<SpriteRenderer>())
                sp.color = new Color(sp.color.r, sp.color.g, sp.color.b, 0.6f);
        }

        CombatManager.Instance.combatStarted = true;

        Invoke("ProcessEquipment", 0.1f);

        targetpicker = GameObject.FindGameObjectWithTag("targetPicker");
    }
    
    //-----------------------------------------------------------------------------------------------------------
    public override void FixedTurnStarted()
    {
        //Debug.Log("Robert Fixed Turn Started");

        //targetpicker.SetActive(true);

        base.FixedTurnStarted();

        //processTurnActionPoints();

        if (_actionPoolManager.Count == 0)
        {
            FixedTurnEnded(false);
            CombatManager.Instance.Logger.SubmitText("Robert turn skipped, no actions defined");
        }
        else
        {
            CombatManager.Instance.Logger.SubmitText("Robert turn started"); 
            _actionPoolManager.pooledActions[0].action.callBack();
        }
    }
	public override void FixedTurnEnded (bool announce = true)
	{
        //targetpicker.SetActive(false);

        if (announce) CombatManager.Instance.Logger.SubmitText("Robert turn finalized");

        //Debug.Log("Robert Fixed Turn Ended");
        base.FixedTurnEnded ();
    }
    //-----------------------------------------------------------------------------------------------------------
    public override void DoAction() // Init Robert's Turn.
    {
        if (_attacksDone == 0)
        {
            CombatManager.Instance.debugPanel.SubmitText("---------------------------------");
            CombatManager.Instance.debugPanel.SubmitText("" + characterName + " Turn Started");
        }

        if (characterStats.Hitpoints >= characterStats.TotalHitpoints * 0.3)
        {
            if (robertSoundManager.LowHP.isPlaying)
                robertSoundManager.stopLowHP();
        }
        else
        {
            if (!robertSoundManager.LowHP.isPlaying)
                robertSoundManager.playLowHP();
        }

        _attacksPerTurn = FocusSystemController.instance.attackBonusAmnt + 1;

        _canDoAction = true;

        characterStats.checkPenalties();

        checkAbnormalStates(continueAction);

    }
    //-----------------------------------------------------------------------------------------------------------
    private void continueAction()
    {
        // Si hay Acciones en el Pool, llamamos el Callback de la ultima de la lista.
        if (_actionPoolManager.pooledActions.Count > 0)
        {
            _actionPoolManager.pooledActions[0].action.callBack();
        }
        else // Si no hay Acciones en el pool, verificamos nuestro LastAction
        {
            // Si puede Realizar Accion:
            if (canDoAction)
            {
                // Si existe una LastAction la Realizamos.
                if (_lastAction != null)
                    _lastAction.callBack();
            }
        }
    }
    //-----------------------------------------------------------------------------------------------------------
    private bool isGameFirstAction()
    {
        // Si el Pool esta Vacio, y No existe Last Action (Significa que estamos en la primer Action del Juego)
        if ((_actionPoolManager.pooledActions.Count == 0) && (_lastAction == null))
            return true;

        return false;
    }
    //-----------------------------------------------------------------------------------------------------------
    #region IngameActions
    //-----------------------------------------------------------------------------------------------------------
    private void CombatResumed()
    {
        // Check if we are on our Turn.
        if (_canDoAction)
        {
            continueAction();
        }
    }
    //--------------------------------------------------------------------------------------------------------------
    public void hideAction()
    {
        // Si el Combate esta pausado
        // - Agregar La accion al pool
        if (CombatManager.Instance.pausedCombat)
        {
            _actionPoolManager.AddAction(new PoolableAction(pooledHide));
        }
        else
        {
            if (_canDoAction)
                hide();
            else
                _actionPoolManager.AddAction(new PoolableAction(pooledHide));
        }
    }
    //--------------------------------------------------------------------------------------------------------------
    public void pooledHide()
    {
        _actionPoolManager.RemoveFirstAction();
        hide();
    }
    //--------------------------------------------------------------------------------------------------------------
    private void hide()
    {
        if (_isActing) return;

        _isActing = true;
        Invoke("ActionFinished", 0.5f);

        CombatManager.Instance.Logger.SubmitText("Cannot Hide");
    }
    //--------------------------------------------------------------------------------------------------------------
    public void waitAction()
    {
        // TODO: Review this Action Entirely.

        /*
        hideInteractions();

        if (CombatManager.Instance.pausedCombat)
        {
            CombatManager.Instance.OnCombatResumed.AddListener(combatResumedAferPool);
            _pooledAction = wait;
            return;
        }

        // Do Wait Action
        if (_canDoAction)
            wait();
        else
            _pooledAction = wait;

        _lastAction = wait;
        */
    }
    //--------------------------------------------------------------------------------------------------------------
    private void wait()
    {
        // TODO: Review this Action Entirely.
        /*
        if (_isActing) return;
        _isActing = true;
        _pooledAction = null;
        Invoke("ActionFinished", 0.5f);

        CombatManager.Instance.Logger.SubmitText("Robert Is Waiting");
        */
    }
    //--------------------------------------------------------------------------------------------------------------
    public void fleeAction()
    {
        // Si el Combate esta pausado
        // - Agregar La accion al pool
        if (CombatManager.Instance.pausedCombat)
        {
            _actionPoolManager.AddAction(new PoolableAction(pooledFlee));
        }
        else
        {
            if (_canDoAction)
                flee();
            else
                _actionPoolManager.AddAction(new PoolableAction(pooledFlee));
        }
    }
    //--------------------------------------------------------------------------------------------------------------
    public void pooledFlee()
    {
        _actionPoolManager.RemoveFirstAction();
        flee();
    }
    //--------------------------------------------------------------------------------------------------------------
    private void flee()
    {
        if (_isActing) return;

        _isActing = true;
        Invoke("ActionFinished", 0.5f);
        CombatManager.Instance.Logger.SubmitText("Robert Tries to run away");
    }
    //--------------------------------------------------------------------------------------------------------------
    public void attackOnTarget(BaseCharacter enemy, BodyObjetive target)
    {
        //if (!CombatManager.Instance.combatStarted) return;
        if (!CombatManager.Instance.combatStarted)
            CombatManager.Instance.combatStarted = true;

        if (_selectedWeapon == null)
            getSelectedWeapon();


        if (_selectedWeapon.ShootsInRound < (int)_shootMode)
        {
            string clipName = "Reload" + UnityEngine.Random.Range(1, 3).ToString();
            CombatManager.Instance.NpcFXController.PlayEventSound(clipName);
            CombatManager.Instance.Logger.SubmitText("Not Enough Ammo in " + _selectedWeapon.Name);
            return;
        }


        if (_isActing)
        {
            _actionPoolManager.AddAction(new AttackPoolableAction(pooledAttack, enemy, target));
            return;
        }

        if (selectedWeapon == null)
        {
            CombatManager.Instance.Logger.SubmitText("<color=red>NO WEAPON EQUIPED</color>");
            return;
        }

        if (CombatManager.Instance.pausedCombat)
        {
            _actionPoolManager.AddAction(new AttackPoolableAction(pooledAttack, enemy, target));
        }
        else
        {
            if (_canDoAction)
            {
                attack();
                _enemyTarget = target;
                _enemyToAttack = (BaseEnemy)enemy;
            }
            else
            {
                _actionPoolManager.AddAction(new AttackPoolableAction(pooledAttack, enemy, target));
            }
        }
    }
    //--------------------------------------------------------------------------------------------------------------
    private void pooledAttack()
    {
        if (!canMakeAction(AttackCost))
        {
            CombatManager.Instance.debugPanel.SubmitText("No Action Points To Perform Action");
            FixedTurnEnded();
            return;
        }

        AttackPoolableAction newAction = _actionPoolManager.GetFirstEntry().action as AttackPoolableAction;

        _enemyTarget = newAction.bodyObjective;
        _enemyToAttack = (BaseEnemy)newAction.target;

        _actionPoolManager.RemoveFirstAction();

        if (_selectedWeapon.ShootsInRound < (int)_shootMode)
        {
            string clipName = "Reload" + UnityEngine.Random.Range(1, 3).ToString();
            CombatManager.Instance.NpcFXController.PlayEventSound(clipName);
            CombatManager.Instance.Logger.SubmitText("Not Enough Ammo in " + _selectedWeapon.Name);
            return;
        }

        if ((_enemyToAttack == null) || ((_enemyToAttack != null && _enemyToAttack.isAlive == false)))
            FinalizeTurn();
        else
            attack();
        //attackAction(_enemyToAttack, _enemyTarget);
    }
    //--------------------------------------------------------------------------------------------------------------
    
    private void attack()
    {
        _isActing = true;

        if (_enemyToAttack == null)
        {
            _lastAction = null;
            _isActing = false;
            return;
        }
		// Desabilitado por que ya no se usara animator 2d.
        //_robertAnimator.SetTrigger("AnimateShoot");
		ShootEvent();
    }
    //--------------------------------------------------------------------------------------------------------------
    public void talkAction(DialogDefinition dialogDefinition)
    {
        if (!CombatManager.Instance.combatStarted)
            CombatManager.Instance.combatStarted = true;

        // Si el Combate esta pausado
        // - Agregar La accion al pool
        if (CombatManager.Instance.pausedCombat)
        {
            _actionPoolManager.AddAction(new TalkPoolableAction(pooledTalk, DialogCombatManager.Instance.talkingTarget, dialogDefinition));
        }
        else
        {
            if (_canDoAction)
            {
                _selectedDialog = dialogDefinition;
                talk();
                //robertAnim.SetBool("hablando", true);//PPP
            }
            else
                _actionPoolManager.AddAction(new TalkPoolableAction(pooledTalk, DialogCombatManager.Instance.talkingTarget, dialogDefinition));
        }
    }
    //--------------------------------------------------------------------------------------------------------------
    private void pooledTalk()
    {
        if (!canMakeAction(TalkCost))
        {
            CombatManager.Instance.Logger.SubmitText("No Action Points to Perfom Action");
            FixedTurnEnded();
            return;
        }

        TalkPoolableAction newAction = _actionPoolManager.GetFirstEntry().action as TalkPoolableAction;

        _selectedDialog = newAction.dialogDefinition;
        DialogCombatManager.Instance.talkSelected((BaseEnemy)newAction.target);

        _actionPoolManager.RemoveFirstAction();

        if ((DialogCombatManager.Instance.talkingTarget == null) || ((DialogCombatManager.Instance.talkingTarget != null && !DialogCombatManager.Instance.talkingTarget.isAlive)))
            FinalizeTurn();
        else
            talk();
    }
    //--------------------------------------------------------------------------------------------------------------
    private void talk()
    {
        if (!CombatManager.Instance.combatStarted) return;

        if (_isActing) return;
        _isActing = true;

        CombatManager.Instance.combatUsedDialogs.Add(_selectedDialog);

        realizedAction(TalkCost);
        DialogCombatManager.Instance.triggerDialog(_selectedDialog, ActionFinished);
        
    }
    //--------------------------------------------------------------------------------------------------------------
    public void reloadAction(ISObject item)
    {
        //if (!CombatManager.Instance.combatStarted) return;

        if (_selectedWeapon == null)
            getSelectedWeapon();

        if (_isActing)
        {
            _actionPoolManager.AddAction(new ConsumeItemPoolableAction(pooledReload, item, "Reload " + (item as ISBullet).caliber + " Ammo"));
            return;
        }

        if (selectedWeapon == null)
        {
            CombatManager.Instance.Logger.SubmitText("<color=red>NO WEAPON EQUIPED</color>");
            return;
        }

        if (CombatManager.Instance.pausedCombat)
        {
            _actionPoolManager.AddAction(new ConsumeItemPoolableAction(pooledReload, item, "Reload " + (item as ISBullet).caliber + " Ammo"));
        }
        else
        {
            if (_canDoAction)
            {
                reloadNow(item);
            }
            else
            {
                _actionPoolManager.AddAction(new ConsumeItemPoolableAction(pooledReload, item, "Reload " + (item as ISBullet).caliber + " Ammo"));
            }
        }
    }
    //--------------------------------------------------------------------------------------------------------------
    void reloadNow(ISObject item)
    {
        reloadAmmo(item as ISBullet);
    }
    //--------------------------------------------------------------------------------------------------------------
    void pooledReload()
    {

        if (!canMakeAction(ReloadCost))
        {
            CombatManager.Instance.Logger.SubmitText("No Action Points to Perfom Action");
            FixedTurnEnded();
            return;
        }
        ConsumeItemPoolableAction newAction = _actionPoolManager.GetFirstEntry().action as ConsumeItemPoolableAction;

        reloadAmmo(newAction.item as ISBullet);

        _actionPoolManager.RemoveFirstAction();

    }
    //--------------------------------------------------------------------------------------------------------------
    void reloadAmmo(ISBullet bullet)
    {
        realizedAction(ReloadCost);

        if (_selectedWeapon == null) getSelectedWeapon();

        if ((bullet == null) || (_selectedWeapon == null))
        {
            //FinalizeTurn(); ?????
            return;
        }

        if (bullet.caliber != _selectedWeapon.Caliber)
        {
            CombatManager.Instance.Logger.SubmitText(bullet.Name + " are not suitable for your selected weapon");
            //FinalizeTurn(); ?????
            return;
        }

        int bulletsMissing = _selectedWeapon.MaxShootsPerRound - _selectedWeapon.ShootsInRound;

        // If no need to reload, just skip.
        if (bulletsMissing == 0) return;


        if (bullet.Stack > bulletsMissing)
        {
            _selectedWeapon.ShootsInRound += bulletsMissing;
            bullet.Stack -= bulletsMissing;
        }
        else
        {
            _selectedWeapon.ShootsInRound += bullet.Stack;
            bullet.Stack = 0;
        }

        EventManager.TriggerEvent("BulletConsumed");
        CombatManager.Instance.Logger.SubmitText("Used " + bullet.Name);

        if (_selectedWeapon.EquipSlot == EquipmentSlot.MAIN_HAND)
            CombatManager.Instance.NpcFXController.PlayEventSound("ReloadLarge");
        else if (_selectedWeapon.EquipSlot == EquipmentSlot.OFF_HAND)
            CombatManager.Instance.NpcFXController.PlayEventSound("ReloadSmall");

        _ingameGuiController.processWeaponBullets(_selectedWeapon);

        FinalizeTurn();
    }
    //--------------------------------------------------------------------------------------------------------------
    #endregion
    //--------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------
    #region CallBacksAndFX
    //--------------------------------------------------------------------------------------------------------------
    public void showFireFX()
    {
        _shootingAnimator.SetTrigger("TriggerExplotion");
    }
    //--------------------------------------------------------------------------------------------------------------
    public void ShootEvent()
    {
        // TODO: Get sound From Weapon
        //if (_shootMode == ShootMode.SINGLE)
        //robertSoundManager.playWeaponShooting(selectedWeapon.ShootSound); ///PPP la cambié de lugar
        //else if (_shootMode == ShootMode.AUTO)

        //else if (_shootMode == ShootMode.BURST)

        /*
		 * Desactivado Temporalmente ProjectileManager, para efectuar el disparo inmediato, sin proyectil.

		ProjectileManager projectileManager = this.gameObject.GetComponent<ProjectileManager>();
        
        if (projectileManager != null)
            projectileManager.shootTo(_enemyToAttack, _enemyTarget, afterSoot, shootCollided);
        else
        */


        //PPPP
        //- Agregar Disparo on DispararIK, llamando al callback afterShoot;
        if (_dispararIK == null)
        {
            afterSoot(); //Guarda que esto está vacío
        }
        else
        {
            if (CombatManager.Instance.COMBAT_FAST)
            {
                afterSoot(); //Guarda que está vacía esta función-
            }
            else
            {
            //Recordemos que _controlDisparo es el dispararIK.cs, lo que hace acá es meterle a la Acción callback que creó en ese script, una función de este script (aftersoot)
            _dispararIK.callBack = afterSoot; //Vacío, vacío.
 
            
            Transform PartesDelCuerpo = _enemyToAttack.transform.GetChild(6);//Este sirve para almacenar el gameobject en que están las partes del cuerpo

            //Determina el targetCuerpo al que va a ir el tiro, en base al _enemyTarget
            if (_enemyTarget == BodyObjetive.HEAD) { targetCuerpo = PartesDelCuerpo.GetChild(1); }
            if (_enemyTarget == BodyObjetive.TORSO){ targetCuerpo = PartesDelCuerpo.GetChild(2); }
            if (_enemyTarget == BodyObjetive.LEGS) { targetCuerpo = PartesDelCuerpo.GetChild(3); }
            if (_enemyTarget == BodyObjetive.ARMS) { targetCuerpo = PartesDelCuerpo.GetChild(4); }
            

                //PPP Acá se activa el rastro de la bala.
            _dispararIK.objetivo = targetCuerpo.transform.parent; //parte del cuerpo a la que voy a tirar


            _dispararIK.Ataque();
            //_dispararIK.ikDisparo = !_dispararIK.ikDisparo;
            StartCoroutine(Esperar());//PPP lo puse en una corutina para que tenga el delay adecuado.
            

            }
        }
        //-
        if (sparky.AImode == Sparky.Behavior.FOLLOW)
            sparky.RobertAttacking(_enemyToAttack);
    }
    IEnumerator Esperar(){

        ////////////////////////////////////////////Esto estaba en el after shoot, pero lo moví para que el disparo fuera más coherente.
        if (!CombatManager.Instance.robertShooted)
            CombatManager.Instance.robertShooted = true;

        if (_selectedWeapon == null) getSelectedWeapon();

        // TODO: Check Weapon Bullets going to -1 In Here! 
        _selectedWeapon.ShootsInRound -= (int)_shootMode;

        _ingameGuiController.processWeaponBullets(_selectedWeapon);
        /////////////////////////////////////////PPP Hasta acá lo del after shoot

        while (!bang) { 
            yield return null; 
        }
        bang = false;
        //Debug.Log("bang <-------------------------------------------------------FALSE");
        //yield return new WaitForSeconds(segundos);

        robertSoundManager.playWeaponShooting(selectedWeapon.ShootSound); //PPP sonido del disparo

        _dispararIK.obj.transform.position = targetCuerpo.position;
        
        //Debug.DrawRay(hombro.position, hits[0].point - hombro.position, Color.red, 5f); //Este es para testeo nuestro

        /*
        if (_dispararIK.rastro != null)
            _dispararIK.rastro.ConfigureFromTo(null, _dispararIK.obj.transform); //Acá se marca hacia dónde va el rastro del tiro.
         * */
        


        ///////////////////////////////////////////////////De acá para abajo tb estaba en el after shoot
        CombatResolver.ResolveAttack(new AttackInfo(this, _enemyToAttack, _enemyTarget, _damageType, _shootMode, false, _isHidden, getAccuracy("Shooting"), getBulletType(), _selectedWeapon), null, obstaculo); //PPP Esta es la función clave que calcula el disparo, está en el combat resolver.

        realizedAction(AttackCost);// Discount Action Points.
        ActionFinished();

        //////////////////////////////////////////////////Fin de lo que había en el after shoot
        
    }

    public void ChequearObstaculos() {
        Ray ray = new Ray(hombro.position, targetCuerpo.position - hombro.position);
        int layerMask = (1 << 13) | (1 << 14) | (1 << 10);
        RaycastHit[] hits;

        Debug.DrawRay(hombro.position, targetCuerpo.position - hombro.position, Color.blue, 5f); //Este es para testeo nuestro

        hits = Physics.RaycastAll(ray, Mathf.Infinity, layerMask);

        float fuerza = 100;

        //foreach (RaycastHit hit in hits)
        for (var i = 0; i <= hits.Length - 2; i++)
        { //"hits.length -2" xq queremos ignorar el collider del enemigo que hizo el disparo

            string tipo = hits[i].collider.tag;

            if (tipo == "Caja") { GameObject particula = Instantiate(rastro.impactos[2], hits[i].point, Quaternion.identity) as GameObject; fuerza -= 10; }
            if (tipo == "Roca") { GameObject particula = Instantiate(rastro.impactos[1], hits[i].point, Quaternion.identity) as GameObject; fuerza -= 20; }
            if (tipo == "Enemy") { GameObject particula = Instantiate(rastro.impactos[1], hits[i].point, Quaternion.identity) as GameObject; fuerza -= 10; }

        }
    }

    //--------------------------------------------------------------------------------------------------------------
    private void multipleShoots(int amount)
    {

    }
    //--------------------------------------------------------------------------------------------------------------
    private void shootCollided()
    {
        if (!CombatManager.Instance.robertShooted)
            CombatManager.Instance.robertShooted = true;

        if (_selectedWeapon == null) getSelectedWeapon();

        // TODO: Check Weapon Bullets going to -1 In Here! 
        _selectedWeapon.ShootsInRound -= (int)_shootMode;

        _ingameGuiController.processWeaponBullets(_selectedWeapon);
        ActionFinished();
    }
    //--------------------------------------------------------------------------------------------------------------
    private void afterSoot() //Saqué todo lo de la función y lo puse en el ienumerator Esperar.
    {

    }
    //--------------------------------------------------------------------------------------------------------------
    private void FinalizeTurn()
    {
        _isActing = false;
        _canDoAction = false;

        if (_firstGameplayAction)
        {
            CombatManager.Instance.ResumeCombat();
            _firstGameplayAction = false;
        }

        CombatManager.Instance.debugPanel.SubmitText("" + characterName + " Turn Ended");
        CombatManager.Instance.debugPanel.SubmitText("---------------------------------");

        CombatManager.Instance.ReinitRobertCD();

       
        
        FixedTurnEnded();
    }
    //--------------------------------------------------------------------------------------------------------------
    public void revealFromHidden()
    {
        if (_isHidden)
        {
            _isHidden = false;

            foreach (SpriteRenderer sp in GetComponentsInChildren<SpriteRenderer>())
                if (sp.gameObject.name != "Shadow")
                    sp.color = new Color(sp.color.r, sp.color.g, sp.color.b, 1.0f);
        }
    }
    //--------------------------------------------------------------------------------------------------------------
    public override void ActionFinished()
    {
        if (_actionPointsLeft == 0)
        {
            FixedTurnEnded();
        }
        else
        {
            FixedTurnStarted();
            return;
        }

        // Temporalmente Cortado para nuevo pool de turnos.

        if (_isHidden)
        {
            _isHidden = false;

            foreach (SpriteRenderer sp in GetComponentsInChildren<SpriteRenderer>())
                if (sp.gameObject.name != "Shadow")
                    sp.color = new Color(sp.color.r, sp.color.g, sp.color.b, 1.0f);
        }

        _attacksDone++;

        if (_attacksDone < _attacksPerTurn)
        {
            _isActing = false;
            DoAction();
        }
        else
        {
            _attacksDone = 0;
            sparky.DoAction();
            Invoke("FinalizeTurn", 1.5f);
        }
    }
    //--------------------------------------------------------------------------------------------------------------
    #endregion
    //--------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------
    #region CHARACTER_BEHAVIOUR
    //--------------------------------------------------------------------------------------------------------------
    private void checkAbnormalStates(Action callBack)
    {
        bool waitForAnimatedFX = false;

        checkUnconcioussTrigger();
        checkPanicTrigger();

        //------------------------------------------------------------
        /* // Does Robert Surrender?
        checkSurrenderTrigger();
        if (checkIfAbnormalStatusIsActive(AbnormalStates.Surrender))
        {
            CombatManager.Instance.Logger.SubmitText(this.characterName + " Surrender and run away!!!");
            OnCharacterDeath();
            return;
        }
        */
        //------------------------------------------------------------

        // TODO: Implement Card VFX On Robert?
        //container.triggerCardVFX(CardVisualFxTriggers.BLEEDING_TICK, callBack);
        if (checkIfAbnormalStatusIsActive(AbnormalStates.Bleeding))
            doBleedEffect();
        if (checkIfAbnormalStatusIsActive(AbnormalStates.Poisoned))
            doPoisonEffect();
        if (checkIfAbnormalStatusIsActive(AbnormalStates.Radiated))
            doRadiatedEffect();

        if (!waitForAnimatedFX)
            callBack();
    }
    //--------------------------------------------------------------------------------------------------------------
    public void setFocus(int value)
    {
        if (value == 1)
        {
            _focus = 0.6f;
            CombatManager.Instance.Logger.SubmitText("Focus: Escape Route");
        }
        else if (value == 2)
        {
            _focus = 0.8f;
            CombatManager.Instance.Logger.SubmitText("Focus: Heal / Analize");
        }
        else if (value == 3)
        {
            _focus = 0.8f;
            CombatManager.Instance.Logger.SubmitText("Focus: Dialogue / Evade");
        }
        else if (value == 4)
        {
            CombatManager.Instance.Logger.SubmitText("Focus: Attack");
            _focus = 1.2f;
        }
    }
    //-----------------------------------------------------------------------------------------------------------
    public void setShootMode(int value)
    {
        if (value == 1)
        {
            _shootMode = ShootMode.SINGLE;
            CombatManager.Instance.Logger.SubmitText("Is shooting on Single");
        }
        else if (value == 2)
        {
            _shootMode = ShootMode.BURST;
            CombatManager.Instance.Logger.SubmitText("Is shooting on Burst");
        }
        else if (value == 3)
        {
            _shootMode = ShootMode.AUTO;
            CombatManager.Instance.Logger.SubmitText("Is shooting on Auto");
        }
    }
    //-----------------------------------------------------------------------------------------------------------
    public override void OnCharacterDeath()
    {
        CombatManager.Instance.NpcFXController.PlayEventSound("RobertDies");

        if (CombatManager.Instance.packLeaderIsAlive)
            EventManager.TriggerEvent("OnRobertDies");

        _ingameGuiController.ShowGameOver();
        CombatManager.Instance.OnRobertDies.Invoke();
        /*Metodo viejo
        PlayerPrefs.SetInt("RobertDied", 1);
        PlayerPrefs.Save();
        */

        PlayerDataManager.Instance.SetValue("RobertDied", 1);
        PlayerDataManager.Instance.Save();


        base.OnCharacterDeath();
    }
    //-----------------------------------------------------------------------------------------------------------
    protected override void recieveDamage(int damage)
    {
        base.recieveDamage(damage);
        if (isAlive)
        {
            if (characterStats.Hitpoints <= characterStats.TotalHitpoints * 0.3)
            {
                if (!robertSoundManager.LowHP.isPlaying)
                    robertSoundManager.playLowHP();
            }
        }
    }
    //-----------------------------------------------------------------------------------------------------------
    public override void recieveDamage(AttackResult attackInfo)
    {
        /*
#if UNITY_EDITOR
        if (ISInventoryManager.instance.IsOnDebugMode)
            return;
#endif
*/
        //Debug.Log("Robert Hitpoints: " + characterStats.Hitpoints);
        base.recieveDamage(attackInfo);

        if (isAlive)
        {
            if (characterStats.Hitpoints <= characterStats.TotalHitpoints * 0.3)
            {
                if (!robertSoundManager.LowHP.isPlaying)
                    robertSoundManager.playLowHP();
            }

            if (attackInfo.isCriticalHit)
                checkBleedTrigger();

            //PPP si le pegás a la cabeza no le saca más vida?? esto está mal
            if (attackInfo.bodyObjective == BodyObjetive.HEAD)
            {
                //PPP acá hay que chequear que no esté ya sangrando, porque si está sangrando, es al pedo repetir
                if (UnityEngine.Random.Range(0.0f, 1.0f) <= 0.5f)
                    triggerAbnormalState(AbnormalStates.Bleeding);
            }

            if (attackInfo.bulletType != BulletType.Normal)
            {
                if (attackInfo.bulletType == BulletType.Poison)
                    checkPoisonedTrigger();
                else if (attackInfo.bulletType == BulletType.Radiated)
                    checkRadiatedTrigger();
            }

            _ingameGuiController.robertHealthBar.value = characterStats.Hitpoints;
            _ingameGuiController.healthLabel.text = characterStats.Hitpoints + " / " + characterStats.TotalHitpoints;
            robertSoundManager.playRecieveDamage(); //PPP acá corre el sonido de estar lastimado
            if (CombatManager.Instance.packLeaderIsAlive)
                EventManager.TriggerEvent("OnRobertGetsHit");
        }
    }
    //-----------------------------------------------------------------------------------------------------------
    #endregion
    //--------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------
    #region InventoryAndGUI
    //-----------------------------------------------------------------------------------------------------------------------------
    void ProcessEquipment()
    {
        if (ISInventoryManager.instance.getEquipedItem(EquipmentSlot.MAIN_HAND).ObjectID > 0)
            _selectedEquipmentSlot = EquipmentSlot.MAIN_HAND;
        else
            _selectedEquipmentSlot = EquipmentSlot.OFF_HAND;

        getSelectedWeapon();

        if (_selectedWeapon.EquipType == WeaponEquipType.Melee)
            _damageType = DamageType.MELEE;
        else if (_selectedWeapon.EquipType == WeaponEquipType.THROWABLE)
            _damageType = DamageType.THROWING;
        else
            _damageType = DamageType.RANGED;

        _ingameGuiController.setEquippedWeapon(_damageType, _selectedWeapon.GraphicRepresentation);

        if (_selectedWeapon.MaxFireRate == 1)
            _ingameGuiController.setShootModeSlider(ShootMode.SINGLE);
        else if (_selectedWeapon.MaxFireRate == 3)
            _ingameGuiController.setShootModeSlider(ShootMode.AUTO);
        else if (_selectedWeapon.MaxFireRate == 6)
            _ingameGuiController.setShootModeSlider(ShootMode.BURST);
    }
    //-----------------------------------------------------------------------------------------------------------------------------
    public void registerGuiController(IngameGUIController controller)
    {
        _ingameGuiController = controller;
        Debug.Log("Ingame GUI Controller Registered");

        string stat = PlayerPrefs.GetString("ConsumeRobertStat","NONE");

        if (stat != "NONE")
        {
            string[] values = stat.Split('_');

            if (values[0] == "Water")
            {
                int waterToConsume = int.Parse(values[1]);

                if (waterToConsume <= characterStats.water)
                {
                    characterStats.water -= waterToConsume;
                }
                else
                {
                    waterToConsume -= characterStats.water;
                    characterStats.water = 0;

                    characterStats.Hitpoints -= (int)(characterStats.TotalHitpoints * 0.25f) * (waterToConsume / 5);
                    if (characterStats.Hitpoints <= 0)
                        characterStats.Hitpoints = 1;
                }
            }
            else if (values[0] == "Health")
            {
                characterStats.Hitpoints -= (int)(characterStats.TotalHitpoints * 0.25f) * int.Parse(values[1]);
            }
            PlayerPrefs.SetString("ConsumeRobertStat", "NONE");
            PlayerPrefs.Save();
        }

        Debug.Log("LA SALUD DE ROBERT A LA HORA DE ACTUALIZAR LA BARRA ES : " + characterStats.Hitpoints + " HP");

        UpdateHealthBar();

        _ingameGuiController.waterReservoir.value = characterStats.water;
        _ingameGuiController.waterReservoir.maxValue = 20;
    }
    //-----------------------------------------------------------------------------------------------------------------------------
    public void RefillWater(int amount, bool completeRefill = false)
    {
        if (completeRefill)
        {
            characterStats.water = 20;
        }
        else
        {
            if (characterStats.water + amount >= 20)
                characterStats.water = 20;
            else
                characterStats.water += amount;
        }
        _ingameGuiController.waterReservoir.value = characterStats.water;

    }
    //-----------------------------------------------------------------------------------------------------------------------------
    private void ItemConsumed(ISObject item)
    {
        Debug.Log(item.Name + " es el nombre del itema consumir");
        if (item is ISBullet)
        {
            reloadAction(item);
            /*
            ISBullet bullet = item as ISBullet;
            if (_selectedWeapon == null) getSelectedWeapon();

            if (bullet.caliber != _selectedWeapon.Caliber)
            {
                CombatManager.Instance.Logger.SubmitText(bullet.Name + " are not suitable for your selected weapon");
                return;
            }

            int bulletsMissing = _selectedWeapon.MaxShootsPerRound - _selectedWeapon.ShootsInRound;

            // If no need to reload, just skip.
            if (bulletsMissing == 0) return;


            if (bullet.Stack > bulletsMissing)
            {
                _selectedWeapon.ShootsInRound += bulletsMissing;
                bullet.Stack -= bulletsMissing;
            }
            else
            {
                _selectedWeapon.ShootsInRound += bullet.Stack;
                bullet.Stack = 0;
            }

            EventManager.TriggerEvent("BulletConsumed");

            CombatManager.Instance.Logger.SubmitText("Used " + bullet.Name);
            */
        }
        else
        {
            if (item.Name == "Water")
            {
                Debug.Log("Ojo que Robert tiene de agua : " + characterStats.water);
                if (characterStats.water < 20)
                {
                    characterStats.water = 20;
                    CombatManager.Instance.NpcFXController.PlayEventSound("WaterDrink1");

                    if (((ISConsumable)item).Stack == 1)
                        ((ISConsumable)item).Stack = 0;
                    else
                        ((ISConsumable)item).Stack--;

                    _ingameGuiController.waterReservoir.value = 20;
                    EventManager.TriggerEvent("BulletConsumed");
                    CombatManager.Instance.Logger.SubmitText("<color=white>Thirst has been recovered.</color>");
                }
            }
            else
            {
                if (item.Name == "Health Concoction")
                {
                    if (characterStats.Hitpoints < 500)
                    {
                        CombatManager.Instance.NpcFXController.PlayEventSound("WaterDrink1");

                        if (((ISConsumable)item).Stack == 1)
                        {
                            ((ISConsumable)item).Stack = 0;
                        }
                        else
                        {
                            ((ISConsumable)item).Stack--;
                        }
                        Debug.Log(characterStats.Hitpoints);
                        if (characterStats.Hitpoints < 451)
                        {
                            characterStats.Hitpoints += 50;
                            if (characterStats.Hitpoints >= characterStats.TotalHitpoints * 0.3)
                            {
                                    robertSoundManager.stopLowHP();
                            }
                        }
                        else
                        {
                            characterStats.Hitpoints = 500;
                        }
                        UpdateHealthBar();
                        Debug.Log(characterStats.Hitpoints);
                        EventManager.TriggerEvent("BulletConsumed");
                        CombatManager.Instance.Logger.SubmitText("<color=green>Robert has recovered +50 health points</color>");
                    }
                }
            }
        }
    }

    public void wonHP(int hp)
    {
        if (characterStats.Hitpoints <= characterStats.TotalHitpoints - hp)
        {
            characterStats.Hitpoints += hp;
            UpdateHealthBar();
            if (characterStats.Hitpoints >= characterStats.TotalHitpoints * 0.3)
            {
                robertSoundManager.stopLowHP();
            }
            CombatManager.Instance.NpcFXController.PlayEventSound("WaterDrink1");
            CombatManager.Instance.Logger.SubmitText("<color=green>Robert has recovered +" + hp + " health points</color>");
        }
        else
        {
            characterStats.Hitpoints = characterStats.TotalHitpoints;
        }
    }

    //-----------------------------------------------------------------------------------------------------------------------------
    private void ItemUnequiped(ISObject item)
    {
        if (item == null)
            return;

        if (item is ISArmor)
        {
            ISArmor armor = item as ISArmor;

            if (armor.EquipSlot == EquipmentSlot.HEAD)
            {
                if (_dispararIK == null) { _dispararIK = GameObject.FindGameObjectWithTag("Player").GetComponent<DispararIK>(); }
                _dispararIK.DesactivarModeloCasco();
            }
        }

        if (!ISInventoryManager.instance.puttingStarterGear)
            CombatManager.Instance.Logger.SubmitText("Unequiped Item: " + item.Name);
    }
    //-----------------------------------------------------------------------------------------------------------------------------
    private void ItemEquiped(ISObject item)
    {
        if (item == null)
            return;

        if (!ISInventoryManager.instance.puttingStarterGear)
            CombatManager.Instance.Logger.SubmitText("Equiped Item: " + item.Name);

        if (item is ISWeapon)
        {
            ISWeapon weapon = item as ISWeapon;

            if (weapon.EquipType == WeaponEquipType.Melee)
                _damageType = DamageType.MELEE;
            else if (weapon.EquipType == WeaponEquipType.THROWABLE)
                _damageType = DamageType.THROWING;
            else
                _damageType = DamageType.RANGED;

            _ingameGuiController.setEquippedWeapon(_damageType, weapon.GraphicRepresentation);

            if (weapon.MaxFireRate == 1)
                _ingameGuiController.setShootModeSlider(ShootMode.SINGLE);
            else if (weapon.MaxFireRate == 3)
                _ingameGuiController.setShootModeSlider(ShootMode.AUTO);
            else if (weapon.MaxFireRate == 6)
                _ingameGuiController.setShootModeSlider(ShootMode.BURST);
        }
        else if (item is ISArmor)
        {
            ISArmor armor = item as ISArmor;

            if (armor.EquipSlot == EquipmentSlot.HEAD) {
                if (_dispararIK == null) { _dispararIK = GameObject.FindGameObjectWithTag("Player").GetComponent<DispararIK>(); }
                _dispararIK.ActivarModeloCasco(armor.Model);
            }
            

            // Si el EquipSlot es Main Hand significa que es un escudo
            if (armor.EquipSlot == EquipmentSlot.MAIN_HAND)
            {

            }
        }
    }
    //-----------------------------------------------------------------------------------------------------------------------------
    public override void getSelectedWeapon()
    {
        /*
        EquipmentSlot slot = EquipmentSlot.MAIN_HAND;

        if (_damageType == DamageType.RANGED)
            slot = EquipmentSlot.MAIN_HAND;
        else if (_damageType == DamageType.MELEE)
            slot = EquipmentSlot.MELEE;
        else if (_damageType == DamageType.THROWING)
            slot = EquipmentSlot.THROWABLE;

        _selectedWeapon = ISInventoryManager.instance.getEquipedItem(slot) as ISWeapon;
        */

        
        _selectedWeapon = ISInventoryManager.instance.getEquipedItem(_selectedEquipmentSlot) as ISWeapon;

        if (_selectedWeapon != null)
        {

            if (_dispararIK == null) { _dispararIK = GameObject.FindGameObjectWithTag("Player").GetComponent<DispararIK>(); }
            if (_dispararIK == null) { Debug.Log("DISPARAR IK SIGUE SIENDO NULL!!!!!!!!!!!!"); }
            _dispararIK.ActivarModeloArma(selectedWeapon.Model);
        }
        else
        {
            if (_dispararIK == null) { _dispararIK = GameObject.FindGameObjectWithTag("Player").GetComponent<DispararIK>(); }
            _dispararIK.DesactivarModeloArma();
        }
            /*
        
             */
        

    }
    //-----------------------------------------------------------------------------------------------------------------------------
    public void setSelectedWeaponSlot(EquipmentSlot slot)
    {
        _selectedEquipmentSlot = slot;

        if (_selectedEquipmentSlot == EquipmentSlot.THROWABLE)
            _damageType = DamageType.RANGED;
        else
            _damageType = DamageType.THROWING;
    }
    //-----------------------------------------------------------------------------------------------------------------------------
    private void generateStarterGear()
    {

    }
    //-----------------------------------------------------------------------------------------------------------------------------
    #endregion
    //--------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------
    #region STATS_INITIALIZATION
    //------------------------------------------------------------------------------------------------------------------------------------------
    public void processHitPoints()
    {

        // TODO: Revisar como afecta el asignar hitpoints en el panel de stats (Para cada parte, ya que solo tenemos el general)
        //PlayerPrefs.GetInt(CharacterStats.StatHitpoints, _robert.characterStats.getStat(CharacterStats.StatAccuracy).OriginalValue);

        // Health Points Generation

        // Necesito Saber:
        /*
             - Si Evoluciono o no los puntos a la stat Hitpoints.

                - Start Hitpoint = 50 + (Stat * 5)
        */
        /*METODO VIEJO
        int statHitPoints = PlayerPrefs.GetInt(CharacterStats.StatTotalHitpoints, 500);
        */
        int statHitPoints = PlayerDataManager.Instance.GetValue(CharacterStats.StatTotalHitpoints, 500);

        bodyPartHealth[BodyObjetive.TORSO] = new CharacterStat("TORSO", statHitPoints / 2);
        bodyPartHealth[BodyObjetive.HEAD] = new CharacterStat("HEAD", statHitPoints / 3);
        bodyPartHealth[BodyObjetive.ARMS] = new CharacterStat("ARMS", statHitPoints / 3);
        bodyPartHealth[BodyObjetive.LEGS] = new CharacterStat("LEGS", statHitPoints / 3);

        // Get from Persisted HP
        /*
#if UNITY_EDITOR
        if (ISInventoryManager.instance.IsOnDebugMode)
        {
            characterStats.AddStat(CharacterStats.StatTotalHitpoints, 500);
            characterStats.AddStat(CharacterStats.StatHitpoints, 500);
            //return;
        }
#endif
*/
        //int actualHitpoints 
        // Revisar!!! (Esta asignando si no encuentra el persistido de datos, los stats como health pool).

        characterStats.AddStat(CharacterStats.StatTotalHitpoints, statHitPoints);
        /* METODO VIEJO
        characterStats.AddStat(CharacterStats.StatHitpoints, PlayerPrefs.GetInt(CharacterStats.StatHitpoints, statHitPoints));
        */

        characterStats.AddStat(CharacterStats.StatHitpoints, PlayerDataManager.Instance.GetValue(CharacterStats.StatHitpoints, statHitPoints));

        //UpdateHealthBar();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------
    void processMight()
    {
        //--------------------------------------------------------------------------------------------------------------------------------
        // Calculate Basic Might, 
        // ++ Equipmennt Might each time an item is equiped
        int startingMight = 0;

        /*METODO VIEJO
        startingMight = PlayerPrefs.GetInt(CharacterStats.StatAccuracy, 0);
        */

        startingMight = PlayerDataManager.Instance.GetValue(CharacterStats.StatAccuracy, 0);

        if (startingMight == 0)
        {
            startingMight += characterStats.Accuracy;
            startingMight += characterStats.Strength;
            startingMight += characterStats.Agility;
            startingMight += characterStats.Evasion;
            startingMight += characterStats.Intelligence;
            startingMight += characterStats.TotalHitpoints;
        }

        characterStats.AddStat(CharacterStats.StatMight, startingMight);
        CombatManager.Instance.debugPanel.SubmitText("---------------------------------");
        CombatManager.Instance.debugPanel.SubmitText("Robert Might: " + characterStats.Might, "Yellow");
        CombatManager.Instance.debugPanel.SubmitText("---------------------------------");
        //--------------------------------------------------------------------------------------------------------------------------------
    }
    //------------------------------------------------------------------------------------------------------------------------------------------
    private void generateBasicStats()
    {
        /* SACAR LINEA PARA VOLVER A METODO ANTERIOR
        characterStats.availablePoints = PlayerPrefs.GetInt(CharacterStats.AvailableStatsPoints, 10);

        characterStats.AddStat(CharacterStats.StatAccuracy, PlayerPrefs.GetInt(CharacterStats.StatAccuracy, 3));
        characterStats.AddStat(CharacterStats.StatStrength, PlayerPrefs.GetInt(CharacterStats.StatStrength, 3));
        characterStats.AddStat(CharacterStats.StatAgility, PlayerPrefs.GetInt(CharacterStats.StatAgility, 3));
        characterStats.AddStat(CharacterStats.StatMorale, PlayerPrefs.GetInt(CharacterStats.StatMorale, 3));
        characterStats.AddStat(CharacterStats.StatCommand, PlayerPrefs.GetInt(CharacterStats.StatCommand, 3));
        //characterStats.AddStat(CharacterStats.StatCommand, 100);
        characterStats.AddStat(CharacterStats.StatEvasion, PlayerPrefs.GetInt(CharacterStats.StatEvasion, 3));
        characterStats.AddStat(CharacterStats.StatIntelligence, PlayerPrefs.GetInt(CharacterStats.StatIntelligence, 3));
        //characterStats.AddStat(CharacterStats.StatIntelligence, 100);
        characterStats.AddStat(CharacterStats.StatStealth, PlayerPrefs.GetInt(CharacterStats.StatStealth, 3));
        //haracterStats.AddStat(CharacterStats.Statwater, PlayerPrefs.GetInt(CharacterStats.Statwater, 5));
        characterStats.water = PlayerPrefs.GetInt(CharacterStats.Statwater, 20);
        //characterStats.Experience = 0;
        characterStats.Experience = PlayerPrefs.GetInt(CharacterStats.StatExperience, 0);
        characterStats.AddStat(CharacterStats.AvailableStatsPoints , PlayerPrefs.GetInt(CharacterStats.AvailableStatsPoints, 10));
        */ //SACAR LINEA PARA VOLVER A METODO ANTERIOR
        //------------------------------------------------------------------------------------------------------------------------------------------
        // STATS SERIALIZADOS
        // /* DESCOMENTAR LINEA PARA VOLVER A METODO ANTERIOR
        characterStats.availablePoints = PlayerDataManager.Instance.GetValue(CharacterStats.AvailableStatsPoints, 10);
        characterStats.AddStat(CharacterStats.StatAccuracy, PlayerDataManager.Instance.GetValue(CharacterStats.StatAccuracy, 3));
        characterStats.AddStat(CharacterStats.StatStrength, PlayerDataManager.Instance.GetValue(CharacterStats.StatStrength, 3));
        characterStats.AddStat(CharacterStats.StatAgility, PlayerDataManager.Instance.GetValue(CharacterStats.StatAgility, 3));
        characterStats.AddStat(CharacterStats.StatMorale, PlayerDataManager.Instance.GetValue(CharacterStats.StatMorale, 3));
        characterStats.AddStat(CharacterStats.StatCommand, PlayerDataManager.Instance.GetValue(CharacterStats.StatCommand, 3));
        characterStats.AddStat(CharacterStats.StatEvasion, PlayerDataManager.Instance.GetValue(CharacterStats.StatEvasion, 3));
        characterStats.AddStat(CharacterStats.StatIntelligence, PlayerDataManager.Instance.GetValue(CharacterStats.StatIntelligence, 3));
        characterStats.AddStat(CharacterStats.StatStealth, PlayerDataManager.Instance.GetValue(CharacterStats.StatStealth, 3));
        characterStats.water = PlayerDataManager.Instance.GetValue(CharacterStats.Statwater, 20);
        characterStats.Experience = PlayerDataManager.Instance.GetValue(CharacterStats.StatExperience, 0);
        characterStats.AddStat(CharacterStats.AvailableStatsPoints, PlayerDataManager.Instance.GetValue(CharacterStats.AvailableStatsPoints, 10));
        // */ DESCOMENTAR LINEA PARA VOLVER A METODO ANTERIOR
    }
    //------------------------------------------------------------------------------------------------------------------------------------------
    public void UpdateHealthBar()
    {
        Debug.Log("UPDATE HEALTH");
        _ingameGuiController.robertHealthBar.maxValue= characterStats.TotalHitpoints;
        _ingameGuiController.robertHealthBar.value = characterStats.Hitpoints;
        _ingameGuiController.healthLabel.text = characterStats.Hitpoints + " / " + characterStats.TotalHitpoints;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------
#endregion
    //--------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------
#region STATS_GETTERS
    // -------------------------------------------------------
    public override int getAccuracy(string opt)
    {
        if (opt == "Self")
        {
            return characterStats.Accuracy;
        }
        else if (opt == "Weapon")
        {
            return selectedWeapon.Accuracy;
            //return (int)_inventoryManager.character.GetStat("Combat", "Accuracy").currentValueRaw;
        }
        else if (opt.ToLower() == "shooting")
        {
            return characterStats.Accuracy < selectedWeapon.Accuracy ? characterStats.Accuracy : selectedWeapon.Accuracy;
            //return characterStats.Accuracy < (int)_inventoryManager.character.GetStat("Combat", "Accuracy").currentValueRaw ? characterStats.Accuracy : (int)_inventoryManager.character.GetStat("Combat", "Accuracy").currentValueRaw;
        }

        return 0;
    }
    // -------------------------------------------------------
    public override int getDamage()
    {
        int rawDamage = selectedWeapon.Damage;
        return UnityEngine.Random.Range((rawDamage - ((rawDamage * 30) / 100)), (rawDamage + ((rawDamage * 30) / 100)));
    }
    // -------------------------------------------------------
    public override int getArmor(BodyObjetive bodyPart)
    {
        EquipmentSlot slot = EquipmentSlot.NONE;

        if (bodyPart == BodyObjetive.HEAD)
            slot = EquipmentSlot.HEAD;
        else if (bodyPart == BodyObjetive.ARMS)
            slot = EquipmentSlot.ARMS;
        else if (bodyPart == BodyObjetive.TORSO)
            slot = EquipmentSlot.TORSO;
        else if (bodyPart == BodyObjetive.LEGS)
            slot = EquipmentSlot.LEGS;

        if (slot != EquipmentSlot.NONE)
        {
            ISArmor armor = ISInventoryManager.instance.getEquipedItem(slot) as ISArmor;
            if (armor != null)
                return armor.Protection;
        }

        return 0;

        /*
        if (bodyPart == BodyObjetive.HEAD)
        {
            if (_inventoryManager.character.GetStat("Armor", "Head Armor") != null)
                return (int)_inventoryManager.character.GetStat("Armor", "Head Armor").currentValueRaw;
        }
        else if (bodyPart == BodyObjetive.ARMS)
        {
            if (_inventoryManager.character.GetStat("Armor", "Arms Armor") != null)
                return (int)_inventoryManager.character.GetStat("Armor", "Arms Armor").currentValueRaw;
        }
        else if (bodyPart == BodyObjetive.TORSO)
        {
            if (_inventoryManager.character.GetStat("Armor", "Torso Armor") != null)
                return (int)_inventoryManager.character.GetStat("Armor", "Torso Armor").currentValueRaw;
        }
        else if (bodyPart == BodyObjetive.LEGS)
        {
            if (_inventoryManager.character.GetStat("Armor", "Legs Armor") != null)
                return (int)_inventoryManager.character.GetStat("Armor", "Legs Armor").currentValueRaw;
        }
        return 0;
        */
    }
    // -------------------------------------------------------
    public override float getRange()
    {
        return selectedWeapon.MaxRange;
        /*
        if (_inventoryManager.character.GetStat("Weapons", "Range") != null)
            return (int)_inventoryManager.character.GetStat("Weapons", "Range").currentValueRaw;
    
        return 0;
        */
    }
    // -------------------------------------------------------
    public override float getEvasion()
    {
        //if (_inventoryManager.character.GetStat("Combat", "Evasion") != null)
            //return _inventoryManager.character.GetStat("Combat", "Evasion").currentValueRaw + characterStats.Evasion;

        return characterStats.Evasion;
    }
    // -------------------------------------------------------
    public override float getFocus()
    {
        return _focus;
    }
    // -------------------------------------------------------
    public override int getMorale()
    {
        //if (_inventoryManager.character.GetStat("Combat", "Morale") != null)
            //return (int)_inventoryManager.character.GetStat("Combat", "Morale").currentValueRaw + characterStats.Morale;

        return characterStats.Morale;
    }
    // -------------------------------------------------------
    public override ShootMode getShootMode()
    {
        return _shootMode;
    }
    // -------------------------------------------------------
    #endregion
    //--------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------
    #region XP
    XpSystem xpManager = new XpSystem();
    public void AddXP(int xp)
    {
        xpManager.AddXP(xp);
    }
    #endregion
    #region NuevasMecanicasdeCombate

    public void ActivarMarca(bool nuevoestado)
    {
        targetpicker.SetActive(nuevoestado);
    }

    public int GetActionsPool()
    {
        return _actionPoolManager.pooledActions.Count;
    }

    #endregion


}

