﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class EnemyContainer : MonoBehaviour {

    [SerializeField]
    private GameObject hiddeableCanvas, ZoomInCanvas, Shadow;

    [SerializeField]
    private SpriteRenderer characterCard;

    private SpriteRenderer characterSprite;

    [SerializeField]
    private CanvasGroup canvasGroup;

    [SerializeField]
    private Slider hostilityBar;

    [SerializeField]
    private GameObject Header;

    [SerializeField]
    private Text headerName;

    [SerializeField]
    private Text distanceField;

    [SerializeField]
    CardVFXControllers _visualFxAnimator;

    [SerializeField]
    HealthBarController healthBarcontroller;

    //GameObject _debuffedStats;
    //public GameObject debuffedStats { get { return _debuffedStats; } }


    private BaseEnemy _parent;
    private Animator _damageAnimator;

    private List<string> maleNames = new List<string>();
    private List<string> femaleNames = new List<string>();
    private List<string> lastNames = new List<string>();
    
    public string selectedName = "";

    private void initializeNames()
    {
        maleNames.Add("Juan");
        maleNames.Add("Pedro");
        maleNames.Add("Mariano");
        maleNames.Add("Martin");
        maleNames.Add("Walter");
        maleNames.Add("Francisco");
        maleNames.Add("Jose");
        maleNames.Add("Romeo");
        maleNames.Add("Federico");
        maleNames.Add("Guido");
        maleNames.Add("Tomas");
        maleNames.Add("Anibal");
        maleNames.Add("Andres");
        maleNames.Add("Agustin");
        maleNames.Add("Jonas");
        maleNames.Add("Jorge");
        maleNames.Add("Marcos");
        maleNames.Add("Gustavo");
        maleNames.Add("Eduardo");

        femaleNames.Add("Maria");
        femaleNames.Add("Cecilia");
        femaleNames.Add("Andrea");
        femaleNames.Add("Macarena");
        femaleNames.Add("Ana");
        femaleNames.Add("Camila");
        femaleNames.Add("Juana");
        femaleNames.Add("Patricia");
        femaleNames.Add("Jimena");
        femaleNames.Add("Dolores");
        femaleNames.Add("Catalina");
        femaleNames.Add("Florencia");
        femaleNames.Add("Clara");
        femaleNames.Add("Lara");
        femaleNames.Add("Ines");
        femaleNames.Add("Francisca");
        femaleNames.Add("Ema");
        femaleNames.Add("Carmela");

        lastNames.Add("Jameson");
        lastNames.Add("Walker");
        lastNames.Add("Beam");
        lastNames.Add("Hayden");
        lastNames.Add("Crow");
        lastNames.Add("Creek");
        lastNames.Add("Smuggler");
        lastNames.Add("Dickel");
        lastNames.Add("Daniels");
        lastNames.Add("Suntory");
        lastNames.Add("Ridge");
        lastNames.Add("Lyons");
        lastNames.Add("Seagram");
        lastNames.Add("Bellerhof");
        lastNames.Add("Mosslein");
        lastNames.Add("Smokey");
        lastNames.Add("Moonshine");
    }

    public void ActivateHeader()
    {
        Header.SetActive(true);
    }

    SpriteRenderer cardRenderer;
    Color color = new Color(1, 1, 1, 0);
    bool _hadTransition = false;

    public void tintCharacterActing(bool tinting)
    {
        if (cardRenderer == null)
            cardRenderer = characterCard.GetComponent<SpriteRenderer>();

        if (tinting)
            cardRenderer.color = Color.yellow;
        else
            cardRenderer.color = Color.white;
    }

    public void SetupForTransition()
    {
        _hadTransition = true;

        Header.SetActive(false);
        canvasGroup.alpha = 0;

        cardRenderer = characterCard.GetComponent<SpriteRenderer>();
        cardRenderer.color = color;
    }

    bool cardFadeEnabled = false;
         
    public void TriggerCardVisible()
    {
        cardFadeEnabled = true;
    }

    void Start()
    {
        if (!_hadTransition)
        {
            cardRenderer = characterCard.GetComponent<SpriteRenderer>();
            cardRenderer.color = new Color(255, 255, 255, 200);
        }

        ConfigureGraphicsToDistance();
    }

    void FixedUpdate()
    {
        if (!_hadTransition) return;


        if (cardFadeEnabled)
        {
            if (canvasGroup.alpha < 0.6f)
            {
                canvasGroup.alpha += 0.01f;
                color.a += 0.01f;
                cardRenderer.color = color;
            }
        }

        if (canvasGroup.alpha >= 0.6f)
        {
            cardFadeEnabled = false;
            Header.SetActive(true);
        }

    }

    public void triggerCardVFX(string fxName, System.Action callBack)
    {
        if (_visualFxAnimator != null)
            _visualFxAnimator.triggerCardVFX(fxName, callBack);
    }

    public void config(BaseEnemy parent)
    {
        initializeNames();
        _parent = parent;

        if (parent.enemyType == BaseEnemy.EnemyType.AGENT)
        {
            selectedName = headerName.text = lastNames[Random.Range(0, lastNames.Count)];

            if (CombatManager.Instance.pickedNames.Contains(selectedName))
                selectedName = headerName.text = lastNames[Random.Range(0, lastNames.Count)];
        }
        else
        {
            if (_parent.genre == BaseEnemy.Genre.FEMALE)
            {
                if (femaleNames.Count > 0)
                    selectedName = headerName.text = femaleNames[Random.Range(0, femaleNames.Count)];
            }
            else
            {
                if (maleNames.Count > 0)
                    selectedName = headerName.text = maleNames[Random.Range(0, maleNames.Count)];
            }
        }

        Configure();
        parent.characterName = selectedName;

        int robertMight = CombatManager.Instance.robert.characterStats.Might;

        if (parent.characterStats.Might < robertMight)
        {
            headerName.color = Color.green;
        }
        else
        {
            int difference = parent.characterStats.Might - robertMight;

            // si la diferencia es de un 10% Amarillo
            // Si la diferencia es entre un 10% y 25% naranja
            // Si la diferencia es mayor a 25% y Rojo
            if (difference <= (robertMight * 0.1f))
                headerName.color = Color.yellow;
            else if (difference <= (robertMight * 0.25f))
                headerName.color = new Color(1, 0.39f, 0, 1);
            else if (difference > (robertMight * 0.25f))
                headerName.color = Color.red;
        }
    }

    public void ZoomOnMe(bool zoomIn)
    {
        if (zoomIn)
        {
            // Activar Tarjeta 
            if (!characterCard.enabled)
                characterCard.enabled = true;
            // Activar HiddeableCanvas.
            if (!hiddeableCanvas.activeInHierarchy)
                hiddeableCanvas.SetActive(true);

            // Desactivar Sombra
            if (Shadow.activeInHierarchy)
                Shadow.SetActive(false);

            // Desactivar ZoomIn Canvas.
            if (ZoomInCanvas.activeInHierarchy)
                ZoomInCanvas.SetActive(false);

            // Normalize Character Alpha
            if (characterSprite != null)
                characterSprite.color = new Color(1, 1, 1, 1);
        }
        else
        {
            float distanceToTarget = CombatResolver.calculateDistance(this.gameObject, CombatManager.Instance.robert.gameObject);
            // Si la Distancia es Mayor a 50:
            if (distanceToTarget > 50)
            {
                // - Desactivar Tarjeta
                characterCard.enabled = false;
                // - Activar Sombra
                if (!Shadow.activeInHierarchy)
                    Shadow.SetActive(true);
                // - Desactivar HiddeableCanvas
                if (hiddeableCanvas.activeInHierarchy)
                    hiddeableCanvas.SetActive(false);
            }

            // - Activar ZoomInCanvas
            if (!ZoomInCanvas.activeInHierarchy)
                ZoomInCanvas.SetActive(true);

            // Procesar Alpha Acorde a Distancia.
            ProcessCharacterAlpha(distanceToTarget);
        }
    }

    // Use this for initialization
    void Configure()
    {
        // TODO: For Security, check every field is not null
        if (Header == null)
            Debug.LogError("Field Not Setted");
        if (headerName == null)
            Debug.LogError("Field Not Setted");
        if (distanceField == null)
            Debug.LogError("Field Not Setted");

        if (healthBarcontroller != null)
            healthBarcontroller.Configure(_parent.characterStats.Hitpoints);

        if (hostilityBar != null)
            updateHostility();

        CombatManager.Instance.OnCharacterReachedNavigationPoint.AddListener(CharacterReachedNavPointEvent);
    }

    void ConfigureGraphicsToDistance()
    {
        return;

        if (GameManager.Instance.levelManager.scenarioSize == LevelManager.ScenarioSize.Small) return;

        characterSprite = transform.GetChild(1).GetComponent<SpriteRenderer>();

        float distanceToTarget = CombatResolver.calculateDistance(this.gameObject, CombatManager.Instance.robert.gameObject);
        distanceField.text = "Dist.: " + distanceToTarget.ToString("f2");

        ProcessCharacterAlpha(distanceToTarget);

        if (distanceToTarget > 50)
        {
            characterCard.enabled = false;
            hiddeableCanvas.SetActive(false);
            ZoomInCanvas.SetActive(true);
            Shadow.SetActive(true);
        }
    }

    private void ProcessCharacterAlpha(float distance)
    {
        if (characterSprite != null)
        {
            if (distance > 50 && distance < 60)
                characterSprite.color = new Color(1, 1, 1, 0.9f);
            else if (distance > 60 && distance < 70)
                characterSprite.color = new Color(1, 1, 1, 0.8f);
            else if (distance > 70 && distance < 80)
                characterSprite.color = new Color(1, 1, 1, 0.7f);
            else if (distance > 90 && distance < 100)
                characterSprite.color = new Color(1, 1, 1, 0.6f);
            else if (distance > 100)
                characterSprite.color = new Color(1, 1, 1, 0.5f);
        }
    }

    public void characterWillDie()
    {
        //   CombatManager.Instance.OnCharacterReachedNavigationPoint.AddListener(CharacterReachedNavPointEvent);
        CombatManager.Instance.OnCharacterReachedNavigationPoint.RemoveListener(CharacterReachedNavPointEvent);
    }
    
    void CharacterReachedNavPointEvent(BaseCharacter character)
    {
        if (character is Robert)
        {
            float distanceToTarget = CombatResolver.calculateDistance(this.gameObject, character.gameObject);
            distanceField.text = "Dist.: " + distanceToTarget.ToString("f2");
        }
    }

    public void updateHostility()
    {
        if (hostilityBar != null)
            hostilityBar.value = _parent.characterStats.Hostility;
    }

    public void updateHealth(int amount, bool isCritical = false)
    {
        if (healthBarcontroller != null)
            healthBarcontroller.ApplyDamage(amount);
    }

    public void OnBehaviorChanged()
    {
        if (_parent.currentBehavior == BaseEnemy.Behavior.NEUTRAL)
            gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 120.0f / 255.0f);
        else if (_parent.currentBehavior == BaseEnemy.Behavior.FRIENDLY)
            gameObject.GetComponent<Image>().color = new Color(162.0f / 255.0f, 1, 144.0f / 255.0f, 120.0f / 255.0f);
        else if (_parent.currentBehavior == BaseEnemy.Behavior.HOSTILE)
            gameObject.GetComponent<Image>().color = new Color(180.0f / 255.0f, 61.0f / 255.0f, 61.0f / 255.0f, 120.0f / 255.0f);
    }

}
