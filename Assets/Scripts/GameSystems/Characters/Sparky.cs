﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sparky : BaseCharacter {

    public enum Behavior
    {
        AGGRESIVE,
        FOLLOW,
        DEFENSIVE
    }

    public Behavior AImode = Behavior.DEFENSIVE;
    private BaseEnemy _lockedTarget;
    private bool _isOnTarget = false;
    private bool _isMoving = false;
    private float _moveSpeed = 250;
    private float _maxDistance = 15f;
    private int _damageRecieved = 0;
    private int _timesAttacked = 0;

    private int OffsetX = 220;
    private int OffsetY = -30;
    private Vector3 _lockedTargetPosition;
    private ZigZagMovement _movement;

    [SerializeField]
    private Animator _sparkyAnimator;

    private Dictionary<string, AudioClip> _sparkySounds = new Dictionary<string, AudioClip>();
    private AudioSource _audioSource;

    public void setHostileBehavior()
    {
        AImode = Behavior.AGGRESIVE;
        CombatManager.Instance.Logger.SubmitText("Sparky is now on Aggresive Mode");
    }

    public void setDefensiveBehavior()
    {
        AImode = Behavior.DEFENSIVE;
        CombatManager.Instance.Logger.SubmitText("Sparky is now on Defensive Mode");
    }

    public void setNeutralBehavior()
    {
        AImode = Behavior.FOLLOW;
        CombatManager.Instance.Logger.SubmitText("Sparky is now on Follow Mode");
    }

    void Start()
    {
        AImode = Behavior.DEFENSIVE;
        generateStarterStats();
        _movement = transform.parent.gameObject.GetComponent<ZigZagMovement>();

        GameObject newObj = new GameObject(name, typeof(AudioSource));
        newObj.transform.SetParent(this.transform);
        _audioSource = newObj.GetComponent<AudioSource>();
        _audioSource.loop = false;

        foreach (AudioClip clip in Resources.LoadAll<AudioClip>("AudioClips/Characters/Sparky"))
        {
            _sparkySounds.Add(clip.name, clip);
        }
        InvokeRepeating("PlayIdleClip", Random.Range(60.0f, 180.0f), Random.Range(60.0f, 180.0f));
    }


    void generateStarterStats()
    {
        characterStats.AddStat(CharacterStats.StatAccuracy, Random.Range(4, 9));
        characterStats.AddStat(CharacterStats.StatStrength, Random.Range(2, 4));
        characterStats.AddStat(CharacterStats.StatAgility, Random.Range(2, 4));
        characterStats.AddStat(CharacterStats.StatMorale, Random.Range(4, 9));
        characterStats.AddStat(CharacterStats.StatCommand, Random.Range(4, 9));
        characterStats.AddStat(CharacterStats.StatEvasion, Random.Range(4, 9));
        characterStats.AddStat(CharacterStats.StatIntelligence, Random.Range(2, 4));
        characterStats.AddStat(CharacterStats.StatStealth, Random.Range(4, 9));

        // Health Points Generation
        bodyPartHealth[BodyObjetive.TORSO] = new CharacterStat("TORSO", 50);
        bodyPartHealth[BodyObjetive.HEAD] = new CharacterStat("HEAD", 20);
        bodyPartHealth[BodyObjetive.ARMS] = new CharacterStat("ARMS", 20);
        bodyPartHealth[BodyObjetive.LEGS] = new CharacterStat("LEGS", 20);

        characterStats.AddStat(CharacterStats.StatTotalHitpoints, bodyPartHealth[BodyObjetive.TORSO].OriginalValue);
        characterStats.AddStat(CharacterStats.StatHitpoints, characterStats.TotalHitpoints);

        reactionTime = Random.Range(4, 8);
        if (characterStats.Agility > 5)
            reactionTime -= 1 * characterStats.Agility - 5;
    }

    public override void recieveDamage(AttackResult attackInfo)
    {
        base.recieveDamage(attackInfo);

        // Si sparky muere, o esta en follow, no hacer nada.
        if ((!_isAlive) || (AImode == Behavior.FOLLOW)) return;

        // Si ya estamos con un target Lockeado chequear damage para cambiar de target.
        if (_lockedTarget != null)
        {
            // Si el atacante es nuestro target, sumar al pool de danio
            if (attackInfo.attacker == _lockedTarget)
            {
                _damageRecieved += attackInfo.damage;
                _timesAttacked++;
            }
            else // Si el atacante no es nuestro locked, chequear si el da;o es mayor al promedio de da;o recivido
            {
                if (_timesAttacked == 0)
                {
                    _damageRecieved += attackInfo.damage;
                    _timesAttacked++;
                }
                else
                {
                    if (attackInfo.damage > (_damageRecieved / _timesAttacked))
                    {
                        if (Random.Range(0.0f, 1.0f) >= 0.5)
                        {
                            // Resetear valores de control de da;o
                            _damageRecieved = attackInfo.damage;
                            _timesAttacked = 1;
                            // Setear nuevo locked target.
                            _lockedTarget = (BaseEnemy)attackInfo.attacker;

                            _lockedTargetPosition = new Vector3(attackInfo.attacker.transform.position.x + OffsetX,
                                                                attackInfo.attacker.transform.position.y + OffsetY);

                            //MoveTowardTarget();
                        }
                    }
                }
            }
        }
        else
        {
            _lockedTarget = (BaseEnemy)attackInfo.attacker;
            _lockedTargetPosition = new Vector3(attackInfo.attacker.transform.position.x + OffsetX,
                                                                attackInfo.attacker.transform.position.y + OffsetY);
        }
    }

    private void MoveTowardTarget()
    {
        _isMoving = true;

        // TODO: Arreglar Idle Sound...
        _audioSource.loop = true;
        PlayAudioClip("SparkyMoving");
    }

    void Update()
    {
        if (!_isMoving) return;

        transform.position = Vector3.MoveTowards(transform.position, _lockedTargetPosition, Time.deltaTime * _moveSpeed);

        float distanceSquared = (transform.position - _lockedTargetPosition).sqrMagnitude;

        if (distanceSquared < _maxDistance * _maxDistance)
        {
            _isMoving = false;
            _isOnTarget = true;

            if ((_audioSource.isPlaying) && (_audioSource.clip.name == "SparkyMoving"))
            {
                _audioSource.loop = false;
                _audioSource.Stop();
            }

            ActionFinished();
        }
    }

    private bool checkIsOnTarget()
    {
        if (_lockedTarget == null) return false;

        float distanceSquared = (transform.position - _lockedTargetPosition).sqrMagnitude;

        if (distanceSquared < _maxDistance * _maxDistance)
            return true;

        return false;
    }

    /*
    Necesitamos El Spawner y su posicion
    Si sparky esta en el spawner o no
    Si hay enemigos vivos en el spawner

    */
    //Transform movementTarget;

    bool isOnTargetPosition = false;
    EnemiesSpawnerManager targetSpawner;

    private void reachedTargetMelee()
    {
        _sparkyAnimator.SetTrigger("ReachedDestiny");
        isOnTargetPosition = true;
        DoAttack();
    }

    private void PlayAudioClip(string clipName)
    {
        if (_audioSource.isPlaying)
            _audioSource.Stop();

        _audioSource.PlayOneShot(_sparkySounds[clipName]);
    }

    private void DoAttack()
    {
        if ((_lockedTarget == null) || (_lockedTarget != null && _lockedTarget.isAlive == false))
        {
            if (targetSpawner.HasAliveEnemies())
                _lockedTarget = targetSpawner.GetRandomEnemy();
        }

        if (_lockedTarget != null)
        {
            if (_lockedTarget.isActiveAndEnabled)
            {
                if (_lockedTarget.currentBehavior == BaseEnemy.Behavior.HOSTILE)
                {
                    CombatResolver.ResolveAttack(new AttackInfo(this, _lockedTarget, BodyObjetive.LEGS, DamageType.MELEE, ShootMode.SINGLE, false, false, getAccuracy("self")));
                    PlayAudioClip("SparkyAttack");
                }
            }
        }
    }

    public override void DoAction()
    {
        /*
        if (targetSpawner == null)
            targetSpawner = GameManager.Instance.levelManager.getSpawnerAtIndex(0);

        if (isOnTargetPosition)
        {
            _sparkyAnimator.SetTrigger("Attack");
            DoAttack();
        }
        else
        {
            _sparkyAnimator.SetTrigger("isWalking");
            _movement.moveToTarget(targetSpawner.meleePosition.transform, reachedTargetMelee);
        }
        */
    }

    public void RobertAttacking(BaseCharacter enemy)
    {

    }

    private void PlayIdleClip()
    {
        int rnd = Random.Range(1, 3);
        string clipName = "SparkyIdle" + rnd.ToString();

        if (!_audioSource.isPlaying)
            PlayAudioClip(clipName);
    }

    public override void ActionFinished()
    {
        CombatManager.Instance.Logger.SubmitText("" + characterName + " Turn Ended");
    }

    public override void OnCharacterDeath()
    {
    }

    #region STATS_GETTERS

    // -------------------------------------------------------
    public override int getAccuracy(string opt)
    {
        return characterStats.Accuracy;
    }
    // -------------------------------------------------------
    public override int getDamage()
    {
        int rawDamage = 20;
        return UnityEngine.Random.Range((rawDamage - ((rawDamage * 30) / 100)), (rawDamage + ((rawDamage * 30) / 100)));
    }
    // -------------------------------------------------------
    public override int getArmor(BodyObjetive bodyPart)
    {
        return 0;
    }
    // -------------------------------------------------------
    public override float getRange()
    {
        return 15;
    }
    // -------------------------------------------------------
    public override float getEvasion()
    {
        return characterStats.Evasion;
    }
    // -------------------------------------------------------
    public override int getMorale()
    {
        return characterStats.Morale;
    }
    #endregion
}
