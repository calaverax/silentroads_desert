﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SerializableEnemy {

    public string characterName;

    public CharacterStats characterStats;
    public int spawnpoint;
    public int spawngroup;
    public string spriteName;
    public string sprefab;
    public int index; //PPP
    public string genero;
    public string tipo;
    public bool following = false;
    public bool scavenger = false;



    public SerializableEnemy(BaseEnemy enemigo, string imagen, int punto, int grupo, string pref)
    {
        characterName = enemigo.characterName;
        genero = enemigo.genre.ToString();
        tipo = enemigo.enemyType.ToString();
        characterStats = enemigo.characterStats;
        spawnpoint = punto;
        spawngroup = grupo;
        spriteName = imagen.Replace ("Sprites/NewCharacters/", "");
        sprefab = pref;
        enemigo.serializableEnemy = this; //PPP


    }

    public BaseEnemy Convert2Enemy()
    {
        BaseEnemy enemy = new BaseEnemy();
        enemy.characterName = characterName;
        enemy.characterStats = characterStats;
        return enemy;
    }

    public BaseEnemy Convert2EnemyNewStats()
    {
        BaseEnemy enemy = new BaseEnemy();
        enemy.init();
        enemy.configureCharacter(false, BaseEnemy.Genre.UNDEFINED);
        characterStats = enemy.characterStats;
        enemy.characterName = characterName;
        return enemy;
    }




}
