﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {


    public bool RandomizedEnemy;
    [Range(0,100)]
    public int chanceToSpawn;
    public BaseEnemy.EnemyType enemyType;

    public BaseEnemy character;

    public bool startWithTransition;
    public GameObject Gravestone;

    private EnemiesSpawnerManager _spawnManager;
    private GameObject _enemyPrefab;
    private GameObject _startPosition;
    private Transform robertTransform;


    // Use this for initialization
    void Awake()
    {
        robertTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        if (character != null)
            if (startWithTransition)
                character.gameObject.SetActive(false);

    }

    public void activateSpawn(EnemiesSpawnerManager manager, int spawnerIDX)
    {
        getStartPosition();

        if (character != null)
            if (startWithTransition)
                character.gameObject.SetActive(true);

        _spawnManager = manager;

        if (RandomizedEnemy)
        {
            if (character != null)
            {
                Destroy(character.gameObject);
                character = null;
            }
                        
            int chance = Random.Range(0, 101);

            if ((chance > 0) && (chance <= chanceToSpawn))
                generateEnemy();
        }
        else
        {
            // If we don't have to Randomize an enemy, just check for the prefab.
            tryGetEnemy();

            if (character == null)
            {
                Debug.Log("Spawn: " + this.gameObject.name + " Has no associated Enemy To Spawn, or Setted");
                return; // This Spawn has no associated enemy.
            }

            character.init();
            character.configureCharacter(RandomizedEnemy);

            if (startWithTransition)
                setupSpawnTransition();
            else
                character.StartCardMovement();

            character.characterName = character.getName();

        }

        if (character != null)
        {
            if (chanceToSpawn < 100)
            {
                if (Random.Range(0, 101) <= chanceToSpawn)
                    character.gameObject.SetActive(true);
                else
                    character.gameObject.SetActive(false);
            }
            character.init();
            character.configureCharacter(RandomizedEnemy);
        }

        if (character != null)
            if (character.gameObject.activeInHierarchy)
                CombatManager.Instance.Register(character);
    }

    private void setupSpawnTransition()
    {
        character.gameObject.transform.position = _startPosition.transform.position;

        character.startWithTransition(_startPosition.transform, this.gameObject.transform);
    }

    private void tryGetEnemy()
    {
        // If we dont have an enemy setted on inspect, search for the component in children
        if (character == null)
        {
            character = gameObject.GetComponentInChildren<BaseEnemy>();
        }
    }

    private void getStartPosition()
    {
        _startPosition = transform.FindChild("Start Position").gameObject;
    }

    public BaseEnemy generateEnemy()
    {
        for (int i = 0; i < _spawnManager.availableEnemiesPrefabs.Length; i++)
        {
            // Search for the correct prefab to spawn
            if (_spawnManager.availableEnemiesPrefabs[i].GetComponent<BaseEnemy>().enemyType == enemyType)
                _enemyPrefab = _spawnManager.availableEnemiesPrefabs[Random.Range(0, _spawnManager.availableEnemiesPrefabs.Length)];
        }

        if (_enemyPrefab == null)
        {
            Debug.LogError("Selected Spawn Enemy is not Available in Spawn Manager prefabs list");
            return null;
        }

        GameObject spawnedEnemy;
        spawnedEnemy = Instantiate(_enemyPrefab, _startPosition.transform.position, _startPosition.transform.rotation) as GameObject;
        //spawnedEnemy.transform.localScale *= (robertTransform.localScale.x * 0.15f) / 2.33f;

        if (startWithTransition)
            spawnedEnemy.gameObject.SetActive(false);

        spawnedEnemy.transform.SetParent(this.gameObject.transform);
        spawnedEnemy.transform.localScale = new Vector3(1, 1, 1);

        character = spawnedEnemy.GetComponent<BaseEnemy>();
        character.init();

        character.configureCharacter(false, character.genre);

        if (startWithTransition)
            setupSpawnTransition();
        else
            character.StartCardMovement();

        character.characterName = character.getName();

        //CombatManager.Instance.Register(character);

        return character;
    }
}
