﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;


public class LevelController : MonoBehaviour
{
    public Transform levelSize;
    //public Vector3 levelStartingScale = new Vector3(1,1,1);
    private Vector2 mousePosition;
    private bool overSprite;

    private bool scaleModified = false;

    void Start()
    {
        ScrollRect rect = gameObject.GetComponent<ScrollRect>();
        if (rect != null)
        {
            rect.verticalNormalizedPosition = 0;
            rect.Rebuild(CanvasUpdate.PostLayout);
           // CombatManager.Instance.Logger.SubmitText("Scroll Rect Rebuilt");
        }
        /*
        else
        {
            //CombatManager.Instance.Logger.SubmitText("Scroll Rect Not Found");
        }
        //levelSize.transform.localScale = new Vector3(2, 2, 2);
        */
    }


    public void LoadLevelSelect()
    {
        GameManager.Instance.ingameBgmController.stopCombatWonAudio();
        ScenesLoader.Instance.LoadScene("Chapter 1 Level Selection");
        /*
        DestroyImmediate(CombatManager.Instance.gameObject);
        DestroyImmediate(GameManager.Instance.gameObject);

        Application.LoadLevel("levelSelection");
        */
    }

    public bool CanBeDragged()
    {
        if (levelSize.localScale.x > 1)
            return true;

        return false;
    }

    void Update()
    {
       // checkForZoom();
        //checkForDrag();

    }
    /*
    void checkForDrag()
    {
        //Gets the world position of the mouse on the screen        
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //Checks whether the mouse is over the sprite
        overSprite = this.GetComponent<SpriteRenderer>().bounds.Contains(mousePosition);

        //If it's over the sprite
        if (overSprite)
        {
            //If we've pressed down on the mouse (or touched on the iphone)
            if (Input.GetButton("Fire1"))
            {
                //Set the position to the mouse position
                this.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                                                    Camera.main.ScreenToWorldPoint(Input.mousePosition).y,
                                                      0.0f);
            }
        }
    }
    */
    Ray2D ray;
    RaycastHit2D hit;

    void checkForZoom()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().PauseCombat();


        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            //Mathf.Clamp(Camera.main.orthographicSize += 10.0f, 500.0f, 760.9f);
            
            hit = Physics2D.Raycast(Input.mousePosition, Input.mousePosition - Camera.main.ScreenToWorldPoint(Input.mousePosition), Mathf.Infinity);//Camera.main.ScreenPointToRay(Input.mousePosition);
            if (hit)
                if (hit.collider.name == "Console")
                    return;

            levelSize.localScale = new Vector3(
                Mathf.Clamp(levelSize.localScale.x + 0.05f, 1, 2.5f),
                Mathf.Clamp(levelSize.localScale.y + 0.05f, 1, 2.5f),
                Mathf.Clamp(levelSize.localScale.z + 0.05f, 1, 2.5f)
                );
            scaleModified = true;
            
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            //Mathf.Clamp(Camera.main.orthographicSize -= 10.0f, 500.0f, 760.9f);
                
            hit = Physics2D.Raycast(Input.mousePosition, Input.mousePosition - Camera.main.ScreenToWorldPoint(Input.mousePosition), Mathf.Infinity);//Camera.main.ScreenPointToRay(Input.mousePosition);
            if (hit)
                if (hit.collider.name == "Console")
                    return;

            levelSize.localScale = new Vector3(
                Mathf.Clamp(levelSize.localScale.x - 0.05f, 1, 2.5f),
                Mathf.Clamp(levelSize.localScale.y - 0.05f, 1, 2.5f),
                Mathf.Clamp(levelSize.localScale.z - 0.05f, 1, 2.5f)
                );
            scaleModified = true;
            
        }

        Vector3 mousePos = Input.mousePosition;
        if (Input.GetMouseButton(0))//or whatever button you want
        {
            Vector3 dragPanning = Camera.main.ScreenToWorldPoint(mousePos) - Camera.main.ScreenToWorldPoint(mousePos);
            Camera.main.transform.position += dragPanning;
        }
    }
/*
    public static Vector3 FromMainScreenToWorld(this Vector3 v)
    {
        return Camera.main.ScreenToWorldPoint(v);
    }
    */
    void LateUpdate()
    {
        if (scaleModified)
        {
            GameObject.FindWithTag("PolyNav2D").GetComponent<PolyNav2D>().GenerateMap(true);
            scaleModified = false;
        }
    }
}
