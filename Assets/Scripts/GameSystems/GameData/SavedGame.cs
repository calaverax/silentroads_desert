﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SavedGame
{
    public bool tutorialCompleted = true;

    public Dictionary<string, int> completedLevels = new Dictionary<string, int>();

    public Dictionary<string, int> defeatedAgents = new Dictionary<string, int>();


    public List<string> levelsWon = new List<string>();
}
