﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class Serializer
{

	public static byte[] SerializeObject<_T>(_T objectToSerialize)
		//same as above, but should technically work anyway
	{
		BinaryFormatter bf = new BinaryFormatter();
		MemoryStream memStr = new MemoryStream();
		
		bf.Serialize(memStr, objectToSerialize);
		memStr.Position = 0;
		
		return memStr.ToArray();
	}
	
	public static _T DeserializeObject<_T>(byte[] dataStream)
	{
		MemoryStream stream = new MemoryStream(dataStream);
		stream.Position = 0;

		BinaryFormatter bf = new BinaryFormatter();
		return (_T)bf.Deserialize(stream);
	}
	
	public static void SaveSerializableObjectToFile<_T>(_T serializableObjectToSave, string path)
	{
		File.WriteAllBytes(path, SerializeObject<_T>(serializableObjectToSave));
	}
	
	public static _T LoadSerializableObjectFromFile<_T>(string path)
	{
		if (File.Exists(path))
		{
			return DeserializeObject<_T>(File.ReadAllBytes(path));
		}
		return default(_T);
	}
}