﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CharacterStats
{
    public static string StatAccuracy = "Accuracy";
    public static string StatStrength = "Strength";
    public static string StatAgility = "Agility";
    public static string StatMorale = "Morale";
    public static string StatCommand = "Command";
    public static string StatEvasion = "Evasion";
    public static string StatIntelligence = "Intelligence";
    public static string StatStealth = "Stealth";
    public static string StatHitpoints = "Hitpoints";
    public static string StatTotalHitpoints = "TotalHitpoints";
    public static string StatHostility = "Hostility";
    public static string AvailableStatsPoints = "AvailablePoints";
    public static string Statwater = "StatWater";
    public static string StatMight = "Might";

    //public static string StatCurrentHitpoints = "CurrentHitpoints";
    //public static string StatMaxHitpoints = "MaxHitpoints";
    public static string StatExperience = "Experience";
    //-----------------------------------------------------------------------------------------
    Dictionary <string, CharacterStat> _stats = new Dictionary<string, CharacterStat>();
    public Dictionary<string, CharacterStat> stats { get { return _stats; } }
    //-----------------------------------------------------------------------------------------
    public bool hasAnyPenalty()
    {
        foreach (KeyValuePair<string, CharacterStat> stat in _stats)
            if (stat.Value.CheckPenalties())
                return true;

        return false;
    }
    //-----------------------------------------------------------------------------------------
    public void checkPenalties()
    {
        foreach (KeyValuePair<string, CharacterStat> stat in _stats)
            stat.Value.CheckPenalties();
    }
    //-----------------------------------------------------------------------------------------
    public CharacterStat getStat(string statName)
    {
        if (_stats.ContainsKey(statName))
            return _stats[statName];

        return null;
            
    }
    //-----------------------------------------------------------------------------------------
    public bool AddStat(string name, int value)
    {
        if (_stats.ContainsKey(name))
            return false;

        _stats.Add(name, new CharacterStat(name, value));

        return true;
    }
    //-----------------------------------------------------------------------------------------
    public int Accuracy
    {
        get
        {
            if (_stats.ContainsKey(StatAccuracy))
                return _stats[StatAccuracy].CurrentValue;
            else
                return -999;
        }
        set
        {
            if (_stats.ContainsKey(StatAccuracy))
                _stats[StatAccuracy].setValue(value);
        }
    }
    //-----------------------------------------------------------------------------------------
    public int Strength
    {
        get
        {
            if (_stats.ContainsKey(StatStrength))
                return _stats[StatStrength].CurrentValue;
            else
                return -999;
        }
        set
        {
            if (_stats.ContainsKey(StatStrength))
                _stats[StatStrength].setValue(value);
        }
    }
    //-----------------------------------------------------------------------------------------
    public int Agility
    {
        get
        {
            if (_stats.ContainsKey(StatAgility))
                return _stats[StatAgility].CurrentValue;
            else
                return -999;
        }
        set
        {
            if (_stats.ContainsKey(StatAgility))
                _stats[StatAgility].setValue(value);
        }
    }
    //-----------------------------------------------------------------------------------------
    public int Morale
    {
        get
        {
            if (_stats.ContainsKey(StatMorale))
                return _stats[StatMorale].CurrentValue;
            else
                return -999;
        }
        set
        {
            if (_stats.ContainsKey(StatMorale))
                _stats[StatMorale].setValue(value);
        }
    }
    //-----------------------------------------------------------------------------------------
    public int Might
    {
        get
        {
            if (_stats.ContainsKey(StatMight))
                return _stats[StatMight].CurrentValue;
            else
                return -999;
        }
        set
        {
            if (_stats.ContainsKey(StatMight))
                _stats[StatMight].setValue(value);
        }
    }
    //-----------------------------------------------------------------------------------------
    public int Command
    {
        get
        {
            if (_stats.ContainsKey(StatCommand))
                return _stats[StatCommand].CurrentValue;
            else
                return -999;
        }
        set
        {
            if (_stats.ContainsKey(StatCommand))
                _stats[StatCommand].setValue(value);
        }
    }
    //-----------------------------------------------------------------------------------------
    public int Evasion
    {
        get
        {
            if (_stats.ContainsKey(StatEvasion))
                return _stats[StatEvasion].CurrentValue;
            else
                return -999;
        }
        set
        {
            if (_stats.ContainsKey(StatEvasion))
                _stats[StatEvasion].setValue(value);
        }
    }
    //-----------------------------------------------------------------------------------------
    public int Intelligence
    {
        get
        {
            if (_stats.ContainsKey(StatIntelligence))
                return _stats[StatIntelligence].CurrentValue;
            else
                return -999;
        }
        set
        {
            if (_stats.ContainsKey(StatIntelligence))
                _stats[StatIntelligence].setValue(value);
        }
    }
    //-----------------------------------------------------------------------------------------
    public int Stealth
    {
        get
        {
            if (_stats.ContainsKey(StatStealth))
                return _stats[StatStealth].CurrentValue;
            else
                return -999;
        }
        set
        {
            if (_stats.ContainsKey(StatStealth))
                _stats[StatStealth].setValue(value);
        }
    }
    //-----------------------------------------------------------------------------------------
    public int Hitpoints
    {
        get
        {
            if (_stats.ContainsKey(StatHitpoints))
                return _stats[StatHitpoints].CurrentValue;
            else
                return -999;
        }
        set
        {
            if (_stats.ContainsKey(StatHitpoints))
                _stats[StatHitpoints].setValue(value);
        }
    }
    //-----------------------------------------------------------------------------------------
    public int TotalHitpoints
    {
        get
        {
            if (_stats.ContainsKey(StatTotalHitpoints))
                return _stats[StatTotalHitpoints].CurrentValue;
            else
                return -999;
        }
        set
        {
            if (_stats.ContainsKey(StatTotalHitpoints))
                _stats[StatTotalHitpoints].setValue(value);
        }
    }
    //-----------------------------------------------------------------------------------------
    public int Hostility
    {
        get
        {
            if (_stats.ContainsKey(StatHostility))
                return _stats[StatHostility].CurrentValue;
            else
                return -999;
        }
        set
        {
            if (_stats.ContainsKey(StatHostility))
                _stats[StatHostility].setValue(value);
        }
    }
    //-----------------------------------------------------------------------------------------
    /*
    //-----------------------------------------------------------------------------------------
    public int CurrentHitpoints
    {
        get
        {
            if (stats.ContainsKey(StatCurrentHitpoints))
                return stats[StatCurrentHitpoints].CurrentValue;
            else
                return -999;
        }
    }
    //-----------------------------------------------------------------------------------------
    public int MaxHitpoints
    {
        get
        {
            if (stats.ContainsKey(StatMaxHitpoints))
                return stats[StatMaxHitpoints].CurrentValue;
            else
                return -999;
        }
    }
    //-----------------------------------------------------------------------------------------
    */
    public int Experience;
    public int availablePoints
    {
        get
        {
            if (_stats.ContainsKey(AvailableStatsPoints))
                return _stats[AvailableStatsPoints].CurrentValue;
            else
                return -999;
        }
        set
        {
            if (_stats.ContainsKey(AvailableStatsPoints))
                _stats[AvailableStatsPoints].setValue(value);
        }
    }
    public int water;
    //-----------------------------------------------------------------------------------------
}
[System.Serializable]
public class CharacterStat
{
    string _name = "";
    int _currentValue;
    int _originalValue;
    List<StatPenalty> _penalties = new List<StatPenalty>();
    public List<StatPenalty> penalties { get { return _penalties; } }

    public string GetName { get { return _name; } }
    public int OriginalValue { get { return _originalValue; } }

    public CharacterStat(string name, int originalValue)
    {
        _name = name;
        _originalValue = originalValue;
        _currentValue = originalValue;
    }

    public int overrideDebugSetValue(int value)
    {
        _originalValue = value;
        _currentValue = value;
        return value;
    }

    public void setValue(int value)
    {
        _currentValue = value;
    }
    
    public void modifyValue(int amnt)
    {
        _currentValue += amnt;
    }
    
    public void ReduceStat()
    {
        _currentValue--;
        _originalValue--;
    }

    public void IncreaseStat()
    {
        _currentValue++;
        _originalValue++;
    }

    public void ReduceStat(int amnt)
    {
        _currentValue -= amnt;
        _originalValue -= amnt;
    }

    public void IncreaseStat(int amnt)
    {
        _currentValue += amnt;
        _originalValue += amnt;
    }


    public void addPenalty(StatPenalty penalty)
    {
        // 
        if (_penalties.Contains(penalty))
        {
            _penalties[_penalties.IndexOf(penalty)].restoreDuration(penalty.duration);
        }
        else
        {
            _penalties.Add(penalty);
            _currentValue -= (int)((float)_originalValue * penalty.ratio);
        }
    }

    public bool CheckPenalties()
    {
        bool needToRemove = false;
        foreach (StatPenalty penalty in _penalties)
        {
            penalty.reduceDuration();

            if (penalty.duration == 0)
            {
                needToRemove = true;
                RemovePenalty(penalty);
            }
        }

        if (needToRemove)
            _penalties.RemoveAll(item => item.duration == 0);

        if (_penalties.Count > 0)
            return true;

        return false;
    }

    public void RemovePenalty(StatPenalty penalty)
    {
        if (_penalties.Contains(penalty))
        {
            // Remove it from the list
            //_penalties.Remove(penalty);

            // Add to the current value the ratio we have removed before
            _currentValue += (int)((float)_originalValue * penalty.ratio);
        }
    }


    public int CurrentValue
    {
        get
        {
            /*
            float reduction = 0;
            if (_penalties.Count > 0)
            {
                for (int i = 0; i < _penalties.Count; i++)
                {
                    _currentValue -= (int)((float)_originalValue * _penalties[i].ratio);
                    //reduction += _penalties[i].ratio;
                }
            }
            */
            return _currentValue;
        }
    } 
}
[System.Serializable]
public class StatPenalty
{
    float _ratio;
    int _duration;

    public float ratio { get { return _ratio; } }
    public int duration { get { return _duration; } }

    public StatPenalty(float factor, int duration)
    {
        _ratio = factor;
        _duration = duration;
    }

    public void reduceDuration()
    {
        if (_duration != -999)
            _duration--;
    }

    public void restoreDuration(int amnt)
    {
        _duration = amnt;
    }
}


