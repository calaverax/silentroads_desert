﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SavedPreferences
{
    public bool backGroundMusic = true;
    public bool soundFX = true;
    public int qualityLevel = 0;
    

}
