﻿using UnityEngine;
using System.Collections;

public class GamePreferences : Singleton<GamePreferences>
{
    public SavedPreferences savedPreferences;

    public void loadGamePreferences()
    {
        savedPreferences = Serializer.LoadSerializableObjectFromFile<SavedPreferences>(Application.persistentDataPath + "/savedPreferences.sf");

        if (savedPreferences == null)
        {
            savedPreferences = new SavedPreferences();
            saveGamePreferences();
        }
    }

    public void saveGamePreferences()
    {
        Serializer.SaveSerializableObjectToFile<SavedPreferences>(savedPreferences, Application.persistentDataPath + "/savedPreferences.sf");
    }

}
