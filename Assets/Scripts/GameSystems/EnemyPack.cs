﻿using UnityEngine;
using System.Collections;

public class EnemyPack : MonoBehaviour {

    public GameObject packLeaderSpawn;
    public MobLeaderFX packLeaderFX;
    public GameObject[] packMembersSpawners;

    private BaseEnemy packLeader;
    private BaseEnemy[] packMembers;
       
	// Use this for initialization
	void Start ()
    {
        if (packLeaderSpawn != null)
        {
            EnemySpawner spawn = packLeaderSpawn.GetComponent<EnemySpawner>();
            if (spawn != null)
            {
                // Try to Get Che spawned Character
                packLeader = spawn.character;

                // If there's none, then Spawn it
                if (packLeader == null)
                {
                    packLeader = spawn.generateEnemy();
                }
                packLeader.isPackLeader = true;
                packLeaderFX.Initialize(packLeader);

                //spawn.character.container.ActivateHeader();
                CombatManager.Instance.packLeaderIsAlive = true;
                CombatManager.Instance.LevelBoss = packLeader;
            }
        }
	
	}
	
}
