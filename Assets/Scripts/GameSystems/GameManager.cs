﻿using UnityEngine;
using System.Collections;

public class GameManager : Singleton<GameManager> {

    private SavedGame _savedGame;
    public SavedGame savedGame { get { return _savedGame; } }

    public int selectedChapter = 1;
    public int selectedLevel = 1;
    public string selectedLevelId;
    public GameObject spawnedLevel;


    private IngameBGMController _ingameBgmController;
    public IngameBGMController ingameBgmController
    {
        get
        {
            if (_ingameBgmController == null)
                _ingameBgmController = GameObject.FindGameObjectWithTag("LevelBGMController").GetComponent<IngameBGMController>();
            return _ingameBgmController;
        }
    }
    

    private LevelManager _levelManager;

    public LevelManager levelManager
    {
        get
        {
            if (_levelManager == null)
                _levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
            return _levelManager;
        }
    }

	// Use this for initialization
	void Awake ()
    {
        loadSavedGame();

	}

    public void loadSavedGame()
    {
        _savedGame = Serializer.LoadSerializableObjectFromFile<SavedGame>(Application.persistentDataPath + "/savedGame.sf");

        if (_savedGame == null)
        {
            Debug.Log("No habia datos guardados");
            _savedGame = new SavedGame();
            saveGame();
        }
    }

    public void saveGame()
    {
        Serializer.SaveSerializableObjectToFile<SavedGame>(_savedGame, Application.persistentDataPath + "/savedGame.sf");
    }

    public void setSelectedLevel()
    {
        selectedLevelId = "Chapter-" + selectedChapter + "Level-" + selectedLevel;
        Debug.Log("Selected Level ID: " + selectedLevelId + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }

    public int getLevelCompletion(string levelId)
    {
        if (_savedGame.completedLevels.ContainsKey(levelId))
            return _savedGame.completedLevels[levelId];
        return 0;
    }

    public int checkDefeatedAgents(int chapter)
    {
        string agentsID = "Chapter" + chapter + "-Agents";

        if (_savedGame.defeatedAgents.ContainsKey(agentsID))
            return _savedGame.defeatedAgents[agentsID];

        return 0;
    }

    public int getLevelCompletion(int chapter, string levelId)
    {
        string id = "Chapter-" + chapter + levelId;

        if (_savedGame.completedLevels.ContainsKey(id))
        {
            return _savedGame.completedLevels[id];
        }

        return 0;
    }

    public void resetSavedGame()
    {
        _savedGame = new SavedGame();
        saveGame();
    }

    public void SetLevelWon(string levelID)
    {
        if (levelID == null) return;

        if (!(isLevelWon(levelID)))
            _savedGame.levelsWon.Add(levelID);

        saveGame();
    }

    public bool isLevelWon(string levelID)
    {
        if (levelID == null)
            return false;

        return _savedGame.levelsWon.Contains(levelID);
    }
}
