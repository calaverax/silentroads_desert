﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataManager : Singleton<PlayerDataManager> {

    public PlayerDataDB _data;

    public void EmptyData()
    {
        _data = new PlayerDataDB();
        Save();
    }

    public void Save()
    {
        Serializer.SaveSerializableObjectToFile<PlayerDataDB>(_data, Application.persistentDataPath + "/" + "PlayerDataSystem.persistence");
    }

    public void Load()
    {
        _data = Serializer.LoadSerializableObjectFromFile<PlayerDataDB>(Application.persistentDataPath + "/" + "PlayerDataSystem.persistence");
        if(_data == null)
        {
            EmptyData();
        }
    }

    public void SetValue(string name, int value)
    {
        if (!_data._dataList.Exists(x => x.GetDataName() == name))
        {
            _data._dataList.Add(new PlayerData(name, value));
        }
        else
        {
            _data._dataList.Find(x => x.GetDataName() == name).SetValue(value);
        }

        Save();
    }

    public int GetValue(string name, int defaultValue)
    {
        if (!_data._dataList.Exists(x => x.GetDataName() == name))
        {
            _data._dataList.Add(new PlayerData(name, defaultValue));
        }

        return _data._dataList.Find(x => x.GetDataName() == name).GetValue(defaultValue);
    }

}
[System.Serializable]
public class PlayerDataDB
{
    public List<PlayerData> _dataList;

    public PlayerDataDB()
    {
        _dataList = new List<PlayerData>();
    }
}

[System.Serializable]
public class PlayerData{
    private bool _initialized;
    private string _name;
    private int _value;

    public PlayerData(string name, int value)
    {
        _initialized = false;
        _name = name;
        _value = value;

    }

    public string GetDataName()
    {
        return _name;
    }

    public int GetValue(int defaultValue)
    {
        if (!_initialized) {
            _value = defaultValue;
            _initialized = true;
        }

        return _value;

    }

    public void SetValue(int value)
    {
        if (!_initialized)
        {
            _initialized = true;
        }

        _value = value;
    }
}
