﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class EnemiesSpawnerManager : MonoBehaviour {

    private List<EnemySpawner> _spawners = new List<EnemySpawner>();
        public List<EnemySpawner> spawners { get
        {
            if (_spawners.Count == 0)
                _spawners = GetComponentsInChildren<EnemySpawner>().ToList();
            return _spawners;
        }
    }

    public GameObject[] availableEnemiesPrefabs;
    public GameObject meleePosition;

    private bool _levelSpawned = false;
    public bool levelSpawned { get { return _levelSpawned; } }

    // Use this for initialization
	void Start ()
    {
        _spawners = GetComponentsInChildren<EnemySpawner>().ToList();
        CombatManager.Instance.OnCharacterDies.AddListener(OnCharacterDeathListener);
	}

    void OnCharacterDeathListener(BaseCharacter character)
    {

        for (int i = 0; i < _spawners.Count; i++)
        {
            if (character == _spawners[i].character)
            {
                if (_spawners[i].Gravestone != null)
                {
                    _spawners[i].Gravestone.SetActive(true);
                    CombatManager.Instance.NpcFXController.PlayEventSound("TombStone");
                }

                _spawners.Remove(_spawners[i]);

                Debug.Log(character.name + " Died, Removing it from spawns list.");
            }
        }

        if (_spawners.Count == 0)
            Debug.Log("All enemies from this group are dead");
    }

    public void activateSpawns()
    {
        Debug.Log("ENTRO AL ACTIVATE SPAWNS");
        Debug.Log("LA CUENTA ES : " + GameManager.Instance.savedGame.levelsWon.Count);
        if (!(GameManager.Instance.isLevelWon(Application.loadedLevelName)))
        {
            Debug.Log("ENTRO AL ACTIVATE SPAWNS Y AL IF");
            for (int i = 0; i < _spawners.Count; i++)
            {
                _spawners[i].activateSpawn(this, i);
            }
        }
        
        _levelSpawned = true;
        

        //Invoke("startInsideMusic", 3.0f);
        //startInsideMusic();
        CombatManager.Instance.levelSpawned = true;

        GameObject combatMode = GameObject.FindGameObjectWithTag("CombatMode");
        if (combatMode) { TarjetasEnemigos tarjeta = combatMode.GetComponent<TarjetasEnemigos>();
        tarjeta.ActualizarNumTarjetas();
        }
        
    }
    /*
    private void startInsideMusic()
    {
        GameManager.Instance.ingameBgmController.entryBGM.Play();
    }
    */

    public bool HasAliveEnemies()
    {
        if (spawners.Count > 0)
            return true;

        return false;
    }

    public BaseEnemy GetRandomEnemy()
    {
        if (spawners.Count > 1)
        {
            return (spawners[Random.Range(0, spawners.Count)].character);
        }
        else
        {
            return spawners[0].character;
        }
    }

}
