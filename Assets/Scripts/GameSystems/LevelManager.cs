﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


    public class LevelManager : MonoBehaviour
    {

        public enum ScenarioSize
        {
            Small = 50,
            Medium = 20,
            Large = 6
        }

        public enum TimeOfDay
        {
            Dawn,
            Day,
            Night
        }
        public bool isInterior;
        public TimeOfDay timeOfday;
        public ScenarioSize scenarioSize;
        public bool startWithLevelInfo;
        public bool spawnEnemiesFirst;
        public GameObject levelInfo;
        public Robert robert;
        public Sparky sparky;
        public EnemySoundsManager enemiesSoundManager1, enemiesSoundManager2;
        public Wasabi.InventorySystem.ISCharactersDatabase charactersDatabase;
        //----------------------------
        private List<EnemiesSpawnerManager> _enemiesSpawners = new List<EnemiesSpawnerManager>();
        public List<EnemiesSpawnerManager> enemiesSpawners { get { return _enemiesSpawners; } }
        //----------------------------
        void Awake()
        {
        PlayerDataManager.Instance.Load();
            SparkyPath sp = FindObjectOfType<SparkyPath>();
        if (sp != null)
        {
            sp.gameObject.SetActive(false);
        }
            LevelHistory.Instance.AddLevel();
            GameObject[] spawners = GameObject.FindGameObjectsWithTag("EnemiesSpawners");
            //GameObject go = GameObject.FindGameObjectWithTag("EnemiesSpawners");

            if (spawners == null) { Debug.LogError("No Enemies Spawners Setted"); return; }

            foreach (GameObject go in spawners)
            {
                _enemiesSpawners.Add(go.GetComponent<EnemiesSpawnerManager>());
            }

        if (!isInterior)
        {       //Equiprobabilidad
                //timeOfday = (TimeOfDay)((int)Random.Range(-0.49f,2.49f));

                float aleatorio = Random.Range(1f, 10f);
                if (aleatorio < 6f)
                {
                    timeOfday = TimeOfDay.Day;
                }else
                {
                    if(aleatorio < 9f)
                    {
                        timeOfday = TimeOfDay.Night;
                    }
                    else
                    {
                        timeOfday = TimeOfDay.Dawn;
                    }   
                }
                UnityStandardAssets.ImageEffects.ColorCorrectionCurves[] correcciones = Camera.main.gameObject.GetComponents<UnityStandardAssets.ImageEffects.ColorCorrectionCurves>();
                switch ((int)timeOfday)
                {
                case 0:
                    correcciones[0].enabled = true;
                    break;
                case 2:
                    correcciones[1].enabled = true;
                    break;
                default:
                    Debug.Log("Is Day");
                    break;
                }
            }

            //_enemiesSpawners = go.GetComponent<EnemiesSpawnerManager>();

            robert.sparky = sparky;
            CombatManager.Instance.Register(robert);

  

        levelInfo.SetActive(startWithLevelInfo);

            if (startWithLevelInfo)
            {
                if (spawnEnemiesFirst)
                    Invoke("startSpawns", .01f);
            }
            else
            //if (!startWithLevelInfo)
            {
                Invoke("startSpawns", .01f);
            }

            //processLoot();
        }

        public void mapInfoClosed()
        {
            CombatManager.Instance.OnCombatResumed.AddListener(startSpawns);
            GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().levelInfoToggle.isOn = false;
            Debug.Log("Map Level Info Closed");
        }

        private void startSpawns()
        {
            Debug.Log("Combat Resumed After Map Level Info");
            CombatManager.Instance.OnCombatResumed.RemoveListener(startSpawns);
            GameManager.Instance.ingameBgmController.changeSnapshot("Normal");

            foreach (EnemiesSpawnerManager spawner in _enemiesSpawners)
            {
                if (!spawner.levelSpawned) spawner.activateSpawns();
            }

            if (PlayerPrefs.GetInt("BGM-Muted") == 0)
                GameManager.Instance.ingameBgmController.entryBGM.mute = false;
            else if (PlayerPrefs.GetInt("BGM-Muted") == 1)
                GameManager.Instance.ingameBgmController.entryBGM.mute = true;


            GameManager.Instance.ingameBgmController.entryBGM.Play();
            

    }

        public List<EnemiesSpawnerManager> getEnemiesSpawners()
        {
            return _enemiesSpawners;
        }

        public EnemiesSpawnerManager getSpawnerAtIndex(int index)
        {
            if (_enemiesSpawners.Count > index)
                return _enemiesSpawners[index];
            else
                Debug.LogError("Requeste Index Out Of Range for Enemies Spawners");

            return null;
        }


    }

