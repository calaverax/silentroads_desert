﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShuffleBag
{

    private System.Random random = new System.Random();
    private List<AudioClip> data;

    private AudioClip currentItem;
    private int currentPosition = -1;

    private int Capacity { get { return data.Capacity; } }
    public int Size { get { return data.Count; } }


    public ShuffleBag(int initCapacity)
    {
        data = new List<AudioClip>(initCapacity);
    }

    public void Add(AudioClip item)
    {
        data.Add(item);

        currentPosition = Size - 1;
    }

    public AudioClip Next()
    {
		if ((data == null) || (data.Count == 0))
			return null;

		if (currentPosition <= 1)
        //if (currentPosition  1)
        {
			currentPosition = 0;//Size - 1;
            currentItem = data[0];

            return currentItem;
        }

        var pos = random.Next(currentPosition);

        currentItem = data[pos];
        data[pos] = data[currentPosition];
        data[currentPosition] = currentItem;

        currentPosition--;

        return currentItem;
    }

    public bool Contains(AudioClip item)
    {
        if (data.Contains(item))
            return true;

        return false;
    }


}
