﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class LevelProperties : MonoBehaviour
{
    public Transform playerSpawnpoint;
    public Transform sparkySpawnpoint;
    public RectTransform levelSize;
    public RectTransform screenSize;
    public GameObject enemiesSpawners;
    public GameObject levelInfo;

    public EnemySoundsManager enemiesSoundManager1, enemiesSoundManager2;

    public bool hasShop = false;
    public GameObject shop;

    public bool definedEncounter;
    public BaseEnemy.EnemyType enemiesType;
    public int enemiesMin, enemiesMax;

    private Transform[] _enemiesSpawnPoints;
    public Transform[] enemiesSpawnPoints { get { return _enemiesSpawnPoints; } }


    private Vector2 _originalLevelSize = Vector2.zero;

    public struct EnemySpawnPoint
    {
        public Transform startPosition, endPosition;
        public EnemySpawnPoint(Transform startPosition, Transform endPosition)
        {
            this.startPosition = startPosition;
            this.endPosition = endPosition;
        }
    }

    public List<EnemySpawnPoint> enemiesSpawns = new List<EnemySpawnPoint>();

    public UnityEvent OnLevelInfoClosed;
    private bool firstInfoClosed = false;

    public void startSpawning()
    {
        if (!firstInfoClosed)
            OnLevelInfoClosed.Invoke();

        firstInfoClosed = true;
    }

    void Awake()
    {
        OnLevelInfoClosed = new UnityEvent();

        _enemiesSpawnPoints = enemiesSpawners.GetComponentsInChildren<Transform>();
        _enemiesSpawnPoints = _enemiesSpawnPoints.Where(val => val != _enemiesSpawnPoints[0]).ToArray();
        _enemiesSpawnPoints = _enemiesSpawnPoints.Where(val => val.name == "Start Position").ToArray();

        foreach (Transform trans in _enemiesSpawnPoints)
        {
            enemiesSpawns.Add(new EnemySpawnPoint(trans, trans.parent));
        }

        /*
        _enemiesSpawnPoints = enemiesSpawners.GetComponentsInChildren<Transform>();
        _enemiesSpawnPoints = _enemiesSpawnPoints.Where(val => val != _enemiesSpawnPoints[0]).ToArray();
        _enemiesSpawnPoints = _enemiesSpawnPoints.Where(val => val.name != "Start Position").ToArray();
        */
        _enemiesSpawnPoints.Shuffle();
        _originalLevelSize = levelSize.sizeDelta;
    }

    void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            levelSize.localScale = new Vector3(
                Mathf.Clamp(levelSize.localScale.x + 0.05f, 1, 2.5f),
                Mathf.Clamp(levelSize.localScale.y + 0.05f, 1, 2.5f),
                Mathf.Clamp(levelSize.localScale.z + 0.05f, 1, 2.5f)
                );
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            levelSize.localScale = new Vector3(
                Mathf.Clamp(levelSize.localScale.x - 0.05f, 1, 2.5f),
                Mathf.Clamp(levelSize.localScale.y - 0.05f, 1, 2.5f),
                Mathf.Clamp(levelSize.localScale.z - 0.05f, 1, 2.5f)
                );
        }
    }

    public void LevelZoomIn()
    {
        //levelSize.localScale = new Vector3(1,1,1);
        levelSize.localScale = new Vector3(2, 2, 1);
    }

    public void LevelZoomOut()
    {
        levelSize.localScale = new Vector3(1, 1, 1);
        levelSize.localPosition = new Vector3(0,0,levelSize.localPosition.z);
        //   levelSize.localScale = new Vector3(0.58f, 0.58f, 1);
        //levelSize.localPosition = new Vector3(0,0,levelSize.localPosition.z);
    }
}
