﻿using UnityEngine;
using Wasabi.InventorySystem;
using System.Collections;
using System.Collections.Generic;

public class LevelsProgressions : Singleton<LevelsProgressions>
{
    // TODO:

    /*
    Persistir Para Cada nivel:

    - Si el nivel Ya se empezo.
    - Enemigos Generados en cada Spawn. (Y si esta vivo o muerto)

    - Loot que hay en la ventana de Loot.

    */

    SavedLevelStatus _savedLevelStatus;
    public SavedLevelStatus savedLevelStatus { get { return _savedLevelStatus; } }

    public bool getLevelStatus(string levelID)
    {
        _savedLevelStatus = Serializer.LoadSerializableObjectFromFile<SavedLevelStatus>(Application.persistentDataPath + "/" + levelID + "-save.data");

        if (_savedLevelStatus == null)
            return false;
        else
            return true;
    }
    
    public void saveLevelStatus(string levelID)
    {
        Serializer.SaveSerializableObjectToFile<SavedLevelStatus>(_savedLevelStatus, Application.persistentDataPath + "/" + levelID + "-save.data");
    }

    public void startLevelSave()
    {
        if (_savedLevelStatus != null)
            Debug.LogError("Saved Status is Not Null");

        _savedLevelStatus = new SavedLevelStatus();
    }
}

[System.Serializable]
public class SavedLevelStatus
{
    bool _isStarted;
    List<SpawnpointInfo> _spawnpointsInfo = new List<SpawnpointInfo>();
    //List<ISObject> _lootableItems = new List<ISObject>();

    public bool isStarted { get { return _isStarted; } }
    public List<SpawnpointInfo> spawnpointsInfo {  get { return _spawnpointsInfo; } }
    //public List<ISObject> lootableItems { get { return _lootableItems; } }

    public SavedLevelStatus()
    {
        _isStarted = true;
    }
}

[System.Serializable]
public class SpawnpointInfo
{
    int _spawnpointIdx;
    int _enemyIdx;
    bool _isAlive;

    public int spawnPointIDX {  get { return _spawnpointIdx; } }
    public int enemyIDX { get { return _enemyIdx; } }
    public bool isAlive { get { return _isAlive; } }

    public SpawnpointInfo(int spawn, int enemy, bool alive)
    {
        _spawnpointIdx = spawn;
        _enemyIdx = enemy;
        _isAlive = alive;
    }
}

