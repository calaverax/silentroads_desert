﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathDefinition : MonoBehaviour {

    public Transform[] points;

    public IEnumerator<Transform> GetReversedPath()
    {
        if ((points == null) || points.Length < 1)
            yield break;

        int index = points.Length -2;

        while (true)
        {
            if (index <= 0)
                index = 0;

            yield return points[index];

            if (points.Length <= 0)
                continue;

            index--;
        }
    }

    public IEnumerator<Transform> GetPathEnumerator()
    {
        if ((points == null) || points.Length < 1)
            yield break;

        int index = 0;

        while (true)
        {
            yield return points[index];

            if ((points.Length == 0) || (index == points.Length -1))
                continue;

            index++;
        }
    }

    void OnDrawGizmosSelected()
    {
        if ((points == null) || points.Length < 2)
            return;

        for (int i = 1; i < points.Length; i++)
            Gizmos.DrawLine(points[i - 1].position, points[i].position);

    }

}
