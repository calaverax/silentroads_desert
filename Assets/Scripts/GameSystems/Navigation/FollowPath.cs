﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowPath : MonoBehaviour
{
    public enum FollowType
    {
        MoveTowards,
        Lerp,
    }

    public FollowType followType;
    public PathDefinition path;
    public float moveSpeed = 1;
    public float maxDistance = .1f;

    private IEnumerator<Transform> _currentPoint;
    private bool _isMoving = false;
    private Vector3 _lastPosition;

    public void MoveToTarget(PathDefinition path, bool inversed = false, FollowType followType = FollowType.MoveTowards)
    {
        if (path == null)
        {
            Debug.LogError("Path cannot be null");
            return;
        }

        this.followType = followType;

        if (inversed)
            _currentPoint = path.GetReversedPath();
        else
            _currentPoint = path.GetPathEnumerator();

        _currentPoint.MoveNext();
        _isMoving = true;
    }

    public void Start()
    {
        if (path == null)
        {
            return;
        }

        _currentPoint = path.GetPathEnumerator();

        _currentPoint.MoveNext();

        if (_currentPoint.Current == null)
            return;

        //transform.position = _currentPoint.Current.position;
    }

    void Update()
    {
        if (!_isMoving) return;

        if ((_currentPoint == null) || (_currentPoint.Current == null))
            return;

        if (followType == FollowType.MoveTowards)
            transform.position = Vector3.MoveTowards(transform.position, _currentPoint.Current.position, Time.deltaTime * moveSpeed);
        else if (followType == FollowType.Lerp)
            transform.position = Vector3.Lerp(transform.position, _currentPoint.Current.position, Time.deltaTime * moveSpeed);

        float distanceSquared = (transform.position - _currentPoint.Current.position).sqrMagnitude;

        if (distanceSquared < maxDistance * maxDistance)
        {
            _currentPoint.MoveNext();

            if (_currentPoint.Current.position == _lastPosition)
                _isMoving = false;
            else
                _lastPosition = _currentPoint.Current.position;
        }
    }
}
