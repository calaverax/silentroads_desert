﻿using UnityEngine;
using System.Collections;

public class LevelNagivationManager : MonoBehaviour {

    public Transform levelMidPoint;

    private Robert _robert;

    private bool _robertIsOnMidPoint = false;
    public bool RobertOnMid {  get { return _robertIsOnMidPoint; } }


    void Start()
    {
        _robert = CombatManager.Instance.robert;
    }

    public void MoveToPoint()
    {
       // _robert.MoveAction(levelMidPoint,this);
    }

    public void robertReachedMid()
    {
        _robertIsOnMidPoint = true;
        levelMidPoint.gameObject.SetActive(false);
    }

}
