﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickMark : MonoBehaviour {

    public float sec;
    SpriteRenderer mesh;

    void Start()
    {
        mesh = gameObject.GetComponent<SpriteRenderer>();
        mesh.enabled = false;
    }

    public void Target()
    {
        mesh.enabled = true;
        StartCoroutine(Ctarget());
    }

    public IEnumerator Ctarget()
    {
        yield return new WaitForSeconds(sec);
        mesh.enabled = false;

    }
}
