﻿using UnityEngine;
using System.Collections;

public class GameChapterState : MonoBehaviour {

	Chapter1Preferences ch1pref = new Chapter1Preferences();
	
    // Use this for initialization
	//void Start () 
	//{
		//ch1pref.ProcessChapter ();
		/*
		ch1pref.GenerateLevelsData ();
		ch1pref.LoadLevels ();
		*/

		//SerializableLevel ch1l1 = Serializer.LoadSerializableObjectFromFile<SerializableLevel>(Application.persistentDataPath /*+ "/LevelsData/"*/ + "Chapter1Level1.LevelData");
	//}

    public void ProcessChapter1()
    {
        ch1pref.ProcessChapter();
    }

}
