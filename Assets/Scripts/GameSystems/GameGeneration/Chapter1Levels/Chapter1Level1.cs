﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Chapter1Level1 : SerializableLevel
{
	// -- Parametros por nivel:
	// --- Tipo de Enemigos.
	// --- Cantidad de enemigos (min - max)
	// ---- Boss?
	// --- Water Well
	// --- Loot Cache
	// --- Vendor with Item List, Inflation Ratio.
	
    
    public override void LoadLevelData ()
	{      
        // Configure Enemy Groups
		EnemyGroup group1 = new EnemyGroup ();

        EnemyProperties enemy;
        //-----------------------------
        enemy = new EnemyProperties();
        enemy.Type = "Agent";
        enemy.Gender = "Male";
        enemy.isAlive = true;

        group1.enemies.Add(enemy);
        //-----------------------------
        enemy = new EnemyProperties();
        enemy.Type = "Agent";
        enemy.Gender = "Female";
        enemy.isAlive = true;

        group1.enemies.Add(enemy);

        //-----------------------------
        enemy = new EnemyProperties();
        enemy.Type = "Agent";
        if (Random.Range(0.0f, 1.0f) >= 0.5f)
            enemy.Gender = "Female";
        else
            enemy.Gender = "Male";

        enemy.isAlive = true;

        group1.enemies.Add(enemy);

        //-----------------------------

        group1.HaveBoss = true;
        group1.BossName = "Level1Boss";

        group1.generatedAmount = group1.enemies.Count;
		//group1.EnemyType = "MaleAgent";

        /*
		group1.minAmout = 2;
		group1.maxAmount = 2;

		if (group1.minAmout != group1.maxAmount)
			group1.generatedAmount = (Random.Range (group1.minAmout, group1.maxAmount));
		else
			group1.generatedAmount = group1.minAmout;
        */

        enemiesGroups.Add(group1);
		//---------------------------------------------
		// Configure Water Well
		// Si no existe en el nivel, quedara como null.
		//levelWaterWell.Available = false;
		//---------------------------------------------
		// Configure Loot Cache
		// Si no existe en el nivel, quedara como null.
		//lootCache.Available = false;
		//---------------------------------------------
		// Configure Vendor
		// Si no existe en el nivel, quedara como null.
		//vendor.Available = false;
		//---------------------------------------------
	}

}
