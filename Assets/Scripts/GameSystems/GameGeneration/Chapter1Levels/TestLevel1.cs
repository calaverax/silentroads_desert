﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TestLevel1 : SerializableLevel
{
    public override void LoadLevelData()
    {
        // Configure Enemy Groups
        EnemyGroup group1 = new EnemyGroup();

        EnemyProperties enemy;
        
        //-----------------------------
        enemy = new EnemyProperties();
        enemy.Type = "Agent";
        enemy.Gender = "Male";
        enemy.isAlive = true;

        group1.enemies.Add(enemy);
        //-----------------------------
        enemy = new EnemyProperties();
        enemy.Type = "Agent";
        enemy.Gender = "Female";
        enemy.isAlive = true;

        group1.enemies.Add(enemy);

        //-----------------------------

        group1.HaveBoss = false;
        group1.generatedAmount = group1.enemies.Count;
        
        //--------------------------------------------------------------
        //--------------------------------------------------------------
        // Configure Enemy Groups

        
        EnemyGroup group2 = new EnemyGroup();

        //---------------------------------------------------------
        //---------------------------------------------------------
        
        enemiesGroups.Add(group1);
        enemiesGroups.Add(group2);
 


        //---------------------------------------------
        // Configure Water Well
        // Si no existe en el nivel, quedara como null.
        //levelWaterWell.Available = false;
        //---------------------------------------------
        // Configure Loot Cache
        // Si no existe en el nivel, quedara como null.
        //lootCache.Available = false;
        //---------------------------------------------
        // Configure Vendor
        // Si no existe en el nivel, quedara como null.
        //vendor.Available = false;
        //---------------------------------------------
    }

}
