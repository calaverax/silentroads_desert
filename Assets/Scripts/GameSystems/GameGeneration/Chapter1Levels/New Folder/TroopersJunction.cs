﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TroopersJunction: SerializableLevel
{
    public override void LoadLevelData()
    {
        // Configure Enemy Groups
        EnemyGroup group1 = new EnemyGroup();

        EnemyProperties enemy;
        
        //-----------------------------
        enemy = new EnemyProperties();
        enemy.Type = "Militia";
        enemy.Gender = "Male";
        enemy.isAlive = true;

        group1.enemies.Add(enemy);
        //-----------------------------
        enemy = new EnemyProperties();
        enemy.Type = "Militia";
        enemy.Gender = "Male";
        enemy.isAlive = true;

        group1.enemies.Add(enemy);

        //-----------------------------
        //-----------------------------
        enemy = new EnemyProperties();
        enemy.Type = "Militia";
        enemy.Gender = "Male";
        enemy.isAlive = true;

        group1.enemies.Add(enemy);

        //-----------------------------
        //-----------------------------
        enemy = new EnemyProperties();
        enemy.Type = "Militia";
        enemy.Gender = "Male";
        enemy.isAlive = true;

        group1.enemies.Add(enemy);


        //-----------------------------


        group1.HaveBoss = false;
        group1.generatedAmount = group1.enemies.Count;

        //--------------------------------------------------------------
        //--------------------------------------------------------------
        // Configure Enemy Groups
        EnemyGroup group2 = new EnemyGroup();

        EnemyProperties enemy2;

        //-----------------------------
        //-----------------------------
        enemy2 = new EnemyProperties();
        enemy.Type = "Militia";
        enemy.Gender = "Male";
        enemy.isAlive = true;

        group2.enemies.Add(enemy);

        //-----------------------------
        //-----------------------------

        enemy2 = new EnemyProperties();
        enemy.Type = "Militia";
        enemy.Gender = "Female";
        enemy.isAlive = true;

        group2.enemies.Add(enemy);

        group2.HaveBoss = false;
        group2.generatedAmount = group2.enemies.Count;
        //--------------------------------------------------------------
        //--------------------------------------------------------------

        // Configure Enemy Groups
        EnemyGroup group3 = new EnemyGroup();
        group3.HaveBoss = false;
        group3.generatedAmount = group2.enemies.Count;

        //---------------------------------------------------------
        //---------------------------------------------------------

        enemiesGroups.Add(group1);
        enemiesGroups.Add(group2);
        enemiesGroups.Add(group3);


        //---------------------------------------------
        // Configure Water Well
        // Si no existe en el nivel, quedara como null.
        //levelWaterWell.Available = false;
        //---------------------------------------------
        // Configure Loot Cache
        // Si no existe en el nivel, quedara como null.
        //lootCache.Available = false;
        //---------------------------------------------
        // Configure Vendor
        // Si no existe en el nivel, quedara como null.
        //vendor.Available = false;
        //---------------------------------------------
    }

}
