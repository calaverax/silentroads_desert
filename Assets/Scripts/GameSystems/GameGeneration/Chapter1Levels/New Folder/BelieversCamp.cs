﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class BelieversCamp : SerializableLevel
{
    public override void LoadLevelData()
    {
        // Configure Enemy Groups
        EnemyGroup group1 = new EnemyGroup();

        EnemyProperties enemy;

        //-----------------------------
        enemy = new EnemyProperties();
        enemy.Type = "Cultist";
        enemy.Gender = "Male";
        enemy.isAlive = true;

        group1.enemies.Add(enemy);

        //-----------------------------
        enemy = new EnemyProperties();
        enemy.Type = "Cultist";
        enemy.Gender = "Male";
        enemy.isAlive = true;

        group1.enemies.Add(enemy);
        //-----------------------------
        enemy = new EnemyProperties();
        enemy.Type = "Cultist";
        enemy.Gender = "Male";
        enemy.isAlive = true;

        group1.enemies.Add(enemy);

        //-----------------------------
        enemy = new EnemyProperties();
        enemy.Type = "Cultist";
        enemy.Gender = "Female";
        enemy.isAlive = true;

        group1.enemies.Add(enemy);


        //-----------------------------


        group1.HaveBoss = false;
        group1.generatedAmount = group1.enemies.Count;

        //--------------------------------------------------------------
        //--------------------------------------------------------------
        // Configure Enemy Groups
        EnemyGroup group2 = new EnemyGroup();

        EnemyProperties enemy2;

        //-----------------------------
        //-----------------------------
        enemy2 = new EnemyProperties();
        enemy2.Type = "Cultist";
        enemy2.Gender = "Male";
        enemy2.isAlive = true;

        group2.enemies.Add(enemy);

        //-----------------------------
        //-----------------------------

        enemy2 = new EnemyProperties();
        enemy2.Type = "Cultist";
        enemy2.Gender = "Female";
        enemy2.isAlive = true;

        group2.enemies.Add(enemy);


        //-----------------------------

        group2.HaveBoss = false;
        group2.generatedAmount = group2.enemies.Count;




        //--------------------------------------------------------------
        EnemyGroup group3 = new EnemyGroup();
        group3.HaveBoss = false;
        group3.generatedAmount = group2.enemies.Count;
        //--------------------------------------------------------------

        //---------------------------------------------------------

        enemiesGroups.Add(group1);
        enemiesGroups.Add(group2);
        enemiesGroups.Add(group3);


    }

}
