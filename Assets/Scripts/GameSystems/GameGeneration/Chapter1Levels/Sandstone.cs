﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Sandstone : SerializableLevel
{
    public override void LoadLevelData()
    {

        //if (Random.Range(0.0f, 1.0f) >= 0.1f)
        if (true)
        {
                        // Configure Enemy Groups
            EnemyGroup group1 = new EnemyGroup();

            EnemyProperties enemy;

            //-----------------------------
            enemy = new EnemyProperties();
            enemy.Type = "Bandit";
            enemy.Gender = "Female";
            enemy.isAlive = true;

            group1.enemies.Add(enemy);

            /*
            //-----------------------------
            enemy = new EnemyProperties();
            enemy.Type = "Militia";
            enemy.Gender = "Female";
            enemy.isAlive = true;

            group1.enemies.Add(enemy);

            
            //-----------------------------
            enemy = new EnemyProperties();
            enemy.Type = "Militia";
            enemy.Gender = "Male";
            enemy.isAlive = true;

            group1.enemies.Add(enemy);
             * */

            //-----------------------------

            group1.HaveBoss = false;
            group1.generatedAmount = group1.enemies.Count;

            //--------------------------------------------------------------
            //--------------------------------------------------------------
            
            
            // Configure Enemy Groups
            EnemyGroup group2 = new EnemyGroup();

            EnemyProperties enemy2;

            group2.HaveBoss = false;
            group2.generatedAmount = group2.enemies.Count;
            /*
            //-----------------------------
            enemy2 = new EnemyProperties();
            enemy2.Type = "Survivor";
            enemy2.Gender = "Female";
            enemy2.isAlive = true;


            group2.enemies.Add(enemy2);
            //-----------------------------
            enemy2 = new EnemyProperties();
            enemy2.Type = "Soldier";
            enemy2.Gender = "Male";
            enemy2.isAlive = true;


            group2.enemies.Add(enemy2);

            //-----------------------------
            enemy2 = new EnemyProperties();
            enemy2.Type = "Militia";
            enemy2.Gender = "Male";
            enemy2.isAlive = true;


            group2.enemies.Add(enemy2);

            //-----------------------------


            //--------------------------------------------------------------
            //--------------------------------------------------------------
            */
            // Configure Enemy Groups
            EnemyGroup group3 = new EnemyGroup();

            //EnemyProperties enemy3;

            group3.HaveBoss = false;
            group3.generatedAmount = group3.enemies.Count;
            /*
            //-----------------------------
            enemy3 = new EnemyProperties();
            enemy3.Type = "Bandit";
            enemy3.Gender = "Female";
            enemy3.isAlive = true;

            group3.enemies.Add(enemy3);
            //-----------------------------
            enemy3 = new EnemyProperties();
            enemy3.Type = "Bandit";
            enemy3.Gender = "Female";
            enemy3.isAlive = true;

            group3.enemies.Add(enemy3);

            //-----------------------------
            enemy3 = new EnemyProperties();
            enemy3.Type = "Bandit";
            enemy3.Gender = "Female";
            enemy3.isAlive = true;

            group3.enemies.Add(enemy3);

            //-----------------------------

            //---------------------------------------------------------
            //---------------------------------------------------------
            */
            enemiesGroups.Add(group1);
            enemiesGroups.Add(group2);
            enemiesGroups.Add(group3);








            //---------------------------------------------
            // Configure Water Well
            // Si no existe en el nivel, quedara como null.
            //levelWaterWell.Available = false;
            //---------------------------------------------
            // Configure Loot Cache
            // Si no existe en el nivel, quedara como null.
            //lootCache.Available = false;
            //---------------------------------------------
            // Configure Vendor
            // Si no existe en el nivel, quedara como null.
            //vendor.Available = false;
            //---------------------------------------------
        }

         else
        { 

        // Configure Enemy Groups
        EnemyGroup group1 = new EnemyGroup();

        EnemyProperties enemy;
            
            //Enemy 1
        enemy = new EnemyProperties();
        enemy.Type = "Survivor";
        enemy.Gender = "Male";
        enemy.isAlive = true;

        group1.enemies.Add(enemy);
            //Enemy 2
        enemy = new EnemyProperties();
        enemy.Type = "Survivor";
        enemy.Gender = "Female";
        enemy.isAlive = true;
        group1.enemies.Add(enemy);

            //Cerrando
        group1.HaveBoss = false;
        group1.generatedAmount = group1.enemies.Count;

        enemiesGroups.Add(group1);
        }


		

    }



}


    /*
		#region Group3
		//--------------------------------------------------------------
		
		// Configure Enemy Groups
		EnemyGroup group3 = new EnemyGroup();
		
		EnemyProperties enemy3;
		
		//-----------------------------
		// Enemy 1 = 100% Chances of Spawn
		enemy3 = new EnemyProperties();
		enemy3.Type = _enemyType;
		
		if (Random.Range(0.0f, 1.0f) >= 0.5f)
			enemy3.Gender = "Female";
		else
			enemy3.Gender = "Male";
		
		enemy3.isAlive = true;
		
		group3.enemies.Add(enemy3);
		//-----------------------------
		// Enemy 2 = 75% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.75f)
		{
			enemy3 = new EnemyProperties();
			enemy3.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy3.Gender = "Female";
			else
				enemy3.Gender = "Male";
			
			enemy3.isAlive = true;
			
			group3.enemies.Add(enemy3);
		}
		//-----------------------------
		// Enemy 2 = 50% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.5f)
		{
			enemy3 = new EnemyProperties();
			enemy3.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy3.Gender = "Female";
			else
				enemy3.Gender = "Male";
			
			enemy3.isAlive = true;
			
			group3.enemies.Add(enemy3);
		}
		//-----------------------------
		// Enemy 2 = 25% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.25f)
		{
			enemy3 = new EnemyProperties();
			enemy3.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy3.Gender = "Female";
			else
				enemy3.Gender = "Male";
			
			enemy3.isAlive = true;
			
			group3.enemies.Add(enemy3);
		}
		//-----------------------------
		group3.HaveBoss = false;
		group3.generatedAmount = group3.enemies.Count;
		//---------------------------------------------------------
		//---------------------------------------------------------
		#endregion
		
		#region Group4
		
		EnemyGroup group4 = new EnemyGroup();
		
		EnemyProperties enemy4;
		
		//-----------------------------
		// Enemy 1 = 100% Chances of Spawn
		enemy4 = new EnemyProperties();
		enemy4.Type = _enemyType;
		
		if (Random.Range(0.0f, 1.0f) >= 0.5f)
			enemy4.Gender = "Female";
		else
			enemy4.Gender = "Male";
		
		enemy4.isAlive = true;
		
		group4.enemies.Add(enemy4);
		//-----------------------------
		// Enemy 2 = 75% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.75f)
		{
			enemy4 = new EnemyProperties();
			enemy4.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy4.Gender = "Female";
			else
				enemy4.Gender = "Male";
			
			enemy4.isAlive = true;
			
			group4.enemies.Add(enemy4);
		}
		//-----------------------------
		// Enemy 2 = 50% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.5f)
		{
			enemy4 = new EnemyProperties();
			enemy4.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy4.Gender = "Female";
			else
				enemy4.Gender = "Male";
			
			enemy4.isAlive = true;
			
			group4.enemies.Add(enemy4);
		}
		//-----------------------------
		// Enemy 2 = 25% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.25f)
		{
			enemy4 = new EnemyProperties();
			enemy4.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy4.Gender = "Female";
			else
				enemy4.Gender = "Male";
			
			enemy4.isAlive = true;
			
			group4.enemies.Add(enemy4);
		}
		//-----------------------------
		group4.HaveBoss = false;
		group4.generatedAmount = group4.enemies.Count;
		//------------------------------------------------------------------
		//------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------
		//---------------------------------------------------------
		//------------------------------------------------------------------
		//------------------------------------------------------------------
		*/

    //enemiesGroups.Add(group3);
    //enemiesGroups.Add(group4);


    //---------------------------------------------
    // Configure Water Well
    // Si no existe en el nivel, quedara como null.
    //levelWaterWell.Available = false;
    //---------------------------------------------
    // Configure Loot Cache
    // Si no existe en el nivel, quedara como null.
    //lootCache.Available = false;
    //---------------------------------------------
    // Configure Vendor
    // Si no existe en el nivel, quedara como null.
    //vendor.Available = false;
    //---------------------------------------------















    //Version vieja

    /* public override void LoadLevelData()
    {
		string _enemyType = "Bandit";
		
		if (Random.Range(0.0f,1.0f) >= 0.5f)
			_enemyType = "Bandit";
		else
			_enemyType = "Survivor";

		
		#region Group1
		
		// Configure Enemy Groups
		EnemyGroup group1 = new EnemyGroup();
		
		EnemyProperties enemy;
		
		
		//-----------------------------
		// Enemy 1 = 100% Chances of Spawn
		enemy = new EnemyProperties();
		enemy.Type = _enemyType;
		
		if (Random.Range(0.0f, 1.0f) >= 0.5f)
			enemy.Gender = "Female";
		else
			enemy.Gender = "Male";
		
		enemy.isAlive = true;
		
		group1.enemies.Add(enemy);
		//-----------------------------
		// Enemy 2 = 75% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.75f)
		{
			enemy = new EnemyProperties();
			enemy.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy.Gender = "Female";
			else
				enemy.Gender = "Male";
			
			enemy.isAlive = true;
			
			group1.enemies.Add(enemy);
		}
		//-----------------------------
		// Enemy 2 = 50% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.5f)
		{
			enemy = new EnemyProperties();
			enemy.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy.Gender = "Female";
			else
				enemy.Gender = "Male";
			
			enemy.isAlive = true;
			
			group1.enemies.Add(enemy);
		}
		//-----------------------------
		// Enemy 2 = 25% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.25f)
		{
			enemy = new EnemyProperties();
			enemy.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy.Gender = "Female";
			else
				enemy.Gender = "Male";
			
			enemy.isAlive = true;
			
			group1.enemies.Add(enemy);
		}
		//-----------------------------
		group1.HaveBoss = false;
		group1.generatedAmount = group1.enemies.Count;
		
		#endregion
		
		#region Group2
		//--------------------------------------------------------------
		//--------------------------------------------------------------
		// Configure Enemy Groups
		EnemyGroup group2 = new EnemyGroup();
		
		EnemyProperties enemy2;
		
		//-----------------------------
		// Enemy 1 = 100% Chances of Spawn
		enemy2 = new EnemyProperties();
		enemy2.Type = _enemyType;
		
		if (Random.Range(0.0f, 1.0f) >= 0.5f)
			enemy2.Gender = "Female";
		else
			enemy2.Gender = "Male";
		
		enemy2.isAlive = true;
		
		group2.enemies.Add(enemy2);
		//-----------------------------
		// Enemy 2 = 75% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.75f)
		{
			enemy2 = new EnemyProperties();
			enemy2.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy2.Gender = "Female";
			else
				enemy2.Gender = "Male";
			
			enemy2.isAlive = true;
			
			group2.enemies.Add(enemy2);
		}
		//-----------------------------
		// Enemy 2 = 50% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.5f)
		{
			enemy2 = new EnemyProperties();
			enemy2.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy2.Gender = "Female";
			else
				enemy2.Gender = "Male";
			
			enemy2.isAlive = true;
			
			group2.enemies.Add(enemy2);
		}
		//-----------------------------
		// Enemy 2 = 25% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.25f)
		{
			enemy2 = new EnemyProperties();
			enemy2.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy2.Gender = "Female";
			else
				enemy2.Gender = "Male";
			
			enemy2.isAlive = true;
			
			group2.enemies.Add(enemy2);
		}
		//-----------------------------
		group2.HaveBoss = false;
		group2.generatedAmount = group2.enemies.Count;
		//--------------------------------------------------------------
		#endregion

		#region Group3
		//--------------------------------------------------------------
		
		// Configure Enemy Groups
		EnemyGroup group3 = new EnemyGroup();
		
		EnemyProperties enemy3;
		
		//-----------------------------
		// Enemy 1 = 100% Chances of Spawn
		enemy3 = new EnemyProperties();
		enemy3.Type = _enemyType;
		
		if (Random.Range(0.0f, 1.0f) >= 0.5f)
			enemy3.Gender = "Female";
		else
			enemy3.Gender = "Male";
		
		enemy3.isAlive = true;
		
		group3.enemies.Add(enemy3);
		//-----------------------------
		// Enemy 2 = 75% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.75f)
		{
			enemy3 = new EnemyProperties();
			enemy3.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy3.Gender = "Female";
			else
				enemy3.Gender = "Male";
			
			enemy3.isAlive = true;
			
			group3.enemies.Add(enemy3);
		}
		//-----------------------------
		// Enemy 2 = 50% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.5f)
		{
			enemy3 = new EnemyProperties();
			enemy3.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy3.Gender = "Female";
			else
				enemy3.Gender = "Male";
			
			enemy3.isAlive = true;
			
			group3.enemies.Add(enemy3);
		}
		//-----------------------------
		// Enemy 2 = 25% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.25f)
		{
			enemy3 = new EnemyProperties();
			enemy3.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy3.Gender = "Female";
			else
				enemy3.Gender = "Male";
			
			enemy3.isAlive = true;
			
			group3.enemies.Add(enemy3);
		}
		//-----------------------------
		group3.HaveBoss = false;
		group3.generatedAmount = group3.enemies.Count;
		//---------------------------------------------------------
		//---------------------------------------------------------
		#endregion
		
		#region Group4
		
		EnemyGroup group4 = new EnemyGroup();
		
		EnemyProperties enemy4;
		
		//-----------------------------
		// Enemy 1 = 100% Chances of Spawn
		enemy4 = new EnemyProperties();
		enemy4.Type = _enemyType;
		
		if (Random.Range(0.0f, 1.0f) >= 0.5f)
			enemy4.Gender = "Female";
		else
			enemy4.Gender = "Male";
		
		enemy4.isAlive = true;
		
		group4.enemies.Add(enemy4);
		//-----------------------------
		// Enemy 2 = 75% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.75f)
		{
			enemy4 = new EnemyProperties();
			enemy4.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy4.Gender = "Female";
			else
				enemy4.Gender = "Male";
			
			enemy4.isAlive = true;
			
			group4.enemies.Add(enemy4);
		}
		//-----------------------------
		// Enemy 2 = 50% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.5f)
		{
			enemy4 = new EnemyProperties();
			enemy4.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy4.Gender = "Female";
			else
				enemy4.Gender = "Male";
			
			enemy4.isAlive = true;
			
			group4.enemies.Add(enemy4);
		}
		//-----------------------------
		// Enemy 2 = 25% Chances of Spawn
		if (Random.Range(0.0f, 1.0f) <= 0.25f)
		{
			enemy4 = new EnemyProperties();
			enemy4.Type = _enemyType;
			
			if (Random.Range(0.0f, 1.0f) >= 0.5f)
				enemy4.Gender = "Female";
			else
				enemy4.Gender = "Male";
			
			enemy4.isAlive = true;
			
			group4.enemies.Add(enemy4);
		}
		//-----------------------------
		group4.HaveBoss = false;
		group4.generatedAmount = group4.enemies.Count;
		//------------------------------------------------------------------
		//------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------
		//---------------------------------------------------------
		//------------------------------------------------------------------
		//------------------------------------------------------------------

		enemiesGroups.Add(group1);
		enemiesGroups.Add(group2);
		//enemiesGroups.Add(group3);
		//enemiesGroups.Add(group4);


        //---------------------------------------------
        // Configure Water Well
        // Si no existe en el nivel, quedara como null.
        //levelWaterWell.Available = false;
        //---------------------------------------------
        // Configure Loot Cache
        // Si no existe en el nivel, quedara como null.
        //lootCache.Available = false;
        //---------------------------------------------
        // Configure Vendor
        // Si no existe en el nivel, quedara como null.
        //vendor.Available = false;
        //---------------------------------------------
    }*/

