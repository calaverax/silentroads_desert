﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameStateGenerator : MonoBehaviour {
		
	List<GameChapterState> _gameChapterStates = new List<GameChapterState>();
	
	// Use this for initialization
	void Start ()
	{
		// Recorrer Cada Chapter
		// GenerateChapter
		// - Lista de Todos los niveles
		// -- Parametros por nivel:
		// --- Tipo de Enemigos.
		// --- Cantidad de enemigos (min - max)
		// ---- Boss?
		// --- Water Well
		// --- Loot Cache
		// --- Vendor with Item List, Inflation Ratio.
		// -- Parametros de Mapa:
		// --- Efectos de Bloqueos de Caminos (Ubicacion, Tipo de Bloqueo)
		// --- Ubicacion de Water Man, Direccion en que viaja.
		// --- 

	}
}

[System.Serializable]
public class EnemyProperties
{
    public string Type;
    public string Gender;
    public bool isAlive;
}

[System.Serializable]
public class EnemyGroup
{
    public List<EnemyProperties> enemies = new List<EnemyProperties>();
	public bool HaveBoss;
	public string BossName;
	public int minAmout, maxAmount, generatedAmount;
}

[System.Serializable]
public class LevelWaterWell
{
	public bool Available;
	public float spawnChance;
}

[System.Serializable]
public class LootCache
{
	public bool Available;
	public bool Explosive;
	public Dictionary<string, int> ItemsContained = new Dictionary<string,int>();	
}

[System.Serializable]
public class Vendor
{
	public bool Available;
	public float vendorChance;
	public Dictionary<string, int> ItemsContained = new Dictionary<string, int> ();
	public float toBuyFactor, toSellFactor;
}
