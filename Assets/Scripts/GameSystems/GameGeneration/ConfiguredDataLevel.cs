﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Wasabi.InventorySystem;
using Wasabi.InventorySystem.Inventory;
using UnityEngine.SceneManagement;

[System.Serializable]
public class LevelSpawnGroup
{
    public Transform[] Spawns;
}







public class ConfiguredDataLevel : MonoBehaviour {

    public string LevelName = "";
    public int LevelNumber = 1;
    public bool NoCombatLevel = false;
    public bool AddFollowers = false;

    public bool repetido = false;
    public int numeroDeEnemigosPersistidos;
    public bool yaInstancio = false;
    public bool yaInstancioFollowers = false;

    public LevelSpawnGroup[] GroupsSpawners;
    public LevelSpawnGroup[] VisitorSpawners;

    public GameObject waterWell, vendorNPC;
    private bool instanceEH = false;
    SerializableLevel _level;
    GameObject _spawningEnemyPrefab;





    void Start(){


        repetido = EnemyHistory.Instance.IsLevelWithEnemies(SceneManager.GetActiveScene().name );
        numeroDeEnemigosPersistidos = EnemyHistory.Instance.NumEnemigosPersistidos();
        
        
        GameManager.Instance.selectedLevel = LevelNumber;
        
        // Load Level Data.
        _level = Serializer.LoadSerializableObjectFromFile<SerializableLevel>(Application.persistentDataPath + LevelName + ".LevelData");

        //Fijate cuál es el nivel previo (2 niveles atrás en realidad)
        EnemyHistory.Instance.DosNivelesAtras();


        //Chequeá si hay enemigos siguiendo que deberían alcanzar al personaje.
        if (EnemyHistory.Instance._edb.followers_list != null)
        {
            if (EnemyHistory.Instance.previousLevelID == SceneManager.GetActiveScene().name && EnemyHistory.Instance._edb.followers_list.Count > 0)
            {
                AddFollowers = true;
            }


        }

        //Aunque sea un NoCombatLevel, va a instanciar personajes persistentes, es decir, personajes repetidos.
        if (NoCombatLevel && AddFollowers)
        {
            repetido = true;
        }

        //Si es un nivel en que no se combate..
        if (NoCombatLevel && !AddFollowers)
        { CombatManager.Instance.nextLevelNavigations.CombatEnded();
        EnemyHistory.Instance.LevelsSinCombate();

        }  //activá las salidas.



        //Podés terner followers; podés tener los propios del nivel; podés tener los que quedaron

        else //Si se combate
        {


            EnemyHistory.Instance.Begin(); //PPP Moví el begin, que estaba mal puesto.


            //PPP El código dentro de esta región está anulado por el WillSpawn que sigue. Servía para que la instanciación fuera random. Por ahora está deshabilitado. Creo que es mejor que eso venga de los scripts de cada nivel.
            #region
            string enemyCompleteString = "";
            for (int i = 0; i < _level.enemiesGroups.Count - 1; i++)
            {
                //Debug.Log("i : " + i);
                bool willSpawn = true;

                // Si es el Spawn uno tiene 100% por lo que no se chequea nada.
                if (i > 0)
                {
                    //float rnd = Random.Range(0.0f, 1.0f);
                    float rnd = Random.Range(0.0f, 1.0f);

                    // Spawn 2 75% de chances de activarse.
                    if (i == 1)
                        if (rnd <= 0.75f)
                            willSpawn = false;

                    // Spawn 3 50% de chances de activarse.
                    if (i == 2)
                        if (rnd <= 0.5f)
                            willSpawn = false;

                    // Spawn 4 25% de chances de activarse.
                    if (i == 3)
                        if (rnd <= 0.25f)
                            willSpawn = false;
                }

            #endregion

                //willSpawn = !repetido; //PPP Fijate si el nivel tiene persistencia almacenada.



                // SACAR : El if de abajo se puso para que siempre se instancien los enemigos iniciales en el modo debug/editor
                //Dejar lo del else, sacar lo del unity_editor
#if UNITY_EDITOR
                Debug.Log("DEBUG MODE");

#else
            
                Debug.Log("BUILD MODE");

                
#endif
                //ARRANCA EL ESPAWNEO//
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////




                //SI NO HAY PERSISTENCIA ALMACENADA
                if (!repetido)
                {
                    // Temporal List to Add Enemies.
                    List<BaseEnemy> enemiesInGroup = new List<BaseEnemy>();
                    if (enemiesInGroup.Count > 0)
                        enemiesInGroup.Clear();

                    // For Each Group Search for Enemies:
                    for (int a = 0; a < _level.enemiesGroups[i].generatedAmount; a++)
                    {

                        // Evaluate Enemy for this Spawnpoint. and construct the prefab name
                        string gender = _level.enemiesGroups[i].enemies[a].Gender;
                        if (gender.ToLower().Contains("male"))
                        {
                            enemyCompleteString = gender + _level.enemiesGroups[i].enemies[a].Type;
                        }
                        else if (gender.Contains("None"))
                        {
                            enemyCompleteString = _level.enemiesGroups[i].enemies[a].Type;
                        }
                        // Analize If Enemy is Junkie (OnlyMale)
                        if (_level.enemiesGroups[i].enemies[a].Type == "Junkie")
                        {
                            enemyCompleteString += "" + Random.Range(1, 3);
                        }

                        if ((_spawningEnemyPrefab == null) || ((_spawningEnemyPrefab != null) && (_spawningEnemyPrefab.name != enemyCompleteString)))
                        {
                            _spawningEnemyPrefab = Resources.Load<GameObject>("Character Prefabs/Characters/" + enemyCompleteString);
                        }
                        // Spawn Enemy (Instantiate Prefab)
                        GameObject Enemy = Instantiate(_spawningEnemyPrefab, GroupsSpawners[i].Spawns[a], false);
                        // Locate it in Transform
                        Enemy.transform.position = GroupsSpawners[i].Spawns[a].position;
                        // Get Script for Character Initialization.
                        BaseEnemy script = Enemy.GetComponent<BaseEnemy>();
                        script.init();
                        script.configureCharacter(false, gender.ToLower() == "male" ? BaseEnemy.Genre.MALE : BaseEnemy.Genre.FEMALE);
                        // Add to CombatManager
                        enemiesInGroup.Add(script);
                        CombatManager.Instance.Register(script);
                        //EnemyHistory.Instance.AddEnemy(script , a , i);

                    }
                    CombatManager.Instance.EnemiesGroups.Add(enemiesInGroup);


                }


                //PPP
                //SI HAY PERSISTENCIA ALMACENADA



                if (repetido || AddFollowers)
                {




                    //Si no persistió ningún personaje y no hay followers, el nivel está ganado, dejá las salidas habilitadas.
                    if (numeroDeEnemigosPersistidos == 0 && !AddFollowers)
                    {
                        CombatManager.Instance.nextLevelNavigations.CombatEnded();

                    }

                    // Lista temporal para agregar enemigos.
                    List<BaseEnemy> enemiesInGroup = new List<BaseEnemy>();
                    if (enemiesInGroup.Count > 0)
                        enemiesInGroup.Clear();

                    //La lista de enemigos que hay en el nivel
                    List<SerializableEnemy> lista = EnemyHistory.Instance._edb.level_list.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name).enemiesPersisted;

                    //La lista de los followers
                    List<SerializableEnemy> followers = EnemyHistory.Instance._edb.followers_list;

                    //Lista de todos los enemigos que hay que instanciar de las dos listas anteriores
                    List<SerializableEnemy> todosLosEnemigosAInstanciar = new List<SerializableEnemy>();




                    //Si toca instanciar algunos followers, agregalos a la lista de instanciación:
                    if (AddFollowers & !yaInstancioFollowers)
                    {
                        
                        foreach (SerializableEnemy enemigo in followers)
                        {
                            todosLosEnemigosAInstanciar.Add(enemigo); //Agregá los folowers a la lista del nivel.
                            enemigo.index = lista.Count;
                            lista.Add(enemigo);
                            EnemyHistory.Instance.SaveDB();
                        }
                        followers.Clear();
                        yaInstancioFollowers = true;
                        EnemyHistory.Instance.SaveDB();
                        
                    }
                    Debug.Log("al finalizar con los followers, follower quedó en " + followers.Count + " y el nivel en  " + lista.Count);

                    
                    

                    //Por cada enemigo que haya en la lista de instanciación...
                    if (!yaInstancio) { 
                    foreach (SerializableEnemy enemy in lista)
                    {


                        //Instanciar prefab.
                        enemyCompleteString = enemy.sprefab; //Buscá el prefab que tenés que instanciar.
                        enemyCompleteString = enemyCompleteString.Replace("(Clone)", ""); //Arreglá el nombre.
                        _spawningEnemyPrefab = Resources.Load<GameObject>("Character Prefabs/Characters/" + enemyCompleteString);
                        GameObject Enemy = Instantiate(_spawningEnemyPrefab, GroupsSpawners[enemy.spawngroup].Spawns[enemy.spawnpoint], false);

                        

                        //Referencia a los spawners.
                        Transform spawnerGroups = GameObject.FindGameObjectWithTag("EnemiesSpawners").transform;
                        Transform spawnerGroup1 = spawnerGroups.GetChild(0);
                        Transform spawnerGroup2 = spawnerGroups.GetChild(1);
                        Transform spawnerGroup3 = spawnerGroups.GetChild(2);

                        //Acá los personajes se ponen como child de un spawner y con su misma posición. Los spawns no dependen de info pregrabada sino que se ponen por el órden de índice que tienen. No van a estar ni en la misma posición ni en el mismo grupo que antes. Esto se puede cambiar más adelante.
                        if (enemy.index < 4) { 
                        Enemy.transform.parent = spawnerGroup1.GetChild(enemy.index);
                        Enemy.transform.position = spawnerGroup1.GetChild(enemy.index).position;
                        }
                        else if (enemy.index < 8)
                        {
                            Enemy.transform.parent = spawnerGroup2.GetChild(enemy.index -4);
                            Enemy.transform.position = spawnerGroup2.GetChild(enemy.index - 4).position;
                        }
                        else { Enemy.transform.parent = spawnerGroup3.GetChild(enemy.index -8);
                        Enemy.transform.position = spawnerGroup3.GetChild(enemy.index - 8).position;
                        }


                        //Buscá su script base enemy y configuralo.
                        BaseEnemy script = Enemy.GetComponent<BaseEnemy>();
                        script.init();
                        script.configureCharacter(false, enemy.genero.ToLower() == "male" ? BaseEnemy.Genre.MALE : BaseEnemy.Genre.FEMALE); //Acá las stats están bien.
                        if (!enemy.scavenger)
                        { script.characterStats = enemy.characterStats;
                        enemy.scavenger = false;
                        }
                        script.characterName = enemy.characterName;
                        Debug.Log("La vida del enemigo es : " + script.characterStats.Hitpoints + " HP");
                        Debug.Log("La vida srializada del enemigo es : " + enemy.characterStats.Hitpoints + " HP");
                        Debug.Log("La vida del enemigo es : " + script.characterStats.TotalHitpoints + " HP");
                        Debug.Log("La vida srializada del enemigo es : " + enemy.characterStats.TotalHitpoints + " HP");


                            script.serializableEnemy = enemy;

                        //Ponéle el sprite adecuado
                        string path = "Sprites/NewCharacters/" + enemy.spriteName;
                        Sprite sprite = Resources.Load<Sprite>(path);
                        script.imagen = sprite;
                        script.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = sprite;

                        //Agregalo al grupo y al Combat Manager.
                        enemiesInGroup.Add(script);
                        CombatManager.Instance.Register(script);
                    }
                    }


                    //Ya que hay enemigos, activá el combat mode.
                    GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().combatMode.SetActive(true);

                    yaInstancio = true; //Para que se instancien una sóla vez. //creo que es obsoleto ahora.



                }



            }


        }



        // Spawn Props (Vendor / Well / WaterMan )
        if (_level.levelWaterWell != null)
        {
        }

        if (_level.vendor != null)
        {

        }

        // Indicates Combat manager that this is a level with the new spawning system.
        CombatManager.Instance.newSpawnerLevel = true;
    }

}


