﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SerializableLevel
{
	public List<EnemyGroup> enemiesGroups = new List<EnemyGroup> ();
	public LevelWaterWell levelWaterWell;
	public LootCache lootCache;
	public Vendor vendor;
	public string LevelName;

	public virtual void LoadLevelData()
	{}

}
