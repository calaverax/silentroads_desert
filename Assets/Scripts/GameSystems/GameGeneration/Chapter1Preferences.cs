﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Chapter1Preferences
{
	// GenerateChapter
	// - Lista de Todos los niveles
	// -- Parametros por nivel:
	// --- Tipo de Enemigos.
	// --- Cantidad de enemigos (min - max)
	// ---- Boss?
	// --- Water Well
	// --- Loot Cache
	// --- Vendor with Item List, Inflation Ratio.
	// -- Parametros de Mapa:
	// --- Efectos de Bloqueos de Caminos (Ubicacion, Tipo de Bloqueo)
	// --- Ubicacion de Water Man, Direccion en que viaja.
	// --- 

	//
	// Loads List of Levels, and Generate


    //PPP En esta lista hay que poner todos los niveles del capítulo 1. Es un array de strings.
    string[] LevelsList = new string[] {/*
                                        "Chapter1Level1.LevelData",
                                        "TestLevel1.LevelData",
                                        "Sandstone.LevelData",
                                        "TheLongRoad.LevelData",
                                        "PlateauHigh.LevelData",
                                        "OldBarn.LevelData",
                                        "OldBarnInside.LevelData",
                                        "MilodonCave.LevelData",
                                        "MilodonCaveInside.LevelData",
                                        "Monoliths.LevelData",
                                        "MilestoneCityA.LevelData",
                                        "MilestoneCityB.LevelData",
                                        "MilestoneCityC.LevelData",
                                        "ForsakenBridge.LevelData",
                                        "RobertRanchInside.LevelData"*/
                                        "Chapter1Level1.LevelData",//0
                                        "TestLevel1.LevelData",//1
                                        "RobertRanchInside.LevelData",//2
                                        "Sandstone.LevelData",//3
                                        "TheLongRoad.LevelData",//4
                                        "PlateauHigh.LevelData",//5
                                        "OldBarn.LevelData",//6
                                        "OldBarnInside.LevelData",//7
                                        "MilodonCave.LevelData",//8
                                        "MilodonCaveInside.LevelData",//9
                                        "Monoliths.LevelData",//10
                                        "MilestoneCityA.LevelData",//11
                                        "MilestoneCityB.LevelData",//12
                                        "MilestoneCityC.LevelData",//13
                                        "ForsakenBridge.LevelData",//14
                                        ////////////////////////////////
                                        "TroopersJunction.LevelData",//15
                                        "FuelStation.LevelData",//16
                                        "BelieversCamp.LevelData",//17
                                        "ChosenCitadel.LevelData",//18
                                        "ChosenCitadelInside.LevelData",//19
                                        "DesertEnd.LevelData",//20
                                        "RadioStation.LevelData",//21
                                        "RiverCrossing.LevelData",//22
                                        "Amazon.LevelData",//23
                                        "Barrens.LevelData",//24
                                        "MilitiaOutpost.LevelData",//25
                                        "Oasis.LevelData",//26
                                        "EternalSands.LevelData",//27
                                        "MilitiaHQ.LevelData",//28
                                        "SolaceBunker.LevelData",//29
                                        "SolaceBunkerInside.LevelData",//30
                                        "DesertRandom.LevelData",//31
                                        "Waterman.LevelData",//32

    
    };


    //Va a hacer una lista de niveles serializables llamada Levels.
    List<SerializableLevel> Levels = new List<SerializableLevel>();
	//---------------------------------------------------------------------------------------------------------
	public void ProcessChapter()
	{
		// Check if dont already have a generated chapter.
		int process = PlayerPrefs.GetInt ("Chapter1DataProcessed", -1);
        Debug.Log(process + " Process...");
		if (process != 1) 
		{
			// /if not, Generate it.
			GenerateLevelsFiles();
			LoadLevels();
			SaveLevelsData ();
			PlayerPrefs.SetInt("Chapter1DataProcessed", 1);
		} // Elseway, it will be loaded on demand by each level to populate.

		// ------------------------------------------------------------------
		// Check and Load General Map Status
	}
	//---------------------------------------------------------------------------------------------------------
	
    //PPP Esta se encarga de generar los archivos persistentes. Hay que poner una entrada para cada uno.
    //La función "Save Serializable Object To File()" está en el script "Serializer.cs" donde se puede ver su funcionamiento. 
    //Toma dos argumentos, el primero es el objeto que va a ser serializado, el segundo es el path en que lo va a guardar.
    //Según parece hay que reiniciar el juego ("New game") para que cree estos archivos, aunque no estoy seguro.
    
    public void GenerateLevelsFiles()
	{
        // Test Stuff 
        
		//Serializer.SaveSerializableObjectToFile<SerializableLevel>(
		//new Chapter1Level1(),
		//Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[0]);

        // Setted Levels:
        // TODO Cast to Class From String Type.GetType(LevelList[i]) Type Level = new Typeoff(Blah)

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new TestLevel1(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[0]); //PPP

         Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new TestLevel1(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[1]); 
 
         Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new RobertRanchInside(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[2]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new Sandstone(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[3]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new TheLongRoad (),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[4]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new PlateauHigh(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[5]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new OldBarn(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[6]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new OldBarnInside(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[7]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new MilodonCave(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[8]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new MilodonCaveInside(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[9]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new Monoliths(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[10]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new MilestoneCityA(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[11]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new MilestoneCityB(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[12]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new MilestoneCityC(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[13]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new ForsakenBridge(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[14]);

     ////////////////////////////////////////////////////////////////////////////////
        
        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new TroopersJunction(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[15]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
           new FuelStation(),
           Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[16]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
           new BelieversCamp(),
           Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[17]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new ChosenCitadel(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[18]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new ChosenCitadelInside(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[19]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new DesertEnd(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[20]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new RadioStation(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[21]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new RiverCrossing(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[22]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new Amazon(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[23]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new Barrens(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[24]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new MilitiaOutpost(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[25]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new Oasis(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[26]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new EternalSands(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[27]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new MilitiaHQ(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[28]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new SolaceBunker(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[29]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new SolaceBunkerInside(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[30]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new DesertRandom(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[31]);

        Serializer.SaveSerializableObjectToFile<SerializableLevel>(
            new Waterman(),
            Application.persistentDataPath /*+ "/LevelsData/" */+ LevelsList[32]);



    }
	//---------------------------------------------------------------------------------------------------------
	public void LoadLevels()
	{
        //LevelsDictionary.Add(new SerializableLevel(), "Chapter1Level1.LevelData");
        //Debug.Log("LevelsList.Length : " + LevelsList.Length);
		for (int i = 0; i < LevelsList.Length; i++) 
		{
			Levels.Add(Serializer.LoadSerializableObjectFromFile<SerializableLevel>(Application.persistentDataPath /*+ "/LevelsData/"*/ + LevelsList[i]));
            //Debug.Log("LevelsList[i] : " + LevelsList[i]);
            //Debug.Log(i);
            //Debug.Log(Application.persistentDataPath /*+ "/LevelsData/"*/ + LevelsList[i]);
			Levels[i].LevelName = LevelsList[i];
			Levels[i].LoadLevelData();
		}

		//SerializableLevel ch1l1 = Serializer.LoadSerializableObjectFromFile<SerializableLevel>(Application.persistentDataPath /*+ "/LevelsData/"*/ + LevelsList[0]);
		//ch1l1.LevelName = LevelsList[0];
		//ch1l1.LoadLevelData ();

		//Debug.Log ("");
	}
	//---------------------------------------------------------------------------------------------------------
	public void SaveLevelsData()
	{
		for (int i = 0; i < Levels.Count; i++) 
		{
			Serializer.SaveSerializableObjectToFile<SerializableLevel>(Levels[i], Application.persistentDataPath /*+ "/LevelsData/"*/ + Levels[i].LevelName);
		}
	}
	//---------------------------------------------------------------------------------------------------------
}
