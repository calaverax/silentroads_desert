﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class AbnormalStatesManager : MonoBehaviour {

    public enum AbnormalState
    {
        Undefined,
        Uncounciouss,
        Panic,
        DamageSelf,
        Traitor,
        Suicide,
        SuddenDeath,
        Poisoned,
        Radiated,
        ParalizedLegs,
        ParalizedArms,
        Surrender,
        Crazy,
        Bleeding,
        CatastrophicDamageSelf,
        Blinded
    }

    private List<AbnormalState> abnormalStates = new List<AbnormalState>();

    private BaseCharacter _affectedCharacter;

    public void Init(BaseCharacter character)
    {
        _affectedCharacter = character;
    }


    public void checkStateTrigger(AbnormalState state)
    {
        if (state == AbnormalState.Bleeding)
            trigger_bleeding();
    }

    public void getStateEffect(AbnormalState state)
    {

    }

    //--------------------------------------------------------------------
    #region States Effects
    #endregion
    //--------------------------------------------------------------------
    #region States Triggers
    //--------------------------------------------------------------------
    private void trigger_bleeding()
    {
        if (Random.Range(0.0f, 1.0f) >= 0.8f)
            abnormalStates.Add(AbnormalState.Bleeding);
    }
    //--------------------------------------------------------------------
    #endregion
}
