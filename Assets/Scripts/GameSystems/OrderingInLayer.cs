﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class OrderingInLayer : MonoBehaviour {
	
	// Update is called once per frame
	void Update ()
    {
        if (GetComponent<Renderer>() != null)
            GetComponent<Renderer>().sortingOrder = (int)(GetComponent<Renderer>().sortingOrder + transform.position.y * 1);
        else if (GetComponent<Canvas>() != null)
            GetComponent<Canvas>().sortingOrder = (int)(GetComponent<Canvas>().sortingOrder + transform.position.y * 1);
    }
}
