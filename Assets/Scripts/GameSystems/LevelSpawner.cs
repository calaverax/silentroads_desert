﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using Devdog.InventorySystem;

public class LevelSpawner : MonoBehaviour 
{
    public GameObject enemyAgent;
    public GameObject enemyMarauder;
    public GameObject enemyMilitia;
    public GameObject enemyCultist;
    public GameObject enemyRobot;
    public GameObject robert;
    public GameObject sparky;

    public enum LevelEventType
    {
        Nothing,
        Agents,
        Shop,
        Combat
    }

    [HideInInspector]
    public LevelEventType levelEvent;

    public List<GameObject> chapter1Levels = new List<GameObject>();
    public List<GameObject> chapter2Levels = new List<GameObject>();
    public List<GameObject> chapter3Levels = new List<GameObject>();
    public List<GameObject> chapter4Levels = new List<GameObject>();
    public List<GameObject> chapter5Levels = new List<GameObject>();
    public List<GameObject> chapter6Levels = new List<GameObject>();
    public List<GameObject> chapter7Levels = new List<GameObject>();

    //private GameObject _spawnedLevel;
    private LevelProperties _levelProperties;
    //private List<BaseEnemy> _levelEnemies = new List<BaseEnemy>();

    private int _enemiesAmnt = 0;
    private BaseEnemy.EnemyType _enemiesType;
//    public InventoryItemBase[] starterItems;

  //  public InventoryItemBase[] lootItems;
    /*
    private void OnRemovedItem(InventoryItemBase item, uint itemID, uint slot, uint amount)
    {
        lootItems[slot] = null;
    }
    */
    void Awake()
    {
        spawnLevel();
        spawnPlayer();

/*
        InventoryManager.instance.loot.OnRemovedItem += OnRemovedItem;

        for (int i = 0; i < starterItems.Length; i++)
            InventoryManager.instance.inventory.AddItem(starterItems[i]);

        BasicItemGenerator generator = new BasicItemGenerator();
        generator.SetItems(ItemManager.instance.items);
        lootItems = generator.Generate(10, 20);
        InventoryManager.instance.loot.SetItems(lootItems,true,true);
        */
        calculateLevelType();

        //Invoke("spawnEnemies", Random.Range(3,5));

        if (_levelProperties.hasShop)
            spawnShop();
    }
    //----------------------------------------------------------

    //----------------------------------------------------------
    private void spawnLevel()
    {
        GameManager.Instance.spawnedLevel = GameObject.FindGameObjectWithTag("Level");
        _levelProperties = GameManager.Instance.spawnedLevel.GetComponent<LevelProperties>();
        _levelProperties.OnLevelInfoClosed.AddListener(spawnEnemies);

        IngameBGMController audio = GameObject.FindGameObjectWithTag("LevelBGMController").GetComponent<IngameBGMController>();

        if ((GameManager.Instance.isLevelWon(GameManager.Instance.selectedLevelId)))
        {
            audio.inCombat.mute = true;
            audio.entryBGM.mute = true;
        }

        /*
        // Analize With Level To Spawn
        List<GameObject> currentChapter = new List<GameObject>();

        if (GameManager.Instance.selectedChapter == 1)
            currentChapter = chapter1Levels;
        else if (GameManager.Instance.selectedChapter == 2)
            currentChapter = chapter2Levels;
        // ----------- .... Add new Chapters .... --------------//

        GameObject levelToSpawn;

        if (currentChapter.Count >= GameManager.Instance.selectedLevel)
            levelToSpawn = currentChapter[GameManager.Instance.selectedLevel - 1];
        else
            levelToSpawn = currentChapter[0]; // Debug Only. (Load First Level)

        GameManager.Instance.spawnedLevel = Instantiate(levelToSpawn);
        _levelProperties = GameManager.Instance.spawnedLevel.GetComponent<LevelProperties>();


        _levelProperties.OnLevelInfoClosed.AddListener(spawnEnemies);
        */
    }
    //----------------------------------------------------------
    private void spawnPlayer()
    {
        GameObject Robert, Sparky;

        Robert = Instantiate(robert, _levelProperties.playerSpawnpoint.position, _levelProperties.playerSpawnpoint.rotation) as GameObject;
        Robert.transform.SetParent(_levelProperties.playerSpawnpoint);
        Robert.transform.localScale = new Vector3(1, 1, 1);

        Sparky = Instantiate(sparky, _levelProperties.sparkySpawnpoint.position, _levelProperties.sparkySpawnpoint.rotation) as GameObject;
        Sparky.transform.SetParent(_levelProperties.sparkySpawnpoint);
        Sparky.transform.localScale = new Vector3(1, 1, 1);

        Robert.GetComponent<Robert>().sparky = Sparky.GetComponent<Sparky>();

        Robert.GetComponent<Robert>().characterName = "Robert";
        Sparky.GetComponent<Sparky>().characterName = "Sparky";

        CombatManager.Instance.Register(Robert.GetComponent<Robert>());
    }
    //----------------------------------------------------------
    private void startInsideMusic()
    {
        IngameBGMController audio = GameObject.FindGameObjectWithTag("LevelBGMController").GetComponent<IngameBGMController>();

        if (PlayerPrefs.GetInt("BGM-Muted") == 0)
            audio.entryBGM.mute = false;
        else if (PlayerPrefs.GetInt("BGM-Muted") == 1)
            audio.entryBGM.mute = true;

        if ((GameManager.Instance.isLevelWon(GameManager.Instance.selectedLevelId)))
        {
            audio.inCombat.mute = true;
            audio.entryBGM.mute = true;
        }
        else
        {
            audio.entryBGM.Play();
        }
    }
    //----------------------------------------------------------
    private void spawnEnemies()
    {
        Invoke("startInsideMusic", 3);

        GameObject enemyToSpawn;

        if (_enemiesType == BaseEnemy.EnemyType.AGENT)
            enemyToSpawn = enemyAgent;
        else if (_enemiesType == BaseEnemy.EnemyType.CULTIST)
            enemyToSpawn = enemyCultist;
        else if (_enemiesType == BaseEnemy.EnemyType.MARAUDER)
            enemyToSpawn = enemyMarauder;
        else if (_enemiesType == BaseEnemy.EnemyType.MILITIA)
            enemyToSpawn = enemyMilitia;
        else //(_enemiesType == BaseEnemy.EnemyType.ROBOT)
            enemyToSpawn = enemyRobot;

        int count = 0;

        GameObject spawnedEnemy;

        foreach (var spawn in _levelProperties.enemiesSpawns)
        {
            spawnedEnemy = Instantiate(enemyToSpawn, spawn.startPosition.position, spawn.startPosition.rotation) as GameObject;
            spawnedEnemy.transform.SetParent(spawn.endPosition);
            spawnedEnemy.transform.localScale = new Vector3(1, 1, 1);

            spawnedEnemy.GetComponent<BaseEnemy>().init();
            spawnedEnemy.GetComponent<BaseEnemy>().startWithTransition(spawn.startPosition, spawn.endPosition);
            spawnedEnemy.GetComponent<BaseEnemy>().characterName = enemyToSpawn.name + " " + count;

            CombatManager.Instance.Register(spawnedEnemy.GetComponent<BaseEnemy>());

            count++;
        }
    }
    //----------------------------------------------------------
    private void spawnShop()
    {

    }
    //----------------------------------------------------------
    private void calculateLevelType()
    {
        if (GameManager.Instance.selectedLevelId == null)
            GameManager.Instance.setSelectedLevel();

        // Check if Robert ended this level 3 times.
        if (GameManager.Instance.getLevelCompletion(GameManager.Instance.selectedLevelId) >= ConstantValues.timesLevelCompletionForAgents)
        {
            // Check if we still can defeat agents (Max Defeated Agents Per Chapter)
            if (GameManager.Instance.checkDefeatedAgents(GameManager.Instance.selectedChapter) < ConstantValues.MaxAgents)
            {
                // Set Encounter With Agents
                levelEvent = LevelEventType.Agents;
                _enemiesType = BaseEnemy.EnemyType.AGENT;
                _enemiesAmnt = ConstantValues.AgentsGroupsCount;
            }
        }
        else
        {
            // Check For defined Enemies Amounts or generate randomized ones.
            int min, max = 0;

            min = _levelProperties.enemiesMin;
            max = _levelProperties.enemiesMax;

            if (min > max)
            {
                // Generate Some Algorithm to define enemies amount.
                min = Random.Range(0,6);
                max = Random.Range(8,15);
            }

            _enemiesAmnt = Random.Range(min, max + 1);

            if (min == max)
                _enemiesAmnt = min;

            // if not, check for defined event
            if (_levelProperties.definedEncounter)
            {
                levelEvent = LevelEventType.Combat;
                _enemiesType = _levelProperties.enemiesType;
            }
            else // if not, generate random event.
            {
                // Have a 5% chance of spawning an empty level without event of any kind.
                if (Random.Range(0.0f, 1.0f) < 0.05)
                {
                    levelEvent = LevelEventType.Nothing;
                }
                else
                {
                    levelEvent = LevelEventType.Combat;

                    int result = Random.Range(0, 3);

                    if (result == 0)
                        _enemiesType = BaseEnemy.EnemyType.MARAUDER;
                    else if (result == 1)
                        _enemiesType = BaseEnemy.EnemyType.MILITIA;
                    else
                        _enemiesType = BaseEnemy.EnemyType.CULTIST;
                }
            }
        }
    }
    //----------------------------------------------------------
}
