﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PoolElement : MonoBehaviour {

    PoolableAction _action;
    public PoolableAction action { get { return _action; } set { _action = value; } }

    Text actionDescription;

    public void configure()
    {
        transform.Find("Button").GetComponent<Button>().onClick.AddListener(RemoveAction);

        if (actionDescription == null)
           actionDescription = transform.Find("Text").GetComponent<Text>();

        if (action.callBack == CombatManager.Instance.robert.pooledHide)
        {
            actionDescription.text = "Hide";

        }
        else if (action.callBack == CombatManager.Instance.robert.pooledHide)
        {
            actionDescription.text = "Flee";
        }
        else
        {
            actionDescription.text = action.actionDescription;
        }
    }

    public void analizeAction()
    {
        if (action is AttackPoolableAction)
        {
            AttackPoolableAction newAction = action as AttackPoolableAction;

            Debug.Log("Actions is an Attack.");
        }
        else if (action is TalkPoolableAction)
        {
            TalkPoolableAction newAction = action as TalkPoolableAction;

            Debug.Log("Actions is a Dialog.");
        }
        else
        {
            Debug.Log("Standar Action");
            // Just Normal CallBack Action.
        }
    }

    void RemoveAction()
    {
        transform.parent.parent.GetComponent<ActionPoolManager>().RemoveAction(this);
    }

}
