﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ActionPoolManager : MonoBehaviour {

    public GameObject poolElementPrefab;

    GameObject elementsContainer;
    RectTransform containerTransform;

    public List<PoolElement> pooledActions = new List<PoolElement>();
    List<GameObject> instanciatedObjects = new List<GameObject>();

	public int Count { get { return pooledActions.Count; }}

    void Start()
    {
        //this.gameObject.transform.localPosition = new Vector3(179, 0, 0);
        elementsContainer = transform.Find("Container").gameObject;
        containerTransform = elementsContainer.GetComponent<RectTransform>();

        elementsContainer.SetActive(false);

        InvokeRepeating("recalculateContainerSize", 0.5f,0.5f);

        /*
        AddAction(new PoolableAction(null));
        AddAction(new AttackPoolableAction(null, null, BaseCharacter.BodyObjetive.ARMS));
        AddAction(new TalkPoolableAction(null, null, null));

        foreach (PoolElement element in pooledActions)
            element.analizeAction();
            */
    }
    /*
    public void triggerAction()
    {
        pooledActions[pooledActions.Count -1].
    }
    */

    public void AddAction(PoolableAction action)
    {
        GameObject obj = Instantiate(poolElementPrefab);
        PoolElement element =  obj.GetComponent<PoolElement>();

        element.action = action;
        element.configure();

        obj.transform.SetParent(elementsContainer.transform);
        obj.transform.localScale = new Vector3(1, 1, 1);

        instanciatedObjects.Add(obj);
        pooledActions.Add(element);

        recalculateContainerSize();
    }

    public void CleanPool()
    {
        if (instanciatedObjects.Count > 0)
            for (int i = 0; i < instanciatedObjects.Count; i++)
                DestroyImmediate(instanciatedObjects[i]);

        instanciatedObjects.Clear();

        pooledActions.Clear();
        recalculateContainerSize();
    }

    public PoolElement GetFirstEntry()
    {
        return pooledActions[0];
    }

    public void RemoveLastAction()
    {
        DestroyImmediate(pooledActions[pooledActions.Count - 1].gameObject);// element.gameObject);
        instanciatedObjects.RemoveAll(x => x == null);
        pooledActions.Remove(pooledActions[pooledActions.Count - 1]);

        recalculateContainerSize();
    }

    public void RemoveFirstAction()
    {
        DestroyImmediate(pooledActions[0].gameObject);// element.gameObject);
        instanciatedObjects.RemoveAll(x => x == null);
        pooledActions.Remove(pooledActions[0]);

        recalculateContainerSize();
    }


    public void RemoveAction(PoolElement element)
    {
        DestroyImmediate(element.gameObject);
        instanciatedObjects.RemoveAll(x => x == null);
        pooledActions.Remove(element);

        recalculateContainerSize();
    }

    private void recalculateContainerSize()
    {
        if (!elementsContainer.activeInHierarchy)
            elementsContainer.SetActive(true);
        
        containerTransform.sizeDelta = new Vector2(containerTransform.sizeDelta.x, 75 * instanciatedObjects.Count);
        //containerTransform.localPosition = new Vector3(0, 0, containerTransform.localPosition.z);
        //containerTransform.gameObject.SetActive(true);
        //--GetComponent<Canvas>().enabled = false;
        //--GetComponent<Canvas>().enabled = true;
        //Canvas.ForceUpdateCanvases();
    }

}
