﻿using UnityEngine;
using System;
using System.Collections;
using Wasabi.InventorySystem;

public class PoolableAction
{
    protected string _actionDescription = "Standar Action";
    public string actionDescription { get { return _actionDescription; } }

    Action _callBack;
    public Action callBack { get { return _callBack; } }

    public PoolableAction(Action callBack)
    {
        _callBack = callBack;
    }
}

public class ConsumeItemPoolableAction : PoolableAction
{
    protected ISObject _item;
    public ISObject item { get { return _item; } }

    public ConsumeItemPoolableAction(Action callBack, ISObject item, string actionDescription = "None") : base(callBack)
    {
        _item = item;
        if (actionDescription != "None")
            _actionDescription = actionDescription;
    }
}

public class TargetedPoolableAction : PoolableAction
{
    protected BaseCharacter _target;
    public BaseCharacter target { get { return _target; } }

    public TargetedPoolableAction(Action callBack, BaseCharacter target) : base(callBack)
    {
        _target = target;
    }
}

public class AttackPoolableAction : TargetedPoolableAction
{
    BaseCharacter.BodyObjetive _bodyObjective;
    public BaseCharacter.BodyObjetive bodyObjective { get { return _bodyObjective; } }

    public AttackPoolableAction(Action callBack, BaseCharacter target, BaseCharacter.BodyObjetive bodyObjective) : base(callBack, target)
    {
        _bodyObjective = bodyObjective;
        _actionDescription = "Attack: " + target.characterName.ToLower();
    }
}

public class TalkPoolableAction : TargetedPoolableAction
{
    DialogDefinition _dialogDefinition;
    public DialogDefinition dialogDefinition {  get { return _dialogDefinition; } }

    public TalkPoolableAction(Action callBack, BaseCharacter target, DialogDefinition dialogDefinition) : base(callBack, target)
    {
        _dialogDefinition = dialogDefinition;
        _actionDescription = "Dialog " + dialogDefinition.shortText + " To " + target.characterName.ToLower();
    }
}

