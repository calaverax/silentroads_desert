﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class DialogDefinition
{
    public enum Category
    {
        Influence,
        Intimidate
    }


    //-----------------------------------------------------------------------
    [SerializeField]
    int _dialogID;
    public int dialogID { get { return _dialogID; } }
    //-----------------------------------------------------------------------
    [SerializeField]
    Category _category;
    public Category category { get { return _category; } }
    //-----------------------------------------------------------------------
    [SerializeField]
    int _intLevel;
    public int intLevel { get { return _intLevel; } }
    //-----------------------------------------------------------------------
    [SerializeField]
    AudioClip _audioClip;
    public AudioClip audioClip { get { return _audioClip; } }
    //-----------------------------------------------------------------------
    [SerializeField]
    string _shortText = "";
    public string shortText {  get { return _shortText; } }
    //-----------------------------------------------------------------------
    [SerializeField]
    string _consoleFeedback = "";
    public string consoleFeedback { get { return _consoleFeedback; } }
    //-----------------------------------------------------------------------
    [SerializeField]
    List<BaseEnemy.EnemyType> _affectedEnemies = new List<BaseEnemy.EnemyType>();
    public List<BaseEnemy.EnemyType> affectedEnemies { get { return _affectedEnemies; } }
    //-----------------------------------------------------------------------
    [SerializeField]
    bool _createEvent;
    public bool createEvent { get { return _createEvent; } }
    //-----------------------------------------------------------------------
    [SerializeField]
    float _hitChance;
    public float hitChance { get { return _hitChance; } }
    //-----------------------------------------------------------------------
    [SerializeField]
    float _maxRange;
    public float maxRange { get { return _maxRange; } }
    //-----------------------------------------------------------------------
    [SerializeField]
    bool _splash;
    public bool splash { get { return _splash; } }
    //-----------------------------------------------------------------------
    [SerializeField]
    string _triggerAnimation = "";
    public string triggerAnimation { get { return _triggerAnimation; } }
    //-----------------------------------------------------------------------
    [SerializeField]
    List<DialogStateEffect> _effects = new List<DialogStateEffect>();
    public List<DialogStateEffect> effect { get { return _effects; } }
    //-----------------------------------------------------------------------
    [SerializeField]
    int _effectsDuration;
    public int effectsDuration {  get { return _effectsDuration; } }
    //-----------------------------------------------------------------------


    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------
    public DialogDefinition(int dialogID)
    {
        _dialogID = dialogID;
    }
    //-----------------------------------------------------------------------------
    public void setCategory(Category category)
    {
        _category = category;
    }
    //-----------------------------------------------------------------------------
    public void setIntLevel(int level)
    {
        _intLevel = level;
    }
    //-----------------------------------------------------------------------------
    public void setAudioClip(AudioClip clip)
    {
        _audioClip = clip;
    }
    //-----------------------------------------------------------------------------
    public void setShortText(string value)
    {
        _shortText = value;
    }
    //-----------------------------------------------------------------------------
    public void setConsoleFeedback(string value)
    {
        _consoleFeedback = value;
    }
    //-----------------------------------------------------------------------------
    public void setAffectedEnemies(BaseEnemy.EnemyType enemyType)
    {
        affectedEnemies.Add(enemyType);
    }
    //-----------------------------------------------------------------------------
    public void setCreateEvent(bool value)
    {
        _createEvent = value;
    }
    
    //-----------------------------------------------------------------------------
    public void setHitChance(float hitChance)
    {
        _hitChance = hitChance;
    }
    //-----------------------------------------------------------------------------
    public void setMaxRange(float maxRange)
    {
        _maxRange = maxRange;
    }
    //-----------------------------------------------------------------------------
    public void setSplash(bool value)
    {
        _splash = value;
    }
    //-----------------------------------------------------------------------------
    public void setTriggerAnimation(string value)
    {
        _triggerAnimation = value;
    }
    //-----------------------------------------------------------------------------
    public void setEffect(DialogStateEffect effect)
    {
        _effects.Add(effect);
    }
    //-----------------------------------------------------------------------------
    public void setEffectsDuration(int value)
    {
        _effectsDuration = value;
    }
    //-----------------------------------------------------------------------------
}

[System.Serializable]
public class DialogStateEffect
{
    public enum AffectedStat
    {
        Hostility,
        Intelligence,
        Acuracy,
        Morale,
        Evasion,
        Agility,
        Hitpoints
    }

    //DialogEffect _effect;
    //public DialogEffect effect { get { return _effect; } }
    //int _duration;
    //public int duration { get { return _duration; } }

    [SerializeField]
    AffectedStat _affectedStats;
    public AffectedStat affectedStats { get { return _affectedStats; } }

    [SerializeField]
    float _affectionRatio;
    public float affectionValue { get { return _affectionRatio; } }

    public DialogStateEffect(AffectedStat affectedStat, float affectionValue)
    {
        _affectedStats = affectedStat;
        _affectionRatio = affectionValue * 1.0f;
    }

    public string GetAffectedStatName(AffectedStat stat)
    {
        if (stat == AffectedStat.Hostility)
            return CharacterStats.StatHostility;
        else if (stat == AffectedStat.Intelligence)
            return CharacterStats.StatIntelligence;
        else if (stat == AffectedStat.Acuracy)
            return CharacterStats.StatAccuracy;
        else if (stat == AffectedStat.Morale)
            return CharacterStats.StatMorale;
        else if (stat == AffectedStat.Evasion)
            return CharacterStats.StatEvasion;
        else if (stat == AffectedStat.Agility)
            return CharacterStats.StatAgility;
        else if (stat == AffectedStat.Hitpoints)
            return CharacterStats.StatHitpoints;
        else
            return "";
    }


}

