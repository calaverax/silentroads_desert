﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogsDatabase : ScriptableObject
{
    public List<DialogDefinition> influenceDialog = new List<DialogDefinition>();
    public List<DialogDefinition> intimitadionDialog = new List<DialogDefinition>();
    private List<DialogDefinition> pool;

    //------------------------------------------------------------------------------------------------------------------
    public void Add(DialogDefinition element)
    {
        if (element.category == DialogDefinition.Category.Influence)
            influenceDialog.Add(element);
        else
            intimitadionDialog.Add(element);
    }
    //------------------------------------------------------------------------------------------------------------------
    public void RemoveElementByID(int id)
    {
        for (int i = 0; i < influenceDialog.Count; i++)
        {
            if (influenceDialog[i].dialogID == id)
            {
                influenceDialog.Remove(influenceDialog[i]);
                return;
            }
        }

        for (int i = 0; i < intimitadionDialog.Count; i++)
        {
            if (intimitadionDialog[i].dialogID == id)
            {
                intimitadionDialog.Remove(intimitadionDialog[i]);
                return;
            }
        }
        /*
        for (int i = 0; i < dialogsDatabase.Count; i++)
            if (dialogsDatabase[i].dialogID == id)
                dialogsDatabase.Remove(dialogsDatabase[i]);
        */
    }
    //------------------------------------------------------------------------------------------------------------------
    public DialogDefinition getElementByID(int ID)
    {
        for (int i = 0; i < influenceDialog.Count; i++)
            if (influenceDialog[i].dialogID == ID)
                return influenceDialog[i];

        for (int i = 0; i < intimitadionDialog.Count; i++)
            if (intimitadionDialog[i].dialogID == ID)
                return intimitadionDialog[i];
        /*
        for (int i = 0; i < dialogsDatabase.Count; i++)
            if (dialogsDatabase[i].dialogID == ID)
                return dialogsDatabase[i];
        */
        return null;
    }
    //------------------------------------------------------------------------------------------------------------------
    public DialogDefinition getDialog(int intLevel, DialogDefinition.Category category, bool randomIndex = false)
    {
        pool = new List<DialogDefinition>();

        if (category == DialogDefinition.Category.Influence)
            for (int i = 0; i < influenceDialog.Count; i++)
                if (influenceDialog[i].intLevel == intLevel)
                    pool.Add(influenceDialog[i]);

        if (category == DialogDefinition.Category.Intimidate)
            for (int i = 0; i < intimitadionDialog.Count; i++)
                if (intimitadionDialog[i].intLevel == intLevel)
                    pool.Add(intimitadionDialog[i]);

        if (pool.Count == 0)
            return null;

        if (randomIndex)
        {
            if (pool.Count > 1)
                return pool[Random.Range(0, pool.Count)];
            else
                return pool[0];
        }
        else
        {
            return pool[0];
        }
    }



}
