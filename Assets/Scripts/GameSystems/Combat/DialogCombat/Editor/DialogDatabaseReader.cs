﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class DialogDatabaseReader : MonoBehaviour
{
    const string DATABASE_FILE_NAME = @"DialogsDatabase.asset";
    const string DATABASE_FOLDER_NAME = @"Databases";

    static DialogsDatabase database;

    //----------------------------------------------------------------------------------------
    [MenuItem(@"Lonely Adventures/Combat Dialogs/Import Database")]
    public static void ImportDialogsDatabase()
    {
        database = GetDatabase<DialogsDatabase>(DATABASE_FOLDER_NAME, DATABASE_FILE_NAME);
        loadDialogsData();
    }
    //----------------------------------------------------------------------------------------
    public static T GetDatabase<T>(string dbPath, string dbName) where T : ScriptableObject
    {
        string dbFullPath = @"Assets/" + dbPath + "/" + dbName;

        T db = AssetDatabase.LoadAssetAtPath(dbFullPath, typeof(T)) as T;

        if (db == null)
        {
            if (!AssetDatabase.IsValidFolder("Assets/" + dbPath))
                AssetDatabase.CreateFolder("Assets", dbPath);

            db = ScriptableObject.CreateInstance<T>() as T;
            AssetDatabase.CreateAsset(db, dbFullPath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        return db;
    }
    //----------------------------------------------------------------------------------------
    
    private static void loadDialogsData()
    {
        if (!GDEDataManager.Init("gde_data"))
            Debug.LogError("Error Initializing GDE");

        GDEDialogsData dialog;

        List<string> allKeys;
        GDEDataManager.GetAllDataKeysBySchema("Dialogs", out allKeys);

        foreach (string key in allKeys)
        {
            if (!(GDEDataManager.DataDictionary.TryGetCustom(key, out dialog)))
                Debug.LogError("Error Reading Data");
            else
            {
                DialogDefinition newDialog = new DialogDefinition(dialog.DialogID);

                if (dialog.Category.ToLower() == DialogDefinition.Category.Influence.ToString().ToLower())
                    newDialog.setCategory(DialogDefinition.Category.Influence);
                else
                    newDialog.setCategory(DialogDefinition.Category.Intimidate);


                newDialog.setIntLevel(dialog.RequiredINT);
                newDialog.setAudioClip(LoadDialogAudio(dialog.AudioFile));
                newDialog.setShortText(dialog.ShortText);
                newDialog.setConsoleFeedback(dialog.ConsoleFeedback);
                analizeAffectedEnemies(newDialog, dialog);
                newDialog.setCreateEvent(dialog.CreateEvent);
                newDialog.setHitChance(dialog.HitFormulaSeed);
                newDialog.setMaxRange(dialog.Range);
                newDialog.setSplash(dialog.Splash);
                newDialog.setTriggerAnimation(dialog.Animation);
                analizeEffect(newDialog, dialog);
                newDialog.setEffectsDuration(dialog.Effects_Duration);

                if (database.getElementByID(newDialog.dialogID) != null)
                    database.RemoveElementByID(newDialog.dialogID);

                database.Add(newDialog);
            }
        }

        EditorUtility.SetDirty(database);

    }
    //----------------------------------------------------------------------------------------
    private static AudioClip LoadDialogAudio(string soundName)
    {
        string[] values = soundName.Split('.');
        return Resources.Load<AudioClip>("AudioClips/Characters/Robert/DialogCombat/" + values[0]);
    }
    //----------------------------------------------------------------------------------------
    private static void analizeAffectedEnemies(DialogDefinition dialog, GDEDialogsData dialogData)
    {
        if (dialogData.Against.Contains(" "))
        {
            string[] values = dialogData.Against.Split(' ');

            if ((values[0] == "All") && (values[1] == "except"))
            {
                if (values[2] != "Agents")
                    dialog.affectedEnemies.Add(BaseEnemy.EnemyType.AGENT);
                if (values[2] != "Cultist")
                    dialog.affectedEnemies.Add(BaseEnemy.EnemyType.CULTIST);
                if (values[2] != "Marauder")
                    dialog.affectedEnemies.Add(BaseEnemy.EnemyType.MARAUDER);
                if (values[2] != "Militia")
                    dialog.affectedEnemies.Add(BaseEnemy.EnemyType.MILITIA);
                if (values[2] != "Robot")
                    dialog.affectedEnemies.Add(BaseEnemy.EnemyType.ROBOT);
            }
        }
        else if (dialogData.Against.Contains("-"))
        {
            string[] values = dialogData.Against.Split('-');

            for (int i = 0; i < values.Length; i++)
            {
                if (values[i] == "Agents")
                    dialog.affectedEnemies.Add(BaseEnemy.EnemyType.AGENT);
                else if (values[i] == "Cultist")
                    dialog.affectedEnemies.Add(BaseEnemy.EnemyType.CULTIST);
                else if (values[i] == "Marauder")
                    dialog.affectedEnemies.Add(BaseEnemy.EnemyType.MARAUDER);
                else if (values[i] == "Militia")
                    dialog.affectedEnemies.Add(BaseEnemy.EnemyType.MILITIA);
                else if (values[i] == "Robot")
                    dialog.affectedEnemies.Add(BaseEnemy.EnemyType.ROBOT);
            }
        }
        else if (dialogData.Against == "All")
        {
            dialog.affectedEnemies.Add(BaseEnemy.EnemyType.AGENT);
            dialog.affectedEnemies.Add(BaseEnemy.EnemyType.CULTIST);
            dialog.affectedEnemies.Add(BaseEnemy.EnemyType.MARAUDER);
            dialog.affectedEnemies.Add(BaseEnemy.EnemyType.MILITIA);
            dialog.affectedEnemies.Add(BaseEnemy.EnemyType.ROBOT);
        }
        else
        {
            if (dialogData.Against == "Agents")
                dialog.affectedEnemies.Add(BaseEnemy.EnemyType.AGENT);
            else if (dialogData.Against == "Cultist")
                dialog.affectedEnemies.Add(BaseEnemy.EnemyType.CULTIST);
            else if (dialogData.Against == "Marauder")
                dialog.affectedEnemies.Add(BaseEnemy.EnemyType.MARAUDER);
            else if (dialogData.Against == "Militia")
                dialog.affectedEnemies.Add(BaseEnemy.EnemyType.MILITIA);
            else if (dialogData.Against == "Robot")
                dialog.affectedEnemies.Add(BaseEnemy.EnemyType.ROBOT);
        }
    }
    //----------------------------------------------------------------------------------------
    private static void analizeEffect(DialogDefinition dialog, GDEDialogsData dialogData)
    {
        // Parse afected and add it to dialog.
        if (dialogData.Effect1_Stat != string.Empty)
        {
            if (dialogData.Effect1_Stat == "Acuracy")
                dialog.setEffect(new DialogStateEffect(DialogStateEffect.AffectedStat.Acuracy, dialogData.Effect1_Value));
            else if (dialogData.Effect1_Stat == "Agility")
                dialog.setEffect(new DialogStateEffect(DialogStateEffect.AffectedStat.Agility, dialogData.Effect1_Value));
            else if (dialogData.Effect1_Stat == "Evasion")
                dialog.setEffect(new DialogStateEffect(DialogStateEffect.AffectedStat.Evasion, dialogData.Effect1_Value));
            else if (dialogData.Effect1_Stat == "Hitpoints")
                dialog.setEffect(new DialogStateEffect(DialogStateEffect.AffectedStat.Hitpoints, dialogData.Effect1_Value));
            else if (dialogData.Effect1_Stat == "Hostility")
                dialog.setEffect(new DialogStateEffect(DialogStateEffect.AffectedStat.Hostility, dialogData.Effect1_Value));
            else if (dialogData.Effect1_Stat == "Intelligence")
                dialog.setEffect(new DialogStateEffect(DialogStateEffect.AffectedStat.Intelligence, dialogData.Effect1_Value));
            else if (dialogData.Effect1_Stat == "Morale")
                dialog.setEffect(new DialogStateEffect(DialogStateEffect.AffectedStat.Morale, dialogData.Effect1_Value));
        }

        if (dialogData.Effect2_Stat != string.Empty)
        {
            if (dialogData.Effect2_Stat == "Acuracy")
                dialog.setEffect(new DialogStateEffect(DialogStateEffect.AffectedStat.Acuracy, dialogData.Effect2_Value));
            else if (dialogData.Effect2_Stat == "Agility")
                dialog.setEffect(new DialogStateEffect(DialogStateEffect.AffectedStat.Agility, dialogData.Effect2_Value));
            else if (dialogData.Effect2_Stat == "Evasion")
                dialog.setEffect(new DialogStateEffect(DialogStateEffect.AffectedStat.Evasion, dialogData.Effect2_Value));
            else if (dialogData.Effect2_Stat == "Hitpoints")
                dialog.setEffect(new DialogStateEffect(DialogStateEffect.AffectedStat.Hitpoints, dialogData.Effect2_Value));
            else if (dialogData.Effect2_Stat == "Hostility")
                dialog.setEffect(new DialogStateEffect(DialogStateEffect.AffectedStat.Hostility, dialogData.Effect2_Value));
            else if (dialogData.Effect2_Stat == "Intelligence")
                dialog.setEffect(new DialogStateEffect(DialogStateEffect.AffectedStat.Intelligence, dialogData.Effect2_Value));
            else if (dialogData.Effect2_Stat == "Morale")
                dialog.setEffect(new DialogStateEffect(DialogStateEffect.AffectedStat.Morale, dialogData.Effect2_Value));
        }
    }
    //----------------------------------------------------------------------------------------
}
