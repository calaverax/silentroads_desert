﻿using UnityEngine;
using System.Collections;

public class XpSystem : MonoBehaviour {
    private int xp;
    private int max;
    // Constructor
    public XpSystem()
    {
        max = 500;
    }
    // Getter
    public int getXP()
    {
        return CombatManager.Instance.robert.characterStats.Experience;
    }
    // Setter
    public void setXP(int value)
    {
        CombatManager.Instance.robert.characterStats.Experience = value;
    }

    // AddXP : Lo llama Robert para agregarse la XP que le mandan los enemigos
    public void AddXP(int exp)
    {
        CombatManager.Instance.Logger.SubmitText("Robert won " + exp + " XP");
        CombatManager.Instance.robert.characterStats.Experience += exp;
        Almacenar();
        //Debug.Log("La experiencia TOTAL es : " + CombatManager.Instance.robert.characterStats.Experience);
        CheckearXP();
    }

	// CheckearXP : Revisa si la XP de Robert supero el tope de 1000pts
    private void CheckearXP()
    {
        if(CombatManager.Instance.robert.characterStats.Experience >= max)
        {
            AgregarPunto();
            RestablecerXP();
        }
    }

    //Agrega un punto de experiencia avisando ademas por consola que Robert dispone de un punto mas
    private void AgregarPunto()
    {
        CombatManager.Instance.robert.characterStats.availablePoints += 1;
        CombatManager.Instance.Logger.SubmitText("<color=green>Robert has one more point of ability</color>");
        CombatManager.Instance.GUIScript.XpNewPoint();
    }

    // Una vez agregado el punto de habilidad asignable se restablece la XP
    private void RestablecerXP()
    {
        CombatManager.Instance.robert.characterStats.Experience -= max;
        Almacenar();
    }

    private void Almacenar()
    {
        PlayerDataManager.Instance.SetValue(CharacterStats.StatExperience, CombatManager.Instance.robert.characterStats.Experience);
        //PlayerPrefs.SetInt(CharacterStats.StatExperience, CombatManager.Instance.robert.characterStats.Experience);
    }
}
