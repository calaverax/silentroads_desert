﻿using UnityEngine;
using System.Collections;
using System;

public static class CombatResolver
{

    /*
     INDICE
     * caculateDistnace() Determina la distancia entre el atacante y la víctima
     *> ResolveAttack() es la función principal que llama al resto
     *> processAttack() esta función (que es llamada por ResolveAttack()) es la más importante, porque calcula casi todos los datos del combate
     * resolveMeleeAttack() Melee es el ataque cuerpo a cuerpo, la función está vacía
     * resolveAoeAttack() La función está prácticamente vacía.
     * calculateChanceToHit() lo que su nombre indica. Esta función no es llamada desde este script por ninguna otra.
     
     DESCRIPCIÓN DEL SCRIPT
     El combat resolver es llamado por varios scripts para ver cómo fue cada ataque en el combate. 
     Si Robert ataca a el Agente Wade, se llama al combat resolver para saber cuál es el resultado. 
     Por lo general los scripts que lo usan son BaseEnemy.cs y Robert.cs (también es posible que BaseCharacter.cs y Sparky.cs lo usen).
       También este script tiene mucho de input para la consola.

     */
    static int[] _accuracyTable       = {  10,  20,  30,  40,  50,  60,  70,  85, 100, 120};
    static int[] _moraleTable         = { -15, -10,  -5,   0,   5,  10,  15,  20,  25,  30};
    static int[] _accuracyWeaponTable = { -30, -20, -10,  -5,   0,   3,   6,  10,  15,  20};
    static int[] _evasionTable        = {   0,  -5, -10, -15, -20, -25, -30, -35, -50, -65};

    static float distanceToTarget;


    // Escala para Escenarios Grandes = 6
    // Escala Escenarios Medianos = 20
    // Escala Escenarios Chicos = 50




    public static float calculateDistance(GameObject from, GameObject to)
    {
        //Debug.Log("está calculando la distancia");
        //PPP
        float distResult = 0; //Para almacenar el resultado de la distancia

        float scale = CombatManager.Instance.robertScale; //La escala de Robert la obetenemos del combat manager.

        distResult = Vector3.Distance(to.transform.position, from.transform.position) /2 /(scale/2.33f); //PPP No tocar!!
        
        return Mathf.Round(distResult * 100f) / 100f; //este fraseo es para obtener 2 decimales.
        
    }

    //backup de la operación para sacar la distancia: 
    //distResult = Vector3.Distance(to.transform.position, from.transform.position) /2 /(scale/2.33f);

    //Script de Nahuel:
    // Temporary Disabled untill new Distance Check in 3D Space.
    //return 50; //solo este estaba sin comentar...
    //return Vector2.Distance(from.transform.position, to.transform.position) / (int)GameManager.Instance.levelManager.scenarioSize;

    //--------------------------------------------------------------------------------------------------------------------------------

    /*El RESOLVERATTACK es un código que sirve para determinar si el disparo dio en el blanco o no.
     * La estructura básica que tiene depende del modo del arma, es decir si tira un tiro o varios.
     * Las funciones son iguales, salvo que si tira varios tiros corre un for loop.
     * Las funciones más importante a la que llama son CalculateDistance() y ProcessAttack()
     
     */
    public static void ResolveAttack(BaseCharacter.AttackInfo attackInfo,  Action callBack = null, string obstacle = null )
    {



        /* PPP BaseCharacter.AttackInfo lleva como parámetros que podés usar:
        BaseCharacter attacker, victim; //Atacante y víctima
        BodyObjetive bodyObjective; //A qué parte del cuerpo le tiró.
        DamageType damageType; //Tipo de daño (Ranged, melee, throwing).
        ShootMode shootMode; //El modo del disparo (single (1), burst (3), auto(6))
        bool scopeWeapon, sneakAttack;
        int attackAccuracy; //La precisión que va a tener
        BulletType bulletType; //Almacena el tipo de bala (normal, con radiación, con veneno)
        PPP */

        //calculá la distancia entre el enemigo y su víctima
        distanceToTarget = calculateDistance(attackInfo.attacker.gameObject, attackInfo.victim.gameObject);
        //Pasá la info a la consola:
        /*CombatManager.Instance.Logger.SubmitText(
            attackInfo.attacker.characterName + " Attacks " + attackInfo.victim.characterName + "" +
            " to the " + attackInfo.bodyObjective.ToString().ToLower()); //PPP lo saqué xq no aportaba demasiado para la consola ya que todo esto lo dice más tarde*/


        CombatManager.Instance.debugPanel.SubmitText("Attacker Distance To Target: " + distanceToTarget, "Red");

        BaseCharacter.AttackResult attackResult = new BaseCharacter.AttackResult(); //Acá va a almacenar el resultado.

        //Más info a la consola:
        CombatManager.Instance.debugPanel.SubmitText("Attacker Hit Chance: " + attackResult.hitChance.ToString(), "Red");
        CombatManager.Instance.debugPanel.SubmitText("Attacker Shoot Mode: " + attackInfo.shootMode.ToString(), "Red");
        CombatManager.Instance.debugPanel.SubmitText("Attacker Damage Type: " + attackInfo.damageType.ToString(), "Red");
        CombatManager.Instance.debugPanel.SubmitText("Attacker Body Objective: " + attackInfo.bodyObjective.ToString(), "Red");
        CombatManager.Instance.debugPanel.SubmitText("Attacker Attack Accuracy: " + attackInfo.attackAccuracy.ToString(), "Red");
        CombatManager.Instance.debugPanel.SubmitText("Attacker: " + attackInfo.attacker.ToString(), "Red");
        CombatManager.Instance.debugPanel.SubmitText("Victim: " + attackInfo.victim.ToString(), "Red");

        //Si el atacante es Robert...
        if (attackInfo.attacker is Robert)
        {
            BaseEnemy enemy = attackInfo.victim as BaseEnemy; //creo que mete el BaseEnemy script del enemigo en "enemy"
            if (enemy.enemyType == BaseEnemy.EnemyType.MARAUDER) //Si Robert ataca a un Marauder, avisa al CombatManager
                CombatManager.Instance.OnMarauderAttacked.Invoke();
        }

        float a = 0; //Float que va a usar como un dado para almacenar valores al azar.

        if (attackInfo.shootMode == BaseCharacter.ShootMode.SINGLE){ //Si el shootmode está en Single...

            attackResult = processAttack(attackInfo, 0); 

            a = UnityEngine.Random.Range(0.0f, 100.0f); //Sacá un número del 1 al 100.



            if (a <= attackResult.hitChance) //Si el número es menor o igual a las hitChances
            {
                attackInfo.victim.recieveDamage(attackResult); //Le acertó, pasale el daño a la víctima.
                //PPP Acá hay restarle al Damage si dio en un obstáculo.
                
                //Pasá la info a la consola
                CombatManager.Instance.Logger.SubmitText(attackInfo.attacker.characterName + " hits " + attackInfo.victim.characterName + " in the " + attackInfo.bodyObjective.ToString().ToLower() + " (" + attackResult.damage + " Damage)");
            }
            else //Si no es menor, no le dio
            {
                //Consola:
                CombatManager.Instance.Logger.SubmitText(attackInfo.attacker.characterName + " Attacks " + attackInfo.victim.characterName + " to the " + attackInfo.bodyObjective.ToString().ToLower() + " with Ranged Attack, but fails");
                //Corré el sonido de que le erraste
                CombatManager.Instance.NpcFXController.PlayMissFX();
                if (attackInfo.attacker is Robert) //Si el atacante fue Robert
                    if (CombatManager.Instance.packLeaderIsAlive) //Y el líder de la banda está vivo
                        EventManager.TriggerEvent("OnRobertMissesAttack"); //Corré el evento "OnRobertMissedAttack"
            }
        }
        else //Si el shootMode no era SINGLE
        {
            for (int i = 0; i < (int)attackInfo.shootMode; i++) //Corré un for loop para cada tiro 
                //(el código es prácticamente igual al de arriba)
            {
                attackResult = processAttack(attackInfo, i);

                a = UnityEngine.Random.Range(0.0f, 100.0f);

                if (a <= attackResult.hitChance)
                {
                    attackInfo.victim.recieveDamage(attackResult); 
                }
                else
                {
                    CombatManager.Instance.Logger.SubmitText(attackInfo.attacker.characterName + " Attacks " + attackInfo.victim.characterName + " to the " + attackInfo.bodyObjective.ToString().ToLower() + " with Ranged Attack, but fails");
                    CombatManager.Instance.NpcFXController.PlayMissFX();
                    if (attackInfo.attacker is Robert)
                        if (CombatManager.Instance.packLeaderIsAlive)
                            EventManager.TriggerEvent("OnRobertMissesAttack");
                }
            }
        }

        //Si el callback está almacenando una función llamalo.
        if (callBack != null)
            callBack();
    }



    //--------------------------------------------------------------------------------------------------------------------------------
    private static BaseCharacter.AttackResult processAttack(BaseCharacter.AttackInfo attackInfo, int attackIndex)
        //Si bien esta no es la función principal es quizás la más importante, se fija las chances de que le pegues al objetivo y cuántos puntos le tenés que sacar.
    {
        //Consola
        CombatManager.Instance.debugPanel.SubmitText(". . . . . . . . . . . . . . .");
        CombatManager.Instance.debugPanel.SubmitText("Processing Attack Formula:");

        //Debug.Log("Max Range" + attackInfo.weapon.MaxRange + "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");

        // La chance de acertar empieza como cero
        int hitChance = 0;
        int hitChance2 = 0;
        float hitChance3 = 0;

        float chance;
        // 1- Primero te fijás el SCOPE del arma

         //Lo que hace esta parte es que si estás a la mitad de la distancia del range del arma sólo tenés la mitad de las chances de dar al blanco. Si estás al 10 porciento, sólo tenés, 0.1, y así.


        ///COMBAT 3//////////////////////////////////////////////////////////////////////////////////
        float rango = attackInfo.weapon.MaxRange;
        float distancia = distanceToTarget;
        float accuracy = attackInfo.attackAccuracy;

        //CERCANÍA
        //La cecanía es un número entre 0 y 1. 0 = lejos, 1= cerca. Se mide en base a la distancia con el enemigo en base al rango del arma con que disparás.
        float cercanía = (rango - distancia) / rango;

        //LEJANÍA
        //La inversa a la cercanía, 0 = cerca, 1 = lejos.
        float lejanía = 1 - cercanía;

        //PUNTERÍA
        //La puntería es un número entre -1 y 1. 0= tirador capaz, 1 = experto, -1 = novato.
        float puntería = accuracy.Remap(0, 1, -1, 1);
 
        //ACÁ EL CÁLCULO FUNDAMENTAL DE LAS CHANCES EN BASE A LA DISTANCIA Y LA PUNTERÍA
        hitChance3 = cercanía + ( ( lejanía / 2 ) * puntería);

        //indices de referencia del cálculo anterior:
        //puntería pésima =     cerca al enemigo 85%;     a media distancia 25%;      lejos -35%
        //puntería intermedia = cerca al enemigo 90%;     a media distancia 50%;      lejos 0%
        //puntería perfecta =   cerca al enemigo 95%;     a media distancia 75%;      lejos 55%

        //La lógica del cálculo: La idea es que el factor más importante es la distancia. La puntería sólo puede afectar

        
        //////////////////////////////////////////////////
        int punteriaTuya = attackInfo.attacker.characterStats.Accuracy;
        int punteriaArma = attackInfo.weapon.Accuracy;
        int moral = attackInfo.attacker.getMorale();
        float evasion = attackInfo.victim.getEvasion();

        float punteríaTotal = (punteriaTuya + punteriaArma) / 2;
        
        

        Debug.Log("````````````````````````````````````````````````````````````");
        Debug.Log("rango = " + rango);
        Debug.Log("punteríaTuya = " + punteriaTuya);
        Debug.Log("punteria del arma = " + punteriaArma);
        Debug.Log("punteria total = " + punteríaTotal);
        Debug.Log("moral = " + moral);
        Debug.Log("evasion = " + evasion);

        Debug.Log("cercanía = " + cercanía * 100 + "%");
        Debug.Log("sumando la puntería += " + lejanía * punteríaTotal);

        Debug.Log("````````````````````````````````````````````````````````````");

        /////////////////////////////////////////////////





        //Esto te da el porcentaje entre tu distancia y el rango. 
        chance = (attackInfo.weapon.MaxRange - distanceToTarget) / attackInfo.weapon.MaxRange;
        hitChance = Mathf.RoundToInt (100* chance);


        /* (attackInfo.attacker.characterStats.Accuracy * 20 /100)
        Debug.Log("accuracy:" + attackInfo.attacker.characterStats.Accuracy);
        Debug.Log(hitChance + "% ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        */


        /*
        if (attackInfo.scopeWeapon)
        {
            hitChance = 80 - (5 * (int)(distanceToTarget / 10)); // Base Chance 80% and -5% Each 10 Feets
            CombatManager.Instance.debugPanel.SubmitText("Weapon With Scope: - Base Chance 80% and -5% Each 10 Feets (HitChance: " + hitChance + ")","Red");
        }
        else
        {
            hitChance = 65 - (1 * (int)(distanceToTarget / 10)); // Base Chance 65% and -1% Each 10 Feets
            CombatManager.Instance.debugPanel.SubmitText("Weapon Without Scope: - Base Chance 65% and -1% Each 10 Feets (HitChance: " + hitChance + ")", "Red");
        }

        */
        //2- incrementa las chances en base a la ACCURACY

        // Modify Chance Based on Accuracy +5% For each Accuracy Point
        hitChance += 5 * (attackInfo.attackAccuracy - 1);
        CombatManager.Instance.debugPanel.SubmitText("Modify Chance Based on Accuracy +5% For each Accuracy Point", "Red");
        CombatManager.Instance.debugPanel.SubmitText("Accuracy: " + attackInfo.attackAccuracy + " New HitChance: " + hitChance, "Red");

        // If we Have Accuracy 10, add a 5% Bonus Chance to hit
        if (attackInfo.attackAccuracy == 10)
        {
            hitChance += 5;
            CombatManager.Instance.debugPanel.SubmitText("We Have Accuracy 10, add a 5% Bonus Chance to hit", "Red");
        }

        if (CombatManager.Instance.focusActiveBonus.Contains(FocusSystemController.FocusCategory.Attack)) 
        {
            hitChance += 20;
            CombatManager.Instance.debugPanel.SubmitText("Focus System is on Attack Mode, Accuracy +20%, New HitChance: " + hitChance, "Red");
        }

        
        // 3-  Si es el primer ataque la adiciona 20 de chances.
        // First Attack of Combat?
        if (attackInfo.sneakAttack) //Si es el primer ataque tenés más chance de pegarle
        {
            hitChance += 20;
            CombatManager.Instance.debugPanel.SubmitText("Sneak Attack, Accuracy +20%, New HitChance: " + hitChance, "Red");
        }

        //4- Si el arma tira varios tiros en el mismo ataque, cada tiro tiene un 15% menos de chances que el anterior (o sea el primero -15, el segundo -30, etc)
        if (attackInfo.shootMode != BaseCharacter.ShootMode.SINGLE)
        {
            hitChance -= 15 * attackIndex;
            CombatManager.Instance.debugPanel.SubmitText("ShootMode is Not on Sinngle -15% Per Attack, New HitChance: " + hitChance, "Red");
        }

        //5- a partir de 5 puntos de moral cada punto suma 3% de chances, debajo de 5 puntos, resta 3% por cada punto
        // Morale(+3 %) after 5(-3 %) below  5
        if (attackInfo.attacker.getMorale() > 5)
            hitChance += 3 * attackInfo.attacker.getMorale() - 5;
        else if (attackInfo.attacker.getMorale() < 5)
            hitChance -= 3 * attackInfo.attacker.getMorale();

        //6- Analize Visual modifiers (se fija el tiempo del día para determinar la visibilidad del tirador)
        if ((GameManager.Instance.levelManager.timeOfday == LevelManager.TimeOfDay.Dawn))
            hitChance -= (int)distanceToTarget / 10;
        else if (GameManager.Instance.levelManager.timeOfday == LevelManager.TimeOfDay.Night)
            hitChance -= (int)distanceToTarget / 5;

        //7- Analize Target Evasion (arriba de 5 suma más 3%, si es debajo, va sacando 3%)
        if (attackInfo.victim.getEvasion() > 5)
            hitChance -= 3 * (int)attackInfo.victim.getEvasion() - 5;
        else if (attackInfo.victim.getEvasion() < 5)
            hitChance += 3 * (int)attackInfo.victim.getEvasion();

        //8- Check if we are aiming to the head or general body (si tirás a la cabeza es más fácil errarle)
        if (attackInfo.bodyObjective == BaseCharacter.BodyObjetive.HEAD)
            hitChance -= (hitChance * 30) / 100;
        else    
        hitChance -= (hitChance * 10) / 100;

        //9- If we are out of max range, divide chance by 3
        if (distanceToTarget > attackInfo.attacker.getRange())
            hitChance = hitChance / 3;
        //==============================================================================================================
        //NUEVA FORMULA : ACTIVAR COMENTARIOS PARA DEBUGGEAR
        //***************
        //Debug.Log("===========================================================");
        //Arranca con la Accuracy
        hitChance2 = _accuracyTable[attackInfo.attacker.getAccuracy("Self") - 1];
        //Debug.Log("ACCURACY : " + attackInfo.attacker.getAccuracy("Self") + " / " + _accuracyTable[attackInfo.attacker.getAccuracy("Self") - 1]);
        //Le suma la moral
        //Debug.Log("MORAL : " + attackInfo.attacker.getMorale() + " / " + _moraleTable[attackInfo.attacker.getMorale() - 1]);
        hitChance2 += _moraleTable[attackInfo.attacker.getMorale() - 1];
        //Le sumo 10 si robert tiene foco en Atacar
        if (attackInfo.attacker is Robert)
        {
            //Cree esta funcion en el CManager para pedir facilmente el focus activo
            if (CombatManager.Instance.getFocusElement() == "Attack")
            {
                //Debug.Log("TIENE FOCO EN ATACAR +10");
                hitChance2 += 10;
            }
        }
        //Le suma el rango
        if (distanceToTarget > attackInfo.attacker.getRange())
        {
            //Debug.Log("ESTA FUERA DE RANGO, -75");
            hitChance2 = hitChance2 - 75;
        }

        //Le suma el Accuracy del arma
        //Debug.Log("ACCURACY DEL ARMA : " + attackInfo.attacker.getAccuracy("Weapon") + " / " + _accuracyWeaponTable[attackInfo.attacker.getAccuracy("Weapon") - 1]);
        hitChance2 += _accuracyWeaponTable[attackInfo.attacker.getAccuracy("Weapon") - 1]; //PPPP Guarda con esto que tira error.
        //Si esta a mas de 50m y el arma tiene mira, tiene 25% mas de chances
        if(distanceToTarget >= 50)
        {
            if (attackInfo.scopeWeapon)
            {
                //Debug.Log("ESTA A MAS DE 50m y TIENE MIRA ENTONCES +25");
                hitChance2 += 25;
            }
        }
        //Le suma el ShootMode
        if (attackInfo.shootMode != BaseCharacter.ShootMode.SINGLE)
        {
            if (attackInfo.shootMode != BaseCharacter.ShootMode.AUTO)
            {
                //Debug.Log("AUTO -40");
                hitChance2 -= 40;
            }else
            {
                //Debug.Log("RAFAGA -20");
                hitChance2 -= 20;
            }
        }
        //hitChance2 += Distance;
        if(distanceToTarget < 50f)
        {
            //Debug.Log("LE RESTA LA DISTANCIA : " + (int)distanceToTarget);
            hitChance2 -= (int)distanceToTarget;
        }else
        {
            //Debug.Log("LE RESTO 50 POR LA DISTANCIA");
            hitChance2 -= 50;
        }
        //Aca si le disparan a Robert y el focus esta en Evadir tiene -20
        if (attackInfo.victim is Robert)
        {
            //Cree esta funcion en el CManager para pedir facilmente el focus activo
            if (CombatManager.Instance.getFocusElement() == "Dialogue")
            {
                //Debug.Log("ES ROBERT LA VICTIMA Y ESTA EN EVADE : - 10");
                hitChance2 -= 10;
            }
        }
        //Le suma la evasion
        hitChance2 += _evasionTable[(int)attackInfo.victim.getEvasion()];
        //Debug.Log("EVACION : " + (int)attackInfo.victim.getEvasion() + " / " + _evasionTable[(int)attackInfo.victim.getEvasion()]);
        //Le resta la moral del enemigo (aca resta porque la moral es un solo array para los dos)
        //Debug.Log("MORAL DEL ENEMIGO : " + attackInfo.victim.getMorale() + " / " + _moraleTable[attackInfo.victim.getMorale() - 1]);
        hitChance2 -= _moraleTable[attackInfo.victim.getMorale() - 1];
        //Le resta 20% de chances si apuntas a la cabeza
        if (attackInfo.bodyObjective == BaseCharacter.BodyObjetive.HEAD)
        {
            //Debug.Log("ES A LA CABEZA ASIQUE -20");
            hitChance2 -= 20;
        }
            
        //Le resta la visibilidad
        if ((GameManager.Instance.levelManager.timeOfday == LevelManager.TimeOfDay.Dawn))
            hitChance2 -= 10;
        else if (GameManager.Instance.levelManager.timeOfday == LevelManager.TimeOfDay.Night)
            hitChance2 -= 20;

        //Debug.Log("===========================================================");
        //***********  RESULTADOS  ***************
        //Debug.Log("hitchance2 = " + hitChance2);
        //Debug.Log("hitchance = " + hitChance);
        //****************************************
        //==============================================================================================================    
        //10- Critical Hit Chance Calculations (Esta es una chance improbable de que el personaje pegue un tirazo que haga mierda al enemigo)
        bool isCritical = false;
        int critChance = UnityEngine.Random.Range(0, 101);
        int value = (((attackInfo.attacker.getIntelligence() + attackInfo.attacker.getAccuracy("Self")) / 6) * 10);

        if (critChance <= value)
            isCritical = true;

        int attackDamage = attackInfo.attacker.getDamage();

        if (isCritical)
            attackDamage *= 2;

        ///PPP Acá le podés reducir las hitChance significativamente; simplemente dividís hitChance y listo.

        CombatManager.Instance.debugPanel.SubmitText("- End Of Attack Formula Process -");
        CombatManager.Instance.debugPanel.SubmitText(". . . . . . . . . . . . . . .");

        return new BaseCharacter.AttackResult(attackInfo.attacker, attackInfo.victim, attackInfo.bodyObjective, attackDamage, isCritical, hitChance);
    }

    //texto que estaba comentado en la función anterior
    /*
if (attackInfo.attacker is Robert)
    hitChance -= 10;
else
    hitChance += 10;

// Analize Focus -20% if Healing -10% if Defensive +10 % if Attacking 
if (attackInfo.attacker.getFocus() == 0.6f)
    hitChance -= 20;
else if (attackInfo.attacker.getFocus() == 0.8f)
    hitChance -= 10;
else if (attackInfo.attacker.getFocus() == 1.2f)
    hitChance += 10;
*/

    //--------------------------------------------------------------------------------------------------------------------------------
    private static void resolveMeleeAttack(BaseCharacter attacker, BaseCharacter target, BaseCharacter.BodyObjetive bodyObjetive)
    {
        CombatManager.Instance.Logger.SubmitText(attacker.characterName + " Attacks " + target.characterName + " with Melee Attack");
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    private static void resolveAoeAttack(BaseCharacter attacker, BaseCharacter target, BaseCharacter.BodyObjetive bodyObjetive)
    {
        CombatManager.Instance.Logger.SubmitText(attacker.characterName + " Attacks " + target.characterName + " with AOE Attack");
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    // TODO:
    // Si la distancia esta en RANGE del arma, todo bien, sino, se corta a un tercio la efectividad
    private static float calculateChanceToHit(BaseCharacter attacker, BaseCharacter target, BaseCharacter.BodyObjetive bodyObjetive, bool sneakAttack = false)
        //Curiosamente no encuentro nada que llame a esta función ni acá ni en otros lugares; me parece que la reemplaza la que está arriba
    {
        // a - Range - Distance
        float a = attacker.getRange() - distanceToTarget;

        // b - Acuracy (Own) *10
        float b = attacker.getAccuracy("Self") * 10;

        // c - Acuracy (Weapon) * 5 
        float c = attacker.getAccuracy("Weapon") * 5;

        // D - Evasion / 30 
        float d = target.getEvasion() / 30;

        // E - Average B and C - D
        float e = (b + (c - d)) / 2;

        // F - e - Focus (Own) 
        float f = e - attacker.getFocus();

        // G - f * Visibility
        float g = f * CombatManager.Instance.visibility;

        // H - Chance To Hit: g - distancia  / 10 
        float h = g - distanceToTarget / 10;

        // I - h * focus(Enemy)
        float i = h * target.getFocus();

        // Modifiers
        if (bodyObjetive == BaseCharacter.BodyObjetive.HEAD)
            i = i - ((i * 20) / 100); // If Head  - 20% minus
        else
            i = i - ((i * 10) / 100); // If BodyPart - 10% minus

        // First Shoot While Hiding 20% Bonus
        if (sneakAttack)
            i = i + ((i * 20) / 100);

        // Shooting Mode Penalties
        if (attacker.getShootMode() == BaseCharacter.ShootMode.BURST) // Burst Penalty -15%
            i = i - ((i * 15) / 100);
        else if (attacker.getShootMode() == BaseCharacter.ShootMode.AUTO) // Auto Penalty -30%
            i = i - ((i * 30) / 100);

        // Acuracy 10 da 10% bonus
        if (attacker.getAccuracy("Self") == 10)
            i = i + ((i * 10) / 100);

        // MORALE BONUS (more than 5, 3% per point)
        if (attacker.getMorale() > 5)
            i = i + (((i * 3) / 100) * (attacker.getMorale() - 5));

        // MORALE PENALTY (less than 5, -4% per point)
        if (attacker.getMorale() < 5)
            i = i - ((i * 4) / 100) * (attacker.getMorale());

        // Si la distancia esta en RANGE del arma, todo bien, sino, se corta a un tercio la efectividad

        Debug.Log(attacker.name + " chance to hit: " + i);
        return i;
    }

    //--------------------------------------------------------------------------------------------------------------------------------
}
