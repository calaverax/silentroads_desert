﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class ObstacleChecker : MonoBehaviour {

    public UnityEvent OnBulletCollision;

    void Awake() {
        OnBulletCollision = new UnityEvent();
    }

    void OnTriggerEnter2D(Collider2D other) {
        OnBulletCollision.Invoke();
    }

}
