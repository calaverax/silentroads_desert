﻿using UnityEngine;
using System;
using System.Collections;

public class DialogCombatManager : Singleton<DialogCombatManager>
{
    BaseEnemy _talkingTarget;
    public BaseEnemy talkingTarget { get { return _talkingTarget; } }
    DialogDefinition _dialogDefinition;
    Robert _robert;
    Action _callBack;
    
    public void talkSelected(BaseEnemy target)
    {
        _talkingTarget = target;
    }

    public void triggerDialog(DialogDefinition dialogDefinition, Action callBack)
    {
        _callBack = callBack;

        if ((_talkingTarget == null) || (dialogDefinition == null)) { if (_callBack != null) _callBack(); return; }

        _dialogDefinition = dialogDefinition;

        if (_robert == null)
            _robert = CombatManager.Instance.robert;

        if (dialogDefinition.audioClip != null)
        {
            _robert.robertSoundManager.playVoiceAudio(dialogDefinition.audioClip);//.playOneShoot(dialogDefinition.audioClip);
            Invoke("processDialog", dialogDefinition.audioClip.length);
        }
        else
        {
            processDialog();
        }
    }

    private void processDialog()
    {
        //CombatManager.Instance.Logger.SubmitText("Robert says: " + _dialogDefinition.consoleFeedback);

        CombatManager.Instance.debugPanel.SubmitText(". . . . . . . . . . . . . . .");
        CombatManager.Instance.debugPanel.SubmitText("Processing Dialog Formula:");

        float robertInt = _robert.characterStats.Intelligence * 0.1f;
        float robertComm = _robert.characterStats.Command * 0.1f;
        float chanceSeed = _dialogDefinition.hitChance;

        CombatManager.Instance.debugPanel.SubmitText("Robert Int: " + _robert.characterStats.Intelligence);
        CombatManager.Instance.debugPanel.SubmitText("Robert Command: " + _robert.characterStats.Command);
        CombatManager.Instance.debugPanel.SubmitText("Chance Seed: " + chanceSeed);

        if (robertInt > _dialogDefinition.intLevel)
        {
            chanceSeed += 0.1f * (robertInt - _dialogDefinition.intLevel);
            CombatManager.Instance.debugPanel.SubmitText("Robert Int > Dialog Int Level + 10% Per Point: (New Chance: " + chanceSeed + ")");
        }

        float totalChance = ((chanceSeed + robertInt + robertComm) / 3);
        CombatManager.Instance.debugPanel.SubmitText("Total Chance (((chanceSeed + robertInt + robertComm) / 3)) = " + totalChance);
        //float totalChance = 500;

        if (UnityEngine.Random.Range(0.0f, 1.0f) <= totalChance)
        {
            float chanceToResist = 0.6f * ((_talkingTarget.characterStats.Intelligence * 0.1f + _talkingTarget.characterStats.Command * 0.1f) * 0.5f);
            CombatManager.Instance.debugPanel.SubmitText("Resist Chance (0.6 * (Objetive Intel * 0.1 + Talker Command * 0.1) * 0.5 = " + chanceToResist);
            
            if (UnityEngine.Random.Range(0.0f, 1.0f) <= chanceToResist)
            //if (false)
            {
                if (chanceToResist > 0.35f)
                {
                    CombatManager.Instance.debugPanel.SubmitText("Resisted for More than 35% of Hit Chance" );
                    CombatManager.Instance.Logger.SubmitText(_talkingTarget.characterName + " Seems Inmune");
                }
                else
                {
                    CombatManager.Instance.debugPanel.SubmitText("Resisted");
                    CombatManager.Instance.Logger.SubmitText(_talkingTarget.characterName + " Resist the effect");
                }
                CombatManager.Instance.debugPanel.SubmitText("Playing Unsuccesfull Dialogue FX");
                CombatManager.Instance.robert.robertSoundManager.playCombatDialogFX(false);
                if (_callBack != null)
                    _callBack();
            }
            else
            {
                // Hit
                if (_dialogDefinition.effect.Count > 0)
                {
                    CombatManager.Instance.debugPanel.SubmitText("Playing Succesfull Dialogue FX");
                    CombatManager.Instance.robert.robertSoundManager.playCombatDialogFX(true);
                    triggerEffects();
                    //triggerState1
                }
                else
                {
                    CombatManager.Instance.debugPanel.SubmitText("Playing Unsuccesfull Dialogue FX (Dialogue Has no Associated Effects)");
                    CombatManager.Instance.robert.robertSoundManager.playCombatDialogFX(false);
                    CombatManager.Instance.Logger.SubmitText("Dialog Has No Effects");
                    if (_callBack != null)
                        _callBack();
                }
            }
        }
        else
        {
            CombatManager.Instance.debugPanel.SubmitText("Playing Unsuccesfull Dialogue FX");
            CombatManager.Instance.robert.robertSoundManager.playCombatDialogFX(false);
            CombatManager.Instance.Logger.SubmitText("Robert's attempt fails");
            if (_callBack != null)
                _callBack();
        }
        CombatManager.Instance.debugPanel.SubmitText("- Ending Dialog Formula: -");
        CombatManager.Instance.debugPanel.SubmitText(". . . . . . . . . . . . . . .");
    }


    private void triggerEffects()
    {
        // Play Effect Animation. (With CallBack) ???
        afterEffectAnimation(); // <--- Temp Call, should be as a callback from the animation.
    }

    private void afterEffectAnimation()
    {
        // Ask for affected Stats. (Effect 1, Effect 2) and apply penalties to stats.
        CombatManager.Instance.debugPanel.SubmitText("Dialogue Effects Count: " + _dialogDefinition.effect.Count);
        for (int i = 0; i < _dialogDefinition.effect.Count; i++)
        {
            string affectedStat = _dialogDefinition.effect[i].GetAffectedStatName(_dialogDefinition.effect[i].affectedStats);

            CombatManager.Instance.debugPanel.SubmitText("Dialogue N"+i+" Affected Stat: " + affectedStat);

            _talkingTarget.characterStats.getStat(affectedStat).addPenalty(
                new StatPenalty(
                    _dialogDefinition.effect[i].affectionValue,
                    _dialogDefinition.effectsDuration));
            CombatManager.Instance.debugPanel.SubmitText("Adding Penalty Value: " + _dialogDefinition.effect[i].affectionValue +
                                                        " By Duration: " + _dialogDefinition.effectsDuration);
        }

        _talkingTarget.ValidateAffectedStats();
        // At the end of effects and animations, show text.
        CombatManager.Instance.Logger.SubmitText(_dialogDefinition.consoleFeedback);

        // Finalize Robert's Turn
        if (_callBack != null)
            _callBack();
        
    }

    private void processDialogEffect(DialogStateEffect effect)
    {
        //if (effect.affectedStats == DialogStateEffect.AffectedStat.Acuracy)
            //_talkingTarget.
    }

    /*
    private void triggerState1()
    {
        BaseCharacter.AbnormalStates state = getTriggerState(_dialogDefinition.effect[0].effect);

        if (state != BaseCharacter.AbnormalStates.Undefined)
            _talkingTarget.triggerAbnormalState(state);
        else
            CombatManager.Instance.Logger.SubmitText("Nothing Happens");

        if (_dialogDefinition.effect.Count > 1)
        {
            Invoke("triggerState2", 0.5f);
        }
        else
        {
            if (_callBack != null)
                _callBack();
        }
    }

    private void triggerState2()
    {
        BaseCharacter.AbnormalStates state = getTriggerState(_dialogDefinition.effect[1].effect);

        if (state != BaseCharacter.AbnormalStates.Undefined)
            _talkingTarget.triggerAbnormalState(state);
        else
            CombatManager.Instance.Logger.SubmitText("Nothing Happens");

        if (_dialogDefinition.effect.Count > 2)
        {
            Invoke("triggerState3", 0.5f);
        }
        else
        {
            if (_callBack != null)
                _callBack();
        }
            
    }
    private void triggerState3()
    {
        BaseCharacter.AbnormalStates state = getTriggerState(_dialogDefinition.effect[2].effect);

        if (state != BaseCharacter.AbnormalStates.Undefined)
            _talkingTarget.triggerAbnormalState(state);
        else
            CombatManager.Instance.Logger.SubmitText("Nothing Happens");

        if (_callBack != null)
            _callBack();
    }
    
    private BaseCharacter.AbnormalStates getTriggerState(DialogStateEffect.DialogEffect effect)
    {
        if (effect == DialogStateEffect.DialogEffect.Become_Ally)
            return BaseCharacter.AbnormalStates.Traitor;
        //else if (effect == DialogStateEffect.DialogEffect.Confusion)
            //return AbnormalStatesManager.AbnormalState. // -> Baja Stats y armadura a 0.5 - menos los hitpoints
        else if (effect == DialogStateEffect.DialogEffect.Crazy)
            return BaseCharacter.AbnormalStates.Crazy;
        //else if (effect == DialogStateEffect.DialogEffect.Flee)
            //return AbnormalStatesManager.AbnormalState.Panic; // -- > Se va del combate (3 turnos para retirarse - se lleva su loot)
        //else if (effect == DialogStateEffect.DialogEffect.HostileToNeutral)
            //return bla
        else if (effect == DialogStateEffect.DialogEffect.Panic)
            return BaseCharacter.AbnormalStates.Panic;
        //else if (effect == DialogStateEffect.DialogEffect.Paralize)
            //return AbnormalStatesManager.AbnormalState.pa // Totalmente paralizado brazos y piernes, nuevo estado.
        else if (effect == DialogStateEffect.DialogEffect.Suicide)
            return BaseCharacter.AbnormalStates.Suicide;
        else if (effect == DialogStateEffect.DialogEffect.Surrender)
            return BaseCharacter.AbnormalStates.Surrender;
        else if (effect == DialogStateEffect.DialogEffect.Unconciouss)
            return BaseCharacter.AbnormalStates.Uncounciouss;

        return BaseCharacter.AbnormalStates.Undefined;
    }
    */
}
