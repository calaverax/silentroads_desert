﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class ProjectileManager : MonoBehaviour {

    public GameObject projectilePrefab;
    public Transform projectileOrigin;

    private GameObject projectile;
    private Transform _headTarget, _torsoTarget, _armsTarget, _legsTarget;

    public Transform headTarget { get { return _headTarget; } }
    public Transform torsoTarget { get { return _torsoTarget; } }
    public Transform armsTarget { get { return _armsTarget; } }
    public Transform legsTarget { get { return _legsTarget; } }

    private Transform _objetiveTarget;
    //private Rigidbody2D _rigidBody;
    private bool _isShooting = false;
    private float _speed = 3500;

    private Action _callBack;
    private Action _collidedBullet;

    void Start ()
    {
        // Get All Transforms for bodyparts
        _headTarget = transform.Find("ProjectileTransforms").Find("Proyectile To Head");
        _torsoTarget = transform.Find("ProjectileTransforms").Find("Proyectile To Torso");
        _armsTarget = transform.Find("ProjectileTransforms").Find("Proyectile To Arms");
        _legsTarget = transform.Find("ProjectileTransforms").Find("Proyectile To Legs");

        ProcessLevelObstacles();
	}

    void ProcessLevelObstacles()
    {
        GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Obstacles");

        for (int i = 0; i < obstacles.Length; i++)
        {
            obstacles[i].GetComponent<ObstacleChecker>().OnBulletCollision.AddListener(BulletCollisionEvent);
        }
    }

    void BulletCollisionEvent()
    {
        if (_isShooting)
        {
            CombatManager.Instance.Logger.SubmitText("Bullet has collided against an obstacle");
            CombatManager.Instance.NpcFXController.PlayMissFX();

            _isShooting = false;
            reinitiProjectile();

            if (_collidedBullet != null)
                _collidedBullet();
        }
    }

    public void shootTo(BaseCharacter target, BaseCharacter.BodyObjetive bodyObjetive, Action callBack = null, Action collidedBullet = null)
    {

        if (target == null)
            return;

        if (callBack != null)
            _callBack = callBack;

        if (collidedBullet != null)
            _collidedBullet = collidedBullet;

        projectile = Instantiate(projectilePrefab, projectileOrigin.position, projectileOrigin.rotation) as GameObject;
        projectile.transform.SetParent(this.gameObject.transform);
        projectile.transform.localScale = new Vector3(1, 1, 1);

        //_rigidBody = projectile.GetComponent<Rigidbody2D>();

        findTargetParts(target.gameObject, bodyObjetive);

        _isShooting = true;
    }

    private void findTargetParts(GameObject target, BaseCharacter.BodyObjetive bodyObjective)
    {
        ProjectileManager targetManager = target.GetComponent<ProjectileManager>();

        if (targetManager != null)
        {
            if (bodyObjective == BaseCharacter.BodyObjetive.HEAD)
                _objetiveTarget = targetManager.headTarget;
            else if (bodyObjective == BaseCharacter.BodyObjetive.TORSO)
                _objetiveTarget = targetManager.torsoTarget;
            else if (bodyObjective == BaseCharacter.BodyObjetive.ARMS)
                _objetiveTarget = targetManager.armsTarget;
            else if (bodyObjective == BaseCharacter.BodyObjetive.LEGS)
                _objetiveTarget = targetManager.legsTarget;
        }
    }

    private void MoveTowardsTarget()
    {
        if (_objetiveTarget == null) return;

        //the speed, in units per second, we want to move towards the target
        //move towards the center of the world (or where ever you like)
        Vector3 targetPosition = new Vector3(_objetiveTarget.position.x, _objetiveTarget.position.y, _objetiveTarget.position.z);

        Vector3 currentPosition = projectile.transform.position;
        //first, check to see if we're close enough to the target
        if (Vector3.Distance(currentPosition, targetPosition) > 100f)
        {
            Vector3 directionOfTravel = targetPosition - currentPosition;
            //now normalize the direction, since we only want the direction information
            directionOfTravel.Normalize();
            //scale the movement on each axis by the directionOfTravel vector components

            //_rigidBody.MovePosition(new Vector2((directionOfTravel.x * _speed * Time.deltaTime),(directionOfTravel.y * _speed * Time.deltaTime)));
            projectile.transform.Translate(
                (directionOfTravel.x * _speed * Time.deltaTime),
                (directionOfTravel.y * _speed * Time.deltaTime),
                (directionOfTravel.z * _speed * Time.deltaTime),
                Space.World);
        }
        else
        {
            _isShooting = false;
            reinitiProjectile();
            if (_callBack != null)
                _callBack();
        }
    }

    

    private void reinitiProjectile()
    {
        Destroy(projectile);
        //DestroyImmediate(projectile);
    }
    
    // Update is called once per frame
    void Update ()
    {
        if (_isShooting)
            MoveTowardsTarget();
	}


}
