﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.AI;

public class CharacterEvent : UnityEvent<BaseCharacter> { }

public class CombatManager : Singleton<CombatManager> {

    public List<List<BaseEnemy>> EnemiesGroups = new List<List<BaseEnemy>>();
    private Dictionary<BaseCharacter, float> registeredActors = new Dictionary<BaseCharacter, float>();
    private List<BaseCharacter> attackingPool = new List<BaseCharacter>();
    private List<BaseCharacter> _enemiesPool = new List<BaseCharacter>();
    private Robert _robert;
    private float _robertCooldown = 0;
    private bool _pausedCombat = true;
    private bool _attackInProgress = false;
    private BaseCharacter _characterAttacking;
    public bool robertIsMoving;
    public bool hasFollowers = false;
    public bool hasMarauders = false;
    //private NavMeshAgent robNav; //el nav agent de robert

    public GameObject robertObj; //El gameobject que contiene a Robert
    public NextLevelNavigations nextLevelNavigations;
   
    public float robertScale; //La escala de Robert con dos decimales
    public BaseEnemy LevelBoss;
    public bool levelSpawned = false;
    public bool soundFxPlaying = false;
    public bool pausedForSound = false;
    public bool pausedCombat { get { return _pausedCombat; } }
    public Robert robert { get { return _robert; } }
    public bool packLeaderIsAlive = false;
    public IngameGUIController GUIScript; //El script de la gui
    public int _turn = 1;
    public int _escturn = 1;
    private CombatLog _logger;
    private bool robertTurn; //PPP una booleana para saber si el turno de robert terminó.
    public bool NoCombatLevel;
    private ConfiguredDataLevel level;

    public CombatLog Logger
    {
        get {
            if (_logger == null)
            {
                Logger = GameObject.FindGameObjectWithTag("CombatLogger").GetComponent<CombatLog>();
            }
            return _logger;
        }
        set { _logger = value; }
    }

    NPCSFXControl _npcFXController;
    public NPCSFXControl NpcFXController
    {
        get
        {
            if (_npcFXController == null)
            {
                GameObject go = GameObject.FindWithTag("NPCSFXControl");

                if (go != null)
                    _npcFXController = go.GetComponent<NPCSFXControl>();

                if (_npcFXController != null)
                {
                    return _npcFXController;
                }
                else
                {
                    GameObject newObj = new GameObject("NPCSFX Control", typeof(NPCSFXControl));
                    newObj.tag = "NPCSFXControl";
                    _npcFXController = newObj.GetComponent<NPCSFXControl>();

                    return _npcFXController;
                }
            }
            else
            {
                return _npcFXController;
            }
        }

    }

    public UnityEvent OnCombatEnded;
    public UnityEvent OnCombatStarted;
    public UnityEvent OnCombatPaused;
    public UnityEvent OnCombatResumed;
    public UnityEvent OnRobertTurnEnded;
    public UnityEvent OnRobertDies;
    public UnityEvent OnMarauderAttacked;
    public UnityEvent OnRobertMoving;
    public UnityEvent OnRobertMoved;

    public UnityEvent OnFixedTurnEnded;

    public UnityEvent RobertCanEscape;

    public CharacterEvent OnCharacterDies;
    public CharacterEvent OnCharacterReachedNavigationPoint;
    


    private bool _combatStarted = false;
    public bool combatStarted { get { return _combatStarted; } set { _combatStarted = value; } }

    private bool _robertShooted = false;
    public bool robertShooted { get { return _robertShooted; } set { _robertShooted = value; } }

    private bool _combatEnded = false;
    public bool combatEnded { get { return _combatEnded; } }

    public bool precisionTargeting = true;
    //public float TIME_DELAY_FOR_TURNS = 1.5f;
    public bool COMBAT_FAST = false;

    public List<DialogDefinition> combatUsedDialogs = new List<DialogDefinition>();
    public List<FocusSystemController.FocusCategory> focusActiveBonus = new List<FocusSystemController.FocusCategory>();
    public List<string> pickedNames = new List<string>();
    /// <summary>
    /// 1.0 Perfect Vision
    /// 0.0 Complete Darkness
    /// </summary>
    public float visibility = 1.0f;

    DebugPanel _debugPanel;
    public DebugPanel debugPanel
    {
        get
        {
            if (_debugPanel == null)
            {
                _debugPanel = Resources.FindObjectsOfTypeAll<DebugPanel>()[0];
                //GameObject obj = GameObject.FindGameObjectWithTag("DebugPanel");
                //if (_debugPanel != null)
                  //  _debugPanel = obj.GetComponent<DebugPanel>();
            }
            
            return _debugPanel;
        }
        set { _debugPanel = value; } }

    public void Awake()
    {
        //_pausedCombat = false;
        Logger = GameObject.FindGameObjectWithTag("CombatLogger").GetComponent<CombatLog>();
        OnCombatEnded = new UnityEvent();
        RobertCanEscape = new UnityEvent();
        OnCombatStarted = new UnityEvent();
        OnRobertTurnEnded = new UnityEvent();
        OnCombatPaused = new UnityEvent();
        OnCombatResumed = new UnityEvent();
        OnRobertDies = new UnityEvent();
        OnCharacterDies = new CharacterEvent();
        OnMarauderAttacked = new UnityEvent();
        OnCharacterReachedNavigationPoint = new CharacterEvent();
        OnRobertMoving = new UnityEvent();
        OnRobertMoved = new UnityEvent();

        OnFixedTurnEnded = new UnityEvent();
        OnFixedTurnEnded.AddListener(FixedTurnEnded);


        CombatManager.Instance.OnRobertDies.AddListener(robertDies);
        GameObject obj = GameObject.FindGameObjectWithTag("DebugPanel");

        GUIScript = GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>();
        robertObj = GameObject.FindGameObjectWithTag("Player");
        //robNav = robert.GetComponent<NavMeshAgent>();

        //La escala de Robert sirve para calcular las distancias.
        robertScale = robertObj.transform.localScale.y;
        robertScale = Mathf.Round(robertScale * 100f) / 100f;
        nextLevelNavigations = GameObject.Find("New Levels Navigation Controllers").GetComponent<NextLevelNavigations>();

        /*
        if (obj != null)
            _debugPanel = obj.GetComponent<DebugPanel>();

        if (_debugPanel != null)
            _debugPanel.SubmitText("Debug Panel Console Found... Submiting Test Text");
            */

        //Chequeá si en el nivel se combate
        level = GameObject.FindGameObjectWithTag("Level").GetComponent<ConfiguredDataLevel>();
        //if (LevelHistory.Instance.CheckIfLevelWasWon()) { NoCombatLevel = true; }
        if (level.NoCombatLevel == true)
        {
            NoCombatLevel = true;
            nextLevelNavigations.CombatEnded();
        }
        _currentTurn = LevelHistory.Instance.GetTurn();
        
    }





    private void robertDies()
    {
        _pausedCombat = true;
        registeredActors.Clear();
        attackingPool.Clear();
        _robert = null;

    }

    public void Register(Robert robert)
    {
        _robert = robert;
        _robertCooldown = _robert.reactionTime;
        //_robert.DoAction();
        debugPanel.SubmitText("Registered Combatant: Robert");
    }

    public void ReinitRobertCD()
    {
        _robertCooldown = _robert.reactionTime;
        OnRobertTurnEnded.Invoke();

        debugPanel.SubmitText("Robert Action Time Restarting");
    }

    public void Register(BaseCharacter character)
    {
        if (!(registeredActors.ContainsKey(character)))
            registeredActors.Add(character, character.reactionTime);

        if (!(_enemiesPool.Contains(character)))
        {
            _enemiesPool.Add(character);

            _enemiesPool = _enemiesPool.OrderBy(o => o.characterStats.Agility).ToList();
        }

        debugPanel.SubmitText("Registered Combatant: " + character.characterName);
    }

    public void FinalizeAttack()
    {
        _attackInProgress = false;
        debugPanel.SubmitText("Finalizing Attack");
    }
        
    public void UnRegister(BaseCharacter character)
    {
        if ((_characterAttacking != null) && (_characterAttacking == character))
        {
            //_attackInProgress = false;
            _characterAttacking = null;
        }
        if (_enemiesPool.Contains(character))
        {
            _enemiesPool.Remove(character);
            _enemiesPool = _enemiesPool.OrderBy(o => o.characterStats.Agility).ToList();
            _currentTurn--;
            if (_currentTurn < -1)
                _currentTurn = -1;
            LevelHistory.Instance.SetTurn(_currentTurn);
        }

        if (registeredActors.ContainsKey(character))
            registeredActors.Remove(character);

        debugPanel.SubmitText("Unregistering Combatant: " + character.characterName);
    }

    public BaseCharacter getRandomEnemy()
    {
        if (registeredActors.Count > 0)
            return registeredActors.ElementAt(Random.Range(0, registeredActors.Count)).Key;
        else
            return null;
    }

    public void PauseCombat()
    {
        if (NoCombatLevel) { return; }
        _pausedCombat = true;
        OnCombatPaused.Invoke();
        Logger.SubmitText("<color=#DDD063FF>Combat Paused (\n) ┌∩┐(◣_◢)┌∩┐</color>");
        debugPanel.SubmitText("Combat Paused");
    }

    public void ResumeCombat()
    {
        if (NoCombatLevel) { return; }
        _pausedCombat = false;
        OnCombatResumed.Invoke();
        Logger.SubmitText("<color=#DDD063FF>Combat Resumed (\n) (⌐■_■)–︻╦╤─ –  (╥﹏╥)</color>");
        debugPanel.SubmitText("Combat Resumed");
    }


    public void ReinitActor(BaseCharacter character)
    {
        _characterAttacking = null;

        if (!_newSpawnerLevel)
        {
            if (registeredActors.ContainsKey(character))
                registeredActors[character] = character.reactionTime;

        }
        else
        {
            character.cooldown = character.reactionTime;
        }

        debugPanel.SubmitText("Restarting Actor: " +character.characterName );
    }

    private void sayFirstAudio()
    {
        if (!(GameManager.Instance.isLevelWon(GameManager.Instance.selectedLevelId)))
            if (packLeaderIsAlive)
                EventManager.TriggerEvent("OnCombatStarted");
    }

    bool combatStartedShooted = false;

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.J)) {
            foreach (BaseEnemy enemigo in _enemiesPool) {
                enemigo.characterStats.Hitpoints = 1;
                Debug.Log("J apretada");
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
            GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().PauseCombat();
        if (_pausedCombat)
        { GUIScript.pausedButton.SetActive(true); }
        else
        { GUIScript.pausedButton.SetActive(false); }
    }

    private bool _newSpawnerLevel = false;
    int _enemiesAlive = 0;
    public bool newSpawnerLevel { get { return _newSpawnerLevel; } set { _newSpawnerLevel = value; } }

    public void ForceDebugEndCombat()
    {
        _pausedCombat = true;
        OnCombatEnded.Invoke();
    }

    void UpdateNewSpawner()
    {
        if (!_pausedCombat)
        {
            #region CombatBreakConditions
            // Break Combat conditions: 
            // - Robert is Null, or Robert is Dead.
            if ((_robert == null) || ((_robert != null) && (!_robert.isAlive)))
            {
                _pausedCombat = true;
                OnCombatEnded.Invoke();
                _npcFXController.PlayEventSound("EndBattle");
                debugPanel.SubmitText("Combat Ended");
                return;
            }
            // No enemies Alive:
            _enemiesAlive = 0;
            for (int i = 0; i < EnemiesGroups.Count; i++)
            {
                _enemiesAlive += EnemiesGroups[i].Count();
            }
            if (_enemiesAlive == 0)
            {
                //audio.combatEnded
                if (!_combatEnded)
                {
                    NpcFXController.PlayEventSound("EndBattle");
                    OnCombatEnded.Invoke();
                }

                _combatEnded = true;
                Debug.Log("Combat Ended");
            }
            #endregion
            // Main Combat Update Loop
            #region MainCombatUpdateLoop
            //---------------------------------------------------------
            // Manage Robert Action Cooldown
            if (_robert.cooldown > 0)
                _robert.cooldown -= Time.fixedDeltaTime;
            else //if (_robert.cooldown <= 0)
            {
                if (!_robert.canDoAction)
                    _robert.DoAction();
            }
            //---------------------------------------------------------
            // Update Reaction Times for each registeredActors
            if ((!_robert.isActing) && (!_attackInProgress))
            {
                for (int i = 0; i < EnemiesGroups.Count; i++)
                {
                    for (int x = 0; x < EnemiesGroups[i].Count; x++)
                    {
                        if (EnemiesGroups[i][x].cooldown > 0)
                        {
                            EnemiesGroups[i][x].cooldown -= Time.fixedDeltaTime;
                        }
                        else
                        {
                            if (!attackingPool.Contains(EnemiesGroups[i][x]))
                                attackingPool.Add(EnemiesGroups[i][x]);
                        }
                    }
                }
            }
            //---------------------------------------------------------
            // If Attacking Pool have at least 1 actor -> Shoots its event and remove it from the pool.
            if ((!_attackInProgress) && (!robert.isActing))
            {
                if (attackingPool.Count > 0)
                {
					if (attackingPool[0] != null)
					{
						_attackInProgress = true;
						_characterAttacking = attackingPool[0];
						attackingPool[0].DoAction();
					}

                    if ((attackingPool != null) && (attackingPool.Count > 0))
                        attackingPool.RemoveAt(0);
                }
            }

            #endregion
        }
    }

    void FixedUpdate()
    {
        
        #region FixedTurnUpdateLoop
        if (!_pausedCombat)
        {
            #region CombatBreakConditions
            // Break Combat conditions: 
            // - Robert is Null, or Robert is Dead.
            if ((_robert == null) || ((_robert != null) && (!_robert.isAlive)))
            {
                _pausedCombat = true;
                OnCombatEnded.Invoke();
                _npcFXController.PlayEventSound("EndBattle");
                debugPanel.SubmitText("Combat Ended");
                return;
                
            }

            // No enemies Alive:
            if (_enemiesPool.Count <= 0)
            {
                if (!_combatEnded)
                {
                    NpcFXController.PlayEventSound("EndBattle");
                    OnCombatEnded.Invoke();
                    //Limpiá el actionPool;
                    GameObject.FindGameObjectWithTag("IngameGUIController").transform.GetChild(2).GetComponent<ActionPoolManager>().CleanPool();
                    //PauseCombat();

                }
                _combatEnded = true;
                robert.ActivarMarca(true);
            }
            #endregion
            // si el combate NO termino, Y no hay un turno en progreso
            if ((combatEnded) || (fixedTurnInProgress)) return;
            //if ((!combatEnded) && (!fixedTurnInProgress))
            UpdateFixedTurns();
        }
        return;
        #endregion

        
        // Previous Combat Loop.
        if (_newSpawnerLevel)
        {
            UpdateNewSpawner();
            return;
        }


        if (!levelSpawned) return;

        if (!_pausedCombat)
        {
            if (_robert == null)
            {
                _pausedCombat = true;
                OnCombatEnded.Invoke();
                _npcFXController.PlayEventSound("EndBattle");
                debugPanel.SubmitText("Combat Ended");
                return;
            }

            //_combatStarted = true;

            if (!combatStartedShooted)
            {
                //IngameBGMController audio = GameObject.FindGameObjectWithTag("LevelBGMController").GetComponent<IngameBGMController>();
                //OnCombatStarted.AddListener(audio.combatStarted);
                OnCombatStarted.Invoke();
                combatStartedShooted = true;
                Invoke("sayFirstAudio", 2);
                debugPanel.SubmitText("Combat Started");
                //EventManager.TriggerEvent("OnCombatStarted");

            }

            if (((!_combatStarted) || (_combatEnded)) && (registeredActors.Count > 0))
                return;

            if (registeredActors.Count == 0)
            {
                //IngameBGMController audio = GameObject.FindGameObjectWithTag("LevelBGMController").GetComponent<IngameBGMController>();
                //audio.combatEnded
                if (!_combatEnded)
                {
                    NpcFXController.PlayEventSound("EndBattle");
                    OnCombatEnded.Invoke();
                }

                _combatEnded = true;

                // if (robert.isAlive)
                //   GameManager.Instance.levelManager.processLoot();
                
                Debug.Log("Combat Ended");
            }

            //---------------------------------------------------------
            // Manage Robert Action Cooldown
            if (_robertCooldown > 0)
                _robertCooldown -= Time.fixedDeltaTime;

            if (_robertCooldown <= 0)
            {
                if (!_robert.canDoAction)
                    _robert.DoAction();
            }

            //---------------------------------------------------------
            //if (robertShooted)
            {
                // Update Reaction Times for each registeredActors
                if ((!_robert.isActing) && (!_attackInProgress))
                {
                    foreach (BaseCharacter key in registeredActors.Keys.ToList())
                    {
                        if (registeredActors[key] > 0)
                            registeredActors[key] -= Time.fixedDeltaTime;

                        // If Any Time Gets To 0, Add it to the Atacking Pool.
                        if (registeredActors[key] <= 0)
                        {
                            if (!attackingPool.Contains(key))
                                attackingPool.Add(key);
                        }
                        //Debug.Log("Base Character: " + key.name + " Reaction Time: " + registeredActors[key]);
                    }
                }
                //---------------------------------------------------------
                // If Attacking Pool have at least 1 actor -> Shoots its event and remove it from the pool.
                if ((!_attackInProgress) && (!robert.isActing))
                {
                    if (attackingPool.Count > 0)
                    {
                        _attackInProgress = true;
                        _characterAttacking = attackingPool[0];
                        attackingPool[0].DoAction();

                        if ((attackingPool != null) && (attackingPool.Count > 0))
                            attackingPool.RemoveAt(0);
                    }
                }
            }
        }
    }

    public string getFocusElement()
    {
        return focusActiveBonus.ElementAt(0).ToString();
    }
    
    int _currentTurn = -1;
	public bool fixedTurnInProgress = false;

    void FixedTurnEnded()
    {
        fixedTurnInProgress = false;

        _currentTurn++;
        if (_currentTurn >= _enemiesPool.Count)
        {
            _currentTurn = -1;
            _turn++;
            Debug.Log(focusActiveBonus.ElementAt(0).ToString());
            if (focusActiveBonus.ElementAt(0).ToString() == "Escape")
            {
                Debug.Log(_escturn);
                _escturn++;
                if(_escturn == 3 || _turn == 10)
                {
                    Debug.Log("ESCAPE");
                    RobertCanEscape.Invoke();
                }
            }
            else
            {
                if (_turn == 10)
                {
                    RobertCanEscape.Invoke();
                }
                if(focusActiveBonus.ElementAt(0).ToString() == "Heal")
                {
                    robert.wonHP(10);
                }
            }
        }
        LevelHistory.Instance.SetTurn(_currentTurn);
            
            
    }
    
    void UpdateFixedTurns()
    {
        //Debug.Log("TURNO ACTUAL = " + _turn);
        // Always Update RobertFirst, Then Enemies in List.
        if (fixedTurnInProgress)
			return;
		
        if (_currentTurn == -1)
        {
            robertTurn = true;
            //RobertTurn
            robert.ActivarMarca(true);
            //fixedTurnInProgress = true;
            //robert.FixedTurnStarted();

            if (robert.GetActionsPool() == 0)
            {
                Logger.SubmitText("<color=white>Robert turn started, waiting instructions..</color>");
                PauseCombat();

            }
            else
            {
                fixedTurnInProgress = true;
                robert.FixedTurnStarted();
            }
            
        }
        else
        {
            //esta bool sirve para saber si terminó el turno de robert e invocar el evento
            if (robertTurn){
                //Debug.Log("se terminó el turno de robert");
                OnRobertTurnEnded.Invoke(); 
                robertTurn = false;}

            robert.ActivarMarca(false);
            // EnemiesTurn
            if (_currentTurn < _enemiesPool.Count)
            {
                if (_enemiesPool[_currentTurn] != null)
                {
                    fixedTurnInProgress = true;
                    _enemiesPool[_currentTurn].FixedTurnStarted();
                }
                else
                {
                    FixedTurnEnded();
                }
            }
            else
            {
                FixedTurnEnded();
            }
        }
    }


    public void attackSelected(BaseCharacter target, BaseCharacter.BodyObjetive objetive)
    {
        if (_robert.isAlive)
            _robert.attackOnTarget(target, objetive);
    }


     public void Escala(){
         //escalaRobert
     }

    //Estoy prácticamente seguro que esta función ya no está en uso; servía para rehacer los indíces de la persistencia cuando matabas a un enemigo. Actualmente lo hace el enemy history in situ. PPP
     public void UpdateIndexOfEnemies(SerializableEnemy enemigo) {
         foreach (BaseEnemy enemy in _enemiesPool) {
             if (enemigo.index < enemy.serializableEnemy.index) {
                 enemy.serializableEnemy.index--;
             }
         }
     }

     public int NumEnemigos() {
         return _enemiesPool.Count;
     }
public void Escape(){
                if (!_combatEnded)
            {
                NpcFXController.PlayEventSound("EndBattle");
                OnCombatEnded.Invoke();
            }
            _combatEnded = true;
            robert.ActivarMarca(true);
}
    

}
