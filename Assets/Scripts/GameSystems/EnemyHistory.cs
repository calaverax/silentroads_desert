﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyHistory : Singleton<EnemyHistory>
{
    public EnemyDB _edb;
    public string previousLevelID = null; //El nivel previo. No se serializa porque lo actualizan en base al level History.
    public int index; //El index sirve para eliminar a los personajes de una lista
    public int ff; //ff es un número para actualizar el número de los personajes cuando un elemento fue eliminado de la lista.



    //Cuando arranca el nivel (función principal).
    public void Begin()
    {


        ff = 0;
        LoadDB();//Cargá los datos


        if (!IsLevelWithEnemies(SceneManager.GetActiveScene().name ))//Fijate si tenés info almacenada del nivel
        {
            //Si no tenés, creá una nueva lista, guardá el nivel y guardá los datos
            Debug.Log("EH -> Es nuevo nivel");
            LevelEnemies nuevoNivel = new LevelEnemies(SceneManager.GetActiveScene().name );
            _edb.level_list.Add(nuevoNivel);
            SaveDB();
        }
        else
        {
            //Si ya lo tenés, marcalo
            Debug.Log("EH ->  NO es nuevo nivel");
            _edb.level_list.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).MarkFalse();
            

        }

        UdpateFollow();
        UdpateMarauder();


        //Guardá los datos
        SaveDB();

    }


    //Esta parece obsoleta actualmente, se llama cuando termina el combate, la dejo porque puede servir.
    public void Fin() {
        /*
        LevelSpawnGroup[] GroupsSpawners;
        //previousLevelID = SceneManager.GetActiveScene().name ;
        foreach (SerializableEnemy enemigo in _edb.level_list.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name).enemiesPersisted) {
            enemigo.spawngroup = 3;
        }*/
        SaveDB();
    }



    //Si el nivel no tiene combate, hacé esto...
    public void LevelsSinCombate()
    {
        LoadDB();
        UdpateFollow();
        UdpateMarauder();
        DosNivelesAtras();
        SaveDB();
    }


    //Sirve para los followers. Se fija si estás volviendo sobre tus pasos.
    public void DosNivelesAtras() {


        //Actulamente usamos -3, pero se puede cambiar el número si querés mire más atrás.
        index = LevelHistory.Instance._levels.level_list.Count - 3;

        if (index >= 0)
        {
        previousLevelID = LevelHistory.Instance._levels.level_list[index];
             }
        
    }

    //Sirve para ver un reporte de lo generado, actualmente sin uso.
    public void Display()
    {
        foreach (var a in _edb.level_list)
        {
            Debug.Log("EH -> El nivel ID con enemigos es  : " + a.levelId);
            foreach (var b in a.enemiesPersisted)
            {
                Debug.Log("EH -> Display() -> Nombre : " + b.characterName);
                Debug.Log("EH -> Display() -> Prefab : " + b.sprefab);
                Debug.Log("EH -> Display() -> Imagen : " + b.spriteName);

            }
        }
    }

    //Devuelve la cantidad de enemigos en el nivel.
    public int EnemiesInThisLevel()
    {
        int cantidad = 0;
        cantidad = _edb.level_list.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).enemiesPersisted.Count;
        //Debug.Log("EH -> Enemigos persistidos con vida en este nivel : " + cantidad);
        return cantidad;
    }

    //Agregar un enemigo a la lista persistente.
    public void AddEnemy(BaseEnemy enemigo,string imagen, int i , int a, string pref)
    {

        LoadDB();
        //Fijate si no los cargaste ya en la lista persistente
        if (_edb.level_list.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).firstCharge)
        {
            SerializableEnemy enemigoS = new SerializableEnemy(enemigo, imagen, i, a, pref);
            enemigoS.index = _edb.level_list.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).enemiesPersisted.Count;
            Debug.Log("AddEnemy, enemigos.index en base al persistence count ^^^^^^^^^^^^^^^^^^^^" + enemigoS.index);
            _edb.level_list.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).enemiesPersisted.Add(enemigoS);
            SaveDB();

        }
    }

    public void UpdateEnemy(SerializableEnemy enemigo)
    {
        List<SerializableEnemy> lista = _edb.level_list.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name).enemiesPersisted;
        foreach (SerializableEnemy enemy in lista)
        {
            if (enemigo.characterName == enemy.characterName && enemigo.spriteName == enemy.spriteName)
            { enemy.characterStats = enemigo.characterStats; }
        }
        SaveDB();
    }

    //Eliminar un enemigo de la lista
    public void KillEnemy(SerializableEnemy enemigo)
    {
        List<SerializableEnemy> lista = _edb.level_list.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).enemiesPersisted;
        int nuevoIndex = 60;

        if (lista != null) { Debug.Log(enemigo.index + " = index / Lista.Count = " + lista.Count); }
        else { Debug.Log("lista es null"); Debug.Log("Scene Name" + SceneManager.GetActiveScene().name); }

        //Esto cambia el índice de la versión persistente
        foreach (SerializableEnemy enemy in lista)
        {

            Debug.Log("Al index = " + enemy.index + ".");
            if (enemigo.characterName == enemy.characterName && enemigo.spriteName == enemy.spriteName)
            { nuevoIndex = enemy.index; }
            if (enemy.index > nuevoIndex)
            //if (enemy.index > enemigo.index)
            {
                Debug.Log("está restándole al índice^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ " + enemy.index);
                enemy.index--;
                Debug.Log("después de restarle el índice es = " + enemy.index +" / Nombre = " + enemy.characterName);
                SaveDB();
                
            }
            Debug.Log("después del save el índice es = " + enemy.index + " / Nombre = " + enemy.characterName);
            
            
        }
        //SaveDB();
        //CombatManager.Instance.UpdateIndexOfEnemies(enemigo); //La función que hay en el combat manager cambia el índice de la instancia.
        if (lista.Count <= nuevoIndex) { lista.RemoveAt(0); Debug.Log("falló el remove"); }
        else { lista.RemoveAt(nuevoIndex); }
        
        //lista.RemoveAt(enemigo.index);
        Debug.Log("Al remover, lista.Count = " + lista.Count);
        SaveDB();



        
    }






    //Cuántos enemigos persistieron en este nivel? //PPP: ojo, la misma función está arriba, por lo que habría que usar una sóla. Actualmente uso esta.
    public int NumEnemigosPersistidos()
    {
        if (IsLevelWithEnemies(SceneManager.GetActiveScene().name ))
        {
            return _edb.level_list.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name ).enemiesPersisted.Count;
        }
        else return 0;
    }


    //Tenemos persistencia almecenada para este nivel?
    public bool IsLevelWithEnemies(string levelName)
    {
        LoadDB();
        bool result = _edb.level_list.Exists(x => x.GetLevelID() == levelName);
        return result;
    }

    //Cargá la info persisente
    public void LoadDB()
    {
        _edb = Serializer.LoadSerializableObjectFromFile<EnemyDB>(Application.persistentDataPath + "/" + "enemy.persistence");
        if (_edb == null)
        {
            _edb = new EnemyDB();
        }
    }

    //Reseteá la info persistente
    public void EmptyHistory()
    {
        _edb = new EnemyDB();
        SaveDB();
    }

    //Guardá los cambios
    public void SaveDB()
    {
        Serializer.SaveSerializableObjectToFile<EnemyDB>(_edb, Application.persistentDataPath + "/" + "enemy.persistence");
    }    





  
    //////////////////////////////////////////////////////////////////////////////////////////////////
    
    //FOLLOWERS

    //Si este nivel es igual al nivel del que venías, los followers te alcanzan (actualmente sin uso).
    public void UdpateFollow() { 
        
    
    }

    //Se fija quienes te siguen y los marca para ser agregados a la lista de followers
    public int MarkFollowers() {
        //En level navigationButton habría que fijarse si el target level es distinto al level del ´que vinimos. Si lo es, no correr esta función (porque retroceder no produce followers).
        List<SerializableEnemy> lista = _edb.level_list.Find(x => x.GetLevelID() == SceneManager.GetActiveScene().name).enemiesPersisted;

        int followersCount = 0;

        int incialFollowersNumber = _edb.followers_list.Count;

        for (var i = 0; i < lista.Count; i++)
        {
            if (true) //Chances de que haya followers
            {

                lista[i].following = true;
                if (_edb.followers_list == null) {
                    _edb.followers_list = new List<SerializableEnemy>();
                }
                _edb.followers_list.Add(lista[i]);

                followersCount++;
            }
        }

        ff = 0;
         int remover = lista.Count;
         for (var i = remover; i > 0; i--)
         {
             int indice = _edb.followers_list.Count-1 -ff;
             //Este es para quitar a los followers de la lista del nivel.
             lista.RemoveAt(_edb.followers_list[indice].index);
             //Este es para ponerles un nuevo índice en base a la posición que ocupan ahora.
             _edb.followers_list[indice].index = indice;

             //_edb.followers_list[incialFollowersNumber + ff].index = incialFollowersNumber + ff;
             ff++;
          }

         //acá termina lo que agregué
        //Lo reemplacé por este otro código mío:
        /*
        foreach (SerializableEnemy enemigo in _edb.followers_list) {
            lista.RemoveAt(enemigo.index - ff);
            ff++; //Este sirve para corregir el índice (una vez que sacaste a alguien todos cambian el índice).   
        }*/
        SaveDB();
        return followersCount;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //MARAUDERS

    //Después de que un fondo fue abandonado por 3 turnos, en cada turno hay una apropiación del loot x parte de los scavengers.
    public void UdpateMarauder()
    {
        foreach (LevelEnemies list in _edb.level_list)
        {
            //Funciona pero no corre begin para los niveles que no tienen combate.
            if (list.enemiesPersisted.Count == 0) {
                if (list.abandono < 6) {
                    list.abandono++;
                    if (SceneManager.GetActiveScene().name  == list.GetLevelID()) 
                    {
                        list.abandono = 0;
                    }
                    if (list.abandono == 3) {
                        PonerMarauders(list);
                    }
                    if (list.abandono > 3) {
                        LootPersistenceManager.Instance.ScavengeLoot(list.levelId);
                        //string p = list.enemiesPersisted[0].characterName;
                    }



                }
                SaveDB();
                
                
            
            }
        }
    }

    //Coloca a los Scavengers en la escena que fue abandonada por 3 turnos. Sin escribir;
    public void PonerMarauders(LevelEnemies list) { 
        
        Debug.Log("Acá debiera poner marauders, el script no está escrito");
        SerializableEnemy scavenger; 
        scavenger = new SerializableEnemy(new BaseEnemy(), "19", 0, 0, "MaleScavenger");
        scavenger.index = 0;
        scavenger.scavenger = true;
        scavenger.characterName = "Blix";
        list.enemiesPersisted.Add(scavenger);
        
        //script.init();
        //script.configureCharacter(false, gender.ToLower() == "male" ? BaseEnemy.Genre.MALE : BaseEnemy.Genre.FEMALE);
        /*
        scavenger = new SerializableEnemy(new BaseEnemy(), "20", 1, 0, "MaleScavenger");
        scavenger.index = 1;
        scavenger.scavenger = true;
        scavenger.characterName = "Pluton";
        list.enemiesPersisted.Add(scavenger);
        */
    }

}




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////777

//OTRAS CLASES QUE SE USAN CON EL ENEMY HISTORY




[System.Serializable]
//ENEMY DB
//Serializa la lista del nivel que guarda la lista de nivels con enemigos persistidos y los followers
public class EnemyDB
{
    public EnemyDB()
    {
        level_list = new List<LevelEnemies>();
        followers_list = new List<SerializableEnemy>();
    }
    public List<LevelEnemies> level_list;
    public List<SerializableEnemy> followers_list;
}




[System.Serializable]
//LEVEL ENEMIES
//Para cada nivel serializa la lista de enemigos.

public class LevelEnemies
{
    public int abandono; //Sirve para los scavengers.
    public string levelId; //El nivel del que guarda la persistencia
    public List<SerializableEnemy> enemiesPersisted; //La lista de enemigos persistidos
    public bool firstCharge; //Se fija si ya cargó los antes o no.

    //El constructor
    public LevelEnemies(string lvlID)
    {
        levelId = lvlID;
        enemiesPersisted = new List<SerializableEnemy>();
        firstCharge = true;
    }

    //Te dice en qué nivel estás.
    public string GetLevelID()
    {
        return this.levelId;
    }

    //Si ya cargó a los enemigos vuelve first charge false.
    public void MarkFalse()
    {
        this.firstCharge = false;
    }

    //Te pasa la lista de enemigos.
    public List<SerializableEnemy> GetEnemies()
    {
        return enemiesPersisted;
    }
}

