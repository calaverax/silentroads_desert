﻿#pragma strict

/*Lo que hace este script es que una textura tileable 
	se mueva horizontalmente una y otra vez hasta el infinito. 
	El truco está en asignar la textura a un material, ponersela 
	a un Quad que creás en la escena y desplazar los UVs*/

function Start () {
		var speed : float = 0.01f; //La variable que define que tan rapido se mueve la textura
}

function Update () {
		GetComponent.<Renderer>().material.mainTextureOffset = new Vector2 ((Time.time * 0.01f)%1, 0f);
		/*El %1 hace un clamp al valor para que no reviente y se salga fuera de control. 
		También tené en cuenta que para que el script ande el modo de textura 
		tiene que estar en repeat.*/
	}