﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tarjeta_Cerrar : MonoBehaviour
{
    private Ray ray;

    // Use this for initialization
    void Start()
    {
    }



    // Update is called once per frame
    void Update()
    {

        /*
if (Input.GetMouseButtonDown(1) && gameObject.activeSelf)
{ this.gameObject.SetActive(false); } */

        if (Input.GetMouseButtonDown(0) && gameObject.activeSelf)
        {//Cuando el jugador apirete el mouse,

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);//Tirá un raycast
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.collider.tag == "Tarjeta" || hit.collider.tag == "GUI") { } //Si pega en la tarjeta o en la gui no hagas nada.
                else { gameObject.SetActive(false); }//Pero si pegó en otro lado, desactivá la tarjeta
            }

        }
    }
}

