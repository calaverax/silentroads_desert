﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ScenesLoader : Singleton<ScenesLoader>
{
    private AsyncOperation loadScene;
    private string _sceneName = "";
    public string sceneName { get { return _sceneName; } }

    public void LoadScene(string sceneName, bool destoyCombatmanager = true, bool destroyGamemanager = true, bool needsLoadingScreen = false)
    {

        //GameManager.Instance.ingameBgmController.stopCombatWonAudio();

        if (destoyCombatmanager)
            DestroyImmediate(CombatManager.Instance.gameObject);

        if (destroyGamemanager)
            DestroyImmediate(GameManager.Instance.gameObject);


        //TODO: Add SceneLoadAsync And Preload LoadingScene before Loading next Scene.
        _sceneName = sceneName;
        //Application.LoadLevel("LoadingScreen");
        //Application.LoadLevel(sceneName);
        SceneManager.LoadScene("LoadingScreen");
        //SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }


    

}
