﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods {




    //Este extension méthod para los diccionarios pretende fijarse si el valor en cuestión ya está y si no está lo agrega.
    public static void DiccionarioAgregar(this Dictionary<string, GameObject> diccionario, string key, GameObject objeto)
    {
            if (!diccionario.ContainsKey(key)) 
            {
                diccionario.Add(key, objeto);
            }
    }


    //Un staticMetod para remapear números.
    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
    
        


}
