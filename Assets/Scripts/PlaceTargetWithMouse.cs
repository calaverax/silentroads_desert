using System;
using UnityEngine;
using UnityEngine.EventSystems;




namespace UnityStandardAssets.SceneUtils
{
    public class PlaceTargetWithMouse : MonoBehaviour
    {
        public Transform robert;
        public float surfaceOffset = 1.5f; //sirve para que el collider no se trabe con el piso
        public GameObject setTargetOn;//Esta es la variable que va a servir para mandarle la info de la posicion
        public Collider nav;
        
        private GameObject limiteMovimiento;
        private SphereCollider limMovCol;
        private Bounds bounds;
        private bool NoCombatLevel;
        //public LayerMask layerMask;



        void Start() {
            
            robert = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

                limiteMovimiento = new GameObject("limiteMovimiento");
                limiteMovimiento.layer = 2;
                limMovCol = limiteMovimiento.AddComponent<SphereCollider>() as SphereCollider;
                limMovCol.radius = 50;
                limMovCol.isTrigger = true;
                limiteMovimiento.transform.position = robert.position;


            CombatManager.Instance.OnRobertTurnEnded.AddListener(MoveLimitRegion);
            //CombatManager.Instance.OnRobertTurnEnded.AddListener(() => MoveLimitRegion());
                            
            nav = GameObject.FindGameObjectWithTag("Nav").GetComponent<Collider>();
            if (nav == null) { Debug.Log("Ten�s que poner el tag Nav al navMesh para que reconozca los hits");}

            if (CombatManager.Instance.NoCombatLevel)
            {
                limMovCol.radius = 500;
            }
            

        }

        /*
        void OnDisable(){
            if (CombatManager.Instance != null) {
            CombatManager.Instance.OnFixedTurnEnded.RemoveListener(MoveLimitRegion); //esta tira errores curiosos
            }
        }
        */


        
        private void Update()
        {          
            if (Input.GetMouseButtonDown(0))
            {
                if (EventSystem.current.IsPointerOverGameObject()) { return; }
                //if (CombatManager.Instance.pausedCombat)
                //    return;

                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); //Tiras un rayo de la camara al lugar en que clickeo
                    RaycastHit hit; //variable que almacena el impacto
                    //if (!Physics.Raycast(ray, out hit, layerMask)) //Si no pega en nada, no hagas nada.
                    if (!Physics.Raycast(ray, out hit)) //Si no pega en nada, no hagas nada.
                    {
                        Debug.Log("Guarda: el hit del mouse no peg� en nada; capaz tengas el nav en el layer IgnoreRaycast");
                        return;
                    }

                    //Si pega, haces que la posicion del objeto sea igual a la posicion en que pego, 
                    //mas el offset hacia arriba del piso (que sirve para que el collider no se trabe o se caiga).
                    
                Debug.Log("peg� en " + hit.collider.gameObject);

                if (hit.collider.gameObject.tag == "Vendor") {
                    VendorNPC vendorNPC = hit.collider.transform.parent.GetComponent<VendorNPC>();
                    vendorNPC.OpenShop();
                }
                    bounds = limMovCol.bounds;

                    if (hit.collider == nav && bounds.Contains(hit.point)){                  
                    transform.position = hit.point + hit.normal * surfaceOffset; 
                    }

                    if (setTargetOn != null)
                    {
                        /*El metodo SendMessage va a mandar esta info (la del transform) a cada script que haya en el gameobject.
                        La funcion que hace correr esta en el script "AIThirdPersonControl", la funcion utiliza el transform que le
                        mandamos como parametro*/
                        setTargetOn.SendMessage("SetTarget", transform);
                    }
            }
        }

        //Esta funci�n se activa desde el combat manager cuando termina el turno de robert. Actualiza el rango en que te pod�s mover.
        public void MoveLimitRegion() { 
            limiteMovimiento.transform.position = robert.position;
            //Debug.Log("Regi�n movida");
        }

    }
}
