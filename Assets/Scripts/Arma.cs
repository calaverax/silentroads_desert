﻿using UnityEngine;
using System.Collections;

public class Arma : MonoBehaviour {
	
	
	public string miBooleana = "Disparo";
	public string nombreAnimacion = "Tiro";
	public string nombreTecla = "space";
	public GameObject armaX;
	private Animator anim;
	
	
	void Start()
	{
		anim = GetComponent<Animator>();
	}
	
	void Update ()
	{
		
		if (Input.GetKeyDown(nombreTecla))
		{
			anim.SetBool(miBooleana, true);
			armaX.SetActive(true);
			
		}
		
		if (anim.GetCurrentAnimatorStateInfo(1).IsName(nombreAnimacion) && anim.GetCurrentAnimatorStateInfo(1).normalizedTime > 1 && !anim.IsInTransition(1))
		{anim.SetBool (miBooleana, false);armaX.SetActive(false);}
		
		
		/*if (anim.GetCurrentAnimatorStateInfo(0).IsName("tiro"))
			{
				anim.SetBool("disparo",false);
				Debug.Log ("Listo");
			}
			*/
		
	}
}	