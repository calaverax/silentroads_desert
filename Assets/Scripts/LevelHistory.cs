﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelHistory : Singleton<LevelHistory>
{
    public Level_h _levels;
    public Level_won _levelsWon;
    // Use this for initialization
    public void AddLevel() {

        _levels = Serializer.LoadSerializableObjectFromFile<Level_h>(Application.persistentDataPath + "/" + "levels.persistence");
        if (_levels == null)
        {
            _levels = new Level_h();
            _levels.level_list.Add(SceneManager.GetActiveScene().name);
        }
        else
        {
            if (_levels.level_list.Count > 0)
            {
                if (_levels.level_list[_levels.level_list.Count - 1] != SceneManager.GetActiveScene().name)
                {
                    _levels.level_list.Add(SceneManager.GetActiveScene().name);
                }
                else
                {
                    //El nivel está repetido.


                }
            } else
            {
                _levels.level_list.Add(SceneManager.GetActiveScene().name);
            }

        }
        Serializer.SaveSerializableObjectToFile<Level_h>(_levels, Application.persistentDataPath + "/" + "levels.persistence");
    }

    public void EmptyHistory()
    {
        _levels = new Level_h();
        Serializer.SaveSerializableObjectToFile<Level_h>(_levels, Application.persistentDataPath + "/" + "levels.persistence");

    }

    public string LastLevel()
    {
        _levels = Serializer.LoadSerializableObjectFromFile<Level_h>(Application.persistentDataPath + "/" + "levels.persistence");
        return _levels.level_list[_levels.level_list.Count - 1];
    }

    public bool ExistLevels(){

        _levels = Serializer.LoadSerializableObjectFromFile<Level_h>(Application.persistentDataPath + "/" + "levels.persistence");
        if(_levels.level_list.Count > 0)
        {
            return true;
        }
        else{
            return false;
        }

    }

    public int GetTurn()
    {
        _levels = Serializer.LoadSerializableObjectFromFile<Level_h>(Application.persistentDataPath + "/" + "levels.persistence");
        if (_levels.level_started != SceneManager.GetActiveScene().name)
        {
            _levels.turn = -1;
            _levels.level_started = SceneManager.GetActiveScene().name;
            Serializer.SaveSerializableObjectToFile<Level_h>(_levels, Application.persistentDataPath + "/" + "levels.persistence");
            
        }
        return _levels.turn;
    }

    public void SetTurn(int actualTurn)
    {
        _levels = Serializer.LoadSerializableObjectFromFile<Level_h>(Application.persistentDataPath + "/" + "levels.persistence");
        _levels.turn = actualTurn;
        Serializer.SaveSerializableObjectToFile<Level_h>(_levels, Application.persistentDataPath + "/" + "levels.persistence");
    }





}

[System.Serializable]
public class Level_h
{
    public Level_h()
    {
        level_list = new List<string>();
        turn = -1;
        level_started = "";
    }
    public List<string> level_list;
    public string level_started;
    public int turn;
}

[System.Serializable]
public class Level_won
{
    public Level_won()
    {
        level_list = new List<string>();
    }
    public List<string> level_list;
}
