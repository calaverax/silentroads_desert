﻿using UnityEngine;
using System.Collections;

public class ScrollingSky : MonoBehaviour {


    public float minPosition, maxPosition;
    public float scrollSpeed;

    private bool _direction = false;
    private Vector3 _targetPosition;

	// Use this for initialization
	void Start ()
    {
        _targetPosition = new Vector3(minPosition, transform.localPosition.y, transform.localPosition.z);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (transform.localPosition.x <= minPosition)
            _targetPosition = new Vector3(maxPosition, transform.localPosition.y, transform.localPosition.z);
        else if (transform.localPosition.x >= maxPosition)
            _targetPosition = new Vector3(minPosition, transform.localPosition.y, transform.localPosition.z);

        transform.localPosition = Vector3.MoveTowards(transform.localPosition, _targetPosition, Time.deltaTime * scrollSpeed);

        /*
        float distanceSquared = (transform.position - _targetPosition).sqrMagnitude;

        if (distanceSquared < 1)
        {
            if (_direction)
                _targetPosition = new Vector3(maxPosition, transform.position.y, transform.position.z);
            else
                _targetPosition = new Vector3(minPosition, transform.position.y, transform.position.z);
        }
        */
    }
}
