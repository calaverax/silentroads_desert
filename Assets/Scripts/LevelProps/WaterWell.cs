﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Wasabi.InventorySystem;
using Wasabi.InventorySystem.Inventory;

public class WaterWell : MonoBehaviour
{
    public bool CompleteRefill;
    public int fillAmount;
    public bool AddsBottle;
    public int BottlesAmount;
    public int wellUses = 1;

    private int _wellUses = 0;

    void Start()
    {
        Sprite[] wellsSprites = Resources.LoadAll<Sprite>("Atlas/Misc/Water well");
        GetComponent<Image>().sprite = wellsSprites[Random.Range(1, wellsSprites.Length)];
        GetComponent<Button>().onClick.AddListener(wellClicked);
    }

    void wellClicked()
    {
        if (_wellUses >= wellUses) return;

        CombatManager.Instance.robert.RefillWater(fillAmount, CompleteRefill);

        if (AddsBottle)
            ISInventoryManager.instance.inventory.addItem(ISInventoryManager.instance.consumablesDatabase.GetElementByID(1), 5);

        CombatManager.Instance.NpcFXController.PlayEventSound("WaterRefill1");

        _wellUses++;
    }

}
