﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Wasabi.InventorySystem;
using Wasabi.InventorySystem.Inventory;

public class LootingCrates : MonoBehaviour
{
    Button _lootBtn;
    ToggleGroup[] groups;
    ISInventoryManager _inventoryManager;

    void Awake()
    {
        _lootBtn = transform.GetChild(1).GetComponent<Button>();
        _lootBtn.onClick.AddListener(lootAll);

        groups = transform.GetChild(2).GetComponentsInChildren<ToggleGroup>();

        _inventoryManager = ISInventoryManager.instance;
    }

    void lootAll()
    {
        Toggle toggle0 = ToggleGroupExtension.GetActive(groups[0]);
        Toggle toggle1 = ToggleGroupExtension.GetActive(groups[1]);
        Toggle toggle2 = ToggleGroupExtension.GetActive(groups[2]);
        Toggle toggle3 = ToggleGroupExtension.GetActive(groups[3]);

        ProcessToggle(toggle0.name);
        ProcessToggle(toggle1.name);
        ProcessToggle(toggle2.name);
        ProcessToggle(toggle3.name);
    }

    void ProcessToggle(string name)
    {
        string[] values = name.Split('_');

        if (values[0] == "Weapon")
        {
            _inventoryManager.inventory.addItem(_inventoryManager.weaponsDatabase.GetElementByID(int.Parse(values[1])));
            //ISInventoryManager.instance.inventory.addItem()
        }
        else if (values[0] == "Bullets")
        {
            _inventoryManager.inventory.addItem(_inventoryManager.bulletsDatabase.GetElementByID(int.Parse(values[1])));
        }
        else if (values[0] == "Consumable")
        {
            _inventoryManager.inventory.addItem(_inventoryManager.consumablesDatabase.GetElementByID(int.Parse(values[1])));
        }
    }

}
