﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Wasabi.InventorySystem;
using Wasabi.InventorySystem.Inventory;

public class VendorNPC : MonoBehaviour {

    //[SerializeField] GameObject TextBubble;
    //[SerializeField] Text textField;
    public GameObject shop;

    [SerializeField] int[] WeaponsIDS, ArmorsIDS, ConsumablesIDS, BulletsIDS;

    void Start()
    {
        Invoke("ActivateBubble", 5.0f);
    }

    void ActivateBubble()
    {
        //TextBubble.SetActive(true);
        //textField.text = "Psssstttt.....";
        //Invoke("ActivateText1", 1.5f);
    }

    void ActivateText1()
    {
        //textField.text += "\nSeeking Good Stuff?";

    }

    public void OpenShopWaterman()//Función porvisoria
    {
        shop.SetActive(true);
    }

    public void OpenShop()
    {
        ISInventoryManager.instance.VendorController.PopulateShop(processList(WeaponsIDS,0));
        ISInventoryManager.instance.VendorController.PopulateShop(processList(ArmorsIDS,1));
        ISInventoryManager.instance.VendorController.PopulateShop(processList(ConsumablesIDS,2));
        ISInventoryManager.instance.VendorController.PopulateShop(processList(BulletsIDS,3));

        ISInventoryManager.instance.OpenShop();
    }

    List<ISObject> processList(int[] elementsIDS, int elementType)
    {
        List<ISObject> elements = new List<ISObject>();

        for (int i = 0; i < elementsIDS.Length; i++)
        {
            if (elementType == 0)
                elements.Add(ISInventoryManager.instance.weaponsDatabase.GetElementByID(elementsIDS[i]));
            else if (elementType == 1)
                elements.Add(ISInventoryManager.instance.armorDatabase.GetElementByID(elementsIDS[i]));
            else if (elementType == 2)
                elements.Add(ISInventoryManager.instance.consumablesDatabase.GetElementByID(elementsIDS[i]));
            else if (elementType == 3)
                elements.Add(ISInventoryManager.instance.bulletsDatabase.GetElementByID(elementsIDS[i]));
        }

        return elements;
    }
}
