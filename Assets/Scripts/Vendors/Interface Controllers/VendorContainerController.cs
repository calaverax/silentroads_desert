﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Wasabi.InventorySystem;
using Wasabi.InventorySystem.Inventory;

public class VendorContainerController : MonoBehaviour {

    [SerializeField] GameObject PurchasableItemPrefab, SellableItemPrefab;
    [SerializeField] RectTransform BuyContainer, SellContainer;
    [SerializeField] Text RobertBrassField, RobertLeadField;

    List<GameObject> childButtons = new List<GameObject>();
    List<GameObject> sellableButtons = new List<GameObject>();

    int itemIndex = 0;

    float _toSell = 1.0f;
    float _toBuy = 0.75f;
    public float SellingFactor { get { return _toSell; } }
    public float BuyingFactor { get { return _toBuy; } }

    public void ConfigureMarketPercent(float ToSell, float ToBuy)
    {
        _toBuy = ToBuy;
        _toSell = ToSell;
    }

    public void PopulateShop(List<ISObject> items)
    {
        GameObject itemListElement;

        foreach (ISObject item in items)
        {
            itemListElement = Instantiate(PurchasableItemPrefab);
            itemListElement.transform.SetParent(BuyContainer.transform);
            itemListElement.transform.localScale = new Vector3(1, 1, 1);

            BuyableItemButtonController script = itemListElement.GetComponent<BuyableItemButtonController>();
            script.ConfigureItem(item, this, itemListElement);

            childButtons.Add(itemListElement);
            itemIndex++;
        }
        BuyContainer.sizeDelta = new Vector2(BuyContainer.sizeDelta.x, 100 * childButtons.Count);
    }
    void UpdateRobertMoneyFields()
    {
        if (RobertBrassField != null)
            RobertBrassField.text = ISInventoryManager.instance.Brass.ToString();
        if (RobertLeadField != null)
            RobertLeadField.text = ISInventoryManager.instance.Lead.ToString();
    }

    void OnEnable()
    {
        UpdateRobertMoneyFields();
        ActivateTab("Buy");
    }

    void OnDisable()
    {
        foreach (GameObject child in childButtons)
            Destroy(child);
        itemIndex = 0;
        childButtons.Clear();
    }

    public void RemoveSpecificChild(GameObject gameObject)
    {
        foreach (GameObject btn in childButtons)
            btn.GetComponent<BuyableItemButtonController>().checkPurchasable();

        childButtons.Remove(gameObject);
        Destroy(gameObject);
        BuyContainer.sizeDelta = new Vector2(BuyContainer.sizeDelta.x, 100 * childButtons.Count);
        UpdateRobertMoneyFields();
    }

    public void ActivateTab(string tab)
    {
        if (tab == "Buy")
        {
            BuyContainer.parent.gameObject.SetActive(true);
            SellContainer.parent.gameObject.SetActive(false);
        }
        else if (tab == "Sell")
        {
            BuyContainer.parent.gameObject.SetActive(false);
            SellContainer.parent.gameObject.SetActive(true);
            PopulateSellTab();
        }
    }

    Dictionary<ISObject, int> SellableItems = new Dictionary<ISObject, int>();

    List<GameObject> sellingItemsList = new List<GameObject>();

    void ResetSellableList()
    {
        foreach (GameObject go in sellingItemsList)
            Destroy(go);

        sellingItemsList.Clear();
    }

    void PopulateSellTab()
    {
        ResetSellableList();
        GameObject itemListElement;
        // Recorrer invenntario de Robert        
        for (int i = 0; i < ISInventoryManager.instance.inventory.slotAmount; i++)
        {
            if (ISInventoryManager.instance.inventory.inventorySlots[i].item != null)
            {
                // Armar Lista con sus Items.
                if (!(SellableItems.ContainsKey(ISInventoryManager.instance.inventory.inventorySlots[i].item)))
                    SellableItems.Add(ISInventoryManager.instance.inventory.inventorySlots[i].item, i);
            }
        }

        // Popular ScrollRect con prefabs de items vendibles.
        foreach (KeyValuePair<ISObject, int> SellItem in SellableItems)
        {
            itemListElement = Instantiate(SellableItemPrefab);
            itemListElement.transform.SetParent(SellContainer.transform);
            itemListElement.transform.localScale = new Vector3(1, 1, 1);

            SellableItemButtonController script = itemListElement.GetComponent<SellableItemButtonController>();
            script.ConfigureItem(SellItem.Key, this, itemListElement);

            SellContainer.sizeDelta = new Vector2(SellContainer.sizeDelta.x, 100 * SellableItems.Count);
            sellingItemsList.Add(itemListElement);
         }

        // Callback para cuando se Vende un Item => Borrarlo, sacarlo del diccionario, cleanup.
        // -> Borrarlo del inventario / Reducir StackSize.
    }

    public void RemoveSellableItem(GameObject gameObject, ISObject configuredItem)
    {

        // Get Item Slot Index:
        int itemIndex = SellableItems[configuredItem];

        Debug.Log("Item To Sell Slot Index: " + itemIndex);
        // Remove From Robert Inventory:
        if (configuredItem is IISStackable)
        {
            // Remove Item if stack is Depleted.
            if (ISInventoryManager.instance.inventory.ReduceItemStackSize(itemIndex))
            {
                SellableItems.Remove(configuredItem);

                // Remove Visually
                Destroy(gameObject);
                SellContainer.sizeDelta = new Vector2(SellContainer.sizeDelta.x, 100 * SellableItems.Count);
            }
        }
        else
        {
            ISInventoryManager.instance.inventory.RemoveItem(itemIndex);
        }

        // Remove From Dictionary:
        if (!(configuredItem is IISStackable))
        {
            SellableItems.Remove(configuredItem);

            // Remove Visully
            Destroy(gameObject);
            SellContainer.sizeDelta = new Vector2(SellContainer.sizeDelta.x, 100 * SellableItems.Count);
        }

        UpdateRobertMoneyFields();
    }
}
