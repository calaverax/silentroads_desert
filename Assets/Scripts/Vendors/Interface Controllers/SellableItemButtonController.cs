﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Wasabi.InventorySystem;
using Wasabi.InventorySystem.Inventory;

public class SellableItemButtonController : MonoBehaviour {

    [SerializeField] Image itemIcon;
    [SerializeField] Text itemDescription, itemPriceLead, itemPriceBrass;
    [SerializeField] Button buyButton;

    ISObject _itemConfigured;

    VendorContainerController _parent;
    //int _index = 0;
    GameObject _gameObject;

	void Start ()
    {
        if (buyButton != null)
            buyButton.onClick.AddListener(SellItemAction);
	}

    public void ConfigureItem(ISObject item, VendorContainerController parent, GameObject go)
    {
        if ((item == null) || (go == null)) return;

        //_index = 0;
        _parent = parent;
        _itemConfigured = item;
        _gameObject = go;

        if (itemIcon != null)
            itemIcon.sprite = item.Icon;
        if (itemDescription != null)
            itemDescription.text = item.Name + "/n" + item.Description;

        int[] itemPrice = ISInventoryManager.instance.ConvertMoney((int)(item.ItemPrice * parent.BuyingFactor));

        itemPriceLead.text = itemPrice[0].ToString();
        itemPriceBrass.text = itemPrice[1].ToString();
        checkPurchasable();
    }

    void SellItemAction()
    {
        // Debug Text
        CombatManager.Instance.debugPanel.SubmitText("Purchased Item: " + _itemConfigured.Name, "Red");

        /* //---> Se encarga el padre, que tiene el indice de slot?
        // Check If Stackable
        if (_itemConfigured is IISStackable)
            ISInventoryManager.instance.inventory.ReduceItemStackSize(_itemConfigured);
        else
            ISInventoryManager.instance.inventory.RemoveItem(_itemConfigured);
        */

        // Add Money
        ISInventoryManager.instance.PlayerMoney += ((int)(_itemConfigured.ItemPrice * _parent.BuyingFactor));
        
        // Remove Item
        _parent.RemoveSellableItem(_gameObject, _itemConfigured);

        // PlaySounds
    }

    public void checkPurchasable()
    {
        if (_itemConfigured.ItemPrice > ISInventoryManager.instance.PlayerMoney)
            buyButton.interactable = false;
        else
            buyButton.interactable = true;
    }
}
