﻿#pragma strict


@script ExecuteInEditMode()
//VAR PUBLICAS
public var miCamara : Camera; //Aca pongo la camara que estoy usando (en la que esta el personaje)


//VAR PRIVADAS
private var distancia : float; //para calcular la distancia entre la camara y el objeto

private var escalaNueva : Vector3; //para modificar la escala en base a la distancia.
private var frustrum : float; //para calcular el frustrum de la camara en el plano del objeto

private var frustrumOriginal : float; //para calcular el frustrum con respecto al objeto cuando la escena abre.
private var escalaOriginal : Vector3; //Esta va a calcular la escala original del objeto.





function Start () 
{
escalaOriginal = transform.localScale; //almacenamos la escala original de la textura
distancia = Vector3.Distance (this.transform.position, miCamara.transform.position); //distancia entre camara y objeto
frustrumOriginal = FrustumHeightAtDistance(distancia); //calcula el frustrum de la camara para el objeto
}

function Update (){
	distancia = Vector3.Distance (this.transform.position, miCamara.transform.position);
	frustrum = FrustumHeightAtDistance(distancia);
	//Debug.Log("La distancia es " + distancia);
	//Debug.Log ("el frustrum es " + frustrum);
	//Nueva Escala = Distancia B * Escala Original / Distancia A
	escalaNueva = frustrum * escalaOriginal / frustrumOriginal;
	/*if ( float.IsInfinity( this.transform.localScale.x ) )
{
    this.transform.localScale.x = 1;
    this.transform.localScale.y = 1;
    this.transform.localScale.z = 1;
}*/
	//Debug.Log ("La escala nueva es "+escalaNueva);
	this.transform.localScale = escalaNueva;

	
	


}


//Este codigo tomado de los docs de unity permite calcular el frustrum de la camara a una distancia dada
function FrustumHeightAtDistance(distance: float) {
    return 2.0 * distance * Mathf.Tan(miCamara.fieldOfView * 0.5 * Mathf.Deg2Rad);
}
