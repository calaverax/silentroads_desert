﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class SpritesOrderInLayer : MonoBehaviour {

    SpriteRenderer[] _renderers;
    int[] _originalOrder;
    // Use this for initialization
    void Start ()
    {
        _renderers = GetComponentsInChildren<SpriteRenderer>();

        _originalOrder = new int[_renderers.Length];
        for (int i = 0; i < _renderers.Length; i++)
            _originalOrder[i] = _renderers[i].sortingOrder;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if ((_renderers == null) || (transform == null)) return;

        for (int i = 0; i < _renderers.Length; i++)
            _renderers[i].sortingOrder =  (int)transform.position.y * -1;
    }
}
