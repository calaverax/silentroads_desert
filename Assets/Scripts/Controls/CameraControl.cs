﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour
{
    public float maxZoom;
    public float minZoom;
    public float panSpeed = -1;

    Vector3 bottomLeft;
    Vector3 topRight;

    float cameraMaxY;
    float cameraMinY;
    float cameraMaxX;
    float cameraMinX;

    Camera camera;
    void Start()
    {
        camera = Camera.main;

        //set max camera bounds (assumes camera is max zoom and centered on Start)
        topRight = camera.ScreenToWorldPoint(new Vector3(camera.pixelWidth, camera.pixelHeight, -transform.position.z));
        bottomLeft = camera.ScreenToWorldPoint(new Vector3(0, 0, -transform.position.z));
        cameraMaxX = topRight.x;
        cameraMaxY = topRight.y;
        cameraMinX = bottomLeft.x;
        cameraMinY = bottomLeft.y;
    }

    void Update()
    {
        //click and drag
        if (Input.GetMouseButton(0))
        {
            float x = Input.GetAxis("Mouse X") * panSpeed;
            float y = Input.GetAxis("Mouse Y") * panSpeed;
            transform.Translate(x, y, 0);
        }

        //zoom
		/*
        if ((Input.GetAxis("Mouse ScrollWheel") > 0) && Camera.main.orthographicSize > minZoom ) // forward
        {
            Camera.main.orthographicSize = Camera.main.orthographicSize - 10;
        }

        if ((Input.GetAxis("Mouse ScrollWheel") < 0) && Camera.main.orthographicSize < maxZoom) // back            
        {
            Camera.main.orthographicSize = Camera.main.orthographicSize + 10f;
        }
*/
		if ((Input.GetAxis("Mouse ScrollWheel") > 0) && Camera.main.fieldOfView > minZoom ) // forward
		{
			Camera.main.fieldOfView = Camera.main.fieldOfView - 10;
		}
		
		if ((Input.GetAxis("Mouse ScrollWheel") < 0) && Camera.main.fieldOfView < maxZoom) // back            
		{
			Camera.main.fieldOfView = Camera.main.fieldOfView + 10f;
		}



        //check if camera is out-of-bounds, if so, move back in-bounds
        topRight = camera.ScreenToWorldPoint(new Vector3(camera.pixelWidth, camera.pixelHeight, -transform.position.z));
        bottomLeft = camera.ScreenToWorldPoint(new Vector3(0, 0, -transform.position.z));

        if (topRight.x > cameraMaxX)
        {
            transform.position = new Vector3(transform.position.x - (topRight.x - cameraMaxX), transform.position.y, transform.position.z);
        }

        if (topRight.y > cameraMaxY)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - (topRight.y - cameraMaxY), transform.position.z);
        }

        if (bottomLeft.x < cameraMinX)
        {
            transform.position = new Vector3(transform.position.x + (cameraMinX - bottomLeft.x), transform.position.y, transform.position.z);
        }

        if (bottomLeft.y < cameraMinY)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + (cameraMinY - bottomLeft.y), transform.position.z);
        }
    }
}
