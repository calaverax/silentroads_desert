﻿using UnityEngine;
using System.Collections;

public class NextLevelNavigations : MonoBehaviour
{
    [SerializeField] LevelNavigationBtn[] _levelsBtns;

    // Use this for initialization
    void Start()
    {
        CombatManager.Instance.OnCombatEnded.AddListener(CombatEnded);
    }

    public void CombatEnded()
    {
        //LevelHistory.Instance.AddLevelWon();
        for (int i = 0; i < _levelsBtns.Length; i++)
            _levelsBtns[i].gameObject.SetActive(true);

    }

}
