﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelNavigationPopup : MonoBehaviour
{
    private PolyNavAgent _agent;

    public PolyNavAgent agent
    {
        get
        {
            if (!_agent)
                _agent = CombatManager.Instance.robert.GetComponent<PolyNavAgent>();
            return _agent;
        }
    }


    [SerializeField] Text _caption;
    [SerializeField] Text _waterWarning;
    [SerializeField] GameObject _notFollowedWarning;
    [SerializeField] Button _okAction, _cancelAction;
    [SerializeField] LevelMapIngame _levelMapIngame;

    int _water = 0;
    string _levelToLoad;
    Transform _walkingObjetive;
    void Start()
    {
        _okAction.onClick.AddListener(okAction);
        _cancelAction.onClick.AddListener(cancelAction);
    }

    public void ShowPopup(string levelToLoad, Transform walkingObjetive, string caption, int water, bool followed = true)
    {
        this.gameObject.SetActive(true);
        _water = water;
        //------------------------
        _levelToLoad = levelToLoad;
        _walkingObjetive = walkingObjetive;

        //------------------------
        _caption.text = caption;
        _waterWarning.text = water + " water will be consumed";

        if(CombatManager.Instance.robert.characterStats.water - water < 0)
        {
            _okAction.interactable = false;
        }else
        {
            _okAction.interactable = true;
        }

        if (followed)
            _notFollowedWarning.SetActive(false);
        else
            _notFollowedWarning.SetActive(true);
    }
    //--------------------------------------------------------

    void okAction()
    {
        // ----
        // Make Robert Walk (Transform _walkingObjetive)
        //MakeRobertWalk(); // Temporalmente deshabilitado ya que la navegacion es distinta.
        // En su lugar llamamos al mapa directamente para transicionar al siguiente nivel.
        ShowChapterMap();
        CombatManager.Instance.robert.characterStats.water -= _water;

        // ActivateMap.
        // GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().enemyDetailsWindow.ActivateWindow(this);

        // Invoke Loader

        this.gameObject.SetActive(false);
    }
    //--------------------------------------------------------
    void MakeRobertWalk()
    {
        agent.OnDestinationReached += RobertDestinationReached;
        agent.SetDestination(_walkingObjetive.position);

        // Walk, Walk, Walk.
        //_levelMapIngame.EnableMap();

        // Show Map and Flag.
        //Invoke("ShowChapterMap", 3.0f);
    }
    //--------------------------------------------------------
    void RobertDestinationReached()
    {
        ShowChapterMap();
    }
    //--------------------------------------------------------
    void ShowChapterMap()
    {
        // Flag Animation and aalpha.
        _levelMapIngame.EnableMap();

        Invoke("loadNewLevel", 3.0f);
    }
    //--------------------------------------------------------
    void loadNewLevel()
    {
        // Load New Scene
        ScenesLoader.Instance.LoadScene(_levelToLoad, true, true, true);
    }
    //--------------------------------------------------------
    void cancelAction()
    {
        this.gameObject.SetActive(false);
    }
    //--------------------------------------------------------
}
