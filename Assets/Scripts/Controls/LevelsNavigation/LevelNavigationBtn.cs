﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelNavigationBtn : MonoBehaviour {

    [SerializeField] string _toLevelName;
    [SerializeField] string _popupCaption;
    [SerializeField] int _consumedWater;
    [SerializeField] bool producesFollowers = false;

    Transform _walkingObjetive;
    // Use this for initialization
    void Start ()
    {
        this.GetComponent<Button>().onClick.AddListener(buttonAction);
        _walkingObjetive = this.gameObject.transform.GetChild(1).transform;
    }

    void buttonAction()
    {
       int followersCount = 0;
        // Find IngameGUi, and get Popup Element
        if (EnemyHistory.Instance.IsLevelWithEnemies(SceneManager.GetActiveScene().name))
        { 
            if (EnemyHistory.Instance.EnemiesInThisLevel() > 0) //&& !CombatManager.Instance.combatEnded) 
            {
                followersCount = EnemyHistory.Instance.MarkFollowers();
            }
        }
        if (followersCount > 0) { producesFollowers = true;  }
        EnemyHistory.Instance.Fin();
        GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().levelNavigationPopup.ShowPopup(
            _toLevelName,
            _walkingObjetive,
            _popupCaption,
            _consumedWater,
            producesFollowers);
        Debug.Log("Followers Count = " + followersCount);
    }
}
