﻿using UnityEngine;
using System.Collections;

public class IngameDebug : MonoBehaviour
{

    public void Toggle()
    {
        if (this.gameObject.activeInHierarchy)
            this.gameObject.SetActive(false);
        else
            this.gameObject.SetActive(true);
    }
    

    public void EndCombat()
    {
        CombatManager.Instance.ForceDebugEndCombat();
    }



}
