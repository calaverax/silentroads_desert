﻿using UnityEngine;
using System.Collections;






[ExecuteInEditMode]
public class ScalesManager : MonoBehaviour {

    /*
    
    // Use this for initialization
    [SerializeField] private float minScale = 0.1f;
    [SerializeField] private float maxScale = 0.5f;
    [SerializeField] private float _scaleOffset = 0.5f;
    [SerializeField] private float _levelScale = 1024;
    [SerializeField] private float _levelEffectiveArea = 1;
    Transform _child;

    // Fields:
    float _oldRange, _newRange, _newValue;
   
    //SpriteRenderer[] _renderers;
    //int[] _originalOrder;

    void Start()
    {
        _child = transform.GetChild(0);
        
        //_renderers = GetComponentsInChildren<SpriteRenderer>();
        //_originalOrder = new int[_renderers.Length];
        //for (int i = 0; i < _renderers.Length; i++)
            //_originalOrder[i] = _renderers[i].sortingOrder;
        
    }

#if UNITY_EDITOR
    void Update()
    {
        if ((transform == null) || (_child == null)) return;

        this.transform.localScale = (getScale(_levelScale - (_child.position.y * _levelEffectiveArea)));
    }
#else
    void FixedUpdate()
    {
        if ((transform == null) || (_child == null)) return;
        
        this.transform.localScale = (getScale(_levelScale - (_child.position.y * _levelEffectiveArea)));

        this.transform.localScale = (getScale(1024 // Screen.height 
    - _child.position.y));
    }
#endif
    public Vector2 getScale(float posY)
    {
        //_oldRange = (1024 //Screen.height 
         0.4f) - 1;
        _oldRange = _levelScale;
        _newRange = (maxScale - minScale);
        _newValue = (((posY - 1) * _newRange) / _oldRange) + minScale;

        return new Vector2(_newValue - _scaleOffset, _newValue - _scaleOffset);
        //return new Vector2(_newValue, _newValue);

    }
    */
}
