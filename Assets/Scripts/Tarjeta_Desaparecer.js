﻿#pragma strict

//Esta funcion sirve para hacer que la imagen desaparezca. Esta hecha para un objeto con dos materiales.
//Si los materiales son unlit asegurate de usar un shader de unlit que tenga alfa (el default de unity no tiene)

var alpha : float = 1; //Quiero que empiece con la imagen de la textura opaca.
var retraso : float = 0.7;
var duracion : float = 1; //factor que prolonga el proceso de desvanecimiento.
private var tiempo :float;

function Start(){
}
function Update () {
    Frena(retraso); //No hagas desaparecer el objeto hasta que hayan pasado 0.7 segundos.


}

//funcion para dar frenar el script por unos segundos.
function Frena(delay : float){
    yield WaitForSeconds(delay);//Banca en base al delay especificado...
    lerpAlpha(); //...cuando hayas terminado corre el lerp para desvanecer la imagen.
}

    //Funcion para desvanecer la imagen
    function lerpAlpha () {
        alpha = Mathf.Lerp(1.0, 0.0, tiempo / duracion) ;
        GetComponent.<Renderer>().material.color.a = alpha;//Modificiando el alpha del primer material.
        GetComponent.<Renderer>().materials[1].color.a = alpha; //Modificando el alpha del segundo material.
        tiempo += 0.01;

    }
