﻿using UnityEngine;
using System.Collections;

public class RobertMenuDialogs : MonoBehaviour {

    public AudioSource source;
    ShuffleBag robertVoices = new ShuffleBag(10);

	// Use this for initialization
	void Start ()
    {
        string pathToFiles = "AudioClips/Characters/Robert/MainMenuDialogs/";

        AudioClip[] allSounds = Resources.LoadAll<AudioClip>(pathToFiles);

        for (int i = 0; i < allSounds.Length; i++)
            robertVoices.Add(allSounds[i]);

        InvokeRepeating("playVoice", 5.0f, 15.0f);

    }

    private void playVoice()
    {
        source.PlayOneShot(robertVoices.Next());
    }
	
}
