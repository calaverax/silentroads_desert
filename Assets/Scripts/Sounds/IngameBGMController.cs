﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class IngameBGMController : MonoBehaviour {

    public AudioMixer masterMixer;
    public AudioSource entryBGM, combatWonBGM, inCombat;
    public AudioMixerSnapshot normalSnapshot, mapInfoSnapshot, mutedSnapshot;


    enum Fade { In, Out };
    float fadeTime = .5F;

    public void BGMVolume(float volume)
    {
        if (volume > 0)
            PlayerPrefs.SetInt("BGM-Muted", 0);
        else if (volume == 0)
            PlayerPrefs.SetInt("BGM-Muted", 1);

        masterMixer.SetFloat("BGMVolume", volume);
    }
    public void SFXVolume(float volume)
    {
        masterMixer.SetFloat("SFXVolume", volume);
    }

    void Start()
    {
        //CombatManager.Instance.OnCombatStarted.AddListener(combatStarted);
        CombatManager.Instance.OnRobertDies.AddListener(robertDies);
        CombatManager.Instance.OnCombatEnded.AddListener(combatEnded);

        if ((GameManager.Instance.isLevelWon(GameManager.Instance.selectedLevelId)))
        {
            inCombat.mute = true;
            combatWonBGM.mute = true;
        }
    }

    void robertDies()
    {
        mutedSnapshot.TransitionTo(0.1f);
    }

    public void changeSnapshot(string snapshoot)
    {
        if (snapshoot == "MapInfo")
        {
            mapInfoSnapshot.TransitionTo(0.5f);
        }
        else if (snapshoot == "Normal")
        {
            normalSnapshot.TransitionTo(0.5f);
        }
    }

    public void combatStarted()
    {
        Debug.Log("Combat Started Event Ivoked");
        FadeAudioIn("InCombatVolume");
        Invoke("delayedFade", 1.0f);
    }

    public void combatEnded()
    {
        if (CombatManager.Instance.robert == null)
        {
            mutedSnapshot.TransitionTo(0.1f);
            return;
        }

        if (CombatManager.Instance.robert.isAlive)
        {
            entryBGM.mute = true;

            if ((GameManager.Instance.isLevelWon(GameManager.Instance.selectedLevelId)))
                return;

            combatWonBGM.Play();

            GameManager.Instance.SetLevelWon(Application.loadedLevelName);
            GameManager.Instance.saveGame();

        }
        else
        {
            mutedSnapshot.TransitionTo(0.1f);
        }
    }

    public void stopCombatWonAudio()
    {
        combatWonBGM.Stop();
    }
    private void delayedFade()
    {
        FadeAudioOut("InsideVolume");
    }

    public void FadeAudioIn(string audioGroup, float fadeTime = 0.5f)
    {
        StartCoroutine(FadeAudio(audioGroup,fadeTime, Fade.Out));
        this.fadeTime = fadeTime;
    }

    public void FadeAudioOut(string audioGroup, float fadeTime = 0.5f)
    {
        StartCoroutine(FadeAudio(audioGroup, fadeTime, Fade.In));
        this.fadeTime = fadeTime;
    }

    IEnumerator FadeAudio(string audioGroup, float timer, Fade fadeType)
    {
        float start = fadeType == Fade.In ? 0.0F : 1.0F;
        float end = fadeType == Fade.In ? 1.0F : 0.0F;
        float i = 0.0F;
        float step = 1.0F / timer;

        while (i <= 1.0F)
        {
            i += step * Time.deltaTime;
            masterMixer.SetFloat(audioGroup, Mathf.Lerp(start, end, i) * -80);
            yield return new WaitForSeconds(step * Time.deltaTime);
        }
    }

}
