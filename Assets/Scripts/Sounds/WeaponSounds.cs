﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponSounds : MonoBehaviour
{
    public AudioClip reloadSound;
    public List<AudioClip> shootSounds = new List<AudioClip>();
}
