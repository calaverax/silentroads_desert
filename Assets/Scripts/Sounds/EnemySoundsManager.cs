﻿using UnityEngine;
using System.Collections;

public class EnemySoundsManager : MonoBehaviour {

    [SerializeField]
    private AudioSource _dialogs, _weapons, _sfx;
    [SerializeField]
    private AudioClip _allShootsFX;

    public AudioClip footSteps;

    private bool _isBusy = false;
    public bool isBusy {  get { return _isBusy; } }

    public void playLoopableFX(AudioClip clip)
    {
        _sfx.loop = true;
        _sfx.clip = clip;
        _sfx.Play();

        _isBusy = true;
    }

    public void stopLoopingFX(AudioClip clip)
    {
        if (_sfx.clip == clip)
        {
            _sfx.Stop();
            _sfx.loop = false;
            _sfx.clip = null;

            _isBusy = false;
        }
    }

    public void playFX(AudioClip clip)
    {
		if ((_sfx == null) || (clip == null)) return;

        _sfx.PlayOneShot(clip);
    }
}
