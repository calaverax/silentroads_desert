﻿using UnityEngine;
using System.Collections;

public class BGMControl : MonoBehaviour {

    [SerializeField] bool _specificAudios;
    [SerializeField] string[] _specificAudiosNames;

    bool _fadingMusic = false;
    float _fadingFactor = -0.001f;

    AudioSource _fadingSource;

	void Start ()
    {
        if (_specificAudios)
        {
            for (int i = 0; i < _specificAudiosNames.Length; i++)
            {
                ConfigureNewAudioObject(_specificAudiosNames[i], (_specificAudiosNames[i].ToLower()).IndexOf("music") > -1 ? true : false);
            }
        }
        else
        {
            ConfigureNewAudioObject("Generic Ambient " + Random.Range(1,3));
            ConfigureNewAudioObject("Generic Music " + Random.Range(1, 10), true);
        }
	}

    void ConfigureNewAudioObject(string fileName, bool isMusic = false)
    {
        GameObject newObj = new GameObject("Audio Source " + fileName, typeof(AudioSource));
        newObj.transform.SetParent(this.transform);

        AudioSource source = newObj.GetComponent<AudioSource>();
        source.clip = Resources.Load<AudioClip>("AudioClips/Levels BGM/" + fileName);
        source.loop = true;
        source.volume = 0.35f;

        if (isMusic)
        {
            _fadingSource = source;
            _fadingSource.volume = 0.0f;
            
            // Start CoRoutines For Fade In, Fade Out, DeadTime.
            StartDeadTime();
        }

        if (source.clip != null)
            source.Play();
    }

    private void StartDeadTime()
    {
        _fadingMusic = false;
        Invoke("StartFade", Random.Range(10.0f, 15.0f));
    }

    private void StartFade()
    {
        _fadingMusic = true;
        _fadingFactor *= -1;
    }

    void Update()
    {
        if (_fadingMusic)
        {
            if ((_fadingSource.volume <= 0.5f) && (_fadingSource.volume >= 0))
            {
                _fadingSource.volume += _fadingFactor;

                if (_fadingSource.volume > 0.5f)
                    _fadingSource.volume = 0.5f;
                else if (_fadingSource.volume < 0.0f)
                    _fadingSource.volume = 0.0f;
            }

            if ((_fadingSource.volume >= 0.5f) || (_fadingSource.volume <= 0.0f))
            {
                StartDeadTime();
            }
        }
    }
}
