﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RobertSoundsManager : MonoBehaviour {

    /*
        Necesitamos Listas:
            - OnBeingHit
            - OnHitEnemy
            - OnEnemyKilled

        -- Dialogos.

        Sonidos De Las armas
        -- Chequear si se pueden agregar en los prefabs de cada arma.
        -- Obtenerlos de ahi (Reload / Shoot); { Equipped lo maneja el asset de inventario.}
    */

    [SerializeField]
    private AudioSource _dialogs, _weapons, _sfx, _heartBeat;

    public AudioSource LowHP { get { return _heartBeat; } }

    [SerializeField]
    private AudioClip _allShootsFX;

    [SerializeField]
    private List<AudioClip> robertRecieveDamage;

    [SerializeField]
    AudioClip _combatDialogSuccess, _combatDialogError;

    public void playCombatDialogFX(bool success)
    {
        _sfx.PlayOneShot(success ? _combatDialogSuccess : _combatDialogError);
    }

    public void playWeaponShooting(List<AudioClip> clips)
    {
        _weapons.PlayOneShot(clips[Random.Range(0, clips.Count - 1)], 1);
        _sfx.PlayOneShot(_allShootsFX, 1.2f);
    }

    public void playWeaponShooting(AudioClip clip)
    {
        _sfx.PlayOneShot(clip);
    }

    public void playRecieveDamage()
    {
        _sfx.PlayOneShot(robertRecieveDamage[Random.Range(0, robertRecieveDamage.Count - 1)]);
    }

    public void playOneShoot(AudioClip clip)
    {
        _sfx.PlayOneShot(clip);
    }

    public void playVoiceAudio(AudioClip clip)
    {
        _dialogs.PlayOneShot(clip);
    }

    public void playLowHP()
    {
        _heartBeat.loop = true;
        _heartBeat.Play();
    }

    public void stopLowHP()
    {
        _heartBeat.loop = false;
        _heartBeat.Stop();
    }
}
