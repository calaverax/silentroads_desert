﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(AudioSource))]
public class MenuBGMcontroller : Singleton<MenuBGMcontroller>
{
    public AudioClip[] bgm;
    public MenuManager manager;

    private AudioClip _currentClip;
    private AudioSource _source;
    public AudioSource source { get { return _source; } }

    void Awake()
    {
        _source = GetComponent<AudioSource>();

        if (bgm == null)
            return;

        if (bgm.Length == 0)
        {
            Debug.LogError("No Audio clips defined for Menu Background Music");
            return;
        }

        InvokeRepeating("checkBGM", 0, 1.0f);
    }

    public void selectRandomClip()
    {

        int clipIdx = 0;//= Random.Range(0, bgm.Length);

        _currentClip = bgm[clipIdx];
        _source.clip = _currentClip;
        if (PlayerPrefs.GetInt("BGM-Muted") == 0)
            _source.mute = false;
        else if(PlayerPrefs.GetInt("BGM-Muted") == 1)
            _source.mute = true;


        _source.Play();

        /*
        if (bgm[clipIdx] == _currentClip)
        {
            selectRandomClip();
        }
        else
        {
            _currentClip = bgm[clipIdx];
            _source.clip = _currentClip;
            _source.Play();
            manager.switchBackground(clipIdx);
        }
        */
    }
    enum Fade { In, Out };
    float fadeTime = .5F;

    public void FadeAudioIn()
    {
        StartCoroutine(FadeAudio(fadeTime, Fade.In));
    }

    public void FadeAudioOut()
    {
        StartCoroutine(FadeAudio(fadeTime, Fade.Out));
    }

    IEnumerator FadeAudio(float timer, Fade fadeType)
    {
        float start = fadeType == Fade.In ? 0.0F : 7F;
        float end = fadeType == Fade.In ? .7F : 0.0F;
        float i = 0.0F;
        float step = 1.0F / timer;

        while (i <= 1.0F)
        {
            i += step * Time.deltaTime;
            _source.volume = Mathf.Lerp(start, end, i);
            yield return new WaitForSeconds(step * Time.deltaTime);
        }
    }

    void checkBGM()
    {
        if (!_source.isPlaying)
            selectRandomClip();
    }

    public void playBGMNow(AudioClip clip)
    {
        _source.clip = clip;
        if (PlayerPrefs.GetInt("BGM-Muted") == 0)
            _source.mute = false;
        else if (PlayerPrefs.GetInt("BGM-Muted") == 1)
            _source.mute = true;

        _source.Play();
    }

    public void stopAudio()
    {
        _source.Stop();
    }
}
