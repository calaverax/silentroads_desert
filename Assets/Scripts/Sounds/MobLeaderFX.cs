﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class SoundEventNames
{
    public static string OnAnotherEnemyDiesClips = "OnAnotherEnemyDiesClips ";
    public static string OnDeathClips = "OnDeathClips";
    public static string OnFleeClips = "OnFleeClips";
    public static string OnHitClips = "OnHitClips";
    public static string OnLowHPClips = "OnLowHPClips";
    public static string OnAttackingMeleeClips = "OnAttackingMeleeClips";
    public static string OnAttackingRangedClips = "OnAttackingRangedClips";
    public static string OnMiddleCombatClips = "OnMiddleCombatClips";
    public static string OnRobertDiesClips = "OnRobertDiesClips";
    public static string OnRobertGetsHitClips = "OnRobertGetsHitClips";
    public static string OnRobertMissesAttackClips = "OnRobertMissesAttackClips";
    public static string OnRobertUnconciousClips = "OnRobertUnconciousClips";
    public static string OnCombatStartedClips = "OnCombatStartedClips";
}

public class MobLeaderFX : MonoBehaviour
{
    bool _needToResumeCombat = false;
    bool _bossIntro = false;
    BaseEnemy _parent;
    AudioSource _source;
    bool _stillPlaying = false;
    System.Action _callBack;

    ShuffleBag OnAnotherEnemyDiesClips = new ShuffleBag(20);
    ShuffleBag OnDeathClips = new ShuffleBag(20);
    ShuffleBag OnFleeClips = new ShuffleBag(20);
    ShuffleBag OnHitClips = new ShuffleBag(20);
    ShuffleBag OnLowHPClips = new ShuffleBag(20);
    ShuffleBag OnAttackingMeleeClips = new ShuffleBag(20);
    ShuffleBag OnAttackingRangedClips = new ShuffleBag(20);
    ShuffleBag OnMiddleCombatClips = new ShuffleBag(20);
    ShuffleBag OnRobertDiesClips = new ShuffleBag(20);
    ShuffleBag OnRobertGetsHitClips = new ShuffleBag(20);
    ShuffleBag OnRobertMissesAttackClips = new ShuffleBag(20);
    ShuffleBag OnRobertUnconciousClips = new ShuffleBag(20);
    ShuffleBag OnCombatStartedClips = new ShuffleBag(20);

    //CombatManager combatManager;
    CombatManager _combatManager;
    /*
    {
        get
            {
                if (combatManager == null)
                    combatManager = CombatManager.Instance;
                return combatManager;
            }
    }
    */
    // Sound Pools
    /*
    List<AudioClip> OnAnotherEnemyDiesClips = new List<AudioClip>();
    List<AudioClip> OnDeathClips = new List<AudioClip>();
    List<AudioClip> OnFleeClips = new List<AudioClip>();
    List<AudioClip> OnHitClips = new List<AudioClip>();
    List<AudioClip> OnLowHPClips = new List<AudioClip>();
    List<AudioClip> OnAttackingMeleeClips = new List<AudioClip>();
    List<AudioClip> OnAttackingRangedClips = new List<AudioClip>();
    List<AudioClip> OnMiddleCombatClips = new List<AudioClip>();
    List<AudioClip> OnRobertDiesClips = new List<AudioClip>();
    List<AudioClip> OnRobertGetsHitClips = new List<AudioClip>();
    List<AudioClip> OnRobertMissesAttackClips = new List<AudioClip>();
    List<AudioClip> OnRobertUnconciousClips = new List<AudioClip>();
    List<AudioClip> OnCombatStartedClips = new List<AudioClip>();
    */
    void Awake()
    {
        _source = gameObject.GetComponent<AudioSource>();
    }

    public void Initialize(BaseEnemy parent)
    {
        // Get Audio Manager

        _parent = parent;

        // Get Audio Files Acording to characterName
        string character = "";
        character += _parent.genre.ToString();
        character += _parent.enemyType.ToString();

        string pathToFiles = "AudioClips/Characters/" + character + "/";

        AudioClip[] allSounds = Resources.LoadAll<AudioClip>(pathToFiles);

        for (int i = 0; i < allSounds.Length; i++)
        {
            string[] clipNameFields = allSounds[i].name.Split('_');

            // Check if the name correspond to current character
            if (clipNameFields[0].ToLower() == character.ToLower())
            {
                if (clipNameFields[1].ToLower() == "anotherenemydies")
                {
                    if (!OnAnotherEnemyDiesClips.Contains(allSounds[i]))
                        OnAnotherEnemyDiesClips.Add(allSounds[i]);
                }
                else if (clipNameFields[1].ToLower() == "dies")
                {
                    if (!OnDeathClips.Contains(allSounds[i]))
                        OnDeathClips.Add(allSounds[i]);
                }
                else if (clipNameFields[1].ToLower() == "flees")
                {
                    if (!OnFleeClips.Contains(allSounds[i]))
                        OnFleeClips.Add(allSounds[i]);
                }
                else if (clipNameFields[1].ToLower() == "getshit")
                {
                    if (!OnHitClips.Contains(allSounds[i]))
                        OnHitClips.Add(allSounds[i]);
                }
                else if (clipNameFields[1].ToLower() == "lowhp")
                {
                    if (!OnLowHPClips.Contains(allSounds[i]))
                        OnLowHPClips.Add(allSounds[i]);
                }
                else if (clipNameFields[1].ToLower() == "meleeattack")
                {
                    if (!OnAttackingMeleeClips.Contains(allSounds[i]))
                        OnAttackingMeleeClips.Add(allSounds[i]);
                }
                else if (clipNameFields[1].ToLower() == "midcombat")
                {
                    if (!OnMiddleCombatClips.Contains(allSounds[i]))
                        OnMiddleCombatClips.Add(allSounds[i]);
                }
                else if (clipNameFields[1].ToLower() == "rangedattack")
                {
                    if (!OnAttackingRangedClips.Contains(allSounds[i]))
                        OnAttackingRangedClips.Add(allSounds[i]);
                }
                else if (clipNameFields[1].ToLower() == "robertdead")
                {
                    if (!OnRobertDiesClips.Contains(allSounds[i]))
                        OnRobertDiesClips.Add(allSounds[i]);
                }
                else if (clipNameFields[1].ToLower() == "robertishit")
                {
                    if (!OnRobertGetsHitClips.Contains(allSounds[i]))
                        OnRobertGetsHitClips.Add(allSounds[i]);
                }
                else if (clipNameFields[1].ToLower() == "roberthidden")
                {
                    // Add List
                }
                else if (clipNameFields[1].ToLower() == "robertismisses")
                {
                    if (!OnRobertMissesAttackClips.Contains(allSounds[i]))
                        OnRobertMissesAttackClips.Add(allSounds[i]);
                }
                else if (clipNameFields[1].ToLower() == "robertunconcious")
                {
                    if (!OnRobertUnconciousClips.Contains(allSounds[i]))
                        OnRobertUnconciousClips.Add(allSounds[i]);
                }
                else if (clipNameFields[1].ToLower() == "startcombat")
                {
                    if (!OnCombatStartedClips.Contains(allSounds[i]))
                        OnCombatStartedClips.Add(allSounds[i]);
                }
            }
        }
        _combatManager = CombatManager.Instance;
        //InvokeRepeating("TestSound",1.5f, 1.5f);
    }

    void TestSound()
    {
        EventManager.TriggerEvent("OnDeath");
    }

    void OnEnable()
    {
        EventManager.StartListening("OnAnotherEnemyDies", OnAnotherEnemyDies);
        EventManager.StartListening("OnDeath", OnDeath);
        EventManager.StartListening("OnFlee", OnFlee);
        EventManager.StartListening("OnHit", OnHit);
        EventManager.StartListening("OnLowHP", OnLowHP);
        EventManager.StartListening("OnAttackingMelee", OnAttackingMelee);
        EventManager.StartListening("OnAttackingRanged", OnAttackingRanged);
        EventManager.StartListening("OnMiddleCombat", OnMiddleCombat);
        EventManager.StartListening("OnRobertDies", OnRobertDies);
        EventManager.StartListening("OnRobertGetsHit", OnRobertGetsHit);
        EventManager.StartListening("OnRobertMissesAttack", OnRobertMissesAttack);
        EventManager.StartListening("OnRobertUnconcious", OnRobertUnconcious);
        EventManager.StartListening("OnCombatStarted", OnCombatStarted);
    }

    void OnDisable()
    {
        EventManager.StopListening("OnAnotherEnemyDies", OnAnotherEnemyDies);
        EventManager.StopListening("OnDeath", OnDeath);
        EventManager.StopListening("OnFlee", OnFlee);
        EventManager.StopListening("OnHit", OnHit);
        EventManager.StopListening("OnLowHP", OnLowHP);
        EventManager.StopListening("OnAttackingMelee", OnAttackingMelee);
        EventManager.StopListening("OnAttackingRanged", OnAttackingRanged);
        EventManager.StopListening("OnMiddleCombat", OnMiddleCombat);
        EventManager.StopListening("OnRobertDies", OnRobertDies);
        EventManager.StopListening("OnRobertGetsHit", OnRobertGetsHit);
        EventManager.StopListening("OnRobertMissesAttack", OnRobertMissesAttack);
        EventManager.StopListening("OnRobertUnconcious", OnRobertUnconcious);
        EventManager.StopListening("OnCombatStarted", OnCombatStarted);
    }

    void EndedPlayingClip()
    {
        _stillPlaying = false;
        if (_callBack != null)
            _callBack();

        _callBack = null;
        if (_combatManager.LevelBoss != null)
            _combatManager.LevelBoss.ShowSpeakingIcon(false);

        if (_needToResumeCombat)
            _combatManager.ResumeCombat();

        _combatManager.soundFxPlaying = false;
        //GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().pauseButton.interactable = true;
//        GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().setPauseBtnTint(true);

        EventManager.TriggerEvent("SoundFinalizedPlaying");
        if (_bossIntro) _bossIntro = false;
    }

    void PlaySound(AudioClip clip)
    {
        if (!_bossIntro)
            if (Random.Range(0.0f, 1.0f) >= 0.5f)
                return;

        if (clip == null) { if (_callBack != null) _callBack(); return; }

        //if (!_combatManager.packLeaderIsAlive) return;
        if (_combatManager.LevelBoss != null)
            _combatManager.LevelBoss.ShowSpeakingIcon(true);

        if (!_combatManager.pausedCombat)
            _combatManager.pausedForSound = _needToResumeCombat = true;
        else
            _combatManager.pausedForSound = false;

        _combatManager.PauseCombat();
//        GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().setPauseBtnTint();
        //GameObject.FindGameObjectWithTag("IngameGUIController").GetComponent<IngameGUIController>().pauseButton.interactable = false;

        _combatManager.soundFxPlaying = true;


        if (!_stillPlaying)
        {
            _stillPlaying = true;
            _source.PlayOneShot(clip);
            Invoke("EndedPlayingClip", clip.length);
        }
    }

    AudioClip GetClipFrom(ShuffleBag bag)
    {
        if (bag.Size > 0)
            return bag.Next();//clips[Random.Range(0, clips.Count)];
        else
            return null;
    }
    #region NewSoundsSystem

    public void PlaySoundForEvent(string forEvent, System.Action callBack)
    {
        _callBack = callBack;

        if (forEvent.Equals(SoundEventNames.OnAnotherEnemyDiesClips))
            PlaySound(GetClipFrom(OnAnotherEnemyDiesClips));
        else if (forEvent.Equals(SoundEventNames.OnDeathClips))
            PlaySound(GetClipFrom(OnDeathClips));
        else if (forEvent.Equals(SoundEventNames.OnFleeClips))
            PlaySound(GetClipFrom(OnFleeClips));
        else if (forEvent.Equals(SoundEventNames.OnHitClips))
            PlaySound(GetClipFrom(OnHitClips));
        else if (forEvent.Equals(SoundEventNames.OnLowHPClips))
            PlaySound(GetClipFrom(OnLowHPClips));
        else if (forEvent.Equals(SoundEventNames.OnAttackingMeleeClips))
            PlaySound(GetClipFrom(OnAttackingMeleeClips));
        else if (forEvent.Equals(SoundEventNames.OnAttackingRangedClips))
            PlaySound(GetClipFrom(OnAttackingRangedClips));
        else if (forEvent.Equals(SoundEventNames.OnMiddleCombatClips))
            PlaySound(GetClipFrom(OnMiddleCombatClips));
        else if (forEvent.Equals(SoundEventNames.OnRobertDiesClips))
            PlaySound(GetClipFrom(OnRobertDiesClips));
        else if (forEvent.Equals(SoundEventNames.OnRobertGetsHitClips))
            PlaySound(GetClipFrom(OnRobertGetsHitClips));
        else if (forEvent.Equals(SoundEventNames.OnRobertMissesAttackClips))
            PlaySound(GetClipFrom(OnRobertMissesAttackClips));
        else if (forEvent.Equals(SoundEventNames.OnRobertUnconciousClips))
            PlaySound(GetClipFrom(OnRobertUnconciousClips));
        else if (forEvent.Equals(SoundEventNames.OnCombatStartedClips))
            PlaySound(GetClipFrom(OnCombatStartedClips));

        _combatManager.debugPanel.SubmitText("Played Sound For Event: " + forEvent);
    }
    #endregion

    #region Events

    private void OnAnotherEnemyDies()
    {
        PlaySound(GetClipFrom(OnAnotherEnemyDiesClips));
    }
    private void OnDeath()
    {
        PlaySound(GetClipFrom(OnDeathClips));
    }
    private void OnFlee()
    {
        PlaySound(GetClipFrom(OnFleeClips));
    }
    private void OnHit()
    {
        PlaySound(GetClipFrom(OnHitClips));
    }
    private void OnLowHP()
    {
        PlaySound(GetClipFrom(OnLowHPClips));
    }
    private void OnAttackingMelee()
    {
        PlaySound(GetClipFrom(OnAttackingMeleeClips));
    }
    private void OnAttackingRanged()
    {
        PlaySound(GetClipFrom(OnAttackingRangedClips));
    }
    private void OnMiddleCombat()
    {
        PlaySound(GetClipFrom(OnMiddleCombatClips));
    }
    private void OnRobertDies()
    {
        PlaySound(GetClipFrom(OnRobertDiesClips));
    }
    private void OnRobertGetsHit()
    {
        PlaySound(GetClipFrom(OnRobertGetsHitClips));
    }
    private void OnRobertMissesAttack()
    {
        PlaySound(GetClipFrom(OnRobertMissesAttackClips));
    }
    private void OnRobertUnconcious()
    {
        PlaySound(GetClipFrom(OnRobertUnconciousClips));
    }
    private void OnCombatStarted()
    {
        _bossIntro = true;
        PlaySound(GetClipFrom(OnCombatStartedClips));
    }
    #endregion

}
