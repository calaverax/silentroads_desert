﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NPCSFXControl : MonoBehaviour
{
    AudioClip[] _maleSounds, _femaleSounds, _missSounds;

    // Estados (Trigger, Tick)
    Dictionary<string, AudioClip> _statesSounds = new Dictionary<string, AudioClip>();
    Dictionary<string, AudioClip> _eventsSounds = new Dictionary<string, AudioClip>();
    Dictionary<string, AudioClip> _enterEnemyFiles = new Dictionary<string, AudioClip>();
    /*
    // Events:
        Heavy damage
        Low hitpoints Robert
        Miss shot
        Sparky attack
        Robert Death
        Loot available
        Hide
        Target chest
        Target maniquí
    */
    AudioSource _maleSource, _femaleSource, _mixxedSource, _enterEnemySource;

    void Awake()
    {
        AddSource("AudioSource Female", out _femaleSource);
        AddSource("AudioSource Male", out _maleSource);
        AddSource("AudioSource States", out _mixxedSource);
        AddSource("AudioSource EnterEnemy", out _enterEnemySource);

        _enterEnemySource.volume = 0.1f;
        _maleSource.mute = _femaleSource.mute = false;
        _maleSource.volume = _femaleSource.volume = 0.8f;

        _maleSounds = Resources.LoadAll<AudioClip>("AudioClips/Characters/OnHitFX/Male");
        _femaleSounds = Resources.LoadAll<AudioClip>("AudioClips/Characters/OnHitFX/Female");
        _missSounds = Resources.LoadAll<AudioClip>("AudioClips/Characters/OnMissFX");  
    }

    public void EnemyBehaviourChange()
    {
        float rndValue = Random.Range(0.0f, 1.0f);
        AudioClip clip;

        if (rndValue <= 0.5f)
        {
            // Play One
            if (!(_enterEnemyFiles.ContainsKey("EnterEnemy1")))
                _enterEnemyFiles.Add("EnterEnemy1",Resources.Load<AudioClip>("AudioClips/Characters/OnEnterEnemy/EnterEnemy1"));

            clip = _enterEnemyFiles["EnterEnemy1"];

            Debug.Log("Playing sound EnterEnemy1");
        }
        else
        {
            // Random Between the others

            int otherRnd = Random.Range(2, 11);
            string fileName = "EnterEnemy" + otherRnd.ToString();

            if (!(_enterEnemyFiles.ContainsKey(fileName)))
                _enterEnemyFiles.Add(fileName, Resources.Load<AudioClip>("AudioClips/Characters/OnEnterEnemy/" + fileName));

            clip = _enterEnemyFiles[fileName];

            Debug.Log("Playing sound " + fileName);
        }

        _enterEnemySource.PlayOneShot(clip);
    }

    void AddSource(string name, out AudioSource source)
    {
        GameObject newObj = new GameObject(name, typeof(AudioSource));
        newObj.transform.SetParent(this.transform);
        source = newObj.GetComponent<AudioSource>();
    }

    public void PlayOnHitFx(BaseEnemy.Genre genre)
    {
        if (genre == BaseEnemy.Genre.MALE)
            _maleSource.PlayOneShot(_maleSounds[Random.Range(0, _maleSounds.Length)]);
        else
            _femaleSource.PlayOneShot(_femaleSounds[Random.Range(0, _femaleSounds.Length)]);
    }

    public void PlayMissFX()
    {
        _mixxedSource.PlayOneShot(_missSounds[Random.Range(0, _missSounds.Length)]);
    }

    public void PlayEventSound(string soundName)
    {
        if (!(_eventsSounds.ContainsKey(soundName)))
        {
            _eventsSounds.Add(soundName, Resources.Load<AudioClip>("AudioClips/EventsSounds/" + soundName));
        }

        if (_eventsSounds.ContainsKey(soundName))
        {
            _mixxedSource.PlayOneShot(_eventsSounds[soundName]);
        }
    }

    public void PlayStatesSound(string soundName)
    {
        if (!(_statesSounds.ContainsKey(soundName)))
        {
            _statesSounds.Add(soundName, Resources.Load<AudioClip>("AudioClips/StatesSounds/" + soundName));
        }

        if (_statesSounds.ContainsKey(soundName))
            _mixxedSource.PlayOneShot(_statesSounds[soundName]);
    }
}
