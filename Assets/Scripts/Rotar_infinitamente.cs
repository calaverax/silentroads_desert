﻿using UnityEngine;
using System.Collections;

public class Rotar_infinitamente : MonoBehaviour {

	
	// la velocidad (grados/sec)
	public int velocidadRot = 30;
	
	void Update () {
	
		transform.Rotate(0, 0, velocidadRot * Time.deltaTime);
	}
}