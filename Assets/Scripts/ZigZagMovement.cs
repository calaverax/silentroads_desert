﻿using UnityEngine;
using System;
using System.Collections;

public class ZigZagMovement : MonoBehaviour
{
    Transform _target;
    Vector3 _zigzag;
    float timer;
    public bool doZigZag = true;
    public bool isMoving = true;
    public float moveSpeed = 180;

    Action _callBack;

    public void moveToTarget(Transform target, Action callBack)
    {
        _target = target;
        _callBack = callBack;
        doZigZag = true;
        isMoving = true;
    }

    void Update()
    {
        if (doZigZag)
        {
            timer += Time.deltaTime;

            if (timer < 2)
                _zigzag = UnityEngine.Random.Range(0, 30) * Vector3.up;//target.transform.up;
            if (timer >= 2 && timer < 4)
                _zigzag = (UnityEngine.Random.Range(0, 30) * -1) * Vector3.up;
            if (timer > 4)
                timer = 0;

            transform.position += _zigzag * Time.deltaTime;
        }

        if (isMoving)
            moveTowards();
        
    }

    private void moveTowards()
    {
        transform.position = Vector3.MoveTowards(transform.position, _target.localPosition, Time.deltaTime * moveSpeed);

        float distanceSquared = (transform.position - _target.localPosition).sqrMagnitude;

        if (distanceSquared < 150 * 150)
            doZigZag = false;
        if (distanceSquared < 15 * 15)
        {
            isMoving = false;
            if (_callBack != null)
                _callBack();
        }
    }
}
