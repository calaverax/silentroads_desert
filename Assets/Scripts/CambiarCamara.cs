﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using System.Linq;

public class CambiarCamara : MonoBehaviour {


    /* el titulo del script es engañoso: lo que hace es asegurarse de que la cámara tenga 2 color correction curves para hacer el efecto atardecer y el efecto noche. Este script suele estar conectado a la gui; basta darle reset para que ande en el editor */
    [SerializeField]
    GameObject cam;
    [SerializeField]
    List <ColorCorrectionCurves> componentes;
    //public GameObject camara;

    [ExecuteInEditMode]
	// Use this for initialization
	void Reset () {
        cam = GameObject.FindGameObjectWithTag("MainCamera");
        componentes = cam.GetComponents<ColorCorrectionCurves>().ToList();
        if (componentes.Count < 2)
        {componentes.Add(cam.AddComponent<ColorCorrectionCurves>());
        if (componentes.Count < 2) { componentes.Add(cam.AddComponent<ColorCorrectionCurves>()); }
        foreach (ColorCorrectionCurves componente in componentes)
        { componente.enabled = false; }
        }
        //camara = Instantiate(Resources.Load("Camera")) as GameObject;
        //camara.transform.position = cam.transform.position;

		
	}


}
