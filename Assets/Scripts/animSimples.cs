﻿using UnityEngine;
using System.Collections;

public class animSimples : MonoBehaviour {
	
	
	public string miBooleana = "disparo";
	public string nombreAnimacion = "Tiro";
	public string nombreTecla = "space";
	private Animator anim;
	
	void Start()
	{
		anim = GetComponent<Animator>();
	}
	
	void Update ()
	{
	
		if (Input.GetKeyDown(nombreTecla))
			{
				anim.SetBool(miBooleana, true);
			
			}
			
			if (anim.GetCurrentAnimatorStateInfo(0).IsName(nombreAnimacion) && anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !anim.IsInTransition(0))
				{anim.SetBool (miBooleana, false);}
			
			
			/*if (anim.GetCurrentAnimatorStateInfo(0).IsName("tiro"))
			{
				anim.SetBool("disparo",false);
				Debug.Log ("Listo");
			}
			*/
		
	}
}	