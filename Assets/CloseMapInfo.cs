﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloseMapInfo : MonoBehaviour {

    [SerializeField]
    private bool firstTime = true;
    [SerializeField]
    private GameObject combatMode;
    TarjetasEnemigos combatScript;
    public GameObject GUI;
    public Toggle boton;


	// Use this for initialization
	void Start () {
        GUI = GameObject.FindGameObjectWithTag("IngameGUIController");
        combatMode = GUI.transform.GetChild(1).gameObject;
        this.gameObject.GetComponent<Button>().onClick.AddListener(TaskOnClick);
        combatScript = combatMode.GetComponent<TarjetasEnemigos>();
        boton = GUI.transform.Find("BottomBar/ActionButtons/Map Info").gameObject.GetComponent<Toggle>();
	}

    void TaskOnClick()
    {
       boton.isOn = false;
       // Debug.Log("OnClick está andando");
        if (firstTime)
        {
            if (!GameManager.Instance.isLevelWon(Application.loadedLevelName))
            {
                if (CombatManager.Instance.EnemiesGroups.Count != 0)
                {
                    combatMode.SetActive(true);
                    combatScript.Esperar();
                }
            }
            firstTime = false;

            
        }

    }
	
}
