﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FocusImage : MonoBehaviour {

    public GameObject[] imagenes;
    public Slider _slider;
    private int _imagenAnterior;

    void Start()
    {
        _imagenAnterior = 0;
    }

    public void CambiarImagen () {
        //imagenes[_imagenAnterior].SetActive(false);
        Debug.Log("CAMBIADO FOCUS IMAGE A : " + ((int)_slider.value - 1));
        for(int i = 0; i < imagenes.Length; i++)
        {
            imagenes[i].SetActive(false);
        }
        imagenes[(int)_slider.value - 1].SetActive(true);
        _imagenAnterior = (int)_slider.value - 1;
    }

}
