﻿using UnityEngine;
using System.Collections;

public class CharacterCustomizedSprite : MonoBehaviour
{
    public CharacterCustomizationManager manager;

    GameObject torsoTall, torsoShort;

    SpriteRenderer _torso, _arms, _head, _legs;
    bool _isAshortGuy = false;

    void Awake()
    {
        //Sprite[] sprites = Resources.LoadAll<Sprite>("CostumizableAgentSprites/PackedAgent1");
        /*
        Sprite temp = Resources.Load<Sprite>("CostumizableAgentSprites/PackedAgent1");
        */

        if (manager == null)
            manager = GameObject.FindWithTag("Level").GetComponent<CharacterCustomizationManager>();
        torsoTall = transform.Find("Torso_Tall").gameObject;
        torsoShort = transform.Find("Torso_Short").gameObject;
        _legs = transform.Find("Legs").GetComponent<SpriteRenderer>();

        Init(Random.Range(0.0f, 1.0f) > 0.5 ? true : false); 
        Debug.Log("");
    }

    void Init(bool isAshortGuy)
    {
        torsoTall.SetActive(!isAshortGuy);
        torsoShort.SetActive(isAshortGuy);

        if (isAshortGuy)
        {
            _torso = torsoShort.GetComponent<SpriteRenderer>();
            _head = torsoShort.transform.Find("Head").GetComponent<SpriteRenderer>();
            _arms = torsoShort.transform.Find("Arms").GetComponent<SpriteRenderer>();
        }
        else
        {
            _torso = torsoShort.GetComponent<SpriteRenderer>();
            _head = torsoShort.transform.Find("Head").GetComponent<SpriteRenderer>();
            _arms = torsoShort.transform.Find("Arms").GetComponent<SpriteRenderer>();
        }

        float color = Random.Range(0.3f, 1.0f);

        _torso.color = new Color(color, color, color, 1);
        _legs.color = new Color(color, color, color, 1);
        _arms.color = new Color(color, color, color, 1);

        _torso.sprite = manager.GetTorsoSprite(isAshortGuy);
        _head.sprite = manager.GetHeadSprite(isAshortGuy);
        _arms.sprite = manager.GetArmsSprite();
        _legs.sprite = manager.GetLegsSprite();

    }

}
