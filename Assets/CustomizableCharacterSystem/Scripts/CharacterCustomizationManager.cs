﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterCustomizationManager : MonoBehaviour
{
    Dictionary<string, List<Sprite>> atlases = new Dictionary<string, List<Sprite>>();

    public void LoadAtlases(string atPath)
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>(atPath);

        for (int i = 0; i < sprites.Length; i++)
        {
            // Si nuestro diccionario posee un entry para cierta textura
            if (atlases.ContainsKey(sprites[i].texture.name))
            {
                // buscamos si ese entry tiene el sprite actual, sino lo contiene, lo agregamos.
                if (!(atlases[sprites[i].texture.name].Contains(sprites[i])))
                    atlases[sprites[i].texture.name].Add(sprites[i]);
            }
            else // Si el diccionario aun no tiene un entry para la textura lo agregamos
            {
                List<Sprite> list = new List<Sprite>();
                list.Add(sprites[i]);

                atlases.Add(sprites[i].texture.name, list);
            }
        }
    }


    public Sprite GetHeadSprite(bool isShortCharacter)
    {
        List<Sprite> acordingSprites = new List<Sprite>();

        foreach (Sprite sprite in atlases["PackedAgent"])
        {
            if (sprite.name.IndexOf("Head") > -1)
                acordingSprites.Add(sprite);
        }

        if (acordingSprites.Count > 1)
            return acordingSprites[Random.Range(0, acordingSprites.Count)];
        else
            return acordingSprites[0];
    }

    public Sprite GetTorsoSprite(bool isShortCharacter)
    {
        List<Sprite> acordingSprites = new List<Sprite>();

        foreach (Sprite sprite in atlases["PackedAgent"])
        {
            if (sprite.name.IndexOf("Torso") > -1)
            {
                if (isShortCharacter)
                {
                    if (sprite.name.IndexOf("Short") > -1)
                        acordingSprites.Add(sprite);
                }
                else
                {
                    if (sprite.name.IndexOf("Short") < 0)
                        acordingSprites.Add(sprite);    
                }
            }
        }

        if (acordingSprites.Count > 1)
            return acordingSprites[Random.Range(0, acordingSprites.Count)];
        else
            return acordingSprites[0];

    }

    public Sprite GetLegsSprite()
    {
        List<Sprite> acordingSprites = new List<Sprite>();

        foreach (Sprite sprite in atlases["PackedAgent"])
        {
            if (sprite.name.IndexOf("Legs") > -1)
                acordingSprites.Add(sprite);
        }

        if (acordingSprites.Count > 1)
            return acordingSprites[Random.Range(0, acordingSprites.Count)];
        else
            return acordingSprites[0];
    }

    public Sprite GetArmsSprite()
    {
        List<Sprite> acordingSprites = new List<Sprite>();

        foreach (Sprite sprite in atlases["PackedAgent"])
        {
            if (sprite.name.IndexOf("Arms") > -1)
                acordingSprites.Add(sprite);
        }

        if (acordingSprites.Count > 1)
            return acordingSprites[Random.Range(0, acordingSprites.Count)];
        else
            return acordingSprites[0];
    }


    void Awake()
    {
        //Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/MaleAgent1/PackedAgent1");

        //Texture2D[] textures = Resources.LoadAll<Texture2D>("CostumizableAgentSprites/");

        Sprite[] atlases = Resources.LoadAll<Sprite>("Sprites/MaleAgent 1/");

        LoadAtlases("Sprites/MaleAgent 1");
        //Sprite sprite = Sprite.Create(textures[0],textures[0].s)

        //Debug.Log("");
    }

}
