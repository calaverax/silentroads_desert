﻿using UnityEngine;

[ExecuteInEditMode]
public class FOWSpriteScript : MonoBehaviour {
	public Canvas canvas;
	public Material FOWSpriteMaterial;

	public void Start () {
		var canvasRectTransform = canvas.GetComponent<RectTransform>();
		float screenAspect = canvasRectTransform.rect.width / canvasRectTransform.rect.height;

		FOWSpriteMaterial.SetFloat( "_ScreenAspect", screenAspect );
	}
}
