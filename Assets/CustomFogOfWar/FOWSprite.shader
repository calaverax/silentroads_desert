﻿Shader "Sprites/Fog of War"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
		_FogOfWarColor("Fog Of War Color", Color) = (.5,.5,.5,.5)
		[HideInInspector] _ScreenAspect("Screen Aspect", Float) = 1
		_LightRangeInv("Light Range Inverted", Float) = 10
		_FOWLight0("FOW Light 0 : Position (XY), Range Inverted(Z)", Vector) = (0,0,1,0)
		_FOWLight1("FOW Light 1 : Position (XY), Range Inverted(Z)", Vector) = (0,0,1,0)
		_FOWLight2("FOW Light 2 : Position (XY), Range Inverted(Z)", Vector) = (0,0,1,0)
		_FOWLight3("FOW Light 3 : Position (XY), Range Inverted(Z)", Vector) = (0,0,1,0)
		_FOWLight4("FOW Light 4 : Position (XY), Range Inverted(Z)", Vector) = (0,0,1,0)
		_FOWLight5("FOW Light 5 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
		_FOWLight6("FOW Light 6 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
		_FOWLight7("FOW Light 7 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
		_FOWLight8("FOW Light 8 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
		_FOWLight9("FOW Light 9 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
		_FOWLight10("FOW Light 10 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
		_FOWLight11("FOW Light 11 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
		_FOWLight12("FOW Light 12 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
		_FOWLight13("FOW Light 13 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
		_FOWLight14("FOW Light 14 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
		_FOWLight15("FOW Light 15 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
		_FOWLight16("FOW Light 16 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
		_FOWLight17("FOW Light 17 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
		_FOWLight18("FOW Light 18 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
		_FOWLight19("FOW Light 19 : Position (XY), Range Inverted(Z)", Vector) = (0,0,10000,0)
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				half2 uniformTexcoord  : TEXCOORD1;
			};

			sampler2D _MainTex;
			
			fixed4 _Color;
			fixed4 _FogOfWarColor;

			half _ScreenAspect;

			half _LightRangeInv;

			half4 _FOWLight0, _FOWLight1, _FOWLight2, _FOWLight3, _FOWLight4,
					_FOWLight5, _FOWLight6, _FOWLight7, _FOWLight8, _FOWLight9,
					_FOWLight10, _FOWLight11, _FOWLight12, _FOWLight13, _FOWLight14,
					_FOWLight15, _FOWLight16, _FOWLight17, _FOWLight18, _FOWLight19;


			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord = IN.texcoord;
				
				OUT.uniformTexcoord = IN.texcoord;
				OUT.uniformTexcoord.x = OUT.uniformTexcoord.x * 2 - 1;
				OUT.uniformTexcoord.x *= _ScreenAspect;
				OUT.uniformTexcoord.x = OUT.uniformTexcoord.x * .5 + .5;

				OUT.color = IN.color * _Color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

#define APPLY_FOW_LIGHT(light) \
distVector = light.xy - IN.uniformTexcoord; \
fowFactor = min(fowFactor, dot(distVector, distVector) * light.z);

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = tex2D(_MainTex, IN.texcoord) * IN.color;
				c.rgb *= c.a;

				fixed3 fowColor = lerp(c, _FogOfWarColor.rgb, _FogOfWarColor.a);

				half fowFactor = 1 / _LightRangeInv;
				half2 distVector;

				APPLY_FOW_LIGHT(_FOWLight0);
				APPLY_FOW_LIGHT(_FOWLight1);
				APPLY_FOW_LIGHT(_FOWLight2);
				APPLY_FOW_LIGHT(_FOWLight3);
				APPLY_FOW_LIGHT(_FOWLight4);

				APPLY_FOW_LIGHT(_FOWLight5);
				APPLY_FOW_LIGHT(_FOWLight6);
				APPLY_FOW_LIGHT(_FOWLight7);
				APPLY_FOW_LIGHT(_FOWLight8);
				APPLY_FOW_LIGHT(_FOWLight9);

				APPLY_FOW_LIGHT(_FOWLight10);
				APPLY_FOW_LIGHT(_FOWLight11);
				APPLY_FOW_LIGHT(_FOWLight12);
				APPLY_FOW_LIGHT(_FOWLight13);
				APPLY_FOW_LIGHT(_FOWLight14);

				APPLY_FOW_LIGHT(_FOWLight15);
				APPLY_FOW_LIGHT(_FOWLight16);
				APPLY_FOW_LIGHT(_FOWLight17);
				APPLY_FOW_LIGHT(_FOWLight18);
				APPLY_FOW_LIGHT(_FOWLight19);

				fowFactor *= _LightRangeInv;

				//Sharp falloff
				//fowFactor = pow(fowFactor, 2);
				fowFactor *= fowFactor;

				fowFactor = saturate(fowFactor);
#pragma target 3.0
				c.rgb = lerp(c.rgb, fowColor, fowFactor);

				return c;
			}
		ENDCG
		}
	}
}