﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApagarCamara : MonoBehaviour {

    public Camera camara;
    public Animator anim;
    public Animator anim2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ApagarCam() {

        camara = Camera.current;
        camara.enabled = false;
    }

    public void ArrancarAnim() {
        anim.SetBool("start", true);
        anim2.SetBool("start", true);
    }
    

}
