﻿using UnityEngine;
using System.Collections;

public class TheLongRoadSunPositioneer : MonoBehaviour {

    [SerializeField] Transform _sun;
    [SerializeField] RectTransform _container;

	// Use this for initialization
	void Start ()
    {
        float PositionX = Random.RandomRange(0, _container.sizeDelta.x);
        float PositionY = Random.RandomRange(0, _container.sizeDelta.y) * -1;

        Debug.Log("Position X: " +PositionX + " Position Y: " + PositionY);

        _sun.localPosition = new Vector2(PositionX, PositionY);
	}
	
}
