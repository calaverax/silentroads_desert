﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class IntroMovieController : MonoBehaviour {

    public MovieTexture _movieTexture;
    private AudioSource _audioSource;

    // Use this for initialization
    void Start()
    {
        GetComponent<RawImage>().texture = _movieTexture as MovieTexture;
        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = _movieTexture.audioClip;
        _movieTexture.Play();
        _audioSource.Play();
        Invoke("ReconfigureStuff", _movieTexture.duration - 0.2f);
        
    }

    void ReconfigureStuff()
    {
        Debug.Log("Reconfiguring Sruff");
        GetComponent<RawImage>().color = Color.black;
        ScenesLoader.Instance.LoadScene("mainMenu", false, false, false);
    }

}
