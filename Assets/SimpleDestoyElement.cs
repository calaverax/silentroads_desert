﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SimpleDestoyElement : MonoBehaviour {

    string _stateName;
    AbnormalStatusPanel _parent;

    public void configure(string stateName, AbnormalStatusPanel parent, Sprite sprite)
    {
        this.gameObject.name = stateName;
        _stateName = stateName;
        _parent = parent;
        GetComponent<Image>().overrideSprite = sprite;
    }

    public void DestroyMe()
    {
        // Avisar a la animacion del padre que se termino esto.
        _parent.addState(_stateName);

        Destroy(this.gameObject);
        Debug.Log("SHOULD DESTROY THIS FUCKING SHIT!");
    }
}
