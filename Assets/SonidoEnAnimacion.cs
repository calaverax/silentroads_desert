﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoEnAnimacion : MonoBehaviour
{
    public AudioClip boomSound;
    public GameObject mainCamera;
    public bool rastro = false;


    void Start()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
    }
    void playBoomSound()
    {

        AudioSource.PlayClipAtPoint(boomSound, mainCamera.transform.position, 0.75f); //PPP 0.75 es el volumen, ponele el que quieras.
        rastro = true;

    }

}
