﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoAnimacion : MonoBehaviour {
    
    public float duracion = 1.4f;
    public Animator anim;
    public GameObject combatMode;



	void OnEnable () {
		StartCoroutine(Deshabilitar());
	}

 
     IEnumerator Deshabilitar(){
         yield return new WaitForSeconds(duracion);
         combatMode.GetComponent<TarjetasEnemigos>().SwitchChilds(true);
         gameObject.SetActive(false);
         

     }

}
