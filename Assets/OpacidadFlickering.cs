﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpacidadFlickering : MonoBehaviour {

    private Material mat; //Referencia al material del objeto, se asigna solo.
    private float valor; //Este es un valor para estabilizar un poco el Random (una curva de gauss pedorra)
    private float estabilizador;
    public float range1 = 0f; 
    public float range2 = 1f;
    public float frecuencia =0.5f;




	void Start () {

        mat = GetComponent<Renderer>().material;
        InvokeRepeating("CambiarOpacidad", 0, frecuencia);



	}
	

	void CambiarOpacidad () {
        
        estabilizador = valor;
        valor = Random.Range (range1,range2);
        mat.SetColor("_Color", new Color (1,1,1, estabilizador *valor));
	}
}
