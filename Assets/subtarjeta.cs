﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class subtarjeta : MonoBehaviour {
    public Image image;
    public int count= 100;
	// Use this for initialization
	void Start () {
		image = transform.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        if (image.enabled){
            //image.material.color.a--;
            image.material.color -= new Color(0, 0, 0, .01f);
            count--;
            if (count < 1){
                count =100; 
                image.material.color = new Color(1, 1, 1, 1); 
                image.enabled = false;
            }

        }
	}
}
