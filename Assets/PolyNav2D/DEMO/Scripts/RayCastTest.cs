﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class RayCastTest : MonoBehaviour, IPointerClickHandler
{
    private PolyNavAgent _agent;

    public PolyNavAgent agent
    {
        get
        {
            if (!_agent)
                _agent = CombatManager.Instance.robert.GetComponent<PolyNavAgent>();
            return _agent;
        }
    }
    Vector3 mousePos = new Vector3(0, 0, 0);

    void Start()
    {
        agent.OnDestinationReached += RobertDestinationReached;
    }

    void RobertDestinationReached()
    {
        CombatManager.Instance.OnCharacterReachedNavigationPoint.Invoke(CombatManager.Instance.robert);
        Debug.Log("<color=red> Robert Reached Dest </color>");
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!CombatManager.Instance.pausedCombat)
        {
            if (CombatManager.Instance.robert.isHidden)
                CombatManager.Instance.robert.revealFromHidden();

            Debug.Log("<color=red> Clicked On Level </color>");
            if ((agent != null) && (!eventData.dragging))
            {
                Vector2 destination = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (destination.x < agent.position.x)
                {
                    if (destination.y > agent.position.y)
                        Debug.Log("<color=red> Point To Left Up </color>");
                    else
                        Debug.Log("<color=red> Point To Left Down </color>");
                }
                else
                {
                    if (destination.y > agent.position.y)
                        Debug.Log("<color=red> Point To Right Up </color>");
                    else
                        Debug.Log("<color=red> Point To Right Down </color>");
                }

                agent.SetDestination(destination);
            }
        }
    }
}
