using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

//example
[RequireComponent(typeof(PolyNavAgent))]
public class ClickToMove : MonoBehaviour{
	
	private PolyNavAgent _agent;
	public PolyNavAgent agent{
		get
		{
			if (!_agent)
				_agent = GetComponent<PolyNavAgent>();
			return _agent;			
		}
	}

    public void clickTriggered()
    {
        Debug.Log("CLICKED AGENT");
        agent.SetDestination(Camera.main.ScreenToWorldPoint(Input.mousePosition));
    }
    
	void Update() {
        if (Input.GetMouseButton(0))
        {
			agent.SetDestination( Camera.main.ScreenToWorldPoint(Input.mousePosition) );

            //RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -1000)), new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1000));
            RaycastHit2D hit = Physics2D.Raycast(Input.mousePosition, Input.mousePosition - Camera.main.ScreenToWorldPoint(Input.mousePosition), Mathf.Infinity);
            if (hit.collider != null)
            {
                Debug.Log("Clicked Object: " + hit.collider.gameObject.name);
            }

            
            //if (EventSystem.current.IsPointerOverGameObject())
              //  Debug.Log(EventSystem.current.gameObject.name);
               
        }
	}
    
}