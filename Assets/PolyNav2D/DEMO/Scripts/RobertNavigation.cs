using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

//example
//[RequireComponent(typeof(PolyNavAgent))]
public class RobertNavigation : MonoBehaviour, IPointerClickHandler
{

    private PolyNavAgent _agent;
	public PolyNavAgent agent{
		get
		{
            if (!_agent)
            {
                _agent = CombatManager.Instance.robert.GetComponent<PolyNavAgent>();
            }
			return _agent;			
		}
	}


    public void MoveTo()
    {
        agent.SetDestination(Camera.main.ScreenToWorldPoint(Input.mousePosition));
    }

    void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            Debug.Log(EventSystem.current.gameObject.name);

        RaycastHit2D hit = Physics2D.Raycast(Input.mousePosition, Input.mousePosition - Camera.main.ScreenToWorldPoint(Input.mousePosition), Mathf.Infinity);
        if (hit.collider != null)
        {
            Debug.Log("Clicked Object: " + hit.collider.gameObject.name);
        }

        //throw new NotImplementedException();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        throw new NotImplementedException();
    }

    /*
	void Update() {
        if (Input.GetMouseButton(0))
        {
			agent.SetDestination( Camera.main.ScreenToWorldPoint(Input.mousePosition) );

            if (EventSystem.current.IsPointerOverGameObject())
                Debug.Log(EventSystem.current.gameObject.name);
        }
	}
    */
}