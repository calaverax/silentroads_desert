﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class rastroBala : MonoBehaviour
{

    public Transform objetivo; //El transform del enemigo
    public Transform pos1; //El transform del que queremos que parta el rayo.
    public float smoothing = 6f;  //Esto controla la velocidad del desplazamiento de la bala.

    public bool activar; //Esta es una bool que se usa para activar el efecto desde otro script (Disparar IK).
    public GameObject particulas; //Este es un sistema de partículas child de este objeto.
    public GameObject[] impactos; //Estos son las partículas cuando impacta el disparo con algo.
    public int impactoNum;
    public bool isNotRobert; //Esta booleana sirve para dar vuelta la dirección del impacto si es robert, así la sangre no sale hacia adelante.

    private Transform robertTransform;

    void Start()
    {
        particulas = this.gameObject.transform.GetChild(0).gameObject;
        particulas.SetActive(false);
        objetivo = GameObject.Find("Objetivo").transform;
        //robertTransform = pos1 = GameObject.Find("rob_aujereado_2:Robert1_RightHand").transform;
        pos1 = GameObject.Find("rob_aujereado_2:Robert1_RightHand").transform;
    }

    public void ConfigureFromTo(Transform origin, Transform objetive)
    {
        if (origin == null)
            pos1 = robertTransform;
        else
            pos1 = origin;

        if (objetive == null)
            objetive = robertTransform;
        else
            objetivo = objetive;
    }

    void Update()
    {
        
        if (activar){//Cuando se activa
            particulas.SetActive(true);
            StartCoroutine(MyCoroutine(objetivo,pos1.position)); //arrancá con la corutina que hace el lerp del rayo.

        }

        /*
        if (particulas.ParticleSystem.IsAlive() == false){//Si las partículas ya terminaron
            gameObject.SetActive(false);//Hacé que el objeto se apague
        } */

    }



    IEnumerator MyCoroutine(Transform objetivo, Vector3 ps1){
        transform.position = ps1;
        
        while ((transform.position - objetivo.position).magnitude > 1f){
            
            transform.position = Vector3.Lerp(transform.position, objetivo.position, smoothing * Time.deltaTime);
            
            yield return null;
        }
        GameObject particulaInstancia;
        /*
        Quaternion quater = pos1.rotation;
        quater.SetLookRotation(objetivo.position, transform.up);
        //Quaternion quater = Quaternion.FromToRotation(Vector3.up, objetivo.position - pos1);
        if (isNotRobert) { Quaternion.Inverse(quater); }
        particulaInstancia = Instantiate(impactos[impactoNum], objetivo.position, quater) as GameObject;
        */


        if (isNotRobert){

            Quaternion quat = Quaternion.identity;
            quat.eulerAngles = new Vector3(0, 180, 0);
            particulaInstancia = Instantiate(impactos[impactoNum], objetivo.position, quat) as GameObject;
        }
        else{
            //particulaInstancia = Instantiate(impactos[impactoNum], objetivo.position, Quaternion.identity) as GameObject;
        }

        activar = false;
        yield return new WaitForSeconds(1);
        particulas.SetActive(false);

    }
}
