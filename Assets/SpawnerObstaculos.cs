﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerObstaculos : MonoBehaviour {
    
    /*
     Este script sirve para espawnear obstáculos de dos tipos: rocas y cajas.
     Elige entre una y otra al azar, y también entre una tercera posibilidad: la de no espawnear nada.
     */


    public float posibilidad = 5f; //Subilo para hacer más improbable el espawneo. Acordate que el valor más alto queda fuera.
    public GameObject[] caja; //Array para poner los prefabs de las cajas que quieras que se puedan espawnear.
    public GameObject[] roca;//Array para poner los prefabs de las rocas que quieras que se puedan espawnear.
    
    private float numero; //El número que efectivamente sale.


	void Start () {
        
        numero = Random.Range(0f, posibilidad);//El número es una posibilidad entre 0 y el valor definido en el inspector
        //Debug.Log("número random = " + numero); //Debug para ver el número que salió en el sorteo

        if (numero < 1){ //Si la probabilidad es mayor a 1, no se espawnea nada, mientras que si es menor:


            //Tiramos una moneda, si sale cara...
            if (Random.Range(0,2) < 1f){
                //Espawneá cajas
                GameObject miCaja = GameObject.Instantiate(caja [Random.Range(0, caja.Length-1)] );
                miCaja.transform.position = this.transform.position; //ponelas en la posición del espawner.
                //Quiero que la caja y su primer child estén en el layer "Obstacles"
                miCaja.layer = 10;
                miCaja.transform.GetChild(0).gameObject.layer = 10;
                
            }

            //si sale seca...
            else{
                //espawneá Espawnea rocas
                GameObject miRoca = GameObject.Instantiate(roca[Random.Range(0, roca.Length)]);
                miRoca.transform.position = this.transform.position; //ponelas en la posición del espawner.
                //Quiero que la roca y su primer child estén en el layer "Obstacles"
                miRoca.layer = 10;
                miRoca.transform.GetChild(0).gameObject.layer = 10;
            }
   
        }

	}
	

}
