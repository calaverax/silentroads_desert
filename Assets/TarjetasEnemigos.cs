﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TarjetasEnemigos : MonoBehaviour {


    public Object tarjetaPrefab; 
    List<GameObject> tarjetas = new List<GameObject>();
    public GameObject tarjetaActiva;
    GameObject marco;
    private RectTransform marcoTrans;
    Canvas marcoCanvas;
    Animator anim;
    public bool inicializado;
    public bool freeze = false;
    RectTransform tarjetaTransf;
    public bool scroll = false;
    public float finalMarco;
    public Animation animacion;
    public GameObject HUD;
    public float scaleDesplegada = 0.075f;
    public float scaleColapsada = 0.065f;
    public float variable;
    public Coroutine corutina;
    public int childCount;
    private EnemyCardCM tarjetaScript;

    public Text hud_distance;
    public Text hud_weapon;




    void Start() {
        Debug.Log("Arranca el combatMode;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;");
        
        /*
        bool noCombat = false;

        
        if (GameManager.Instance.isLevelWon(Application.loadedLevelName) && CombatManager.Instance.NumEnemigos() == 0)
        {
            noCombat = true;
        }
        if (noCombat)
        {
            HUD.SetActive(false);
            gameObject.SetActive(false);
        }
        else
        {
         */
            marco = this.transform.GetChild(2).gameObject;
            marcoCanvas = marco.GetComponent<Canvas>();
            marcoCanvas.overrideSorting = true;
            marcoCanvas.sortingOrder = 1;
            marcoTrans = this.transform.GetChild(1).gameObject.GetComponent<RectTransform>();
            //ActualizarNumTarjetas();            
            HUD.SetActive(true);
            HUD.transform.parent.SetParent(null);

            anim = this.GetComponent<Animator>();

            CombatManager.Instance.OnCombatEnded.AddListener(Close);







    }

    /*
    void OnDisable() { 
        CombatManager.Instance.OnCombatEnded.RemoveListener(Close); 
    }
    */


    void Update() {
        if (Input.GetAxis("Mouse ScrollWheel") > 0 ) { anim.SetBool("cerrar", true); }
        if (Input.GetAxis("Mouse ScrollWheel") < 0 && tarjetas.Count > 0 ) { anim.SetBool("cerrar", false); }

        //if (Input.GetKeyDown(KeyCode.N)){ActualizarNumTarjetas();}

        if (tarjetaActiva != null && Input.mousePosition.y > Screen.height * 0.6 && Input.mousePosition.y < Screen.height * 0.9 && !freeze && scroll)
        {
            float value = marcoTrans.anchoredPosition.x - (Input.mousePosition.x - Screen.width / 2) / 100;

            

            if (childCount > 4)
                //if (Screen.width < finalMarco)
                {

                    //Debug.Log(Screen.width + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + finalMarco);
                    marcoTrans.anchoredPosition = new Vector2(
                        Mathf.Clamp(value, -finalMarco/2, 0),

                    marcoTrans.anchoredPosition.y
                    );

                }
                else if (childCount >3) {
                    //Debug.Log(Screen.width + "***********************************************************************" + finalMarco);
                    marcoTrans.anchoredPosition = new Vector2(Mathf.Clamp(value, Mathf.Clamp(value, -finalMarco/3, 0), 0),marcoTrans.anchoredPosition.y);

                }
            
            if (tarjetaActiva)
            {

                hud_distance.text = "DIST > " + tarjetaScript.distancia + "MTS.";
                hud_weapon.text = "" + tarjetaScript.arma;
            }
            else
            {
                hud_distance.text = "select a target";
                hud_weapon.text = "";
            }
            




        }



    }

    public void Esperar() {
        StartCoroutine(Esperar(2.38f, true));

    }



    public IEnumerator Esperar (float tiempo, bool open){
        yield return new WaitForSeconds(tiempo);
        if (open) {
        }
        else { this.gameObject.SetActive(false); }
    }
    
    //Esta se llama al principio para crear todas las tarjetas y el espacio en que se despliegan.
    public GameObject NuevaTarjeta(BaseEnemy enemy) 
    {

        float scale = 0.065f;
        GameObject elemento = Instantiate(tarjetaPrefab) as GameObject;
        EnemyCardCM script;


        elemento.transform.SetParent(gameObject.transform.GetChild(1));
        elemento.transform.localScale = new Vector3 (scale, scale, scale);
        
        script = elemento.GetComponent<EnemyCardCM>();
        script.scriptDelMenu = this;
        script._enemy = enemy.GetComponent<BaseEnemy>();
        script.ActivateWindow(enemy);

        Canvas canvas = elemento.transform.GetChild(0).GetComponent<Canvas>();
        canvas.overrideSorting = true;
        canvas.sortingOrder = 1;
        


        tarjetas.Add(elemento);
        return elemento;
        
    }



    public void Activar (GameObject tarjeta)
    {
        if (tarjeta == null) { return; } 
        if (tarjetaActiva == tarjeta)
        { return; }

        else if (tarjetaActiva != null)
        { Desactivar(tarjetaActiva); }



        tarjetaActiva = tarjeta;
        tarjetaTransf = tarjetaActiva.GetComponent<RectTransform>();
        tarjetaScript = tarjetaActiva.GetComponent<EnemyCardCM>();
        tarjetaScript._enemy.soyTarjetaActiva = true;
  




        Transform canvas = tarjeta.transform.GetChild(0);
        GameObject selector = canvas.GetChild(0).gameObject;
        GameObject boton = canvas.GetChild(6).gameObject;
        GameObject capaNegra = boton.transform.GetChild(2).gameObject;
        capaNegra.SetActive(false);
        Canvas canv = canvas.gameObject.GetComponent<Canvas>();
        canv.overrideSorting = true;
        canv.sortingOrder = 2;
        selector.SetActive(true);
        tarjetaScript.panel.SetActive(true);

        //La corrutina para desplegar la tarjeta
        if (tarjetaScript.corutina != null) { StopCoroutine(tarjetaScript.corutina); } //si está corriendo una corrutina, frenala.

        tarjetaScript.corutina = StartCoroutine(tarjetaScript.Desplegar(tarjeta, tarjeta.transform.localScale,
            new Vector3(scaleDesplegada, scaleDesplegada, scaleDesplegada), 0.25f));


        

            



        
    }

    public void Desactivar(GameObject tarjeta) 
    {


            Transform canvas = tarjeta.transform.GetChild(0);
            GameObject boton = canvas.GetChild(6).gameObject;
            GameObject selector = canvas.GetChild(0).gameObject;
            GameObject capaNegra = boton.transform.GetChild(2).gameObject;
            GameObject targets = boton.transform.GetChild(0).gameObject;
            EnemyCardCM tarjetaScript = tarjetaActiva.GetComponent<EnemyCardCM>();

            //La corutina para colapsar la tarjeta
            if (tarjetaScript.corutina != null) { StopCoroutine(tarjetaScript.corutina); } //si está corriendo una corutina, frenala.

            tarjetaScript.corutina = StartCoroutine(tarjetaScript.Desplegar(tarjeta, tarjeta.transform.localScale, 
                new Vector3(scaleColapsada, scaleColapsada, scaleColapsada), 0.1f));

            Canvas canv = canvas.gameObject.GetComponent<Canvas>();
            canv.sortingOrder = 1;

            targets.SetActive(false);
            capaNegra.SetActive(true);
            selector.SetActive(false);
            
            tarjetaScript._enemy.soyTarjetaActiva = false;
            tarjetaScript.panel.SetActive(false);
            tarjetaActiva = null;
            
            scroll = true;

            
            


    }
                











    public void ActivarTarjeta (GameObject capaNegra, bool entra){

        if (CombatManager.Instance.pausedCombat && freeze == false)
        {
            GameObject tarjeta = capaNegra.transform.parent.parent.parent.gameObject;

            if (entra) { Activar(tarjeta); }
        }

    }

    //esta sirve para que el jugador no pueda controlar el menú
    public void CongelarMenu (bool booleana ) {
        freeze = booleana;
    }


    //Para activar la animación en el animator
    public void Close() {
        anim.enabled = true;
        anim.SetBool("cerrar", true);
        HUD.SetActive (false);

    }

    //este es para apagar o encender los childs (para que no cague el estado del animator)
    public void SwitchChilds(bool booleana) {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive (booleana);
        } 
    
    }

    public void Centrar()
    {


        if (Screen.width < finalMarco + Screen.width * 0.9)
        {
            
            
            Vector2 destino = new Vector2 (Mathf.Clamp(-tarjetaActiva.transform.localPosition.x, -finalMarco / 2, 0),
            marcoTrans.anchoredPosition.y);

            if (corutina != null) { StopCoroutine(corutina); }
            corutina = StartCoroutine(Centrando(marcoTrans.anchoredPosition, destino, 0.2f));
        }
        anim.SetBool("cerrar", false);

    }


    //Esta corutina cambia el tamaño de la tarjeta seleccionada por sobre las otras.
    public IEnumerator Centrando (Vector2 desde, Vector2 hacia, float overTime)
    {


        float startTime = Time.time;
        while (Time.time < startTime + overTime)
        {
            marcoTrans.anchoredPosition = Vector2.Lerp (desde, hacia, (Time.time - startTime) / overTime);

            yield return null;
        }

    }

    //Esta función sirve para definir el combat mode una vez que se espanwearon los enemigos en la escena
    //La llama el Enemie Spawner Manager después de que terminó el spawneo.
    public void ActualizarNumTarjetas (){
    
        childCount = marcoTrans.childCount;
            finalMarco = childCount * 135;
            if (this.transform.GetChild(1).transform.childCount != 0)
            {
            Debug.Log("actualizando el número de tarjetas en el Combat Mode");  
            HUD.transform.parent.gameObject.SetActive(true);
            Activar(this.transform.GetChild(1).GetChild(0).gameObject);
            }
            else {
                HUD.transform.parent.gameObject.SetActive(false); 
                Debug.Log("apagando el hud");
                this.gameObject.SetActive(false);
            
            }

    }


}
