// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by the Game Data Editor.
//
//      Changes to this file will be lost if the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using UnityEngine;
using System;
using System.Collections.Generic;

using GameDataEditor;

namespace GameDataEditor
{
    public class GDEDialogsData : IGDEData
    {
        private static string CreateEventKey = "CreateEvent";
		private bool _CreateEvent;
        public bool CreateEvent
        {
            get { return _CreateEvent; }
            set {
                if (_CreateEvent != value)
                {
                    _CreateEvent = value;
                    GDEDataManager.SetBool(_key+"_"+CreateEventKey, _CreateEvent);
                }
            }
        }

        private static string SplashKey = "Splash";
		private bool _Splash;
        public bool Splash
        {
            get { return _Splash; }
            set {
                if (_Splash != value)
                {
                    _Splash = value;
                    GDEDataManager.SetBool(_key+"_"+SplashKey, _Splash);
                }
            }
        }

        private static string DialogIDKey = "DialogID";
		private int _DialogID;
        public int DialogID
        {
            get { return _DialogID; }
            set {
                if (_DialogID != value)
                {
                    _DialogID = value;
                    GDEDataManager.SetInt(_key+"_"+DialogIDKey, _DialogID);
                }
            }
        }

        private static string RequiredINTKey = "RequiredINT";
		private int _RequiredINT;
        public int RequiredINT
        {
            get { return _RequiredINT; }
            set {
                if (_RequiredINT != value)
                {
                    _RequiredINT = value;
                    GDEDataManager.SetInt(_key+"_"+RequiredINTKey, _RequiredINT);
                }
            }
        }

        private static string Effects_DurationKey = "Effects_Duration";
		private int _Effects_Duration;
        public int Effects_Duration
        {
            get { return _Effects_Duration; }
            set {
                if (_Effects_Duration != value)
                {
                    _Effects_Duration = value;
                    GDEDataManager.SetInt(_key+"_"+Effects_DurationKey, _Effects_Duration);
                }
            }
        }

        private static string HitFormulaSeedKey = "HitFormulaSeed";
		private float _HitFormulaSeed;
        public float HitFormulaSeed
        {
            get { return _HitFormulaSeed; }
            set {
                if (_HitFormulaSeed != value)
                {
                    _HitFormulaSeed = value;
                    GDEDataManager.SetFloat(_key+"_"+HitFormulaSeedKey, _HitFormulaSeed);
                }
            }
        }

        private static string RangeKey = "Range";
		private float _Range;
        public float Range
        {
            get { return _Range; }
            set {
                if (_Range != value)
                {
                    _Range = value;
                    GDEDataManager.SetFloat(_key+"_"+RangeKey, _Range);
                }
            }
        }

        private static string Effect1_ValueKey = "Effect1_Value";
		private float _Effect1_Value;
        public float Effect1_Value
        {
            get { return _Effect1_Value; }
            set {
                if (_Effect1_Value != value)
                {
                    _Effect1_Value = value;
                    GDEDataManager.SetFloat(_key+"_"+Effect1_ValueKey, _Effect1_Value);
                }
            }
        }

        private static string Effect2_ValueKey = "Effect2_Value";
		private float _Effect2_Value;
        public float Effect2_Value
        {
            get { return _Effect2_Value; }
            set {
                if (_Effect2_Value != value)
                {
                    _Effect2_Value = value;
                    GDEDataManager.SetFloat(_key+"_"+Effect2_ValueKey, _Effect2_Value);
                }
            }
        }

        private static string CategoryKey = "Category";
		private string _Category;
        public string Category
        {
            get { return _Category; }
            set {
                if (_Category != value)
                {
                    _Category = value;
                    GDEDataManager.SetString(_key+"_"+CategoryKey, _Category);
                }
            }
        }

        private static string AudioFileKey = "AudioFile";
		private string _AudioFile;
        public string AudioFile
        {
            get { return _AudioFile; }
            set {
                if (_AudioFile != value)
                {
                    _AudioFile = value;
                    GDEDataManager.SetString(_key+"_"+AudioFileKey, _AudioFile);
                }
            }
        }

        private static string ROBERTKey = "ROBERT";
		private string _ROBERT;
        public string ROBERT
        {
            get { return _ROBERT; }
            set {
                if (_ROBERT != value)
                {
                    _ROBERT = value;
                    GDEDataManager.SetString(_key+"_"+ROBERTKey, _ROBERT);
                }
            }
        }

        private static string ShortTextKey = "ShortText";
		private string _ShortText;
        public string ShortText
        {
            get { return _ShortText; }
            set {
                if (_ShortText != value)
                {
                    _ShortText = value;
                    GDEDataManager.SetString(_key+"_"+ShortTextKey, _ShortText);
                }
            }
        }

        private static string ConsoleFeedbackKey = "ConsoleFeedback";
		private string _ConsoleFeedback;
        public string ConsoleFeedback
        {
            get { return _ConsoleFeedback; }
            set {
                if (_ConsoleFeedback != value)
                {
                    _ConsoleFeedback = value;
                    GDEDataManager.SetString(_key+"_"+ConsoleFeedbackKey, _ConsoleFeedback);
                }
            }
        }

        private static string AgainstKey = "Against";
		private string _Against;
        public string Against
        {
            get { return _Against; }
            set {
                if (_Against != value)
                {
                    _Against = value;
                    GDEDataManager.SetString(_key+"_"+AgainstKey, _Against);
                }
            }
        }

        private static string SituationKey = "Situation";
		private string _Situation;
        public string Situation
        {
            get { return _Situation; }
            set {
                if (_Situation != value)
                {
                    _Situation = value;
                    GDEDataManager.SetString(_key+"_"+SituationKey, _Situation);
                }
            }
        }

        private static string SpecialRuleKey = "SpecialRule";
		private string _SpecialRule;
        public string SpecialRule
        {
            get { return _SpecialRule; }
            set {
                if (_SpecialRule != value)
                {
                    _SpecialRule = value;
                    GDEDataManager.SetString(_key+"_"+SpecialRuleKey, _SpecialRule);
                }
            }
        }

        private static string AnimationKey = "Animation";
		private string _Animation;
        public string Animation
        {
            get { return _Animation; }
            set {
                if (_Animation != value)
                {
                    _Animation = value;
                    GDEDataManager.SetString(_key+"_"+AnimationKey, _Animation);
                }
            }
        }

        private static string Effect1_StatKey = "Effect1_Stat";
		private string _Effect1_Stat;
        public string Effect1_Stat
        {
            get { return _Effect1_Stat; }
            set {
                if (_Effect1_Stat != value)
                {
                    _Effect1_Stat = value;
                    GDEDataManager.SetString(_key+"_"+Effect1_StatKey, _Effect1_Stat);
                }
            }
        }

        private static string Effect2_StatKey = "Effect2_Stat";
		private string _Effect2_Stat;
        public string Effect2_Stat
        {
            get { return _Effect2_Stat; }
            set {
                if (_Effect2_Stat != value)
                {
                    _Effect2_Stat = value;
                    GDEDataManager.SetString(_key+"_"+Effect2_StatKey, _Effect2_Stat);
                }
            }
        }

        public GDEDialogsData()
		{
			_key = string.Empty;
		}

		public GDEDialogsData(string key)
		{
			_key = key;
		}
		
        public override void LoadFromDict(string dataKey, Dictionary<string, object> dict)
        {
            _key = dataKey;

			if (dict == null)
				LoadFromSavedData(dataKey);
			else
			{
                dict.TryGetBool(CreateEventKey, out _CreateEvent);
                dict.TryGetBool(SplashKey, out _Splash);
                dict.TryGetInt(DialogIDKey, out _DialogID);
                dict.TryGetInt(RequiredINTKey, out _RequiredINT);
                dict.TryGetInt(Effects_DurationKey, out _Effects_Duration);
                dict.TryGetFloat(HitFormulaSeedKey, out _HitFormulaSeed);
                dict.TryGetFloat(RangeKey, out _Range);
                dict.TryGetFloat(Effect1_ValueKey, out _Effect1_Value);
                dict.TryGetFloat(Effect2_ValueKey, out _Effect2_Value);
                dict.TryGetString(CategoryKey, out _Category);
                dict.TryGetString(AudioFileKey, out _AudioFile);
                dict.TryGetString(ROBERTKey, out _ROBERT);
                dict.TryGetString(ShortTextKey, out _ShortText);
                dict.TryGetString(ConsoleFeedbackKey, out _ConsoleFeedback);
                dict.TryGetString(AgainstKey, out _Against);
                dict.TryGetString(SituationKey, out _Situation);
                dict.TryGetString(SpecialRuleKey, out _SpecialRule);
                dict.TryGetString(AnimationKey, out _Animation);
                dict.TryGetString(Effect1_StatKey, out _Effect1_Stat);
                dict.TryGetString(Effect2_StatKey, out _Effect2_Stat);
                LoadFromSavedData(dataKey);
			}
		}

        public override void LoadFromSavedData(string dataKey)
		{
			_key = dataKey;
			
            _CreateEvent = GDEDataManager.GetBool(_key+"_"+CreateEventKey, _CreateEvent);
            _Splash = GDEDataManager.GetBool(_key+"_"+SplashKey, _Splash);
            _DialogID = GDEDataManager.GetInt(_key+"_"+DialogIDKey, _DialogID);
            _RequiredINT = GDEDataManager.GetInt(_key+"_"+RequiredINTKey, _RequiredINT);
            _Effects_Duration = GDEDataManager.GetInt(_key+"_"+Effects_DurationKey, _Effects_Duration);
            _HitFormulaSeed = GDEDataManager.GetFloat(_key+"_"+HitFormulaSeedKey, _HitFormulaSeed);
            _Range = GDEDataManager.GetFloat(_key+"_"+RangeKey, _Range);
            _Effect1_Value = GDEDataManager.GetFloat(_key+"_"+Effect1_ValueKey, _Effect1_Value);
            _Effect2_Value = GDEDataManager.GetFloat(_key+"_"+Effect2_ValueKey, _Effect2_Value);
            _Category = GDEDataManager.GetString(_key+"_"+CategoryKey, _Category);
            _AudioFile = GDEDataManager.GetString(_key+"_"+AudioFileKey, _AudioFile);
            _ROBERT = GDEDataManager.GetString(_key+"_"+ROBERTKey, _ROBERT);
            _ShortText = GDEDataManager.GetString(_key+"_"+ShortTextKey, _ShortText);
            _ConsoleFeedback = GDEDataManager.GetString(_key+"_"+ConsoleFeedbackKey, _ConsoleFeedback);
            _Against = GDEDataManager.GetString(_key+"_"+AgainstKey, _Against);
            _Situation = GDEDataManager.GetString(_key+"_"+SituationKey, _Situation);
            _SpecialRule = GDEDataManager.GetString(_key+"_"+SpecialRuleKey, _SpecialRule);
            _Animation = GDEDataManager.GetString(_key+"_"+AnimationKey, _Animation);
            _Effect1_Stat = GDEDataManager.GetString(_key+"_"+Effect1_StatKey, _Effect1_Stat);
            _Effect2_Stat = GDEDataManager.GetString(_key+"_"+Effect2_StatKey, _Effect2_Stat);
         }

        public void Reset_CreateEvent()
        {
            GDEDataManager.ResetToDefault(_key, CreateEventKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetBool(CreateEventKey, out _CreateEvent);
        }

        public void Reset_Splash()
        {
            GDEDataManager.ResetToDefault(_key, SplashKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetBool(SplashKey, out _Splash);
        }

        public void Reset_DialogID()
        {
            GDEDataManager.ResetToDefault(_key, DialogIDKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetInt(DialogIDKey, out _DialogID);
        }

        public void Reset_RequiredINT()
        {
            GDEDataManager.ResetToDefault(_key, RequiredINTKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetInt(RequiredINTKey, out _RequiredINT);
        }

        public void Reset_Effects_Duration()
        {
            GDEDataManager.ResetToDefault(_key, Effects_DurationKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetInt(Effects_DurationKey, out _Effects_Duration);
        }

        public void Reset_HitFormulaSeed()
        {
            GDEDataManager.ResetToDefault(_key, HitFormulaSeedKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetFloat(HitFormulaSeedKey, out _HitFormulaSeed);
        }

        public void Reset_Range()
        {
            GDEDataManager.ResetToDefault(_key, RangeKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetFloat(RangeKey, out _Range);
        }

        public void Reset_Effect1_Value()
        {
            GDEDataManager.ResetToDefault(_key, Effect1_ValueKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetFloat(Effect1_ValueKey, out _Effect1_Value);
        }

        public void Reset_Effect2_Value()
        {
            GDEDataManager.ResetToDefault(_key, Effect2_ValueKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetFloat(Effect2_ValueKey, out _Effect2_Value);
        }

        public void Reset_Category()
        {
            GDEDataManager.ResetToDefault(_key, CategoryKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(CategoryKey, out _Category);
        }

        public void Reset_AudioFile()
        {
            GDEDataManager.ResetToDefault(_key, AudioFileKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(AudioFileKey, out _AudioFile);
        }

        public void Reset_ROBERT()
        {
            GDEDataManager.ResetToDefault(_key, ROBERTKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(ROBERTKey, out _ROBERT);
        }

        public void Reset_ShortText()
        {
            GDEDataManager.ResetToDefault(_key, ShortTextKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(ShortTextKey, out _ShortText);
        }

        public void Reset_ConsoleFeedback()
        {
            GDEDataManager.ResetToDefault(_key, ConsoleFeedbackKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(ConsoleFeedbackKey, out _ConsoleFeedback);
        }

        public void Reset_Against()
        {
            GDEDataManager.ResetToDefault(_key, AgainstKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(AgainstKey, out _Against);
        }

        public void Reset_Situation()
        {
            GDEDataManager.ResetToDefault(_key, SituationKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(SituationKey, out _Situation);
        }

        public void Reset_SpecialRule()
        {
            GDEDataManager.ResetToDefault(_key, SpecialRuleKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(SpecialRuleKey, out _SpecialRule);
        }

        public void Reset_Animation()
        {
            GDEDataManager.ResetToDefault(_key, AnimationKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(AnimationKey, out _Animation);
        }

        public void Reset_Effect1_Stat()
        {
            GDEDataManager.ResetToDefault(_key, Effect1_StatKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(Effect1_StatKey, out _Effect1_Stat);
        }

        public void Reset_Effect2_Stat()
        {
            GDEDataManager.ResetToDefault(_key, Effect2_StatKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(Effect2_StatKey, out _Effect2_Stat);
        }

        public void ResetAll()
        {
            GDEDataManager.ResetToDefault(_key, DialogIDKey);
            GDEDataManager.ResetToDefault(_key, CategoryKey);
            GDEDataManager.ResetToDefault(_key, RequiredINTKey);
            GDEDataManager.ResetToDefault(_key, AudioFileKey);
            GDEDataManager.ResetToDefault(_key, ROBERTKey);
            GDEDataManager.ResetToDefault(_key, ShortTextKey);
            GDEDataManager.ResetToDefault(_key, ConsoleFeedbackKey);
            GDEDataManager.ResetToDefault(_key, AgainstKey);
            GDEDataManager.ResetToDefault(_key, CreateEventKey);
            GDEDataManager.ResetToDefault(_key, SituationKey);
            GDEDataManager.ResetToDefault(_key, SpecialRuleKey);
            GDEDataManager.ResetToDefault(_key, HitFormulaSeedKey);
            GDEDataManager.ResetToDefault(_key, RangeKey);
            GDEDataManager.ResetToDefault(_key, SplashKey);
            GDEDataManager.ResetToDefault(_key, AnimationKey);
            GDEDataManager.ResetToDefault(_key, Effect1_StatKey);
            GDEDataManager.ResetToDefault(_key, Effect1_ValueKey);
            GDEDataManager.ResetToDefault(_key, Effect2_StatKey);
            GDEDataManager.ResetToDefault(_key, Effect2_ValueKey);
            GDEDataManager.ResetToDefault(_key, Effects_DurationKey);


            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            LoadFromDict(_key, dict);
        }
    }
}
