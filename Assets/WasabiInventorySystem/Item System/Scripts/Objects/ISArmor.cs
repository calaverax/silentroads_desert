﻿using UnityEngine;
using System.Collections;
using System;

namespace Wasabi.InventorySystem
{
    [System.Serializable]
    public class ISArmor : ISObject, IISEquipable, IISBreakable, IISArmor
    {
        // IISAmor
        [SerializeField] int _protection;
        [SerializeField] string _model = null;

        // IISBreakable
        [SerializeField] int _currentDurability;
        [SerializeField] int _maxDurability;

        // IISEquipable
        [SerializeField] EquipmentSlot _equipSlot;
        [SerializeField] float _might;
        [SerializeField] Sprite _onCharacterSprite;
        [SerializeField] bool _equipVisually;

        // Events
        public ItemBrokenEvent OnArmorBroken;

        DispararIK dispararIK;

        public ISArmor()
        {
            OnArmorBroken = new ItemBrokenEvent();
        }

        #region IISBreackable_Interface

        public int CurrentDurability { get { return _currentDurability; } }

        public int MaxDurability
        {
            get { return _maxDurability; }
            set { _maxDurability = value; }
        }

        public void Break()
        {
            OnArmorBroken.Invoke(this);
        }

        public void RecieveDamage(int amount)
        {
            _currentDurability -= amount;

            if (_currentDurability <= 0)
                Break();
        }

        public void Repair(int amount)
        {
            if (_maxDurability == 0)
                return;

            _maxDurability--;
            _currentDurability = _maxDurability;
        }
        #endregion

        #region IISArmor_Interface

        public bool EquipVisually
        {
            get { return _equipVisually; }
            set { _equipVisually = value; }
        }

        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }

        public int Protection
        {
            get { return _protection; }

            set { _protection = value; }
        }
        #endregion

        #region IISEquipable_Interface
        
        public EquipmentSlot EquipSlot
        {
            get { return _equipSlot; }
            set { _equipSlot = value; }
        }

        public float Might
        {
            get { return _might; }
            set { _might = value; }
        }

        public Sprite OnCharacterSprite { get { return _onCharacterSprite; } set { _onCharacterSprite = value; } }

        #endregion


        public GameObject GetModel(Transform parent)
        {

            if (dispararIK == null) { dispararIK = GameObject.FindGameObjectWithTag("Player").GetComponent<DispararIK>(); }
            if (!dispararIK.Cascos.ContainsKey(Model))
            {
                //PPP podría cargar el modelo directamente pero al parecer eso cargaría todos los modelos en la memoria.
                string path = "Armas/cascos/" + Model;
                GameObject modelo = GameObject.Instantiate(Resources.Load(path), parent) as GameObject;
                modelo.name = modelo.name.Replace("(Clone)", "");
                dispararIK.Cascos.Add(modelo.name, modelo);
                modelo.SetActive(false);


                return modelo;
            }
            else
            {
                return null;
            }
        }

        public ISArmor Clone()
        {
            ISArmor newArmor = new ISArmor();
            // Values From IISObject
            newArmor.ObjectID = ObjectID;
            newArmor.Name = Name;
            newArmor.Description = Description;
            newArmor.Icon = Icon;
            newArmor.GraphicRepresentation = GraphicRepresentation;
            newArmor.Burden = Burden;
            newArmor.Quality = Quality;
            newArmor.ItemPrice = ItemPrice;
            //newArmor.SellPrice = SellPrice;

            // Values from IISEquipable
            newArmor.EquipSlot = EquipSlot;
            newArmor.Might = Might;
            //newArmor.OnCharacterSprite = armor.OnCharacterSprite;
            newArmor.EquipVisually = EquipVisually;

            // Values From IISArmor
            newArmor.Protection = Protection;
            newArmor.MaxDurability = MaxDurability;
            newArmor.Model = Model;

            return newArmor;
        }

    }
}