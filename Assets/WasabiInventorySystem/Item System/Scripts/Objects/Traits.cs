﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Traits {
    [SerializeField]
    public string traitName = "";
    [SerializeField]
    public  float Accuracy = 0;
    [SerializeField]
    public  float Strength = 0;
    [SerializeField]
    public  float Agility = 0;
    [SerializeField]
    public  float Morale = 0;
    [SerializeField]
    public  float Command = 0;
    [SerializeField]
    public  float Evasion = 0;
    [SerializeField]
    public  float floatelligence = 0;
    [SerializeField]
    public  float Stealth = 0;
    [SerializeField]
    public  float Hitpofloats = 0;
    [SerializeField]
    public  float TotalHitpofloats = 0;
    [SerializeField]
    public  float Hostility = 0;
    [SerializeField]
    public  float AvailablesPofloats = 0;
    [SerializeField]
    public  float water = 0;
    [SerializeField]
    public  float Might = 0;
}
