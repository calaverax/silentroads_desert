﻿using UnityEngine;
using System.Collections;
using System;

namespace Wasabi.InventorySystem
{
    [System.Serializable]
    public class ISObject : IISObject
    {
        [SerializeField] string _name;
        [SerializeField] int _objectID;
        [SerializeField] string _description;
        [SerializeField] Sprite _icon;
        [SerializeField] Sprite _graphicRepresentation;
        [SerializeField] int _burden;
        [SerializeField] ISQuality _quality;
        [SerializeField] int _itemPrice;
        //[SerializeField] int _sellPrice;    

        public ISObject()
        {
        }

        public int ObjectID { get { return _objectID; } set { _objectID = value; } }
        public string Name { get { return _name; } set { _name = value; } }
        public string Description { get { return _description; } set { _description = value; } }
        public Sprite Icon { get { return _icon; } set { _icon = value; } }
        public Sprite GraphicRepresentation { get { return _graphicRepresentation; } set { _graphicRepresentation = value; } }
        public int Burden { get { return _burden; } set { _burden = value; } }
        public ISQuality Quality { get { return _quality; } set { _quality = value; } }
        public int ItemPrice { get { return _itemPrice; } set { _itemPrice = value; } }
        //public int SellPrice { get { return _sellPrice; } set { _sellPrice = value; } }

    }
}