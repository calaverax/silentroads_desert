﻿using UnityEngine;
using System.Collections;
using System;

namespace Wasabi.InventorySystem
{
    [System.Serializable]
    public class ISCharacter : IISCharacter
    {
        
        
        // Misc Data
        [SerializeField]
        string _characterType;
        [SerializeField]
        string _startingBehaviour;
        [SerializeField]
        string[] _items;
        [SerializeField]
        string[] _maleAvatar;
        [SerializeField]
        string[] _femaleAvatar;
        
        
        
        
        // STATS Max / Min 
        [SerializeField] int _maxAccuracy;
        [SerializeField] int _minAccuracy;
        [SerializeField] int _minStrength;
        [SerializeField] int _maxStrength;
        [SerializeField] int _minAgility;
        [SerializeField] int _maxAgility;
        [SerializeField] int _minMorale;
        [SerializeField] int _maxMorale;
        [SerializeField] int _minCommand;
        [SerializeField] int _maxCommand;
        [SerializeField] int _minEvasion;
        [SerializeField] int _maxEvasion;
        [SerializeField] int _minIntelligence;
        [SerializeField] int _maxIntelligence;
        [SerializeField] int _minStealth;
        [SerializeField] int _maxStealth;
        [SerializeField] int _minHitpoints;
        [SerializeField] int _maxHitpoints;
        [SerializeField] int _minHostility;
        [SerializeField] int _maxHostility;
        [SerializeField] int _minAwardedExp;
        [SerializeField] int _maxAwardedExp;
        [SerializeField] int _minAwardedCurrency;
        [SerializeField] int _maxAwardedCurrency;

        //------------------------------------------


        float _bossFactor { get; set; }

        public string[] FemaleAvatar
        {
			get { return _femaleAvatar; } set { _femaleAvatar = value; }
        }

		public string[] MaleAvatar
		{
			get { return _maleAvatar; } set { _maleAvatar = value; }
		}

        public float BossFactor
        {
            get { return _bossFactor; }
            set { _bossFactor = value; }
        }

        public string CharacterType
        {
            get { return _characterType; }
            set { _characterType = value; }
        }

        public string[] Items
        {
            get { return _items; }
            set { _items = value; }
        }

        public int MaxAccuracy
        {
            get { return _maxAccuracy; }
            set { _maxAccuracy = value; }
        }

        public int MaxAgility
        {
            get { return _maxAgility; }
            set { _maxAgility = value; }
        }

        public int MaxAwardedCurrency
        {
            get { return _maxAwardedCurrency; }
            set { _maxAwardedCurrency = value; }
        }

        public int MaxAwardedExp
        {
            get { return _maxAwardedExp; }
            set { _maxAwardedExp = value; }
        }

        public int MaxCommand
        {
            get { return _maxCommand; }
            set { _maxCommand = value; }
        }

        public int MaxEvasion
        {
            get { return _maxEvasion; }
            set { _maxEvasion = value; }
        }

        public int MaxHitpoints
        {
            get { return _maxHitpoints; }
            set { _maxHitpoints = value; }
        }

        public int MaxHostility
        {
            get { return _maxHostility; }
            set { _maxHostility = value; }
        }

        public int MaxIntelligence
        {
            get { return _maxIntelligence; }
            set { _maxIntelligence = value; }
        }

        public int MaxMorale
        {
            get { return _maxMorale; }
            set { _maxMorale = value; }
        }

        public int MaxStealth
        {
            get { return _maxStealth; }
            set { _maxStealth = value; }
        }

        public int MaxStrength
        {
            get { return _maxStrength; }
            set { _maxStrength = value; }
        }

        public int MinAccuracy
        {
            get { return _minAccuracy; }
            set { _minAccuracy = value; }
        }

        public int MinAgility
        {
            get { return _minAgility; }
            set { _minAgility = value; }
        }

        public int MinAwardedCurrency
        {
            get { return _minAwardedCurrency; }
            set { _minAwardedCurrency = value; }
        }

        public int MinAwardedExp
        {
            get { return _minAwardedExp; }
            set { _minAwardedExp = value; }
        }

        public int MinCommand
        {
            get { return _minCommand; }
            set { _minCommand = value; }
        }

        public int MinEvasion
        {
            get { return _minEvasion; }
            set { _minEvasion = value; }
        }

        public int MinHitpoints
        {
            get { return _minHitpoints; }
            set { _minHitpoints = value; }
        }

        public int MinHostility
        {
            get { return _minHostility; }
            set { _minHostility = value; }
        }

        public int MinIntelligence
        {
            get { return _minIntelligence; }
            set { _minIntelligence = value; }
        }

        public int MinMorale
        {
            get { return _minMorale; }
            set { _minMorale = value; }
        }

        public int MinStealth
        {
            get { return _minStealth; }
            set { _minStealth = value; }
        }

        public int MinStrength
        {
            get { return _minStrength; }
            set { _minStrength = value; }
        }

        public string StartingBehaviour
        {
            get { return _startingBehaviour; }
            set { _startingBehaviour = value; }
        }
    }
}