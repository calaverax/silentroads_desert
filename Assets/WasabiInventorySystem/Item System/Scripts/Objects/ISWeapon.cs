﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{
    [System.Serializable]
    public class ISWeapon : ISObject, IISEquipable, IISWeapon
    {
        // IISEquipable
        [SerializeField] EquipmentSlot _equipSlot;
        [SerializeField] float _might;
        [SerializeField] Sprite _onCharacterSprite;
        [SerializeField] bool _equipVisually;

        // IISWeapon
        [SerializeField] WeaponEquipType _equipType;
        [SerializeField] int _maxFireRate;
        [SerializeField] int _damage;
        [SerializeField] int _maxRange;
        [SerializeField] int _maxShootsPerRound;
        [SerializeField] int _shootsInRound;
        [SerializeField] int _noiseLevel;
        [SerializeField] int _accuracy;
        [SerializeField] float _caliber;
        [SerializeField] bool _scope;
        [SerializeField] AudioClip _shootSound;
        [SerializeField] AudioClip _reloadSound;
        [SerializeField] int _level;
        [SerializeField] string _model;
        [SerializeField] Sprite _casio;
        DispararIK dispararIK;

        public ISWeapon()
        {
        }

        #region IISWeapon_Interface

        public string Model {
            get { return _model; }
            set { _model = value; }
        }

        public Sprite Casio {
            get { return _casio; }
            set { _casio = value; }
        }

        public AudioClip ShootSound
        {
            get { return _shootSound; }
            set { _shootSound = value; }
        }

        public AudioClip ReloadSound
        {
            get { return _reloadSound; }
            set { _reloadSound = value; }
        }

        public bool EquipVisually
        {
            get { return _equipVisually; }
            set { _equipVisually = value; }
        }

        public WeaponEquipType EquipType
        {
            get { return _equipType; }
            set { _equipType = value; }
        }

        public int MaxFireRate
        {
            get { return _maxFireRate; }
            set { _maxFireRate = value; }
        }

        public int Damage
        {
            get { return _damage; }
            set { _damage = value; }
        }

        public int MaxRange
        {
            get { return _maxRange; }
            set { _maxRange = value; }
        }

        public int MaxShootsPerRound
        {
            get { return _maxShootsPerRound; }
            set { _maxShootsPerRound = value; }
        }

        public int ShootsInRound
        {
            get { return _shootsInRound; }
            set { _shootsInRound = value; }
        }

        public int NoiseLevel
        {
            get { return _noiseLevel; }
            set { _noiseLevel = value; }
        }

        public int Accuracy
        {
            get { return _accuracy; }
            set { _accuracy = value; }
        }

        public float Caliber
        {
            get { return _caliber; }
            set { _caliber = value; }
        }

        public bool Scope
        {
            get { return _scope; }
            set { _scope = value; }
        }

        public int Level
        {
            get { return _level; }
            set { _level = value; }
        }
        #endregion

        #region IISEquipable_Interface

        public float Might
        {
            get  { return _might; }
            set { _might = value; }
        }

        public EquipmentSlot EquipSlot
        {
            get { return _equipSlot; }
            set { _equipSlot = value; }
        }

        public Sprite OnCharacterSprite { get { return _onCharacterSprite; } set { _onCharacterSprite = value; } }
        #endregion


        public GameObject GetModel(Transform parent)
        {

            if (dispararIK == null) { dispararIK = GameObject.FindGameObjectWithTag("Player").GetComponent<DispararIK>(); }
            if (!dispararIK.Armas.ContainsKey(Model))
            {
                //PPP podría cargar el modelo directamente pero al parecer eso cargaría todos los modelos en la memoria.
                string path = "Armas/" + Model;
                GameObject modelo = GameObject.Instantiate(Resources.Load(path), parent) as GameObject;
                modelo.name = modelo.name.Replace("(Clone)", "");
                dispararIK.Armas.Add(modelo.name, modelo);
                modelo.SetActive(false);


                return modelo;
            }
            else
            {
                return null; 
            }
        }

        public ISWeapon Clone()
        {
            ISWeapon newWeapon = new ISWeapon();

            // Values From ISSObject
            newWeapon.ObjectID = ObjectID;
            newWeapon.Name = Name;
            newWeapon.Description = Description;
            newWeapon.Icon = Icon;
            newWeapon.GraphicRepresentation = GraphicRepresentation;
            newWeapon.Burden = Burden;
            newWeapon.Quality = Quality;
            newWeapon.ItemPrice = ItemPrice;
            //newWeapon.SellPrice = SellPrice;

            // Values from IISEquipable
            newWeapon.EquipSlot = EquipSlot;
            newWeapon.Might = Might;
            //newWeapon.OnCharacterSprite = weapon.OnCharacterSprite;
            newWeapon.EquipVisually = EquipVisually;

            //Values From IISWeapon
            newWeapon.EquipType = EquipType;
            newWeapon.MaxFireRate = MaxFireRate;
            newWeapon.Damage = Damage;
            newWeapon.MaxRange = MaxRange;
            newWeapon.MaxShootsPerRound = MaxShootsPerRound;
            newWeapon.NoiseLevel = NoiseLevel;
            newWeapon.Accuracy = Accuracy;
            newWeapon.Caliber = Caliber;
            newWeapon.Scope = Scope;
            // TODO Add sounds to ExcelFile.
            newWeapon.ShootSound = ShootSound;
            //newWeapon.ReloadSound = LoadWeaponSound("");
            newWeapon.Model = Model;
            newWeapon.Casio = Casio;

            return newWeapon;
        }

        public string GetModelName() {
            return Model;
        }
    }
}