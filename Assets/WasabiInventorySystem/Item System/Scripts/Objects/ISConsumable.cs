﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace Wasabi.InventorySystem
{
    [System.Serializable]
    public class ISConsumable : ISObject, IISConsumable, IISStackable
    {
        [SerializeField] float _cooldown;
        [SerializeField] int _maxStack;
        [SerializeField] int _stack;
        [SerializeField] int _level;
        
        //public ItemUsedEvent OnConsumed;

        #region IISConsumable_interface

        public ISConsumable() : base()
        {
            //OnConsumed = new ItemUsedEvent();
        }

        public float Cooldown
        {
            get { return _cooldown; }
            set { _cooldown = value; }
        }

        public virtual void Consume()
        {
            //OnConsumed.Invoke(this);
        }

        public int Level
        {
            get { return _level; }
            set { _level = value; }
        }

        #endregion

        public int MaxStack
        {
            get
            {
                return _maxStack;
            }
            set
            {
                _maxStack = value;
            }
        }
        public int Stack
        {
            get { return _stack; }
            set { _stack = value; }
        }

        public ISConsumable Clone()
        {
            ISConsumable clone = new ISConsumable();

            // Values From IISObject
            clone.ObjectID = ObjectID;
            clone.Name = Name;
            clone.Description = Description;
            clone.Icon = Icon;
            clone.GraphicRepresentation = GraphicRepresentation;
            clone.Burden = Burden;
            clone.Quality = Quality;
            clone.ItemPrice = ItemPrice;
            //clone.SellPrice = SellPrice;

            // Values From IISConsumable, IISStackable
            clone.Cooldown = Cooldown;
            clone.MaxStack = MaxStack;
            clone.Stack = Stack;

            return clone;
        }
    }
}