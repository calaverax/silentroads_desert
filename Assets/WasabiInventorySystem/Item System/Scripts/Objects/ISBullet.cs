﻿using UnityEngine;
using System.Collections;
using System;

namespace Wasabi.InventorySystem
{
    [System.Serializable]
    public class ISBullet : ISConsumable
    {
        [SerializeField]
        float _caliber;

        [SerializeField]
        bool _radiated;

        [SerializeField]
        bool _poisoned;

        public float caliber { get { return _caliber; }  set { _caliber = value; } }
        public bool radiated { get { return _poisoned; } set { _poisoned = value; } }
        public bool poisoned { get { return _radiated; } set { _radiated = value; } }

        public ISBullet() : base()
        {
        }

        public new ISBullet Clone()
        {
            ISBullet clone = new ISBullet();

            // Values From IISObject
            clone.ObjectID = ObjectID;
            clone.Name = Name;
            clone.Description = Description;
            clone.Icon = Icon;
            clone.GraphicRepresentation = GraphicRepresentation;
            clone.Burden = Burden;
            clone.Quality = Quality;
            clone.ItemPrice = ItemPrice;
            //clone.SellPrice = SellPrice;

            // Values From IISConsumable, IISStackable
            clone.Cooldown = Cooldown;
            clone.MaxStack = MaxStack;
            clone.Stack = Stack;

            // Values From ISBullets
            clone.caliber = caliber;
            clone.poisoned = poisoned;
            clone.radiated = radiated;

            return clone;
        }
    }
}
