﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Wasabi.InventorySystem
{
    public class ISCurrenciesDatabase : ScriptableObjectDatabase<ISCurrency>
    {
        public string GetName(int index)
        {
            return database[index].Name;
        }

        public ISCurrency GetElementByName(string name)
        {
            for (int i = 0; i < database.Count; i++)
                if (database[i].Name.ToLower() == name.ToLower())
                    return database[i];

            return null;
        }
        public List<ISCurrency> GetList()
        {
            return database;
        }
    }
}