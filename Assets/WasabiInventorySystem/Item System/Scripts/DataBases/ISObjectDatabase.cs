﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Wasabi.InventorySystem
{
    public class ISObjectDatabase : ScriptableObjectDatabase<ISObject>
    {
        public string GetName(int index)
        {
            return database[index].Name;
        }

        public ISObject GetElementByName(string name)
        {
            for (int i = 0; i < database.Count; i++)
                if (database[i].Name.ToLower() == name.ToLower())
                    return database[i];

            return null;
        }

    }
}