﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Wasabi.InventorySystem
{
    public class ISConsumableDatabase : ScriptableObjectDatabase<ISConsumable>
    {
        public string GetName(int index)
        {
            return database[index].Name;
        }

        public ISConsumable GetElementByName(string name)
        {
            for (int i = 0; i < database.Count; i++)
                if (database[i].Name.ToLower() == name.ToLower())
                    return database[i].Clone();

            return null;
        }
        public ISConsumable GetElementByID(int id)
        {

            Debug.Log("Entró en a buscar el element by ID.");
            for (int i = 0; i < database.Count; i++)
                if (database[i].ObjectID == id)
                {
                    Debug.Log("database[i].name" + database[i].Name);
                    return database[i].Clone();}

            return null;
        }
    }
}