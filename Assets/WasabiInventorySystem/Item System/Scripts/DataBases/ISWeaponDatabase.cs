﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Wasabi.InventorySystem
{
    public class ISWeaponDatabase : ScriptableObjectDatabase<ISWeapon>
    {
        public string GetName(int index)
        {
            return database[index].Name;
        }

        public ISWeapon GetElementByName(string name)
        {
            for (int i = 0; i < database.Count; i++)
                if (database[i].Name.ToLower() == name.ToLower())
                    return database[i].Clone();

            return null;
        }

        public ISObject GetElementByID(int id)
        {
            for (int i = 0; i < database.Count; i++)
                if (database[i].ObjectID == id)
                    return database[i].Clone();

            return null;
        }

        public void RemoveItemByID(int id)
        {
            for (int i = 0; i < database.Count; i++)
                if (database[i].ObjectID == id)
                    database.Remove(database[i]);
        }
    }
}