﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Wasabi.InventorySystem
{
    public class ISBulletsDatabase : ScriptableObjectDatabase<ISBullet>
    {
        public string GetName(int index)
        {
            return database[index].Name;
        }

        public ISBullet GetElementByName(string name)
        {
            for (int i = 0; i < database.Count; i++)
                if (database[i].Name.ToLower() == name.ToLower())
                    return database[i].Clone();

            return null;
        }
        public ISBullet GetElementByID(int id)
        {
            for (int i = 0; i < database.Count; i++)
                if (database[i].ObjectID == id)
                    return database[i].Clone();

            return null;
        }
    }
}