﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Wasabi.InventorySystem
{
    public class ISQualityDatabase : ScriptableObjectDatabase<ISQuality>
    {
        public string GetName(int index)
        {
            return database[index].Name;
        }

        public ISQuality GetElementByName(string name)
        {
            for (int i = 0; i < database.Count; i++)
                if (database[i].Name.ToLower() == name.ToLower())
                    return database[i];

            return null;
        }

    }

}