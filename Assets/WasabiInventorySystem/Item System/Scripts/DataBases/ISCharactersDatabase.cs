﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{
    public class ISCharactersDatabase : ScriptableObjectDatabase<ISCharacter>
    {
        // For Now, this is the only method we need.
        public ISCharacter GetByType(string characterType)
        {
            foreach (ISCharacter character in database)
            {
                if (character.CharacterType == characterType)
                    return character;
            }
            return null;
        }

        public ISCharacter GetByIndex(int idx)
        {
            return database[idx];
        }

        public void RemoveByIndex(int idx)
        {
            database.Remove(database[idx]);
        }


        /*
        public string GetName(int index)
        {
            return database[index].Name;
        }

        public ISCharacter GetElementByName(string name)
        {
            for (int i = 0; i < database.Count; i++)
                if (database[i].Name.ToLower() == name.ToLower())
                    return database[i].Clone();

            return null;
        }

        public ISObject GetElementByID(int id)
        {
            for (int i = 0; i < database.Count; i++)
                if (database[i].ObjectID == id)
                    return database[i].Clone();

            return null;
        }

        public void RemoveItemByID(int id)
        {
            for (int i = 0; i < database.Count; i++)
                if (database[i].ObjectID == id)
                    database.Remove(database[i]);
        }
        */
    }
}