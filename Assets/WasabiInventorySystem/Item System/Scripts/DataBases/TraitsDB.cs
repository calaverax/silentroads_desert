﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wasabi.InventorySystem
{
    [CreateAssetMenu(menuName = "Traits")]
    public class TraitsDB : ScriptableObjectDatabase<Traits>
    {
        public string GetName(int index)
        {
            return database[index].traitName;
        }

        public void RemoveItemByID(string id)
        {
            for (int i = 0; i < database.Count; i++)
                if (database[i].traitName == id)
                    database.Remove(database[i]);
        }
    }
}
