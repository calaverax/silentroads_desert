﻿using UnityEngine;
using System.Collections;
using System;

namespace Wasabi.InventorySystem
{
    [System.Serializable]
    public class ISQuality : IISQuality
    {
        [SerializeField] string _name;
        [SerializeField] Sprite _icon;
        [SerializeField] Color _color;


        public ISQuality(string Name)
        {
            _name = Name;
            _icon = new Sprite();
            _color = new Color();
        }

        public Sprite Icon { get { return _icon; } set { _icon = value; } }

        public string Name { get { return _name; } set { _name = value; } }

        public Color TitleColor { get { return _color; } set { _color = value; } }

    }
}
