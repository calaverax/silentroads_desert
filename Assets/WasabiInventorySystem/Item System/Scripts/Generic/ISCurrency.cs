﻿using UnityEngine;
using System.Collections;
using System;

namespace Wasabi.InventorySystem
{
    [System.Serializable]
    public class ISCurrency : IISCurrency
    {
        [SerializeField] string _name;
        [SerializeField] int _value;
        [SerializeField] Sprite _icon;

        [SerializeField]
        bool _conversible;
        [SerializeField]
        ISCurrency _targetCurrency;
        [SerializeField] float _conversionRatio;
        

        //[SerializeField]
        //ISCurrencyConvertion _convertion;


        public bool ISConversible { get { return _conversible; } set { _conversible = value; }}
        public ISCurrency ISTargetCurrency { get { return _targetCurrency; } set { _targetCurrency = value; } }
        public float ConversionRatio { get { return _conversionRatio; } set { _conversionRatio = value; } }


        //public ISCurrencyConvertion ISConvertion { get { return _convertion; } set { _convertion = value; }}

        public ISCurrency(string Name)
        {
            _name = Name;
            Value = 0;
        }

        public string Name { get { return _name; } set { _name = value; }}

        public int Value { get { return _value; } set { _value = value; }}

        public Sprite Icon { get { return _icon; } set { _icon = value; } }
    }
}