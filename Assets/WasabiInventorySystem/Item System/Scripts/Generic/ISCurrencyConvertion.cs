﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{
    [System.Serializable]
    public class ISCurrencyConvertion : IISCurrencyConvertion
    {
        [SerializeField] bool _autoConvert;
        [SerializeField] ISCurrency _targetCurrency;
        [SerializeField] float _convertRatio;

        public bool Autoconvert { get { return _autoConvert; } set { _autoConvert = value; } }
        public ISCurrency TargetCurrency { get { return _targetCurrency; } set { _targetCurrency = value; } }
        public float Ratio { get { return _convertRatio; } set { _convertRatio = value; } }
    }
}