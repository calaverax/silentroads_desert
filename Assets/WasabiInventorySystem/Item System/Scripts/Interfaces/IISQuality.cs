﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{
    public interface IISQuality
    {
        string Name { get; set; }
        Sprite Icon { get; set; }
        Color TitleColor { get; set; }

    }
}