﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{
    public interface IISCharacter 
    {
        // STATS Max / Min 
        int MaxAccuracy { get; set; }
        int MinAccuracy { get; set; }

        int MinStrength { get; set; }
        int MaxStrength { get; set; }

        int MinAgility { get; set; }
        int MaxAgility { get; set; }

        int MinMorale { get; set; }
        int MaxMorale { get; set; }

        int MinCommand { get; set; }
        int MaxCommand { get; set; }

        int MinEvasion { get; set; }
        int MaxEvasion { get; set; }

        int MinIntelligence { get; set; }
        int MaxIntelligence { get; set; }

        int MinStealth { get; set; }
        int MaxStealth { get; set; }

        int MinHitpoints { get; set; }
        int MaxHitpoints { get; set; }

        int MinHostility { get; set; }
        int MaxHostility { get; set; }

        int MinAwardedExp { get; set; }
        int MaxAwardedExp { get; set; }

        int MinAwardedCurrency { get; set; }
        int MaxAwardedCurrency { get; set; }

        //------------------------------------------
        // Misc Data
        string CharacterType { get; set; }
        string StartingBehaviour { get; set; }
        string[] Items { get; set; }
        string[] MaleAvatar { get; set; }
		string[] FemaleAvatar { get; set; }

        float BossFactor { get; set; }

    }
}