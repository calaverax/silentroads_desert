﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace Wasabi.InventorySystem
{
    public interface IISConsumable
    {
        int Level { get; set; }
        float Cooldown { get; set; }
        void Consume();
    }
}