﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{
    public interface IISStackable
    {
        int MaxStack { get; set; }
        int Stack { get; set; }
    }
}