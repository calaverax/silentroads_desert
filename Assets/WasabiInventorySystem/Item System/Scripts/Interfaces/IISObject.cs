﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{
    public interface IISObject
    {
        int ObjectID { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        Sprite Icon { get; set; }
        Sprite GraphicRepresentation { get; set; }
        int Burden { get; set; }

        // Custom Objects
        ISQuality Quality { get; set; }
        int ItemPrice { get; set; }
        //int SellPrice { get; set; }

        // Equip
        // QuestItemFlag
        // Durability

    }
}
