﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{
    public interface IISBreakable
    {
        int CurrentDurability { get; }
        int MaxDurability { get; set; }
        void Repair(int amount);
        void RecieveDamage(int amount);
        void Break();
    }
}