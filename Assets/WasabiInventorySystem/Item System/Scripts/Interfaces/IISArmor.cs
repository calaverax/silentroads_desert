﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{
    public interface IISArmor 
    {
        int Protection { get; set; }
    }
}