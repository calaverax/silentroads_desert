﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{

    public interface IISCurrency
    {
        string Name { get; set; }
        int Value { get; set; }
        Sprite Icon { get; set; }
        //bool ISConversible { get; set; }
        //ISCurrencyConvertion ISConvertion { get; set; }
	
	}
}
