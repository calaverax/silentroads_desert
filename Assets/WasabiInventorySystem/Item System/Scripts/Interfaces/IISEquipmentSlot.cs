﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{
    public interface IISEquipmentSlot
    {
        string Name { get; set; }
    }
}