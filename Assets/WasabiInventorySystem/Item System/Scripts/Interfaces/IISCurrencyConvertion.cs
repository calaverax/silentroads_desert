﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{
    public interface IISCurrencyConvertion 
    {
        bool Autoconvert { get; set; }
        ISCurrency TargetCurrency { get; set; }
        float Ratio { get; set; }
    }
}