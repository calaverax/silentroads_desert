﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{
    public interface IISTradePair
    {
        ISCurrency Currency { get; }
        int Value { get; }
    }
}