﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{
    public interface IISEquipable 
    {
        EquipmentSlot EquipSlot { get; set; }
        float Might { get; set; }
        Sprite OnCharacterSprite { get; set; }
        bool EquipVisually { get; set; }

        //ISEquipmentSlot EquipmentSlot { get; }
        //bool Equip();
    }
}