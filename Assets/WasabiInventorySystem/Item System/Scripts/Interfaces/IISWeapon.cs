﻿using UnityEngine;
using System.Collections;
namespace Wasabi.InventorySystem
{
    public interface IISWeapon 
    {
        WeaponEquipType EquipType { get; set; }

        int MaxFireRate { get; set; }
        int Damage { get; set; }
        int MaxRange { get; set; }
        int MaxShootsPerRound { get; set; }
        int NoiseLevel { get; set; }
        int Accuracy { get; set; }
        int Level { get; set; }

        float Caliber { get; set; }

        bool Scope { get; set; }

        AudioClip ReloadSound { get; set; }
        AudioClip ShootSound { get; set; }

    }
}