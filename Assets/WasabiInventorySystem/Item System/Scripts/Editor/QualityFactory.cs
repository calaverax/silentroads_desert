﻿using UnityEngine;
using System.Collections;
using Wasabi.InventorySystem;

public class QualityFactory : MonoBehaviour {

    public ISQualityDatabase database;

	void Start ()
    {
        if (database == null)
            return;

        if (database.GetElementByName("Trash") == null)
            database.Add(new ISQuality("Trash"));

        if (database.GetElementByName("Common") == null)
            database.Add(new ISQuality("Common"));

        if (database.GetElementByName("Uncommon") == null)
            database.Add(new ISQuality("Uncommon"));

        if (database.GetElementByName("Rare") == null)
            database.Add(new ISQuality("Rare"));

        if (database.GetElementByName("Epic") == null)
            database.Add(new ISQuality("Epic"));

        if (database.GetElementByName("Unique") == null)
            database.Add(new ISQuality("Unique"));

    }
	
}
