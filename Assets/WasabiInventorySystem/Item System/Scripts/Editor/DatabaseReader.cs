﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using Wasabi.InventorySystem;
using GameDataEditor;

public class DatabaseReader : MonoBehaviour {

    static ISQualityDatabase qualityDatabase;
    static ISCurrenciesDatabase currenciesDatabase;
    static ISArmorDatabase armorDatabase;
    static ISWeaponDatabase weaponsDatabase;
    static ISConsumableDatabase consumableDatabase;
    static ISBulletsDatabase bulletsDatabase;
    static ISCharactersDatabase charactersDatabase;
    static List<Sprite> GraphicRepresentations = new List<Sprite>();
    static List<Sprite> ItemIcons = new List<Sprite>();

    const string CURRENCIES_DATABASE_FILE_NAME = @"ISCurrenciesDatabase.asset";
    const string QUALITY_DATABASE_FILE_NAME = @"ISQualityDatabase.asset";
    const string ARMORS_DATABASE_FILE_NAME = @"ISArmorDatabase.asset";
    const string WEAPONS_DATABASE_FILE_NAME = @"ISWeaponsDatabase.asset";
    const string CONSUMABLES_DATABASE_FILE_NAME = @"ISConsumablesDatabase.asset";
    const string BULLETS_DATABASE_FILE_NAME = @"ISBulletsDatabase.asset";
    const string CHARACTERS_DATABASE_FILE_NAME = @"ISCharactersDatabase.asset";
    const string DATABASE_FOLDER_NAME = @"Databases";

    public static T GetDatabase<T>(string dbPath, string dbName) where T : ScriptableObject
    {
        string dbFullPath = @"Assets/" + dbPath + "/" + dbName;

        T db = AssetDatabase.LoadAssetAtPath(dbFullPath, typeof(T)) as T;

        if (db == null)
        {
            if (!AssetDatabase.IsValidFolder("Assets/" + dbPath))
                AssetDatabase.CreateFolder("Assets", dbPath);

            db = ScriptableObject.CreateInstance<T>() as T;
            AssetDatabase.CreateAsset(db, dbFullPath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        return db;
    }

    [MenuItem(@"Lonely Adventures/Inventory System/Process Databases/All Databases", false, 1)]
    public static void ProcessAllDatabases()
    {
        LoadAllDatabases();

        LoadQualities();
        LoadCurrencies();
        LoadConsumables();
        LoadBullets();
        loadArmors();
        loadWeapons();
    }

    [MenuItem(@"Lonely Adventures/Inventory System/Process Databases/Weapons Database")]
    public static void ProcessWeaponsDatabase()
    {
        LoadAllDatabases();
        if (weaponsDatabase != null)
            loadWeapons();
    }

    [MenuItem(@"Lonely Adventures/Inventory System/Process Databases/Armors Database")]
    public static void ProcessArmorsDatabase()
    {
        LoadAllDatabases();

        if (armorDatabase != null)
            loadArmors();
    }

    [MenuItem(@"Lonely Adventures/Inventory System/Process Databases/Consumables Database")]
    public static void ProcessConsumablesDatabase()
    {
        LoadAllDatabases();

        if (consumableDatabase != null)
            LoadConsumables();
    }

    [MenuItem(@"Lonely Adventures/Inventory System/Process Databases/Bullets Database")]
    public static void ProcessBulletsDatabase()
    {
        LoadAllDatabases();

        if (bulletsDatabase != null)
            LoadBullets();
    }

    [MenuItem(@"Lonely Adventures/Inventory System/Process Databases/Qualities Database")]
    public static void ProcessQualitiesDatabase()
    {
        LoadAllDatabases();

        if (qualityDatabase != null)
            LoadQualities();
    }

    [MenuItem(@"Lonely Adventures/Inventory System/Process Databases/Currencies Database")]
    public static void ProcessCurrenciesDatabase()
    {
        LoadAllDatabases();

        if (currenciesDatabase != null)
            LoadCurrencies();
    }

    [MenuItem(@"Lonely Adventures/Inventory System/Process Databases/Characters Database")]
    public static void ProcessCharactersDatabase()
    {
        charactersDatabase = GetDatabase<ISCharactersDatabase>(DATABASE_FOLDER_NAME, CHARACTERS_DATABASE_FILE_NAME);

        LoadAllCharacters();
        /*
        LoadCharacterAgent();
        LoadCharacterBandit();
        LoadCharacterCannibal();
        LoadCharacterCultist();
        LoadCharacterJunkie();
        LoadCharacterKraken();
        LoadCharacterMarauder();
        LoadCharacterMilitia();
        LoadCharacterPolice();
        LoadCharacterRobot();
        LoadCharacterScavenger();
        LoadCharacterSurvivor();
        LoadCharacterWaterMan();
        */
    }

    private static void LoadAllDatabases()
    {
        qualityDatabase = GetDatabase<ISQualityDatabase>(DATABASE_FOLDER_NAME, QUALITY_DATABASE_FILE_NAME);
        currenciesDatabase = GetDatabase<ISCurrenciesDatabase>(DATABASE_FOLDER_NAME, CURRENCIES_DATABASE_FILE_NAME);
        armorDatabase = GetDatabase<ISArmorDatabase>(DATABASE_FOLDER_NAME, ARMORS_DATABASE_FILE_NAME);
        weaponsDatabase = GetDatabase<ISWeaponDatabase>(DATABASE_FOLDER_NAME, WEAPONS_DATABASE_FILE_NAME);
        consumableDatabase = GetDatabase<ISConsumableDatabase>(DATABASE_FOLDER_NAME, CONSUMABLES_DATABASE_FILE_NAME);
        bulletsDatabase = GetDatabase<ISBulletsDatabase>(DATABASE_FOLDER_NAME, BULLETS_DATABASE_FILE_NAME);
        charactersDatabase = GetDatabase<ISCharactersDatabase>(DATABASE_FOLDER_NAME, CHARACTERS_DATABASE_FILE_NAME);
    }

    private static AudioClip LoadWeaponSound(string soundName)
    {
        return Resources.Load<AudioClip>("AudioClips/Items/Weapons/" + soundName);
    }

    private static Sprite getSpriteFromTextures(string SpriteName, Texture2D[] textureList)
    {
        foreach (Texture2D atlas in textureList)
        {
            //Debug.Log("Atlas Name: " + atlas.name);

            Sprite[] sprites = Resources.LoadAll<Sprite>("Atlas/Weapons/GraphicRepresentations/" + atlas.name);

            for (int i = 0; i < sprites.Length; i++)
            {
                if (sprites[i].name == SpriteName)
                    return sprites[i];
            }

        }

        return null;

    }

    private static void processGraphics(string Path, string Atlas)
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>(Path + Atlas);

        for (int i = 0; i < sprites.Length; i++)
            GraphicRepresentations.Add(sprites[i]);
    }

    private static void processIcons(string Path, string Atlas)
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>(Path + Atlas);

        for (int i = 0; i < sprites.Length; i++)
            ItemIcons.Add(sprites[i]);
    }

    private static Sprite getIconByName(string iconName)
    {
        foreach (Sprite sprite in ItemIcons)
            if (sprite.name.ToLower() == iconName.ToLower())
                return sprite;
        return null;
    }

    private static Sprite getSpriteByName(string spriteName)
    {
        foreach (Sprite sprite in GraphicRepresentations)
            if (sprite.name.ToLower() == spriteName.ToLower())
                return sprite;
        return null;
    }

    private static void LoadAtlases(string GraphicRepresentationsPath, string IconsPaths)
    {
        GraphicRepresentations.Clear();
        ItemIcons.Clear();

        foreach (Texture2D atlas in Resources.LoadAll<Texture2D>(GraphicRepresentationsPath))
        {
            processGraphics(GraphicRepresentationsPath, atlas.name);
        }

        foreach (Texture2D atlas in Resources.LoadAll<Texture2D>(IconsPaths))
        {
            processIcons(IconsPaths, atlas.name);
        }
    }

	private static Sprite LoadItemGraphic(string graphicName)
	{
		Sprite sp = Resources.Load<Sprite>("Sprites/ItemsGraphics/"+graphicName);
		if (sp == null)
			Resources.Load<Sprite>("Sprites/QuestionIcon");
		return sp;
	}

	private static AudioClip LoadItemSound(string soundID)
	{
		return Resources.Load<AudioClip>("ItemSounds");
	}

    private static void loadWeapons()
    {
        if (!GDEDataManager.Init("gde_data"))
            Debug.LogError("Error Initializing GDE");

        GDEWeaponsData weapon;
        
        List<string> allKeys;
        GDEDataManager.GetAllDataKeysBySchema("Weapons", out allKeys);

        string path = "Atlas/Weapons/GraphicRepresentations/";
        string iconsPath = "Atlas/Weapons/Icons/";

        //LoadAtlases(path, iconsPath);

        foreach (string key in allKeys)
        {
            if (!(GDEDataManager.DataDictionary.TryGetCustom(key, out weapon)))
                Debug.LogError("Error Reading Data");
            else
            {
                ISWeapon newWeapon = new ISWeapon();

                // Values From ISSObject
				// Weapon Textures For Icon and Representation, Overrided, New DB
				//newWeapon.Icon = getIconByName(weapon.Icon);
				//newWeapon.GraphicRepresentation = getSpriteByName(weapon.GraphicRepresentation);
				newWeapon.Icon = LoadItemGraphic(weapon.GraphicRepresentation);
				newWeapon.GraphicRepresentation = LoadItemGraphic(weapon.GraphicRepresentation);

                newWeapon.ObjectID = weapon.ID;
                newWeapon.Name = weapon.Name;
                newWeapon.Level = weapon.Level;
                newWeapon.Description = weapon.Description;
                newWeapon.Burden = weapon.Burden;
                newWeapon.Quality = getQuality(weapon.Quality);
                newWeapon.ItemPrice = weapon.BuyPrice;//new ISTradePair(currenciesDatabase.GetElementByName(weapon.BuyCurrency), weapon.BuyPrice);
                //newWeapon.SellPrice = weapon.SellPrice;//new ISTradePair(currenciesDatabase.GetElementByName(weapon.SellCurrency), weapon.SellPrice);

                // Values from IISEquipable
                newWeapon.EquipSlot = getEquipSlot(weapon.EquipSlot);
                newWeapon.Might = weapon.Might;
                //newWeapon.OnCharacterSprite = weapon.OnCharacterSprite;
                newWeapon.EquipVisually = weapon.EquipVisually;

                //Values From IISWeapon
                newWeapon.EquipType = getWeaponSlot(weapon.EquipType);
                newWeapon.MaxFireRate = weapon.MaxFireRate;
                newWeapon.Damage = weapon.Damage;
                newWeapon.MaxRange = weapon.MaxRange;
                newWeapon.MaxShootsPerRound = weapon.MaxShotsPerRound;
                newWeapon.NoiseLevel = weapon.NoiseLevel;
                newWeapon.Accuracy = weapon.Accuracy;
                newWeapon.Caliber = weapon.Caliber;
                newWeapon.Scope = weapon.Scope;
                // TODO Add sounds to ExcelFile.
				newWeapon.ShootSound = LoadItemSound(weapon.GraphicRepresentation);//LoadWeaponSound("00AK47");
				newWeapon.ReloadSound = LoadItemSound("3001");

                if (weaponsDatabase.GetElementByID(newWeapon.ObjectID) != null)
                    weaponsDatabase.RemoveItemByID(newWeapon.ObjectID);

                weaponsDatabase.Add(newWeapon);
            }
        }
        EditorUtility.SetDirty(weaponsDatabase); 
        
    }

    private static void loadArmors()
    {
        
        if (!GDEDataManager.Init("gde_data"))
            Debug.LogError("Error Initializing GDE");

        GDEArmorData armor;
        List<string> allKeys;
        GDEDataManager.GetAllDataKeysBySchema("Armor", out allKeys);

        string path = "Atlas/Armors/GraphicRepresentations/";
        string iconsPath = "Atlas/Armors/Icons/";

        //LoadAtlases(path, iconsPath);

        foreach (string key in allKeys)
        {
            if (!(GDEDataManager.DataDictionary.TryGetCustom(key, out armor)))
                Debug.LogError("Error Reading Data");
            else
            {
                ISArmor newArmor = new ISArmor();

                // Values From IISObject
				// Overrided Graphics with New DB.
				//newArmor.Icon = getIconByName(armor.Icon);
				//newArmor.GraphicRepresentation = getSpriteByName(armor.GraphicRepresentation);
				newArmor.Icon = LoadItemGraphic(armor.GraphicRepresentation);
				newArmor.GraphicRepresentation = LoadItemGraphic(armor.GraphicRepresentation);

                newArmor.ObjectID = armor.ID;
                newArmor.Name = armor.Name;
                newArmor.Description = armor.Description;
                newArmor.Burden = armor.Burden;
                newArmor.Quality = getQuality(armor.Quality);
                newArmor.ItemPrice = armor.BuyPrice;//new ISTradePair(currenciesDatabase.GetElementByName(armor.BuyCurrency), armor.BuyPrice);
                //newArmor.SellPrice = armor.SellPrice;//new ISTradePair(currenciesDatabase.GetElementByName(armor.SellCurrency), armor.SellPrice);

                // Values from IISEquipable
                newArmor.EquipSlot = getEquipSlot(armor.EquipSlot);
                newArmor.Might = armor.Might;
                //newArmor.OnCharacterSprite = armor.OnCharacterSprite;
                newArmor.EquipVisually = armor.EquipVisually;

                // Values From IISArmor
                newArmor.Protection = armor.Protection;
                newArmor.MaxDurability = armor.maxDurability;
                //newArmor.CurrentDurability = armor.maxDurability;

                if (armorDatabase.GetElementByID(newArmor.ObjectID) == null)
                    armorDatabase.Add(newArmor);
            }
        }
        EditorUtility.SetDirty(armorDatabase);
        
    }

    private static void LoadConsumables()
    {
        
        if (!GDEDataManager.Init("gde_data"))
            Debug.LogError("Error Initializing GDE");

        GDEConsumablesData consumable;
        List<string> allKeys;
        GDEDataManager.GetAllDataKeysBySchema("Consumables", out allKeys);

        string path = "Atlas/Consumables/GraphicRepresentations/";
        string iconsPath = "Atlas/Consumables/Icons/";

        //LoadAtlases(path, iconsPath);

        foreach (string key in allKeys)
        {
            if (!(GDEDataManager.DataDictionary.TryGetCustom(key, out consumable)))
                Debug.LogError("Error Reading Data");
            else
            {
                ISConsumable newConsumable = new ISConsumable();

                // Values From IISObject
				// Overrided Load Graphics for new DB
				//newConsumable.Icon = getIconByName(consumable.Icon);
				//newConsumable.GraphicRepresentation = getSpriteByName(consumable.GraphicRepresentation);
				newConsumable.Icon = LoadItemGraphic(consumable.GraphicRepresentation);
				newConsumable.GraphicRepresentation = LoadItemGraphic(consumable.GraphicRepresentation);


                newConsumable.ObjectID = consumable.ID;
                newConsumable.Level = consumable.Level;
                newConsumable.Name = consumable.Name;
                newConsumable.Description = consumable.Description;
                newConsumable.Burden = consumable.Burden;
                newConsumable.Quality = getQuality(consumable.Quality);
                newConsumable.ItemPrice = consumable.BuyPrice;//new ISTradePair(currenciesDatabase.GetElementByName(consumable.BuyCurrency), consumable.BuyPrice);
                //newConsumable.SellPrice = consumable.SellPrice;//new ISTradePair(currenciesDatabase.GetElementByName(consumable.SellCurrency), consumable.SellPrice);

                // Values From IISConsumable, IISStackable
                newConsumable.Cooldown = consumable.Cooldown;
                newConsumable.MaxStack = consumable.MaxStackSize;

                if (consumableDatabase.GetElementByID(newConsumable.ObjectID) == null)
                    consumableDatabase.Add(newConsumable);
            }
        }
        EditorUtility.SetDirty(consumableDatabase);
    }

	private static void LoadBullets()
	{
		if (!GDEDataManager.Init("gde_data"))
			Debug.LogError("Error Initializing GDE");

		GDEAmmoData ammo;
		//GDEBulletsData bullets;
		
		List<string> allKeys;
		GDEDataManager.GetAllDataKeysBySchema("Ammo", out allKeys);
		
		string path = "Atlas/Consumables/Bullets/GraphicRepresentations/";
		string iconsPath = "Atlas/Consumables/Bullets/Icons/";
		
		//LoadAtlases(path, iconsPath);
		
		foreach (string key in allKeys)
		{
			if (!(GDEDataManager.DataDictionary.TryGetCustom(key, out ammo)))
				Debug.LogError("Error Reading Data");
			else
			{
				ISBullet newBullet = new ISBullet();
				
				// Values From IISObject
				// Overrided Graphic Loading with new DB
				//newBullet.Icon = getIconByName(ammo.Icon);
				//newBullet.GraphicRepresentation = getSpriteByName(ammo.GraphicRepresentation);
				newBullet.Icon = LoadItemGraphic(ammo.GraphicRepresentation);
				newBullet.GraphicRepresentation = LoadItemGraphic(ammo.GraphicRepresentation);


				newBullet.ObjectID = ammo.ID;
				newBullet.Name = ammo.Name;
				newBullet.Description = ammo.Description;
				newBullet.Burden = ammo.Burden;
				newBullet.Quality = getQuality(ammo.Quality);
				newBullet.ItemPrice = ammo.BuyPrice;//new ISTradePair(currenciesDatabase.GetElementByName(bullets.BuyCurrency), bullets.BuyPrice);
				//newBullet.SellPrice = bullets.SellPrice;//new ISTradePair(currenciesDatabase.GetElementByName(bullets.SellCurrency), bullets.SellPrice);
				
				// Values From IISConsumable, IISStackable
				newBullet.Cooldown = ammo.Cooldown;
				newBullet.MaxStack = ammo.MaxStackSize;
				
				// Values From ISBullets
				newBullet.caliber = ammo.Caliber;
				newBullet.poisoned = ammo.Poisoned;
				newBullet.radiated = ammo.Radiated;
				
				if (bulletsDatabase.GetElementByID(newBullet.ObjectID) == null)
					bulletsDatabase.Add(newBullet);
			}
		}
		EditorUtility.SetDirty(bulletsDatabase);
		
	}

	/*
    private static void LoadBullets()
    {
        if (!GDEDataManager.Init("gde_data"))
            Debug.LogError("Error Initializing GDE");

        GDEBulletsData bullets;

        List<string> allKeys;
        GDEDataManager.GetAllDataKeysBySchema("Bullets", out allKeys);

        string path = "Atlas/Consumables/Bullets/GraphicRepresentations/";
        string iconsPath = "Atlas/Consumables/Bullets/Icons/";

        LoadAtlases(path, iconsPath);

        foreach (string key in allKeys)
        {
            if (!(GDEDataManager.DataDictionary.TryGetCustom(key, out bullets)))
                Debug.LogError("Error Reading Data");
            else
            {
                ISBullet newBullet = new ISBullet();

                // Values From IISObject
                newBullet.ObjectID = bullets.ID;
                newBullet.Name = bullets.Name;
                newBullet.Description = bullets.Description;
                newBullet.Icon = getIconByName(bullets.Icon);
                newBullet.GraphicRepresentation = getSpriteByName(bullets.GraphicRepresentation);
                newBullet.Burden = bullets.Burden;
                newBullet.Quality = getQuality(bullets.Quality);
                newBullet.ItemPrice = bullets.BuyPrice;//new ISTradePair(currenciesDatabase.GetElementByName(bullets.BuyCurrency), bullets.BuyPrice);
                //newBullet.SellPrice = bullets.SellPrice;//new ISTradePair(currenciesDatabase.GetElementByName(bullets.SellCurrency), bullets.SellPrice);

                // Values From IISConsumable, IISStackable
                newBullet.Cooldown = bullets.Cooldown;
                newBullet.MaxStack = bullets.MaxStackSize;

                // Values From ISBullets
                newBullet.caliber = bullets.Caliber;
                newBullet.poisoned = bullets.Poisoned;
                newBullet.radiated = bullets.Radiated;

                if (bulletsDatabase.GetElementByID(newBullet.ObjectID) == null)
                    bulletsDatabase.Add(newBullet);
            }
        }
        EditorUtility.SetDirty(bulletsDatabase);

    }
*/
    private static void LoadQualities()
    {
        
        if (!GDEDataManager.Init("gde_data"))
            Debug.LogError("Error Initializing GDE");

        GDEQualitiesData quality;
        List<string> allKeys;
        GDEDataManager.GetAllDataKeysBySchema("Qualities", out allKeys);

        foreach (string key in allKeys)
        {
            if (!(GDEDataManager.DataDictionary.TryGetCustom(key, out quality)))
                Debug.LogError("Error Reading Data");
            else
            {
                ISQuality newQuality = new ISQuality(quality.Name);
                //newQuality.Icon = quality.Icon;
                newQuality.TitleColor = quality.Color;

                if (qualityDatabase.GetElementByName(newQuality.Name) == null)
                    qualityDatabase.Add(newQuality);
            }
        }
        EditorUtility.SetDirty(qualityDatabase);
        
    }

    private static void LoadCurrencies()
    {
        if (!GDEDataManager.Init("gde_data"))
            Debug.LogError("Error Initializing GDE");

        GDECurrenciesData currency;
        List<string> allKeys;
        GDEDataManager.GetAllDataKeysBySchema("Currencies", out allKeys);

        foreach (string key in allKeys)
        {
            if (!(GDEDataManager.DataDictionary.TryGetCustom(key, out currency)))
                Debug.LogError("Error Reading Data");
            else
            {
                ISCurrency newCurrency = new ISCurrency(currency.Name);
                //newCurrency.Icon = currency.Icon;

                if (currenciesDatabase.GetElementByName(newCurrency.Name) == null)
                    currenciesDatabase.Add(newCurrency);
            }
        }
        EditorUtility.SetDirty(currenciesDatabase);
        
    }

    public static void LoadAllCharacters()
    {
        if (!GDEDataManager.Init("gde_data"))
            Debug.LogError("Error Initializing GDE");

        GDECharactersData charactersData;
        List<string> allKeys;
        GDEDataManager.GetAllDataKeysBySchema("Characters", out allKeys);

        foreach (string key in allKeys)
        {
            if (!(GDEDataManager.DataDictionary.TryGetCustom(key, out charactersData)))
                Debug.LogError("Error Reading Data");
            else
            {
                ISCharacter newCharacter = new ISCharacter();

                newCharacter.CharacterType = charactersData.CharacterType;
                newCharacter.Items = charactersData.ItemsList.Split(',');
                newCharacter.MaleAvatar = charactersData.MaleAvatar.Split(',');
				newCharacter.FemaleAvatar = charactersData.FemaleAvatar.Split(',');
                newCharacter.BossFactor = charactersData.BossFactor;
                newCharacter.StartingBehaviour = charactersData.StartingBehaviour;

                // Stats
                newCharacter.MaxAccuracy = charactersData.MaxAccuracy;
                newCharacter.MinAccuracy = charactersData.MinAccuracy;
                newCharacter.MinStrength = charactersData.MinStrength;
                newCharacter.MaxStrength = charactersData.MaxStrength;
                newCharacter.MinAgility = charactersData.MinAgility;
                newCharacter.MaxAgility = charactersData.MaxAgility;
                newCharacter.MinMorale = charactersData.MinMorale;
                newCharacter.MaxMorale = charactersData.MaxMorale;
                newCharacter.MinCommand = charactersData.MinCommand;
                newCharacter.MaxCommand = charactersData.MaxCommand;
                newCharacter.MinEvasion = charactersData.MinEvasion;
                newCharacter.MaxEvasion = charactersData.MaxEvasion;
                newCharacter.MinIntelligence = charactersData.MinIntelligence;
                newCharacter.MaxIntelligence = charactersData.MaxIntelligence;
                newCharacter.MinStealth = charactersData.MinStealth;
                newCharacter.MaxStealth = charactersData.MaxStealth;
                newCharacter.MinHitpoints = charactersData.MinHitpoints;
                newCharacter.MaxHitpoints = charactersData.MaxHitpoints;
                newCharacter.MinHostility = charactersData.MinHostility;
                newCharacter.MaxHostility = charactersData.MaxHostility;
                newCharacter.MinAwardedExp = charactersData.MinAwardedExp;
                newCharacter.MaxAwardedExp = charactersData.MaxAwardedExp;
                newCharacter.MinAwardedCurrency= charactersData.MinAwardedCurrency;
                newCharacter.MaxAwardedCurrency= charactersData.MaxAwardedCurrency;

                if (charactersDatabase.GetByType(charactersData.CharacterType) == null)
                    charactersDatabase.Add(newCharacter);
            }
        }
        EditorUtility.SetDirty(charactersDatabase);
    }

    public static ISQuality getQuality(string name)
    {
        ISQuality quality = qualityDatabase.GetElementByName(name);

        if (quality == null)
            quality = qualityDatabase.Get(0);

        return quality;
    }

    public static EquipmentSlot getEquipSlot(string name)
    {
        name = name.ToLower();

        if (name == "arms")
            return EquipmentSlot.ARMS;
        else if (name == "head")
            return EquipmentSlot.HEAD;
        else if (name == "legs")
            return EquipmentSlot.LEGS;
        else if (name == "torso")
            return EquipmentSlot.TORSO;
        else if (name == "mainhand")
            return EquipmentSlot.MAIN_HAND;
        else if (name == "melee")
            return EquipmentSlot.MELEE;
        else if (name == "offhand")
            return EquipmentSlot.OFF_HAND;
        else if (name == "throwable")
            return EquipmentSlot.THROWABLE;

        return EquipmentSlot.NONE;

    }

    public static WeaponEquipType getWeaponSlot(string name)
    {
        name = name.ToLower();

        if (name == "pistol")
            return WeaponEquipType.PISTOL;
        else if (name == "rifle")
            return WeaponEquipType.RIFLE;
        else if (name == "throwable")
            return WeaponEquipType.THROWABLE;
        else if (name == "melee")
            return WeaponEquipType.Melee;

        return WeaponEquipType.NONE;
    }

}
