﻿using UnityEngine;
using UnityEditor;
using System.Collections;
        /*

namespace Wasabi.InventorySystem.Editor
{
    public class ISDatabaseGenerators
    {
        static ISQualityDatabase qualityDatabase;
        static ISCurrenciesDatabase currenciesDatabase;
        static ISArmorDatabase armorDatabase;
        static ISWeaponDatabase weaponsDatabase;
        static ISConsumableDatabase consumableDatabase;

        const string CURRENCIES_DATABASE_FILE_NAME = @"ISCurrenciesDatabase.asset";
        const string QUALITY_DATABASE_FILE_NAME = @"ISQualityDatabase.asset";
        const string ARMORS_DATABASE_FILE_NAME = @"ISArmorDatabase.asset";
        const string WEAPONS_DATABASE_FILE_NAME = @"ISWeaponsDatabase.asset";
        const string CONSUMABLES_DATABASE_FILE_NAME = @"ISConsumablesDatabase.asset";
        const string DATABASE_FOLDER_NAME = @"Databases";


        [MenuItem(@"Inventory System/Create Databases/All Databases",false,1)]
        public static void CreateAllDatabases()
        {
            qualityDatabase = GetDatabase<ISQualityDatabase>(DATABASE_FOLDER_NAME, QUALITY_DATABASE_FILE_NAME);
            currenciesDatabase = GetDatabase<ISCurrenciesDatabase>(DATABASE_FOLDER_NAME, CURRENCIES_DATABASE_FILE_NAME);
            armorDatabase = GetDatabase<ISArmorDatabase>(DATABASE_FOLDER_NAME, ARMORS_DATABASE_FILE_NAME);
            weaponsDatabase = GetDatabase<ISWeaponDatabase>(DATABASE_FOLDER_NAME, WEAPONS_DATABASE_FILE_NAME);
            consumableDatabase = GetDatabase<ISConsumableDatabase>(DATABASE_FOLDER_NAME, CONSUMABLES_DATABASE_FILE_NAME);
        }

        [MenuItem(@"Inventory System/Create Databases/Quality Database")]
        public static void CreateQualityDatabase()
        {
            qualityDatabase = GetDatabase<ISQualityDatabase>(DATABASE_FOLDER_NAME, QUALITY_DATABASE_FILE_NAME);
        }

        [MenuItem(@"Inventory System/Create Databases/Currencies Database")]
        public static void CreateCurrencyDatabase()
        {
            currenciesDatabase = GetDatabase<ISCurrenciesDatabase>(DATABASE_FOLDER_NAME, CURRENCIES_DATABASE_FILE_NAME);
        }

        [MenuItem(@"Inventory System/Create Databases/Armors Database")]
        public static void CreateArmorsDatabase()
        {
            armorDatabase = GetDatabase<ISArmorDatabase>(DATABASE_FOLDER_NAME, ARMORS_DATABASE_FILE_NAME);
        }

        [MenuItem(@"Inventory System/Create Databases/Weapons Database")]
        public static void CreateWeaponsDatabase()
        {
            weaponsDatabase = GetDatabase<ISWeaponDatabase>(DATABASE_FOLDER_NAME, WEAPONS_DATABASE_FILE_NAME);
        }

        [MenuItem(@"Inventory System/Create Databases/Consumables Database")]
        public static void CreateConsumablesDatabase()
        {
            consumableDatabase = GetDatabase<ISConsumableDatabase>(DATABASE_FOLDER_NAME, CONSUMABLES_DATABASE_FILE_NAME);
        }


        public static T GetDatabase<T>(string dbPath, string dbName) where T : ScriptableObject
        {
            string dbFullPath = @"Assets/" + dbPath + "/" + dbName;

            T db = AssetDatabase.LoadAssetAtPath(dbFullPath, typeof(T)) as T;

            if (db == null)
            {
                if (!AssetDatabase.IsValidFolder("Assets/" + dbPath))
                    AssetDatabase.CreateFolder("Assets", dbPath);

                db = ScriptableObject.CreateInstance<T>() as T;
                AssetDatabase.CreateAsset(db, dbFullPath);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
            return db;
        }
    }
}
        */