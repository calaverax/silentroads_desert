﻿using UnityEngine;
using System.Collections;
using Wasabi.InventorySystem;

public class CurrenciesFactory : MonoBehaviour {

    public ISCurrenciesDatabase database;

	// Use this for initialization
	void Start ()
    {
        if (database == null)
            return;

        if (database.GetElementByName("Brass") == null)
            database.Add(new ISCurrency("Brass"));

        if (database.GetElementByName("Lead") == null)
            database.Add(new ISCurrency("Lead"));
    }
	
}
