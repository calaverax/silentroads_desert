﻿using UnityEngine;
using System.Collections;
namespace Wasabi.InventorySystem
{
    public enum EquipmentSlot
    {
        NONE,
        HEAD,
        TORSO,
        ARMS,
        LEGS,
        MAIN_HAND,
        OFF_HAND,
        MELEE,
        THROWABLE
    }
}