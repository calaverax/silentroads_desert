﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace Wasabi.InventorySystem
{
    public class ItemBrokenEvent : UnityEvent<ISObject> { }
    public class ItemUsedEvent : UnityEvent<ISObject> { }
    public class ItemEquipedEvent : UnityEvent<ISObject> { }
    public class ItemUnequipedEvent : UnityEvent<ISObject> { }
}
