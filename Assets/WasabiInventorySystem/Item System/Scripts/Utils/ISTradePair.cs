﻿using UnityEngine;
using System.Collections;

namespace Wasabi.InventorySystem
{
    public class ISTradePair : IISTradePair
    {
        ISCurrency _currency;
        int _value;

        public ISTradePair(ISCurrency Currency, int Value)
        {
            _currency = Currency;
            _value = Value;
        }

        public ISCurrency Currency { get { return _currency; } }
        public int Value { get {return _value; } }

    }
}