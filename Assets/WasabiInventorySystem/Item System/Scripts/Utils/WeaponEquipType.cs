﻿using UnityEngine;
using System.Collections;


namespace Wasabi.InventorySystem
{
    public enum WeaponEquipType
    {
        NONE,
        PISTOL,
        RIFLE,
        Melee,
        THROWABLE
    }
}
