﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

namespace Wasabi.InventorySystem.Inventory
{

    public class ISLootWindow : MonoBehaviour, IDropHandler
    {
        List<ISLootSlot> lootSlots = new List<ISLootSlot>();

        public GameObject slotPrefab;
        public RectTransform scrollContainer;
        
        public bool HaveLoot
        {
            get
            {
                foreach (ISLootSlot slot in lootSlots)
                    if (slot.item != null)
                        return true;
                return false;
            }
        }

        public void addItem(ISObject item)
        {
            //CombatManager.Instance.NpcFXController.PlayEventSound("LootDropped");

            GameObject slot = Instantiate(slotPrefab);
            slot.transform.SetParent(scrollContainer.transform);

            lootSlots.Add(slot.GetComponent<ISLootSlot>());
            slot.GetComponent<ISLootSlot>().addItem(item);
            slot.transform.localScale = new Vector3(1, 1, 1);
            repaint();
            //LOOT PERSISTENCE -----------------------------------------------------------------------------

            if (item is ISBullet)
            {
                LootPersistenceManager.Instance._lvlLootPersistence.AddItem("bullets", item.ObjectID);
            }
            else
            {
                if (item is ISConsumable)
                {
                    LootPersistenceManager.Instance._lvlLootPersistence.AddItem("consumables", item.ObjectID);
                }
                else
                {
                    LootPersistenceManager.Instance._lvlLootPersistence.AddItem("weapons", item.ObjectID);

                }
            }
            //LOOT PERSISTENCE -----------------------------------------------------------------------------
        }
        public void addItemL(ISObject item)
        {
            //CombatManager.Instance.NpcFXController.PlayEventSound("LootDropped");

            GameObject slot = Instantiate(slotPrefab);
            slot.transform.SetParent(scrollContainer.transform);

            lootSlots.Add(slot.GetComponent<ISLootSlot>());
            slot.GetComponent<ISLootSlot>().addItem(item);
            slot.transform.localScale = new Vector3(1, 1, 1);
            repaint();
        }

        public void LootItem(ISLootSlot slot)
        {
            if (slot.item is ISBullet)
            {
                //if (ISInventoryManager.instance.inventory.addItem(slot.item, ((ISConsumable)slot.item).Stack))
                if (ISInventoryManager.instance.inventory.addItem(slot.item, 1))
                {
                    lootSlots.Remove(slot);
                    Destroy(slot.gameObject);
                    repaint();
                }
            }
            else
            {
                if (slot.item is ISConsumable)
                {
                    //if (ISInventoryManager.instance.inventory.addItem(slot.item, ((ISConsumable)slot.item).Stack))
                    if (ISInventoryManager.instance.inventory.addItem(slot.item, 1))
                    {
                        lootSlots.Remove(slot);
                        Destroy(slot.gameObject);
                        repaint();
                    }
                }
                else
                {
                    if (ISInventoryManager.instance.inventory.addItem(slot.item))
                    {
                        lootSlots.Remove(slot);
                        Destroy(slot.gameObject);
                        repaint();
                    }
                }
            }
            //LOOT PERSISTENCE -----------------------------------------------------------------------------
            LootPersistenceManager.Instance._lvlLootPersistence.RemoveItem(slot.item);
            //LOOT PERSISTENCE -----------------------------------------------------------------------------
        }

        public void LootAll()
        {
            Debug.Log("JOJO");
            bool itemlooted = false;
            foreach (ISLootSlot slot in lootSlots)
            {
                if (slot.item is ISBullet)
                {
                    //if (ISInventoryManager.instance.inventory.addItem(slot.item, ((ISBullet)slot.item).Stack))
                    if (ISInventoryManager.instance.inventory.addItem(slot.item, 1))
                    {
                        lootSlots.Remove(slot);
                        Destroy(slot.gameObject);
                        itemlooted = true;
                        LootPersistenceManager.Instance._lvlLootPersistence.RemoveItem(slot.item);
                        break;
                    }
                }
                else
                {


                    if (slot.item is ISConsumable)
                    {
                        //if (ISInventoryManager.instance.inventory.addItem(slot.item, ((ISConsumable)slot.item).Stack))
                        if (ISInventoryManager.instance.inventory.addItem(slot.item, 1))
                        {
                            lootSlots.Remove(slot);
                            Destroy(slot.gameObject);
                            itemlooted = true;
                            LootPersistenceManager.Instance._lvlLootPersistence.RemoveItem(slot.item);
                            break;
                        }
                    }
                    else
                    {
                        if (ISInventoryManager.instance.inventory.addItem(slot.item))
                        {
                            lootSlots.Remove(slot);
                            Destroy(slot.gameObject);
                            itemlooted = true;
                            LootPersistenceManager.Instance._lvlLootPersistence.RemoveItem(slot.item);
                            break;
                        }
                    }
                }
                //LOOT PERSISTENCE -----------------------------------------------------------------------------
                
                //LOOT PERSISTENCE -----------------------------------------------------------------------------
                /*
                    if (ISInventoryManager.instance.inventory.addItem(slot.item))
                    {
                        lootSlots.Remove(slot);
                        Destroy(slot.gameObject);
                        itemlooted = true;
                        break;
                    }
              */
            }

            if (itemlooted) LootAll();

            repaint();

            //this.gameObject.SetActive(false);
        }

        private void repaint()
        {
            scrollContainer.sizeDelta = new Vector2(scrollContainer.sizeDelta.x, lootSlots.Count * 82.5f);
        }

        public void OnDrop(PointerEventData eventData)
        {
            Debug.Log("Name : " + eventData.pointerDrag.gameObject.name);
            Debug.Log("Name2 : " + eventData.pointerEnter.gameObject.name);
            Debug.Log("Dropped Over LootWindow");

            ISSlotItem droppedItem = eventData.pointerDrag.GetComponent<ISSlotItem>();
            if (droppedItem == null)
                return;
            ISInventoryManager.instance.lootWindow.addItem(droppedItem.item);
            if (droppedItem.item is ISConsumable)
            {
                Debug.Log("ES CONSUMIBLE");
                droppedItem.stackAmount--;
                if (droppedItem.stackAmount > 0) {
                    droppedItem.repaint();
                }else
                {
                    droppedItem.ResetSlot();
                }      
            }else
            {
                droppedItem.ResetSlot();
            }

        }

    
    }
}