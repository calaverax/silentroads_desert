﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

namespace Wasabi.InventorySystem.Inventory
{
    public class ISLootSlot : MonoBehaviour, IPointerClickHandler
    {
        public Text itemNameField;
        public Image itemIconField;

        ISObject _item;
        public ISObject item { get { return _item; } }

        public void addItem(ISObject item)
        {
            _item = item;
            itemNameField.text = item.Name;
            itemIconField.sprite = item.Icon;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Right)
            {
                ISInventoryManager.instance.lootWindow.LootItem(this);
            }
        }
    }
}
