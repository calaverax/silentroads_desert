﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Wasabi.InventorySystem;


namespace Wasabi.InventorySystem.Inventory
{
    public class ISInventory : MonoBehaviour
    {
        [SerializeField]
        GameObject inventoryPanel;
        [SerializeField]
        GameObject slotsContainer;

        //Para cargar el modelo del casco;

        public Transform cabeza;
        private DispararIK dispararIK;


        public GameObject InventorySlot;
        public GameObject InventoryItem;
        Text weightLabel;

        public int slotAmount;
        public float weightLimit = 50000.0f;

        private float _actualBurden = 0.0f;
        public float actualBurden { get { return _actualBurden; } }

        public List<ISSlot> inventorySlots = new List<ISSlot>();



        public void Initialize()
        {
            if ((inventoryPanel == null) || (slotsContainer == null))
                Debug.LogError("Fields on inventory are not linked");

            weightLabel = transform.FindChild("Weight Label").GetComponent<Text>();

            GameObject slot;
            ISSlot slotScript;

            for (int i = 0; i < slotAmount; i++)
            {
                slot = Instantiate(InventorySlot);
                slot.transform.SetParent(slotsContainer.transform);
                slot.name = "Slot-" + i;

                slotScript = slot.GetComponent<ISSlot>();
                slotScript.slotIndex = i;

                inventorySlots.Add(slotScript);
                slot.transform.localScale = new Vector3(1, 1, 1);
            }

        }

        public void tryAddItemAtSlot(ISObject item, int index, int amount = 0)
        {
            float totalBurden = amount > 1 ? item.Burden * amount : item.Burden;

            if (totalBurden + _actualBurden > weightLimit)
                return;

            if (inventorySlots[index].item == null)
            {
                inventorySlots[index].AddItem(item);

                if (amount > 0)
                    inventorySlots[index].increaseItemStack(amount - 1);

                _actualBurden += totalBurden;
                weightLabel.text = "Weight " + _actualBurden + "/" + weightLimit;
            }
        }
        /// <summary>
        /// Used For Stackeable Items
        /// </summary>
        /// <param name="item"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public bool addItem(ISObject item, int amount)
        {


            //Debug.Log("EL AMOUNT ES : " + amount);
            if (item == null) return false;
            int sortedAmount = amount;
            float totalBurden = amount > 1 ? item.Burden * amount : item.Burden;

            if (totalBurden + actualBurden > weightLimit)
                return false;

            // If the Items is not Stackeable then return.
            if (!(item is IISStackable))
            {
                Debug.LogError("Trying To add a a non-stackeable item with an amount greather to 1");
                return false;
            }

            ISConsumable newItem = item as ISConsumable;

            // Search if we have this item on our inventory
            for (int i = 0; i < slotAmount; i++)
            {
                if (sortedAmount <= 0)
                    break;

                if (inventorySlots[i].item != null)
                {
                    if (inventorySlots[i].item.Name == item.Name)
                    {
                        ISConsumable hadItem = inventorySlots[i].item as ISConsumable;

                        // Amount of item i have + amount to add lower than maxx stack just add it.
                        if ((inventorySlots[i].stackSize + sortedAmount) < hadItem.MaxStack)
                        {
                            inventorySlots[i].increaseItemStack(sortedAmount);
                            sortedAmount = 0;
                        }
                        else // Else, add the diference till max amount, and search another stack
                        {
                            int amountToAdd = hadItem.MaxStack - inventorySlots[i].stackSize;
                            inventorySlots[i].increaseItemStack(hadItem.MaxStack - inventorySlots[i].stackSize);
                            sortedAmount -= amountToAdd;
                        }
                    }
                }
            }

            // If we had not find any item of the same type on inventory, just add it on the first empty slot.
            if (sortedAmount > 0)
            {
                do
                {
                    for (int i = 0; i < slotAmount; i++)
                    {
                        if (sortedAmount <= 0) break;

                        if (inventorySlots[i].item == null)
                        {
                            inventorySlots[i].AddItem(item);
                            sortedAmount--;
                            if (sortedAmount > 0)
                            {
                                // Check Parameter amount is greather than max stack
                                if (newItem.MaxStack > sortedAmount) // If Max Stack is Greather than Parameter Amount, just add it.
                                    inventorySlots[i].increaseItemStack(sortedAmount);
                                else // If Parameter Amount is Greather than Item.MaxStack
                                {
                                    inventorySlots[i].increaseItemStack(newItem.MaxStack);
                                }
                                sortedAmount -= inventorySlots[i].stackSize;
                            }
                        }
                    }

                } while (sortedAmount > 0);
            }

            if (sortedAmount <= 0)
            {
                _actualBurden += totalBurden;
                weightLabel.text = "Weight " + _actualBurden + "/" + weightLimit;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Used For Non-Stackeable Items
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool addItem(ISObject item)
        {
            if (item.Burden + _actualBurden > weightLimit)
                return false;





            for (int i = 0; i < slotAmount; i++)
            {
                if (inventorySlots[i].item == null)
                {
                    inventorySlots[i].AddItem(item);
                    _actualBurden += item.Burden;
                    weightLabel.text = "Weight " + _actualBurden + "/" + weightLimit;
                    return true;
                }
            }
            return false;
        }

        private bool findStackableItem(ISObject item)
        {
            for (int i = 0; i < slotAmount; i++)
            {
                if (inventorySlots[i].isStackable)
                {
                    if (inventorySlots[i].item != null)
                    {
                        if (inventorySlots[i].item.ObjectID == item.ObjectID)
                        {
                            inventorySlots[i].increaseItemStack();
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public ISObject findBulletsByCaliber(float caliber)
        {
            for (int i = 0; i < slotAmount; i++)
            {
                if (inventorySlots[i].item != null)
                {
                    if (inventorySlots[i].item is ISBullet)
                    {
                        if (((ISBullet)inventorySlots[i].item).caliber == caliber)
                        {
                            return inventorySlots[i].item;
                        }
                    }
                }
            }
            return null;
        }


        public void RemoveItem(int SlotIndex)
        {
            inventorySlots[SlotIndex].slotItem.ResetSlot();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SlotIndex"></param>
        /// <param name="byAmount"></param>
        /// <returns> True if item stack is depeleted</returns>
        public bool ReduceItemStackSize(int SlotIndex, int byAmount = 1)
        {
            return inventorySlots[SlotIndex].reduceItemStack();
        }


    }
}