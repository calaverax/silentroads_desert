﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

namespace Wasabi.InventorySystem.Inventory
{
    public class ISCharacterEquipment : MonoBehaviour, IDropHandler
    {
        [SerializeField]
        public GameObject container;
        [SerializeField]
        Image HeadGraphic, TorsoGraphic, ArmoredArms, NormalArms, ArmoredLegs, NormalLegs;
        public GameObject player;
        public Material playerMat;
        public Material materialArmadura;
        public int numTexturas;
        public Texture torso;
        public Texture brazos;
        public Texture piernas;
        public Renderer renderer;


        Dictionary<EquipmentSlot, ISEquipmentSlot> _equipmentSlots = new Dictionary<EquipmentSlot, ISEquipmentSlot>();

        public Dictionary<EquipmentSlot, ISEquipmentSlot> equipmentSlots
        {
            get { return _equipmentSlots; }
        }


        //PPP Sin este overload no se le sale la armadura a Robert.
        private void CheckArmorGraphic(EquipmentSlot armor, bool isEquiping) {
            if (player == null)
            {
                player = GameObject.FindGameObjectWithTag("Player");
                renderer = player.transform.GetChild(1).GetChild(3).GetComponent<Renderer>();
            }

            
            if (armor == EquipmentSlot.HEAD)
            {
                if (isEquiping)
                    HeadGraphic.enabled = true;
                else
                    HeadGraphic.enabled = false;
            }
            else if (armor == EquipmentSlot.TORSO)
            {
                if (isEquiping){
                    TorsoGraphic.enabled = true;
                    renderer.material.SetTextureOffset("_t2", Vector2.zero);
                    materialArmadura.SetTextureOffset("_t2", Vector2.zero);
                    
                }
                else { 
                    TorsoGraphic.enabled = false;
                    renderer.material.SetTextureOffset("_t2", new Vector2(1.2f, 0));
                    renderer.sharedMaterial.SetTextureOffset("_t2", new Vector2(1.2f, 0));
                
                }
            }
            else if (armor == EquipmentSlot.LEGS)
            {
                if (isEquiping)
                {
                    ArmoredLegs.enabled = true;
                    renderer.material.SetTextureOffset("_t5", Vector2.zero);
                    renderer.sharedMaterial.SetTextureOffset("_t5", Vector2.zero);
                    

                }
                else
                {
                    ArmoredLegs.enabled = false;
                    renderer.material.SetTextureOffset("_t5", new Vector2(1.2f, 0));
                    renderer.sharedMaterial.SetTextureOffset("_t5", new Vector2(1.2f, 0));

                }
            }
            else if (armor == EquipmentSlot.ARMS)
            {
                if (isEquiping)
                {
                    ArmoredArms.enabled = true;
                    renderer.material.SetTextureOffset("_t3", Vector2.zero);
                    renderer.sharedMaterial.SetTextureOffset("_t3", Vector2.zero);
                }
                else
                {
                    ArmoredArms.enabled = false;
                    renderer.material.SetTextureOffset("_t3", new Vector2(1.2f, 0));
                    renderer.sharedMaterial.SetTextureOffset("_t3", new Vector2(1.2f, 0));
                    
                }
            }

 
        }
 
                //private void PonerArmadura(EquipmentSlot elemento, bool poner = true) {}


        public void itemUnequiped(ISObject item)
        {
            if (item is ISArmor)
            {
                ISArmor arm = item as ISArmor;
                CheckArmorGraphic(arm.EquipSlot,false);

            }
        }

        public void itemUnequiped(EquipmentSlot armor) {
            CheckArmorGraphic(armor, false);
        }

        void itemEquiped(ISObject item)
        {
            if (item is ISArmor)
            {
                ISArmor arm = item as ISArmor;

                CheckArmorGraphic(arm.EquipSlot,true);
                
            }
        }

        //PPP Mi overload
        public void itemEquiped(EquipmentSlot armor)
        {
            CheckArmorGraphic(armor, true);
        }

        public void Initialize()
        {
            ISInventoryManager.instance.OnItemEquiped.AddListener(itemEquiped);
            ISInventoryManager.instance.OnItemUnequiped.AddListener(itemUnequiped);

            if (container != null)
            {
                ISEquipmentSlot[] slots = container.GetComponentsInChildren<ISEquipmentSlot>(true);

                for (int i = 0; i < slots.Length; i++)
                {
                    if (_equipmentSlots.ContainsKey(slots[i].EquipType))
                    {
                        Debug.LogError("Equipment Slot Duplicated");
                    }
                    _equipmentSlots.Add(slots[i].EquipType, slots[i]);
                }
            }

        }

        public void EquipItem(ISObject item)
        {

        }

        public void EquipInSlot(ISSlotItem item)
        {
            if (_equipmentSlots[((IISEquipable)item.item).EquipSlot].slotItem.item == null)
            {
                Debug.Log("");
            }
        }

        public void test()
        {
            Debug.Log("Equiped In Torso: " + getEquipedItem(EquipmentSlot.TORSO).Name);
        }

        public ISObject getEquipedItem(EquipmentSlot slot)
        {
            if (_equipmentSlots.ContainsKey(slot))
                return _equipmentSlots[slot].slotItem.item;

            return null;
        }

        public void OnDrop(PointerEventData eventData)
        {
            Debug.Log("Name : " + eventData.pointerDrag.gameObject.name);
            Debug.Log("Name2 : " + eventData.pointerEnter.gameObject.name);
            Debug.Log("Dropped Over Character");

            ISSlotItem droppedItem = eventData.pointerDrag.GetComponent<ISSlotItem>();

            if (droppedItem == null)
                return;

            //if (slotItem == droppedItem)
            //    return;

            if (droppedItem.item is IISEquipable)
            {
                Debug.Log("ES EQUIPABLE");
                IISEquipable item = droppedItem.item as IISEquipable;

                if (_equipmentSlots[item.EquipSlot].slotItem.item != null)
                {
                    if (_equipmentSlots[item.EquipSlot].slotItem.item.ObjectID != 0)
                    {
                        Debug.Log("El casillero que lo recibe ya tiene algo");
                        ISInventoryManager.instance.inventory.addItem(_equipmentSlots[item.EquipSlot].slotItem.item);
                        ISInventoryManager.instance.ItemUnequiped(_equipmentSlots[item.EquipSlot].slotItem.item);
                    }
                }
                
                //_equipmentSlots[item.EquipSlot].AddSlotItem(droppedItem); //PPP lo comenté para remplazarlo por la línea que sigue:

                _equipmentSlots[item.EquipSlot].EquipItem(droppedItem.item); //PPP Esto usa el script como estaba pensado y corrige los problemas con los fondos del casillero.

                droppedItem.ResetSlot(); //Vacía el casillero del objeto que tiraste
                
                

                
                ISInventoryManager.instance.ItemEquiped(_equipmentSlots[item.EquipSlot].slotItem.item);

                /*
                if (item.EquipSlot == EquipType)
                {
                    AddSlotItem(droppedItem);
                    droppedItem.ResetSlot();
                    ISInventoryManager.instance.ItemEquiped(slotItem.item);
                }
                */
            }
            else { Debug.Log("NO, NO ES EQUIPABLE !"); }

        }
    }
}