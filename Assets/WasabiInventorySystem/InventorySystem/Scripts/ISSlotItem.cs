﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System;

namespace Wasabi.InventorySystem.Inventory
{
    public class ISSlotItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler
    {
        public Image itemImage;
        public Image qualityImage;

        public Text stackAmountField;
        public Image cooldownFill;
        public Image brillo;

        public ISObject item = null;


        private Transform manoRobert;
        private DispararIK DispararIK;

        public void Awake()
        {
            manoRobert = GameObject.FindGameObjectWithTag("Player").transform.Find(
                "Robert/rob_aujereado_2:Robert1_Reference/rob_aujereado_2:Robert1_Hips/rob_aujereado_2:Robert1_Spine/rob_aujereado_2:Robert1_Spine1/rob_aujereado_2:Robert1_Spine2/rob_aujereado_2:Robert1_RightShoulder/rob_aujereado_2:Robert1_RightArm/rob_aujereado_2:Robert1_RightForeArm/rob_aujereado_2:Robert1_RightHand"
                );
            //Harcodeo la jerarquía de la mano para que la encuentre sin tags o Finds complejos.
            DispararIK = GameObject.FindGameObjectWithTag("Player").GetComponent<DispararIK>();
            //item = null; //PPP comenté esta línea x no entender su utilidad. Traía la contra de que el objeto que Robert lleva en la mano se borraba al activar el inventario.

        }


        //public int stackAmount = 0;
        public int stackAmount {
            get
            {
                if (item != null)
                {
                    if (item.ObjectID == 0) { return 0; } //PPP los unity object fallan el chequeo de si es null o no.

                    if (item is ISConsumable)
                    {
                        ISConsumable newItem = item as ISConsumable;
                        return newItem.Stack;
                    }
                    else return 0;
                }
                else return 0;
            }
            set
            {
                if (item != null)
                {
                    if (item is ISConsumable)
                    {
                        ISConsumable newItem = item as ISConsumable;
                        newItem.Stack = value;
                    }
                }
            }
        }

        private Transform _originalParent;
        public Transform originalParent { get { return _originalParent; } }

        public ISSlot parentSlot;
        public ISEquipmentSlot parentEquipmentSlot; //PPP la variable anterior es defectuosa, porque el parent a veces puede ser de otra clase. Este es un parche medio sucio que contempla esa otra clase.
 

        public void obtainParenSlot()
        {
            if (gameObject.transform.parent == null) return;


            //PPP modifiqué la función xq se cometía el error de pensar que el parent siempre era de la clase ISSlot
            if (gameObject.transform.parent.gameObject.GetComponent<ISSlot>())
            { parentSlot = gameObject.transform.parent.gameObject.GetComponent<ISSlot>(); parentEquipmentSlot = null; }
            else if (parentEquipmentSlot = gameObject.transform.parent.gameObject.GetComponent<ISEquipmentSlot>())
            { parentEquipmentSlot = gameObject.transform.parent.gameObject.GetComponent<ISEquipmentSlot>(); parentSlot = null; }
            else { Debug.Log("El padre el item es de una clase que no es ni ISSlot, ni ISEquipment, fijate que clase es, y agregala a esta función"); }
                
        }

        void Start()
        {
            
            obtainParenSlot();
        }

        public void ResetSlot()
        {

            item = null;
            stackAmount = 0;
            stackAmountField.enabled = false;
            itemImage.enabled = false;
            qualityImage.enabled = false;
            
            //brillo.enabled = true;

        }

        public void AddItem(ISObject item)
        {
            this.item = item;
            repaint();


            //Si el arma no está en las cargadas, cargarla en la escena
            if (item is ISWeapon)
            {
                ISWeapon downcast = (ISWeapon)item;

                    GameObject arma = downcast.GetModel(manoRobert.transform);


            }




            if (item is ISConsumable)
            //if (item.GetType() == typeof(ISConsumable))
            {
                stackAmount++;

                if (stackAmount > 0)
                {
                    stackAmountField.text = stackAmount.ToString();
                    stackAmountField.enabled = true;
                }
            }
        }

        //la permutacion entre items.
        public void ChangeItem(ISObject item)
        {

            this.item = item;
            repaint();

            if (item is ISConsumable)
            //if (item.GetType() == typeof(ISConsumable))
            {

                if (stackAmount > 0)
                {
                    stackAmountField.text = stackAmount.ToString();
                    stackAmountField.enabled = true;
                }
            }

        }

        public void AddSlotItem(ISSlotItem item)
        {
            this.item = item.item;
            stackAmount = item.stackAmount;
            repaint();
        }

        public void afterConsumeRepaint()
        {
            if (item is IISStackable)
            {
                if (stackAmount > 0)
                {
                    stackAmountField.text = stackAmount.ToString();
                    stackAmountField.enabled = true;
                }
                else
                {
                    itemImage.enabled = false;
                    stackAmountField.enabled = false;
                    ResetSlot();
                }
            }
            else
            {
                itemImage.enabled = true;
                itemImage.sprite = item.Icon;
            }
            EventManager.StopListening("BulletConsumed", afterConsumeRepaint);
        }

        public void repaint()
        {

            if (item != null)
            {
                if (item.ObjectID == 0) { return; } //PPP los unity object fallan el chequeo de si es null o no.


                itemImage.enabled = true;
                itemImage.sprite = item.Icon;

            }
            else { itemImage.enabled = false; } //mío
            //qualityImage.enabled = true;
            //qualityImage.color = item.Quality.TitleColor;

            if (stackAmount > 0)
            {
                stackAmountField.text = stackAmount.ToString();
                stackAmountField.enabled = true;
            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (item != null)
            {
                if (item.ObjectID == 0) { return; } //PPP los unity object fallan el chequeo de si es null o no.
                _originalParent = this.transform.parent;
                gameObject.transform.SetParent(_originalParent.parent.parent.parent);
                gameObject.transform.position = new Vector3(eventData.position.x, eventData.position.y, -10);
                GetComponent<CanvasGroup>().blocksRaycasts = false;
            }

            /*
            if (_item != null)
            {
                _originalParent = this.transform;
                slotItem.gameObject.transform.SetParent(this.transform.parent);
                slotItem.gameObject.transform.position = eventData.position;
            }
            */
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (item != null)
                gameObject.transform.position = new Vector3(eventData.position.x, eventData.position.y, -10); //eventData.position;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            
            ISEquipmentSlot parentSlot = _originalParent.gameObject.GetComponent<ISEquipmentSlot>();

            if (parentSlot != null && parentSlot.slotItem.item == null && parentSlot is ISEquipmentSlot)
            {
                parentSlot.ItemUnequiped();
                
            }

            gameObject.transform.SetParent(_originalParent);
            gameObject.transform.localPosition = new Vector2(0, 0);
            GetComponent<CanvasGroup>().blocksRaycasts = true;

            CombatManager.Instance.robert.getSelectedWeapon();
            CombatManager.Instance.GUIScript.ProcessSelectedWeaponImage();
            //Debug.Log("selected Weapon ================================================= " + CombatManager.Instance.robert.selectedWeapon);
            /*
            slotItem.gameObject.transform.SetParent(_originalParent);
            slotItem.gameObject.transform.SetAsFirstSibling();
            slotItem.gameObject.transform.localPosition = new Vector2(0, 0);
            */

        }

        public void OnPointerClick(PointerEventData eventData)
        {
            
            if (eventData.button == PointerEventData.InputButton.Right)
            {
                ISSlotItem clickedItem = eventData.pointerPress.GetComponent<ISSlotItem>();
                
                if (clickedItem.item is ISConsumable)
                {
                    
                    ISConsumable newConsumable = clickedItem.item as ISConsumable;
                    EventManager.StartListening("BulletConsumed", afterConsumeRepaint);
                    ISInventoryManager.instance.OnItemUsed.Invoke(newConsumable);
                }
                else if (clickedItem.item is IISEquipable)
                {
                    
                    ISInventoryManager.instance.equipment.EquipInSlot(clickedItem);
                }
            }
        }
    }
}