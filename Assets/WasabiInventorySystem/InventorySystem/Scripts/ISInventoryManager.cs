﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wasabi.InventorySystem;

namespace Wasabi.InventorySystem.Inventory
{

    public class ISInventoryManager : MonoBehaviour
    {
        public bool puttingStarterGear = true;
        public static ISInventoryManager instance;

        [SerializeField] ISArmorDatabase _armorDatabase;
        public ISArmorDatabase armorDatabase { get { return _armorDatabase; } }
        [SerializeField] ISConsumableDatabase _consumablesDatabase;
        public ISConsumableDatabase consumablesDatabase { get { return _consumablesDatabase; } }
        [SerializeField] ISWeaponDatabase _weaponsDatabase;
        public ISWeaponDatabase weaponsDatabase { get { return _weaponsDatabase; } }
        [SerializeField] ISBulletsDatabase _bulletsDatabase;
        public ISBulletsDatabase bulletsDatabase { get { return _bulletsDatabase; } }
        [SerializeField] ISCurrenciesDatabase _currenciesDaatabase;
        public ISCurrenciesDatabase currenciesDatabase { get { return _currenciesDaatabase; } }

        public ISInventory inventory;
        public ISCharacterEquipment equipment;
        public ISLootWindow lootWindow;

        public ItemEquipedEvent OnItemEquiped;
        public ItemUnequipedEvent OnItemUnequiped;
        public ItemUsedEvent OnItemUsed;

        //public ISCurrency Brass, Lead;
        private int _playerMoney;

        [SerializeField] GameObject VendorShopWindow;
        public VendorContainerController VendorController { get { return VendorShopWindow.GetComponent<VendorContainerController>(); } }

        //Get-Sets
        public int Brass { get { return ConvertMoney(_playerMoney)[1]; } }
        public int Lead { get { return ConvertMoney(_playerMoney)[0]; } }
        public int PlayerMoney { get { return _playerMoney; } set { _playerMoney = value; } }


        //Cargar modelos
        public GameObject manoRobert;
        public Transform cabeza;
        [SerializeField]
        private Transform inventoryObjects;
        [SerializeField]
        private ISInventory inventario;
        [SerializeField]
        private Transform armasEnMano;
        [SerializeField]
        private IngameGUIController gui;
        private DispararIK DispararIK;

#if UNITY_EDITOR
        public bool IsOnDebugMode;
#endif

        public void OpenShop()
        {
            VendorShopWindow.SetActive(true);
        }

        // Use this for initialization
        void Awake()
        {
            
            instance = this;
            OnItemEquiped = new ItemEquipedEvent();
            OnItemUnequiped = new ItemUnequipedEvent();
            OnItemUsed = new ItemUsedEvent();

            inventory.Initialize();
            equipment.Initialize();


            //Referencias para cargar el modelo PPP
            DispararIK = GameObject.FindGameObjectWithTag("Player").GetComponent<DispararIK>();
            //  aFinger = transform.Find("LeftShoulder/Arm/Hand/Finger");

            manoRobert = GameObject.FindGameObjectWithTag("Player").transform.Find(
                "Robert/rob_aujereado_2:Robert1_Reference/rob_aujereado_2:Robert1_Hips/rob_aujereado_2:Robert1_Spine/rob_aujereado_2:Robert1_Spine1/rob_aujereado_2:Robert1_Spine2/rob_aujereado_2:Robert1_RightShoulder/rob_aujereado_2:Robert1_RightArm/rob_aujereado_2:Robert1_RightForeArm/rob_aujereado_2:Robert1_RightHand"
                ).gameObject;
            //Harcodeo la jerarquía de la mano para que la encuentre sin tags o Finds complejos.

            //manoRobert = GameObject.FindGameObjectWithTag("RobertRightHand");
            inventoryObjects = GameObject.FindGameObjectWithTag("IngameGUIController").transform.GetChild(15);
            inventario = inventoryObjects.GetChild(4).gameObject.GetComponent<ISInventory>();
            armasEnMano = inventoryObjects.GetChild(2).GetChild(1);
            if (inventario == null) { Debug.LogError("No se pudo encontrar el inventario. Asegurate que en el interior del gameObject GUI Inventory Objects sea el 16avo elemento, y su 5to child sea Is Inventory Window ;)"); }
            

#if UNITY_EDITOR
            if (IsOnDebugMode)
            {
                Invoke("PopulateStarterGear", .1f);
            }
            else
#endif
            {
                int isNewGame = PlayerPrefs.GetInt("StartedNewGame", 1);

                if (isNewGame == 1)
                //if (true)
                {
                    Invoke("PopulateStarterGear", .1f);
                }
                else
                {
                    LoadInventory();
                    LoadEquipment();

                }
                
            }
            InitWallet();
            LootPersistenceManager.Instance.LoadLoot();

            
            //equipment.equipmentSlots[armorDatabase.GetElementByName("Austro Hungarian Helmet").EquipSlot].EquipItem(armorDatabase.GetElementByName("Austro Hungarian Helmet"));
            //equipment.equipmentSlots[weaponsDatabase.GetElementByName("AK 47").EquipSlot].EquipItem(weaponsDatabase.GetElementByName("AK 47"));
            




        }


        private void InitWallet()
        {
            int isNewGame = PlayerPrefs.GetInt("StartedNewGame", 1);

            //int[] money = WoWMoney(2500);

            if (isNewGame == 1)
            {
                _playerMoney = 1050;
                PlayerPrefs.SetInt("RobertGold", _playerMoney);
                PlayerPrefs.Save();
            }
            else
            {
                _playerMoney = PlayerPrefs.GetInt("RobertGold");
            }

            Debug.Log("Loggin Money: Lead(" + Lead + ") Brass("+ Brass +")");
        }




        public int[] ConvertMoney(int m)
        {
            int[] result = new int[2];
            int lead = m % 100;
            m = (m - lead) / 100;
            int Brass = m % 100;
            //int gold = (m - Brass) / 100;
            result[0] = lead;
            result[1] = Brass;
            return result;
        }





        // Poniendo el inventario con el que arrancás.
        private void PopulateStarterGear()
        {
            Debug.Log("*** Populate ***");
            // Add Weapon to Inventory
            ISWeapon weapon;
            

            
            //AK 47
            weapon = (ISWeapon)_weaponsDatabase.GetElementByID(2);
            weapon.ShootsInRound = weapon.MaxShootsPerRound; // Max Magazine Bullets.
            equipment.equipmentSlots[EquipmentSlot.MAIN_HAND].EquipItem(weapon); // Equip It on Main-Hand
            ItemEquiped(weapon);

            weapon = (ISWeapon)_weaponsDatabase.GetElementByID(31);
            weapon.ShootsInRound = weapon.MaxShootsPerRound; // Max Magazine Bullets.
            equipment.equipmentSlots[EquipmentSlot.OFF_HAND].EquipItem(weapon); //Off-Hand
            ItemEquiped(weapon);

            weapon = (ISWeapon)_weaponsDatabase.GetElementByID(29);
            weapon.ShootsInRound = weapon.MaxShootsPerRound; // Max Magazine Bullets.
            equipment.equipmentSlots[EquipmentSlot.THROWABLE].EquipItem(weapon); //Throwable
            ItemEquiped(weapon);
            

            // Ak 47
            //weapon = (ISWeapon)_weaponsDatabase.GetElementByID(2);
            //weapon.ShootsInRound = weapon.MaxShootsPerRound;
            //equipment.equipmentSlots[EquipmentSlot.MAIN_HAND].EquipItem(weapon); // Equip it on Hand
            //LO DE ACA ARRIBA LO COMENTE PARA QUE ARRANQUE SOLO CON LA MAGNUM
            /*
            for (int i = 4; i < 13; i++)
            {
                weapon = (ISWeapon)_weaponsDatabase.GetElementByID(i);
                weapon.ShootsInRound = weapon.MaxShootsPerRound;
                inventory.addItem(weapon);
            }
            */
            // Tower Shield
            //inventory.addItem(armorDatabase.GetElementByID(5));

            // Add Helmet to Head Slot.
            //equipment.equipmentSlots[EquipmentSlot.HEAD].EquipItem(_armorDatabase.GetElementByID(2));

            // Call Inventory OnEquip Callbacks.
            //ItemEquiped(_armorDatabase.GetElementByID(2));
            

            // Add Water and one Heal Potion
            //inventory.addItem(consumablesDatabase.GetElementByID(1), 60); // Water.
            inventory.addItem(armorDatabase.GetElementByID(3013)); //Chaleco kevlar.
            inventory.addItem(armorDatabase.GetElementByID(3010)); //Chaleco kevlar.
            inventory.addItem(armorDatabase.GetElementByID(3006)); //Casco
            inventory.addItem(consumablesDatabase.GetElementByID(5001), 5); // Heal Potion.
            inventory.addItem(consumablesDatabase.GetElementByID(5002), 5); // Heal Potion.
            inventory.addItem(consumablesDatabase.GetElementByID(5003), 5); // Heal Potion.
            /*inventory.addItem(weaponsDatabase.GetElementByID(4));
            inventory.addItem(weaponsDatabase.GetElementByID(6));
            inventory.addItem(weaponsDatabase.GetElementByID(7));
            inventory.addItem(weaponsDatabase.GetElementByID(8));
            inventory.addItem(weaponsDatabase.GetElementByID(9));
            inventory.addItem(weaponsDatabase.GetElementByID(10));
            inventory.addItem(weaponsDatabase.GetElementByID(11));
            inventory.addItem(weaponsDatabase.GetElementByID(12));
            inventory.addItem(weaponsDatabase.GetElementByID(13));
            inventory.addItem(weaponsDatabase.GetElementByID(14));
            inventory.addItem(weaponsDatabase.GetElementByID(15));
            inventory.addItem(weaponsDatabase.GetElementByID(16));
            inventory.addItem(weaponsDatabase.GetElementByID(17));
            inventory.addItem(weaponsDatabase.GetElementByID(18));*/
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(11);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(12);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(13);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(14);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(15);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(16);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(17);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(18);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(19);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(20);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(23);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(24);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(26);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(27);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            weapon = (ISWeapon)weaponsDatabase.GetElementByID(28);
            weapon.ShootsInRound = weapon.MaxShootsPerRound;
            inventory.addItem(weapon);
            //weapon = (ISWeapon)weaponsDatabase.GetElementByID(29); weapon.ShootsInRound = weapon.MaxShootsPerRound; inventory.addItem(weapon);
            //weapon = (ISWeapon)weaponsDatabase.GetElementByID(30); weapon.ShootsInRound = weapon.MaxShootsPerRound; inventory.addItem(weapon);
            //weapon = (ISWeapon)weaponsDatabase.GetElementByID(31); weapon.ShootsInRound = weapon.MaxShootsPerRound; inventory.addItem(weapon);
   






            inventory.addItem(weaponsDatabase.GetElementByID(20));
            inventory.addItem(weaponsDatabase.GetElementByID(21));
            inventory.addItem(weaponsDatabase.GetElementByID(19));
            inventory.addItem(weaponsDatabase.GetElementByID(23));
            inventory.addItem(weaponsDatabase.GetElementByID(24));
            //inventory.addItem(weaponsDatabase.GetElementByID(25));
            inventory.addItem(weaponsDatabase.GetElementByID(26));
            inventory.addItem(weaponsDatabase.GetElementByID(27));
            inventory.addItem(weaponsDatabase.GetElementByID(28));
            //inventory.addItem(weaponsDatabase.GetElementByID(29));
            inventory.addItem(weaponsDatabase.GetElementByID(30));
            //inventory.addItem(weaponsDatabase.GetElementByID(31));

            // Add Ak47 Bullets and 9mm
            //inventory.addItem(bulletsDatabase.GetElementByID(991), 30);
            //inventory.addItem(bulletsDatabase.GetElementByID(993), 30);
            //inventory.addItem(bulletsDatabase.GetElementByID(994), 30);


            puttingStarterGear = false;
            
            CargarModelos(); //PPP esta función sirve para agregar los modelos 3D a la escena en base a las armas que tenés.


        }

        public void ItemEquiped(ISObject item)
        {
            OnItemEquiped.Invoke(item);
        }

        public void ItemUnequiped(ISObject item)
        {
            OnItemUnequiped.Invoke(item);
        }

        public void ToggleInventory()
        {
            if (inventory.gameObject.activeInHierarchy)
            {
                inventory.transform.parent.gameObject.SetActive(false);
                inventory.gameObject.SetActive(false);
                equipment.gameObject.SetActive(false);
                if (lootWindow.gameObject.activeInHierarchy)
                    lootWindow.gameObject.SetActive(false);
            }
            else
            {
                inventory.transform.parent.gameObject.SetActive(true);
                inventory.gameObject.SetActive(true);
                equipment.gameObject.SetActive(true);
                lootWindow.gameObject.SetActive(true);
                //lootWindow.gameObject.SetActive(lootWindow.HaveLoot); TomasLacerra : SIEMPRE MOSTRAR LOOT
            }

            // Open Caharacter + Inventory
            // If there is loot, Open it Also
        }

        public ISObject getEquipedItem(EquipmentSlot forSlot)
        {
            return equipment.getEquipedItem(forSlot);
        }

        void OnApplicationQuit()
        {
#if UNITY_EDITOR
            if (!IsOnDebugMode)
#endif
                SaveInventory();
        }

        void OnDisable()
        {
#if UNITY_EDITOR
            if (!IsOnDebugMode)
#endif
                SaveInventory();
        }

        private void SaveInventory()
        {
            PlayerPrefs.SetInt("RobertGold", _playerMoney);
            // Get All Items From Equipments
            foreach (KeyValuePair<EquipmentSlot, ISEquipmentSlot> key in equipment.equipmentSlots)
            {
                if (key.Value.slotItem.item != null)
                {
                    string itemType = "Empty";

                    if (key.Value.slotItem.item is ISArmor)
                    {
                        PlayerPrefs.SetString(key.Value.EquipType.ToString(), "Armor_" + key.Value.slotItem.item.ObjectID);
                    }
                    else if (key.Value.slotItem.item is ISWeapon)
                    {
                        PlayerPrefs.SetString(key.Value.EquipType.ToString(), "Weapon_" + key.Value.slotItem.item.ObjectID);
                        PlayerPrefs.SetInt("Weapon_" + key.Value.slotItem.item.ObjectID + "_Bullets", ((ISWeapon)key.Value.slotItem.item).ShootsInRound);
                    }
                    else
                    {
                        PlayerPrefs.SetString(key.Value.EquipType.ToString(), "Empty");
                    }

                }

            }

            // Get All Items From Inventory
            string item = "Empty";

            for (int i = 0; i < inventory.slotAmount; i++)
            {
                if (inventory.inventorySlots[i].item != null)
                {
                    // Armor, consumables, weapon
                    if (inventory.inventorySlots[i].item is ISWeapon)
                    {
                        item = "WeaponType_" + inventory.inventorySlots[i].item.ObjectID;
                        PlayerPrefs.SetInt("InventorySlot_" + i + "_Bullets", ((ISWeapon)inventory.inventorySlots[i].item).ShootsInRound);
                    }
                    else if (inventory.inventorySlots[i].item is ISArmor)
                    {
                        item = "ArmorType_" + inventory.inventorySlots[i].item.ObjectID;
                    }
                    else if (inventory.inventorySlots[i].item is ISConsumable)
                    {

                        PlayerPrefs.SetInt("StackeableInventorySlot_" + i, inventory.inventorySlots[i].stackSize);

                        if (inventory.inventorySlots[i].item is ISBullet)
                            item = "BulletType_" + inventory.inventorySlots[i].item.ObjectID;
                        else
                            item = "ConsumableType_" + inventory.inventorySlots[i].item.ObjectID;
                    }

                    PlayerPrefs.SetString("InventorySlot_" + i, item);
                }
                else
                {
                    PlayerPrefs.SetString("InventorySlot_" + i, "Empty");
                }
            }
            PlayerPrefs.SetInt("StartedNewGame", 0);

            PlayerPrefs.Save();
        }

        private void LoadInventory()
        {
            Debug.Log("*** Load ***///////////////////////////////////////////////////////////////////////////////////////////////////////////7");
            // Load Inventory -> add it
            string item = "Empty";
            for (int i = 0; i < inventory.slotAmount; i++)
            {
                item = PlayerPrefs.GetString("InventorySlot_" + i);

                if (item != "Empty")
                {
                    string[] values = item.Split('_');

                    if (values[0] == "WeaponType")
                    {
                        ISWeapon weapon = (ISWeapon)_weaponsDatabase.GetElementByID(int.Parse(values[1]));
                        weapon.ShootsInRound = PlayerPrefs.GetInt("InventorySlot_" + i + "_Bullets", 0);
                        inventory.tryAddItemAtSlot(weapon, i);
                        
                        //inventory.tryAddItemAtSlot(_weaponsDatabase.GetElementByID(int.Parse(values[1])), i);
                    }
                    else if (values[0] == "ArmorType")
                    {
                        inventory.tryAddItemAtSlot(_armorDatabase.GetElementByID(int.Parse(values[1])), i);
                    }
                    else if (values[0] == "ConsumableType")
                    {
                        inventory.tryAddItemAtSlot(_consumablesDatabase.GetElementByID(int.Parse(values[1])), i, PlayerPrefs.GetInt("StackeableInventorySlot_" + i, 0));
                    }
                    else if (values[0] == "BulletType")
                    {
                        inventory.tryAddItemAtSlot(_bulletsDatabase.GetElementByID(int.Parse(values[1])), i, PlayerPrefs.GetInt("StackeableInventorySlot_" + i,0));
                    }
                }
            }
        }

        private void LoadEquipment()
        {
            string item = "Empty";
            foreach (KeyValuePair<EquipmentSlot, ISEquipmentSlot> key in equipment.equipmentSlots)
            {
                item = PlayerPrefs.GetString(key.Value.EquipType.ToString());

                if (item != "Empty")
                {
                    string[] values = item.Split('_');

                    if (values[0] == "Armor")
                    {
                        key.Value.EquipItem(_armorDatabase.GetElementByID(int.Parse(values[1])));
                    }
                    else if (values[0] == "Weapon")
                    {
                        ISWeapon weapon = (ISWeapon)_weaponsDatabase.GetElementByID(int.Parse(values[1]));
                        weapon.ShootsInRound = PlayerPrefs.GetInt("Weapon_" + weapon.ObjectID + "_Bullets", 0);
                        key.Value.EquipItem(weapon);
                     
                        //key.Value.EquipItem(_weaponsDatabase.GetElementByID(int.Parse(values[1])));
                    }

                }
            }
        }

        public void CargarModelos()
        {

            //Destruir los modelos de armas que había en la escena (si había)
            foreach (Transform item in manoRobert.transform)
            {
                if (item.gameObject.tag == "Arma")
                {
                    //Debug.Log(item.gameObject.name + "- modelo retirado de la escena");
                    DispararIK.Armas.Clear();
                    Destroy(item.gameObject);
                }

            }



            //Cargar los modelos de armas (que estan en el inventario) en la escena.

            //Por cada elemento en el inventario
            foreach (ISSlot slot in inventario.inventorySlots)
            {
                if (slot.item is ISWeapon) //Fijate si es un arma...
                {

                    ISWeapon downcast = (ISWeapon)slot.item;
                    GameObject arma = downcast.GetModel(manoRobert.transform);
                }
                if (slot.item is ISArmor) { //Si es una armadura...
                    ISArmor armor = (ISArmor)slot.item;
                    if (armor.EquipSlot == EquipmentSlot.HEAD) {
                        if (!cabeza)
                        { cabeza = DispararIK.cabeza; }
                        armor.GetModel(cabeza);
                    }
                
                
                }

                //gui.newSlotsOrder.Add(slot);
            }


            foreach (Transform elemento in armasEnMano)
            {

                if (elemento.name == "Main Weapon" || elemento.name == "Off Weapon" || elemento.name == "Throwable")
                {

                    ISSlotItem item = elemento.transform.GetChild(1).gameObject.GetComponent<ISSlotItem>();
                    if (item == null) { Debug.Log("CUIDADO en el is inventory, cargarModelos no pudo cargar el slot del item "); }
                    if (item.item is ISWeapon)
                    {

                        ISWeapon downcast = (ISWeapon)item.item;
                        GameObject arma = downcast.GetModel(manoRobert.transform);
                        if (elemento.name == "MainWeapon") { 
                            arma.SetActive(true);
                            DispararIK.nombArma = arma.name;
                        }

                        }


                    }
                }
            }
        }









    }







 
