﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Wasabi.InventorySystem;
using System;

namespace Wasabi.InventorySystem.Inventory
{
    public class ISEquipmentSlot : ISSlotMain, IDropHandler
    {
        //private ISObject _item;
        //public ISObject item { get { return _item; } }
        public EquipmentSlot EquipType;
        public ISSlotItem slotItem;
        public Image bgImage;
        public bool saleEnLaVentanaEnQueEstaRobert; //Se ve el item en la imagen del inventario en que está Robert con armas y armaudras?
        public ISCharacterEquipment ISCharacterEquipment;




        public void AddSlotItem(ISSlotItem item)
        {
            slotItem.AddSlotItem(item);
            //ISCharacterEquipment.itemEquiped(EquipType);
        }

        public void OnDrop(PointerEventData eventData)
        {
            
            
            ISSlotItem droppedItem = eventData.pointerDrag.GetComponent<ISSlotItem>();

            //Si no tiró nada, cortá el loop
            if (droppedItem == null)
                return;

            //Si el objeto dropeado es el mismo que ya estaba, no hagas nada.
            if (slotItem == droppedItem)
                return;

            //Si el objeto dropeado es equipable
            if (droppedItem.item is IISEquipable)
            {

                if (saleEnLaVentanaEnQueEstaRobert)
                {
                    ISCharacterEquipment.itemEquiped(EquipType);
                    if (EquipType == EquipmentSlot.HEAD)
                    {
                        ISArmor armor = droppedItem.item as ISArmor;
                        GameObject.FindGameObjectWithTag("Player").GetComponent<DispararIK>().ActivarModeloCasco(armor.Model);
                    }
                }
                IISEquipable item = droppedItem.item as IISEquipable; //Lo downcasteás a una variable

                //Equip type es por ejemplo "main hand"; se fija si encaja o no con lo que requiere acá.
                if (item.EquipSlot == EquipType)
                {
                    if (item == null) {

                        AddSlotItem(droppedItem); //Guarda el item en el slot (o lo aumenta si es etacable)
                        droppedItem.ResetSlot(); //Resetea el slot del item que tiró.
                        ISInventoryManager.instance.ItemEquiped(slotItem.item);
                        
                        if (bgImage != null) //si hay una imagen de fondo (sólo tienen los slots especiales)
                            bgImage.enabled = false; //Apagala.
                    }


                    else // If there's an item, check if its a stackable.
                    {

                                switchItems(droppedItem);
                                if (bgImage != null)
                                    bgImage.enabled = false;

                    }
                }
            }
        }

        //Este método lo puse para solucionar el problema de las clases en el main hand weapon. No sé si todavía sirve.
        public void ChangeItem(ISObject item)
        {
            slotItem.ChangeItem(item);
           
        }

        //PPP otro método agregado por mí para solucionar los problemas
        private void switchItems(ISSlotItem droppedItem)
        {
            // Create a temporal Slot Item
            // Assign it with the values of the droppedItem
            ISObject item = droppedItem.item;


            droppedItem.ResetSlot();
            if (droppedItem.parentSlot != null) { droppedItem.parentSlot.ChangeItem(slotItem.item); }
            else if (droppedItem.parentEquipmentSlot != null) { droppedItem.parentEquipmentSlot.ChangeItem(slotItem.item); }

            slotItem.ResetSlot();
            ChangeItem(item);


        }

        public void EquipItem(ISObject item)
        {
            ISSlotItem newSlotItem = Instantiate(slotItem);
            newSlotItem.item = item;
            AddSlotItem(newSlotItem);

            if (bgImage != null)
                bgImage.enabled = false;

        }


        public void ItemUnequiped()
        {
            if (bgImage != null)
                bgImage.enabled = true;
            if (saleEnLaVentanaEnQueEstaRobert) 
            {
                ISCharacterEquipment.itemUnequiped(EquipType);
                if (EquipType == EquipmentSlot.HEAD) {
                    GameObject.FindGameObjectWithTag("Player").GetComponent<DispararIK>().DesactivarModeloCasco();
                }
                
            }
        }
    }
}