﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Wasabi.InventorySystem;
using System;

namespace Wasabi.InventorySystem.Inventory
{
    public class ISSlot : ISSlotMain, IDropHandler
    {
        public int slotIndex;
        public ISSlotItem slotItem;
        //public Sprite brilloSlot; //////////////////////////////////////PPPPPPPPP trando de poner los brillos

        public ISObject item
        {
            get
            {
                if (slotItem.item == null)
                    return null;
                else if (slotItem.item.ObjectID <= 0)
                    return null;
                else
                    return slotItem.item;
                //return slotItem.item;
            }
        }

        public void ChangeItem(ISObject item)
        {
            slotItem.ChangeItem(item);
        }

        public void AddItem(ISObject item)
        {
            slotItem.AddItem(item);
        }
        public void AddSlotItem(ISSlotItem item)
        {
            slotItem.AddSlotItem(item);
        }

        public int stackSize { get { return slotItem.stackAmount; } }

        public bool isStackable
        {
            get
            {
                if (slotItem.item == null) return false;

                //if (slotItem.item is IISStackable)
                if (slotItem.item.GetType() == typeof(ISConsumable))
                {
                    if ((((ISConsumable)slotItem.item).MaxStack > 0) && (slotItem.stackAmount < ((ISConsumable)slotItem.item).MaxStack))
                        return true;
                }

                return false;
            }
        }

        public bool reduceItemStack()
        {
            slotItem.stackAmount--;

            if (slotItem.stackAmount <= 0)
            {
                slotItem.ResetSlot();
                return true;
            }
            else
            {
                slotItem.repaint();
                return false;
            }
        }

        public void reduceItemStack(int amount)
        {
            slotItem.stackAmount -= amount;

            if (slotItem.stackAmount <= 0)
                slotItem.ResetSlot();
            else
                slotItem.repaint();
        }

        public void increaseItemStack(int amount)
        {
            slotItem.stackAmount += amount;
            slotItem.repaint();
        }

        public void increaseItemStack()
        {
            slotItem.stackAmount++;
            slotItem.repaint();
        }

        public void OnDrop(PointerEventData eventData)
        {
            ISSlotItem droppedItem = eventData.pointerDrag.GetComponent<ISSlotItem>();

            if (slotItem == droppedItem)
                return;

            if (item == null) // If There's no item on slot just add it
            {
                AddSlotItem(droppedItem);
                droppedItem.ResetSlot();
            }
            else // If there's an item, check if its a stackable.
            {
                if (isStackable)
                {
                    if (droppedItem.item.ObjectID == item.ObjectID) // If Stackable of the same type increase stack and delete the dragged one
                    {
                        switchItems(droppedItem);
                    }
                    else // if is not a stackable of the same type. switch the items
                    {
                        switchItems(droppedItem);
                    }
                }
                else
                {
                    // switch items
                    switchItems(droppedItem);
                }

            }
        }

        private void switchItems(ISSlotItem droppedItem)
        {
            // Create a temporal Slot Item
            // Assign it with the values of the droppedItem
            ISObject item = droppedItem.item;



            droppedItem.ResetSlot();
            if (droppedItem.parentSlot != null) { droppedItem.parentSlot.ChangeItem(slotItem.item); }
            else if (droppedItem.parentEquipmentSlot != null)
            {
                

                if (droppedItem.parentEquipmentSlot.bgImage != null)
                { droppedItem.parentEquipmentSlot.bgImage.enabled = false; }
                droppedItem.parentEquipmentSlot.ChangeItem(slotItem.item);

            }
            
            
            //droppedItem.obtainParenSlot();

            slotItem.ResetSlot();
            ChangeItem(item);
            //slotItem.obtainParenSlot();


        }
    }
}