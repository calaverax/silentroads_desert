﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparkyPath : MonoBehaviour {

    //public Transform path;
    //private Transform destination;
    private UnityEngine.AI.NavMeshAgent agent ;
    private Animator animator;
    private float vel;
    public Transform player;
    public float distMin = 25;
    public bool atacando;
    public Transform enemigo;

	// Use this for initialization
	void Start () {
		agent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
        animator = gameObject.GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

	}
	
	// Update is called once per frame
	void Update () {

        //Pasa la velocidad del navagent al animator para que sepa si caminar o no
        vel = agent.velocity.magnitude;
        animator.SetFloat("velocidad", vel);

        //Si el personaje cayó, detené al nav-mesh agent
        if (animator.GetBool("caida")) { StopAgent(); }
        else {
            //ataque (ir hasta el enemigo)
            if (atacando)
            {
                agent.SetDestination(enemigo.position+ new Vector3(0, 0, 2));
                //Si estás cerca lo mordés
                //Debug.Log(agent.remainingDistance);
                if (agent.remainingDistance < 0.09f && agent.remainingDistance > 0) 
                { StopAgent(); animator.SetBool("morder", true);
                
                    if (animator.GetCurrentAnimatorStateInfo(0).IsName("morder") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1.0f){
                        animator.SetBool("morder", false);  atacando = false;
                    } 
                }
            }
            else
            {
                //Si estás al pedo seguí al jugador si se alejó
                float dist = Vector3.Distance(player.position, transform.position);
                if (dist > distMin) { agent.SetDestination(player.position); }

                //y si estás muy cerca detené al agente
                else { StopAgent(); }
            }

        }

    }


    //Esta función es para detener al navmeshagent.   
    void StopAgent(){
            agent.velocity = new Vector3(0, 0, 0); agent.ResetPath();
        }
        


        


}
