﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonTrait : MonoBehaviour {

    public string trait;
    public string description;
    public string nextScene;

    public Text txtDescription;

	// Use this for initialization
	public void Select () {
        if(txtDescription != null)
        {
            txtDescription.text = description;
        }		
	}

    public void GO()
    {
        TraitManager.Instance.CreateTrait(trait);
        ScenesLoader.Instance.LoadScene(nextScene);
    }

}
