﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Trait {

    public string _traitSelected;

}

public class TraitManager : Singleton<TraitManager>
{
    public Trait trait;

    public void SetTrait(string value)
    {
        LoadData();
        trait._traitSelected = value;
        SaveData();
    }

    public string GetTrait()
    {
        LoadData();
        return trait._traitSelected;
    }

    public void SaveData()
    {
        Serializer.SaveSerializableObjectToFile<Trait>(trait, Application.persistentDataPath + "/" + "trait.persistence");
    }

    public void LoadData()
    {
        trait = Serializer.LoadSerializableObjectFromFile<Trait>(Application.persistentDataPath + "/" + "trait.persistence");
        if(trait == null)
        {
            trait = new Trait();
        }
    }

    public void CreateTrait(string traitName)
    {
        trait = new Trait();
        SetTrait(traitName);
        SaveData();
    }
}
