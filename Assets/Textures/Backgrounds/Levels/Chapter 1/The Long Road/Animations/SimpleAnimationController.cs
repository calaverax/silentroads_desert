﻿using UnityEngine;
using System.Collections;

public class SimpleAnimationController : MonoBehaviour 
{
	public Animator animator;
	public string animationTrigger;
	public int minTime, maxTime;

	void Start()
	{

		Invoke("PlayAnimation", Random.Range(minTime,maxTime));
	}

	void PlayAnimation()
	{
		if (animator != null)
		{
			animator.SetTrigger(animationTrigger);
		}
		Invoke("PlayThunder", Random.Range(minTime,maxTime));
	}

}
